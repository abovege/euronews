import { IonText,IonContent, IonHeader, IonItem, IonLabel, IonList, IonPage, IonTitle, IonIcon } from '@ionic/react';
import { bookmarkOutline, chevronForwardOutline, earthOutline, settingsOutline, shieldCheckmarkOutline, stopOutline } from 'ionicons/icons';
import './Services.css';
import React, {useEffect,useState,} from 'react';
import { Storage } from '@ionic/storage';
import {useLocation} from "react-router-dom";

interface ServicesProps {
  keywords?: any;
}



const Services: React.FC<ServicesProps> = ({keywords} :ServicesProps) => {
  const [readItLaterArray, setReadItLaterArray] = useState<any>([])
  const location = useLocation()

  useEffect(() => {
    const fetchArrayLength = async () =>{
      const  store = new Storage();
      await store.create();
      if(await store.get("readItLaterList") !== null) {
      setReadItLaterArray(await store.get("readItLaterList"));
      }
    }
    fetchArrayLength()
  }, [location.key])
  
  return (
    <IonPage>
      <IonHeader className="ion-no-border en-services-header">
        
          <IonTitle>{keywords.SERVICES}</IonTitle>
        
      </IonHeader>
      <IonContent fullscreen>
        
        <IonList >
          <IonItem  routerLink="/readitlater" className='ion-padding-start ion-padding-end en-services-item-title'>
            <IonIcon icon={bookmarkOutline} className='ion-padding-end' />
            <IonLabel style={{
              display:"flex",
              alignItems:"center",
            }}>
              <IonText className='ion-padding-end' 
              >{keywords.READ_IT_LATER}
              </IonText>
              {readItLaterArray.length !== 0 &&
              <IonText
              style={{
              display:"flex",
              justifyContent:"center",
              alignItems:"center",
              borderRadius:"50%",
              width:"25px",
              height:"25px",
              background:"orange",
              color:"white"}} >{readItLaterArray && readItLaterArray.length}</IonText>}
            </IonLabel>
            <IonIcon icon={chevronForwardOutline} />
          </IonItem>

          {/* <IonItem routerLink="/languages" className='ion-padding-start ion-padding-end en-services-item-title'>
            <IonIcon icon={earthOutline} className=' ion-padding-end' />
              <IonLabel>{keywords.LANGUAGES}</IonLabel>
            <IonIcon icon={chevronForwardOutline} />
          </IonItem> */}

          <IonItem routerLink="/settings" className='ion-padding-start ion-padding-end en-services-item-title'>
            <IonIcon icon={settingsOutline} className=' ion-padding-end' />
            <IonLabel>{keywords.SETTINGS}</IonLabel>
            <IonIcon icon={chevronForwardOutline} />
          </IonItem>

          <IonItem routerLink="/notifications" className='ion-padding-start ion-padding-end en-services-item-title'>
            <IonIcon icon={stopOutline} className=' ion-padding-end' />
            <IonLabel>{keywords.NOTIFICATIONS}</IonLabel>
            <IonIcon icon={chevronForwardOutline} />
          </IonItem>

          <IonItem routerLink="/privacy" className='ion-padding-start ion-padding-end en-services-item-title'>
            <IonIcon icon={shieldCheckmarkOutline} className='ion-padding-end' />
            <IonLabel>{keywords.PRIVACY}</IonLabel>
            <IonIcon icon={chevronForwardOutline} />
          </IonItem>

          <IonItem routerLink="/abouteuronews" className='ion-padding-start ion-padding-end en-services-item-title-lighter'>
            <IonLabel>{keywords.ABOUT_EURONEWS_GEORGIA}</IonLabel>
            <IonIcon icon={chevronForwardOutline} />
          </IonItem>

          {/*-- 
          <IonItem routerLink="/aboutallviews" className='ion-padding-start ion-padding-end en-services-item-title-lighter'>
            <IonLabel>{keywords.ABOUT_ALL_VIEWS}</IonLabel>
            <IonIcon icon={chevronForwardOutline} />
          </IonItem> 
          --*/}

          <IonItem routerLink="/aboutthisapp" className='ion-padding-start ion-padding-end en-services-item-title-lighter'>
            <IonLabel>{keywords.ABOUT_THIS_APP}</IonLabel>
            <IonIcon icon={chevronForwardOutline}/>
          </IonItem>
          
          {/*-- 
          <IonItem routerLink="/euronewsgeorgiaapp" className='ion-padding-start ion-padding-end en-services-item-title-lighter'>
            <IonLabel>{keywords.EURONEWS_GEORGIA_APPS}</IonLabel>
            <IonIcon icon={chevronForwardOutline} />
          </IonItem>
          --*/}
        </IonList>
        
        
      </IonContent>
    </IonPage>
  );
};

export default Services;
