import { IonIcon,IonBackButton, IonCol, IonContent, IonGrid, IonHeader, IonLabel, IonList, IonPage, IonRadio, IonRadioGroup, IonRange, IonRow, IonTitle, IonToggle } from '@ionic/react';
import { chevronBackOutline,cloudOfflineOutline,moon } from 'ionicons/icons';
import React, { useState,useEffect, useContext } from 'react';
import { RangeValue } from '@ionic/core';
import './Settings.css';
import { Storage } from '@ionic/storage';
import {offlineModeContext, darkThemeContext, fontSizeContext, articleAPContext, videoAPContext} from '../state/State'


interface SettingsProps {
  keywords?: any;
  
  
  
}



const Settings: React.FC<SettingsProps> = ({keywords} : SettingsProps) => {
 

  const [selectedArticleAP, setSelectedArticleAP] = useContext<any>(articleAPContext);
  const [selectedVideoeAP, setSelectedVideoAP] = useContext<any>(videoAPContext);
  const [time, setTime] = useState('time');
  
  

  const [checked, setChecked] = useState(false);
  const [value, setValue] = useContext<any>(fontSizeContext);
  const store = new Storage();


  const [themeTogglerBoolean,setThemeTogglerBoolean] = useContext<any>(darkThemeContext);

  const [offlineModeTogglerBoolean,setOfflineModeTogglerBoolean] = useContext<any>(offlineModeContext)
  

  const [rangeValue, setRangeValue] = useState<{
    lower: number;
    upper: number;
  }>({ lower: 0, upper: 20 });
  
  
  const customFormatter = (value: number) => `${value}%`;

  const inputStateManagment = async () =>{
    await store.create();
    setThemeTogglerBoolean(await store.get("theme"));
    setOfflineModeTogglerBoolean(await store.get("offlineMode"));
  }
  
  const setFontSizeOnLoad= async () => {
    await store.create();
    setValue(await store.get("fontSize"))
  }

  const setFontSize = async (size : any) =>{
    await store.create();
    await store.set("fontSize",size)
    setValue(size)
  } 

  useEffect(() => {
    inputStateManagment();
    setFontSizeOnLoad();

  async function getVideoAP() {

      const store = new Storage();
      await store.create();
      let defaultVideoAP = await store.get('videoAP');
      setSelectedVideoAP(defaultVideoAP);
      console.log(defaultVideoAP)

    };

    async function getArticleAP() {

      const store = new Storage();
      await store.create();
      let defaultArticleAP = await store.get('articleAP');
      setSelectedArticleAP(defaultArticleAP);
      console.log(defaultArticleAP)

    };


    getArticleAP();
    getVideoAP();
  }, [])

 

  

  const  themeToggler = async (checked: boolean) =>{
    await store.create();
    await store.set('theme', checked);
     if(checked){
      setThemeTogglerBoolean(await store.get("theme"));
      document.body.setAttribute("color-theme","dark");
     } else{
       setThemeTogglerBoolean(await store.get("theme"));
      document.body.setAttribute("color-theme","light") ;
      
     } 
  }

  const  offlineModeToggler = async (checked: boolean) =>{
    await store.create();

    let time = new Date();
    await store.set('offlineModeTime', time);

    await store.set('offlineMode', checked);
    setOfflineModeTogglerBoolean(await store.get("offlineMode"));
  }

  const onchangeArticleAP=async (value: string)=>{
    const store = new Storage();
    await store.create();
    setSelectedArticleAP(value)
    await store.set('articleAP', value);
  }

  const onchangeVideoAP=async (value: string)=>{
    const store = new Storage();
    await store.create();
    setSelectedVideoAP(value)
    await store.set('videoAP', value);
  }

  

  return (
    <IonPage>
      <IonHeader className="ion-no-border en-settings-header ion-no-padding">
        <IonBackButton icon={chevronBackOutline} className='ion-float-left ion-no-padding en-settings-back-button'/>
        <IonTitle className='en-settings-title ion-no-padding'>
          
          {keywords.SETTINGS}
        </IonTitle>
      </IonHeader>
      <IonContent fullscreen>

        <IonGrid>
          <IonRow className='ion-padding'>

            <IonCol className="en-settings-item ion-padding-bottom" size='12'>
              <IonIcon icon={cloudOfflineOutline}></IonIcon>
              <IonLabel className="ion-padding-horizontal">Offline რეჟიმი</IonLabel>
              <IonToggle id="offlineModeToggler" checked={offlineModeTogglerBoolean} onIonChange={e => offlineModeToggler(e.detail.checked)} className='ion-float-right en-toggle' ></IonToggle>
            </IonCol>

            <IonCol className="en-settings-item ion-padding-bottom ion-padding-top" size='12'>
              <IonIcon icon={moon}></IonIcon>
              <IonLabel className="ion-padding-horizontal">მუქი ფონი</IonLabel>
              <IonToggle id="themeToggler" checked={themeTogglerBoolean} onIonChange={e => {
                 themeToggler(e.detail.checked )
                 }} className='ion-float-right en-toggle' ></IonToggle>
            </IonCol>

            <IonCol className='en-settings-item ion-padding-bottom' size='12'>
              <IonLabel className='en-settings-item-title'>{keywords.TEXT_SIZE}</IonLabel>
              <IonRange snaps={true}  step={2} min={12} max={18} pin={true} value={value} onIonChange={e => setFontSize(e.detail.value as number)} >
                
              </IonRange>
              <div className="en-settings-text-size-div">
              <p style={{fontSize: value}}>ტექსტის ნიმუში</p>
              </div>
            </IonCol>

            <IonCol className='en-settings-item' size='12'>
              <h4 className='en-settings-item-title'>ვიდეოს ავტომატური გაშვება </h4>
              <div className='en-settings-item-descripton'><i>აირჩიე როდის გაეშვას ვიდეო ავტომატურად</i></div>
              <IonList>
                <IonRadioGroup value={selectedArticleAP} onIonChange={async e =>{
                  const value = e.detail.value;
                  if (value === selectedArticleAP) return;
                  onchangeArticleAP(e.detail.value) 
                }  }>
                  <div className='ion-margin-bottom'>
                    <IonLabel>ყოველთვის</IonLabel>
                    <IonRadio className='ion-float-right' value='articles-always'></IonRadio>
                  </div>
                  <div className='ion-margin-bottom'>
                    <IonLabel>მხოლოდ Wifi</IonLabel>
                    <IonRadio className='ion-float-right' value='articles-wifi'></IonRadio>
                  </div>
                  <div className='ion-margin-bottom'>
                    <IonLabel>არასოდეს</IonLabel>
                    <IonRadio className='ion-float-right' value='articles-never'></IonRadio>
                  </div>
                </IonRadioGroup>
              </IonList>
            </IonCol>

            {/* <IonCol size='12'>
              <h4 className='en-settings-item-title'>Video playlists: video autoplay</h4>
              <div className='en-settings-item-descripton'><i>Choose when to play videos automatically</i></div>
              <IonList>
                <IonRadioGroup value={selectedVideoeAP} onIonChange={async e =>{
                  const value = e.detail.value;
                  if (value === selectedArticleAP) return;
                  onchangeVideoAP(e.detail.value) 
                }}>
                  <div className='ion-margin-bottom'>
                    <IonLabel>Always</IonLabel>
                    <IonRadio className='ion-float-right' value='video-always'></IonRadio>
                  </div>
                  <div className='ion-margin-bottom'>
                    <IonLabel>On Wifi</IonLabel>
                    <IonRadio className='ion-float-right' value='video-wifi'></IonRadio>
                  </div>
                  <div className='ion-margin-bottom'>
                    <IonLabel>Never</IonLabel>
                    <IonRadio className='ion-float-right' value='video-never'></IonRadio>
                  </div>
                </IonRadioGroup>
              </IonList>
            </IonCol> */}
          </IonRow>
        </IonGrid>
        
      </IonContent>
    </IonPage>
  );
};

export default Settings;
