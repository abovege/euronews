import { IonContent, IonHeader, IonItem, IonLabel, IonList, IonPage, IonTitle, IonRadio, IonRadioGroup, IonBackButton } from '@ionic/react';
import React, { useState, useEffect } from 'react';
import { Storage } from '@ionic/storage';
import { chevronBackOutline } from 'ionicons/icons';

interface LanguageProps {
  keywords?: any;
  changeLanguage: () => void;
}


const Languages: React.FC<LanguageProps> = ({keywords,changeLanguage} : LanguageProps) => {
  const [selected, setLanguage] = useState<string>();

  useEffect(() => {
    async function getLanguage() {
      //Connect to Local Storage
      const store = new Storage();
      await store.create();

      //Get language from the local storage
      const defaultLanguage = await store.get('language');

      
      //Set language state
      setLanguage(defaultLanguage);
    }

    getLanguage()
  }, [])
  
  const onchange=async (language: string)=>{
    const store = new Storage();
    await store.create();
    setLanguage(language);
    await store.set('language', language);
    changeLanguage();
  }

  return (
    <IonPage>
      <IonHeader className="ion-no-border en-readitlater-header ion-no-padding">
        <IonBackButton icon={chevronBackOutline} className='ion-float-left ion-no-padding en-settings-back-button'/>
          <IonTitle className='en-readitlater-title ion-no-padding'>{keywords.LANGUAGES}</IonTitle>
      </IonHeader>
     
      <IonContent fullscreen>
        <IonList>
          <IonRadioGroup  value={selected} onIonChange={async e =>{
                 const value = e.detail.value;
                 if (value === selected) return;
                 onchange(e.detail.value) 
          }  }>
            <IonItem className='ion-padding-end'>
              <IonLabel>English</IonLabel>
              <IonRadio className='ion-float-right' value='en'></IonRadio>
            </IonItem>

            <IonItem className='ion-padding-end'>
              <IonLabel>ქართული</IonLabel>
              <IonRadio className='ion-float-right' value='ka'></IonRadio>
            </IonItem>
          </IonRadioGroup>
        </IonList>
      </IonContent>
    </IonPage>
  );
};

export default Languages;
