import {
    IonHeader,
    IonImg,
    IonBackButton,
    IonContent,
    IonPage,
    isPlatform,
    getPlatforms,
    IonRouterLink, IonButton, IonIcon, IonButtons
} from '@ionic/react';
import React, { useEffect } from 'react';
import ReactPlayer from 'react-player';
import {ScreenOrientation} from "@awesome-cordova-plugins/screen-orientation";
import { useLocation} from 'react-router-dom';
import { StatusBar } from "@awesome-cordova-plugins/status-bar"
import {bookmark, bookmarkOutline, chevronBackOutline, homeOutline, shareOutline} from 'ionicons/icons';


const Languages: React.FC = () => {
    let location = useLocation();
    const launchIntoFullscreen = (element: any) => {
        if(element.requestFullscreen) {
            element.requestFullscreen();
        } else if(element.mozRequestFullScreen) {
            element.mozRequestFullScreen();
        } else if(element.webkitRequestFullscreen) {
            element.webkitRequestFullscreen();
        } else if(element.msRequestFullscreen) {
            element.msRequestFullscreen();
        }
    }
    useEffect(() => {

        // document.addEventListener("fullscreenchange", function(e) {
        //
        //     if(!document.fullscreenElement) {
        //         console.log('fullscreen changes',document.fullscreenElement);
        //     }
        // });

        StatusBar.hide();
        StatusBar.overlaysWebView(false);
        // launchIntoFullscreen(document.getElementById("video_container"))
        // ScreenOrientation.lock(ScreenOrientation.ORIENTATIONS.LANDSCAPE);

        // let player = document.getElementById('test');

        // player!.addEventListener("pause", function() {
        //   window.location.href = "/"
        // }, false);

        // player!.addEventListener('webkitendfullscreen', function() {
        //   window.location.href = "/"
        // }, false);
    }, [])

    useEffect(() => {
        ScreenOrientation.unlock();
        StatusBar.show();
    }, [location.pathname])



    //  function toggleFullscreen() {
    //   let elem = document.getElementById("test");

    //   // const docElmWithBrowsersFullScreenFunctions = elem as HTMLElement & {
    //   //   requestFullscreen(): Promise<void>;
    //   //   mozRequestFullScreen(): Promise<void>;
    //   //   webkitRequestFullscreen(): Promise<void>;
    //   //   msRequestFullscreen(): Promise<void>;
    //   // };



    //   // if (!document.fullscreenElement) {
    //   //   let rfs = docElmWithBrowsersFullScreenFunctions.requestFullscreen() ||
    //   //    docElmWithBrowsersFullScreenFunctions.webkitRequestFullscreen() ||
    //   //     docElmWithBrowsersFullScreenFunctions.mozRequestFullScreen() ||
    //   //     docElmWithBrowsersFullScreenFunctions.msRequestFullscreen();

    //   //     if (docElmWithBrowsersFullScreenFunctions.requestFullscreen) {
    //   //       docElmWithBrowsersFullScreenFunctions.requestFullscreen().catch(err => {
    //   //         alert(`Error attempting to enable fullscreen mode: ${err.message} (${err.name})`);
    //   //       });
    //   //     } else if (docElmWithBrowsersFullScreenFunctions.mozRequestFullScreen) { /* Firefox */
    //   //       docElmWithBrowsersFullScreenFunctions.mozRequestFullScreen().catch(err => {
    //   //         alert(`Error attempting to enable fullscreen mode: ${err.message} (${err.name})`);
    //   //       });
    //   //     } else if (docElmWithBrowsersFullScreenFunctions.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
    //   //       docElmWithBrowsersFullScreenFunctions.webkitRequestFullscreen().catch(err => {
    //   //         alert(`Error attempting to enable fullscreen mode: ${err.message} (${err.name})`);
    //   //       });
    //   //     } else if (docElmWithBrowsersFullScreenFunctions.msRequestFullscreen) { /* IE/Edge */
    //   //       docElmWithBrowsersFullScreenFunctions.msRequestFullscreen().catch(err => {
    //   //         alert(`Error attempting to enable fullscreen mode: ${err.message} (${err.name})`);
    //   //       });
    //   //     }
    //   // } else {
    //   //   document.exitFullscreen();
    //   // }
    // }



    return (
        <IonPage>
            <IonContent>
                {/* <div style={{
          position: "relative",
          paddingTop: "56.25%"
        }}> */}
                {/*<IonHeader style={{height:"95px",paddingTop:"35px"}} className="ion-no-border en-settings-header">*/}
                {/*    <IonBackButton style={{display: "block",marginTop: "16px"}} icon={chevronBackOutline} className='ion-float-left ion-no-padding en-settings-back-button'/>*/}
                {/*    <IonImg style={{marginLeft: "30%",marginTop: "15px"}} src='../assets/images/header-logo2.png' class='en-header-logo'/>*/}
                {/*</IonHeader>*/}

                <IonHeader
                    style={{backgroundColor: "#ffffff"}}
                    className={"ion-no-border en-header " + "en-header-bg"}>

                    <IonRouterLink color="dark" routerLink="/" class='en-header-logo' style={{marginTop: "10px", display: "flex", justifyContent: "start", maxWidth: "48px"}}>
                        {/*<IonImg src='../assets/images/header-logo2.png'/>*/}
                        <IonImg style={{height: "48px"}} src='../assets/images/logo_eun_geo.png'/>
                        {/*<IonImg src='../assets/images/en_logo_new_transparent.png'/>*/}
                    </IonRouterLink>

                    <IonButtons class='en-header-offline-mode'>
                                <IonButton size="small" style={{color: "#007df8"}} routerLink={'/'}>
                                    <IonIcon size="large" icon={homeOutline}/>
                                </IonButton>
                    </IonButtons>

                </IonHeader>


                <ReactPlayer
                    id='video_container'
                    style={{
                        position: "absolute",
                        top: "60px",
                        left: "0"
                    }}
                    width='100%'
                    height='100%'
                    config={
                        {
                            file: {
                                forceHLS: isPlatform("ios") ? /^((?!chrome|android).)*safari/i.test(navigator.userAgent) : true,
                                attributes: {
                                    controls: true,
                                    controlsList: "noremoteplayback"
                                }
                            }
                        }
                    }
                    type="application/x-mpegURL"
                    frameBorder="0"
                    allow="autoplay; encrypted-media; picture-in-picture"
                    allowFullScreen
                    playing={true}
                    url="https://live2.tvg.ge/eng/EURONEWSGEORGIA/playlist.m3u8"
                    onEnded={() => console.log("rame")}
                />
                {/* </div> */}
            </IonContent>
        </IonPage>
    );
};

export default Languages;
