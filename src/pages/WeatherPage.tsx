import {
    IonSpinner,
    IonSegmentButton,
    IonSegment,
    IonToolbar,
    IonBackButton,
    IonContent,
    IonHeader,
    IonPage,
    IonTitle,
    IonGrid,
    IonSearchbar,
    IonRow,
    IonCol,
    IonIcon,
    IonList,
    IonItem,
    IonInfiniteScrollContent,
    IonInfiniteScroll,
    IonVirtualScroll,
    IonImg,
    IonAccordionGroup,
    IonAccordion,
    IonLabel,
    IonRouterLink
} from '@ionic/react';
import { analytics, chevronBackOutline, cloudOutline, partlySunnyOutline, rainyOutline, sunnyOutline, thunderstormOutline,} from 'ionicons/icons';
import { useMemo, useState,useEffect, useContext,useRef } from 'react';
import SwiperCore,{ Pagination,Navigation } from 'swiper';
import { SwiperSlide, Swiper } from 'swiper/react';
import { Keyboard } from '@awesome-cordova-plugins/keyboard';

import {darkThemeContext} from '../state/State'

import './WeatherPage.css';
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import 'chart.js';
import axios from "axios";
import { Chart, registerables } from 'chart.js';
import ChartDataLabels from 'chartjs-plugin-datalabels';
import { Geolocation } from '@awesome-cordova-plugins/geolocation'
Chart.register(...registerables);
Chart.register(ChartDataLabels);

var DB = require("../JSON/DB.json");


interface WeatherPage {
    keywords?: any;
    title: any;
}
let myChart: any = null;
setTimeout(()=>{
    let ctx: any = null
    const canvas =  document.getElementById('myChart') as HTMLCanvasElement;
    ctx = canvas?.getContext('2d');
    const data =  {
        // labels: [['15\u2103','1:00'], ['19\u2103','2:00'],  ['20\u2103','3:00'], ['21\u2103','4:00'],],
        datasets: [{
            label: 'პროცენტი',
            data: [0],
            backgroundColor: [
                "rgba(238, 238, 238, 0.2)"
            ],
            borderColor: [
                'rgba(238, 238, 238, 1)'
            ],
            borderWidth: 1,
        }]
    }

    if(myChart) {
        myChart.destroy();
    }
    myChart = new Chart(ctx, {
        type: 'bar',
        data: data,
        options: {
            maintainAspectRatio: false,
            responsive: true,
            scales: {
                y: {
                    display:false,
                    suggestedMin: 0,
                    suggestedMax: 100,
                    beginAtZero: true,
                    ticks:{
                        display:false
                    },

                },
                x: {
                    ticks: {
                        color:"white",
                        font: {
                            size: 15,
                        }
                    }
                }
            },
            plugins: {
                legend: {
                    labels: {
                        color: "white",
                        font: {
                            size: 14
                        }
                    }
                },
                datalabels: {
                    display:"auto",
                    color: "white",
                    formatter: function (value) {
                        return value !== null && value !== undefined ? Math.round(value) + '%' : null;
                    },
                    font: {
                        size: 16,
                    },
                    anchor :'end',
                    align :'top',
                }
            }
        },
    });
},1000)




const WeatherPage: React.FC<WeatherPage> = ({keywords} : WeatherPage) => {

    const [darkTheme] = useContext<any>(darkThemeContext);

    const [searchText, setSearchText] = useState('');
    const [tabTitle, setTabTitle] = useState("ნალექი");
    const [timeArr, setTimeArr] = useState<any[]>([]);
    const [tempArr, setTempArr] = useState<any[]>([]);
    const [precipChance, setPrecipChance] = useState<any[]>([]);
    const [windArr, setWindArr] = useState<any[]>([]);
    const [windDir, setWindDir] = useState<any[]>([]);
    const [UVindex, setUVindex] = useState<any[]>([]);
    const [visibility, setVisibility] = useState<any[]>([]);
    const [pressure,setPressure] = useState<any[]>([]);
    const [feelsLike,setFeelsLike] = useState<any[]>([]);
    const [hourHumidity, setHourHumidity] = useState<any[]>([]);
    const [dewPoint,setDewPoint] = useState<any[]>([]);
    const [todayIcon,setTodayIcon] = useState();
    const [tempMaxArr, setTempMaxArr] = useState<any[]>([]);
    const [tempMinArr, setTempMinArr] = useState<any[]>([]);
    const [weekDaysArr, setWeekDaysArr] = useState<any[]>([]);
    const [weekDayTime, setWeekDayTime] = useState<any[]>([]);

    const [ dayPartArr, setDayPartArr] = useState<any[]>([]);
    const [ descText, setDescText] = useState<any[]>([]);

    const [sunriseArr,setSunriseArr] = useState<any[]>([]);
    const [sunsetArr,setSunsetArr] = useState<any[]>([]);
    const [QPF, setQPF] = useState<any[]>([]);
    const [QPFSnow, setQPFSnow] = useState<any[]>([]);

    const [humidity,setHumidity] = useState<any[]>([]);
    const [windSpeed, setWindSpeed] = useState<any[]>([]);
    const [UVINDEX,setUVINDEX] = useState<any[]>([]);
    const [precChance, setPrecChance] = useState<any[]>([]);
    const [iconArr,setIconsArr] = useState<any[]>([]);

    const [AQI,setAQI] = useState(0);
    const [AQIMessage,setAQIMessage] = useState<any[]>([]);
    const [sensitiveMessage,setSensitiveMessage] = useState<any[]>([]);

    const [loading, setLoading] = useState(true)
    const [loading1, setLoading1] = useState(true)
    const [counter,setCounter] = useState(0)

    // const [GEOCODE, setGEOCODE] = useState("41.72044,44.79027")
    const [Location,setLocation] = useState("");

    const GEOCODE = useRef("41.70301989775022,44.79371192597676");

    const [currentLocation, setCurrentLocation] = useState("თბილისი");
    const [currentLocationChanged, setCurrentLocationChanged] = useState(false);
    const [keyboardIsVisible, setKeyboardIsVisible] = useState<boolean>();




    useEffect(() => {
        let isMounted = true;
        Keyboard.disableScroll(true);

        Geolocation.getCurrentPosition().then((resp)=>{

            let Array: any = []
            function distance(lat1: any, lon1: any, lat2: any, lon2: any, unit: any) {
                var radlat1 = Math.PI * lat1/180
                var radlat2 = Math.PI * lat2/180
                var theta = lon1-lon2
                var radtheta = Math.PI * theta/180
                var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
                if (dist > 1) {
                    dist = 1;
                }
                dist = Math.acos(dist)
                dist = dist * 180/Math.PI
                dist = dist * 60 * 1.1515
                if (unit=="K") { dist = dist * 1.609344 }
                if (unit=="N") { dist = dist * 0.8684 }
                Array.push(dist)
            }

            // console.log("testing...", resp);

            let test = () => {
                DB.map((item:any)=>{
                    distance(
                        resp.coords.latitude, resp.coords.longitude, parseFloat(item.geocode.split(",")[0]), parseFloat(item.geocode.split(",")[1]),"K");
                })
                let newArray = Array.filter((element : any) => ![NaN].includes(element))
                const min = Math.min.apply(Math, newArray)
                GEOCODE.current = (DB[Array.indexOf(min)].geocode)
                setCurrentLocation(DB[Array.indexOf(min)].city);
                setCurrentLocationChanged(true);
            }
            test();
        })

        window.addEventListener('keyboardDidShow', (event) => {
            // Describe your logic which will be run each time when keyboard is about to be shown.
            setKeyboardIsVisible(true)
        });
        window.addEventListener('keyboardDidHide', () => {
            // Describe your logic which will be run each time keyboard is closed.
            setKeyboardIsVisible(false)
        });

        //
        // setTimeout(()=>{
        //
        //     let ctx: any = null
        //     const canvas =  document.getElementById('myChart') as HTMLCanvasElement;
        //     ctx = canvas.getContext('2d');
        //     const data =  {
        //         // labels: [['15\u2103','1:00'], ['19\u2103','2:00'],  ['20\u2103','3:00'], ['21\u2103','4:00'],],
        //         datasets: [{
        //             label: 'პროცენტი',
        //             data: [0],
        //             backgroundColor: [
        //                 "rgba(238, 238, 238, 0.2)"
        //             ],
        //             borderColor: [
        //                 'rgba(238, 238, 238, 1)'
        //             ],
        //             borderWidth: 1,
        //         }]
        //     }
        //     myChart = new Chart(ctx, {
        //         type: 'bar',
        //         data: data,
        //         options: {
        //             maintainAspectRatio: false,
        //             responsive: true,
        //             scales: {
        //                 y: {
        //                     suggestedMin: 0,
        //                     suggestedMax: 100,
        //                     ticks:{
        //                         display:false
        //                     },
        //                     beginAtZero: true,
        //
        //                 },
        //                 x: {
        //                     ticks: {
        //                         color:"white",
        //                         font: {
        //                             size: 15,
        //                         }
        //                     }
        //                 }
        //             },
        //             plugins: {
        //                 legend: {
        //                     labels: {
        //                         color: "white",
        //                         font: {
        //                             size: 14
        //                         }
        //                     }
        //                 },
        //                 datalabels: {
        //                     display:"auto",
        //                     color: "white",
        //                     formatter: function (value) {
        //                         return value !== null && value !== undefined ? Math.round(value) + '%' : null;
        //                     },
        //                     font: {
        //                         size: 16,
        //                     },
        //                     anchor :'end',
        //                     align :'top',
        //                 }
        //             }
        //         },
        //     });
        //
        //     const updateLabel = (timeArr: any, tempArr: any) =>{
        //         let Array: object[] = [];
        //         timeArr.map((item: any,index : any)=>{
        //             tempArr.map((data: any,index1: any)=>{
        //                 index == index1 && index >= 0 && index < 24 && Array.push([`${data}\u2103`,item.substring(11,16)])
        //
        //             })
        //         })
        //         myChart.config.data.labels = Array;
        //         myChart.update()
        //     }
        //     const updateDataset = (data: any) =>{
        //         let Array:number [] = [];
        //         data.map((item: any,index : any)=>{
        //             index >= 0 && index < 24 && Array.push(item)
        //         })
        //         myChart.config.data.datasets[0].data = Array;
        //         myChart.update()
        //     }
        //
        //     const fetchChartData = async ()=> {
        //         let isMounted = true;
        //         const body = {
        //             lat_long : GEOCODE.current
        //         }
        //
        //         axios.post(`https://euronewsgeorgia.com/wp-json/above_rest_api/v1/fetch/hourly_15day_file`,body)
        //             .then(response => {
        //                 let responseData = JSON.parse(response.data[0]);
        //                 setTimeArr(responseData.validTimeLocal);
        //                 setTempArr(responseData.temperature);
        //                 setPrecipChance(responseData.precipChance);
        //                 setWindArr(responseData.windSpeed);
        //                 setWindDir(responseData.windDirectionCardinal);
        //                 setUVindex(responseData.uvIndex);
        //                 setVisibility(responseData.visibility);
        //                 setPressure(responseData.pressureMeanSeaLevel);
        //                 setFeelsLike(responseData.temperatureFeelsLike)
        //                 setHourHumidity(responseData.relativeHumidity);
        //                 setDewPoint(responseData.temperatureDewPoint)
        //
        //                 setTodayIcon(responseData.iconCode[0])
        //
        //                 setLoading1(false);
        //
        //                 updateLabel(responseData.validTimeLocal,responseData.temperature);
        //                 updateDataset(responseData.precipChance);
        //             })
        //             .catch(error => {
        //                 console.error('There was an error!', error);
        //             });
        //         return () => { isMounted = false };
        //     }
        //     const fetchDailyData = async () => {
        //         let isMounted = true;
        //         const body = {
        //             lat_long : GEOCODE.current
        //         }
        //         axios.post(`https://euronewsgeorgia.com/wp-json/above_rest_api/v1/fetch/daily_15day_file`,body)
        //             .then(response => {
        //                 let responseData = JSON.parse(response.data[0])
        //                 setTempMaxArr(responseData.calendarDayTemperatureMax);
        //                 setTempMinArr(responseData.calendarDayTemperatureMin);
        //                 setWeekDaysArr(responseData.dayOfWeek);
        //                 setWeekDayTime(responseData.validTimeLocal)
        //
        //                 setDayPartArr(responseData.daypart);
        //                 setHumidity(responseData.daypart[0].relativeHumidity);
        //                 setWindSpeed(responseData.daypart[0].windSpeed);
        //                 setDescText(responseData.narrative);
        //                 setUVINDEX(responseData.daypart[0].uvIndex)
        //                 setPrecChance(responseData.daypart[0].precipChance);
        //                 setIconsArr(responseData.daypart[0].iconCode)
        //
        //
        //                 setSunriseArr(responseData.sunriseTimeLocal);
        //                 setSunsetArr(responseData.sunsetTimeLocal);
        //
        //                 setQPF(responseData.qpf);
        //                 setQPFSnow(responseData.qpfSnow);
        //
        //
        //                 setLoading(false)
        //
        //             })
        //             .catch(error => {
        //                 console.error('There was an error!', error);
        //             });
        //         return () => { isMounted = false };
        //     }
        //     const fetchAQI = async () => {
        //         let isMounted = true;
        //         const body = {
        //             lat_long : GEOCODE.current
        //         }
        //         axios.post(`https://euronewsgeorgia.com/wp-json/above_rest_api/v1/fetch/global_air_quality_file`,body)
        //             .then(response => {
        //                 let responseData = JSON.parse(response.data[0])
        //
        //                 setAQI(responseData.globalairquality.airQualityIndex)
        //                 setAQIMessage(responseData.globalairquality.messages.General.text)
        //                 setSensitiveMessage(responseData.globalairquality.messages["Sensitive Group"].text)
        //             })
        //             .catch(error => {
        //                 console.error('There was an error!', error);
        //             });
        //         return () => { isMounted = false };
        //     }
        //     fetchDailyData();
        //     fetchChartData();
        //     fetchAQI();
        // },500)
        return () => { isMounted = false };

    }, []);

    useEffect(() => {
        if(currentLocationChanged)
            cityChange();
    }, [currentLocationChanged]);

    useEffect(() => {
        // if(currentLocationChanged)
            cityChange();
    }, []);

    const cityChange = () => {
// console.log("cityChange",myChart);

        setTimeout(()=>{
            if(myChart)
                myChart.destroy();

            let ctx: any = null
            const canvas =  document.getElementById('myChart') as HTMLCanvasElement;
            ctx = canvas.getContext('2d');
            const data =  {
                // labels: [['15\u2103','1:00'], ['19\u2103','2:00'],  ['20\u2103','3:00'], ['21\u2103','4:00'],],
                datasets: [{
                    label: 'პროცენტი',
                    data: [0],
                    backgroundColor: [
                        "rgba(238, 238, 238, 0.2)"
                    ],
                    borderColor: [
                        'rgba(238, 238, 238, 1)'
                    ],
                    borderWidth: 1,
                }]
            }
            myChart = new Chart(ctx, {
                type: 'bar',
                data: data,
                options: {
                    maintainAspectRatio: false,
                    responsive: true,
                    scales: {
                        y: {
                            display:false,
                            suggestedMin: 0,
                            suggestedMax: 100,
                            beginAtZero: true,
                            ticks:{
                                display:false
                            },

                        },
                        x: {
                            ticks: {
                                color:"white",
                                font: {
                                    size: 15,
                                }
                            }
                        }
                    },
                    plugins: {
                        legend: {
                            labels: {
                                color: "white",
                                font: {
                                    size: 14
                                }
                            }
                        },
                        datalabels: {
                            display:"auto",
                            color: "white",
                            formatter: function (value) {
                                return value !== null && value !== undefined ? Math.round(value) + '%' : null;
                            },
                            font: {
                                size: 16,
                            },
                            anchor :'end',
                            align :'top',
                        }
                    }
                },
            });

            const updateLabel = (timeArr: any, tempArr: any) =>{
                let Array: object[] = [];
                timeArr.map((item: any,index : any)=>{
                    tempArr.map((data: any,index1: any)=>{
                        index == index1 && index >= 0 && index < 24 && Array.push([`${data}\u2103`,item.substring(11,16)])

                    })
                })
                myChart.config.data.labels = Array;
                myChart.update()
            }



            const updateDataset = (data: any) =>{
                let Array:number [] = [];
                data.map((item: any,index : any)=>{
                    index >= 0 && index < 24 && Array.push(item)
                })
                myChart.config.data.datasets[0].data = Array;
                myChart.update()
            }

            const fetchChartData = async ()=> {
                let isMounted = true;
                // const body = {
                //   lat_long : GEOCODE.current
                // }

                axios.post(`https://euronewsgeorgia.com/wp-json/above_rest_api/v1/fetch/hourly_15day_file`,{
                    lat_long : GEOCODE.current
                } )
                    .then(response => {
                        let responseData = JSON.parse(response.data[0])
                        setTimeArr(responseData.validTimeLocal);
                        setTempArr(responseData.temperature);
                        setPrecipChance(responseData.precipChance);
                        setWindArr(responseData.windSpeed);
                        setWindDir(responseData.windDirectionCardinal);
                        setUVindex(responseData.uvIndex);
                        setVisibility(responseData.visibility);
                        setPressure(responseData.pressureMeanSeaLevel);
                        setFeelsLike(responseData.temperatureFeelsLike)
                        setHourHumidity(responseData.relativeHumidity);
                        setDewPoint(responseData.temperatureDewPoint)

                        setTodayIcon(responseData.iconCode[0])

                        setLoading1(false);

                        updateLabel(responseData.validTimeLocal,responseData.temperature);
                        updateDataset(responseData.precipChance);
                    })
                    .catch(error => {
                        console.error('There was an error!', error);
                    });
                return () => { isMounted = false };
            }
            const fetchDailyData = async () => {
                let isMounted = true;
                // const body = {
                //   lat_long : GEOCODE.current
                // }
                axios.post(`https://euronewsgeorgia.com/wp-json/above_rest_api/v1/fetch/daily_15day_file`,{
                    lat_long : GEOCODE.current
                } )
                    .then(response => {
                        let responseData = JSON.parse(response.data[0])
                        console.log(responseData);
                        setTempMaxArr(responseData.calendarDayTemperatureMax);
                        setTempMinArr(responseData.calendarDayTemperatureMin);
                        setWeekDaysArr(responseData.dayOfWeek);
                        setWeekDayTime(responseData.validTimeLocal)

                        setDayPartArr(responseData.daypart);
                        setHumidity(responseData.daypart[0].relativeHumidity);
                        setWindSpeed(responseData.daypart[0].windSpeed);
                        setDescText(responseData.narrative);
                        setUVINDEX(responseData.daypart[0].uvIndex)
                        setPrecChance(responseData.daypart[0].precipChance);
                        setIconsArr(responseData.daypart[0].iconCode)


                        setSunriseArr(responseData.sunriseTimeLocal);
                        setSunsetArr(responseData.sunsetTimeLocal);

                        setQPF(responseData.qpf);
                        setQPFSnow(responseData.qpfSnow);


                        setLoading(false)

                    })
                    .catch(error => {
                        console.error('There was an error!', error);
                    });
                return () => { isMounted = false };
            }
            const fetchAQI = async () => {
                let isMounted = true;
                // const body = {
                //   lat_long : GEOCODE.current
                // }
                axios.post(`https://euronewsgeorgia.com/wp-json/above_rest_api/v1/fetch/global_air_quality_file`,{
                    lat_long : GEOCODE.current
                } ).then(response => {
                    let responseData = JSON.parse(response.data[0])

                    setAQI(responseData.globalairquality.airQualityIndex)
                    setAQIMessage(responseData.globalairquality.messages.General.text)
                    setSensitiveMessage(responseData.globalairquality.messages["Sensitive Group"].text)
                })
                    .catch(error => {
                        console.error('There was an error!', error);
                    });
                return () => { isMounted = false };
            }
            fetchDailyData();
            fetchChartData();
            fetchAQI();
        },500)
    }

    const tabChange = (tabName: any) =>{
        myChart.destroy();
        if(tabName == "ქარი"){
            setTimeout(()=>{
                let ctx: any = null
                const canvas =  document.getElementById('myChart') as HTMLCanvasElement;
                ctx = canvas.getContext('2d');
                const data =  {
                    labels: [['15\u2103','1:00'], ['19\u2103','2:00'],  ['20\u2103','3:00'], ['21\u2103','4:00']],
                    datasets: [{
                        label: 'კმ/სთ',
                        data: [0],
                        backgroundColor: [
                            "rgba(238, 238, 238, 0.2)"
                        ],
                        borderColor: [
                            'rgba(238, 238, 238, 1)'
                        ],
                        borderWidth: 1,
                    }]
                }
                myChart = new Chart(ctx, {
                    type: 'bar',
                    data: data,
                    options: {
                        maintainAspectRatio: false,
                        responsive: true,
                        scales: {
                            y: {
                                display:false,
                                suggestedMin: 0,
                                suggestedMax: 100,
                                beginAtZero: true,
                                ticks:{
                                    display:false
                                },
                            },
                            x: {
                                ticks: {
                                    color:"white",
                                    font: {
                                        size: 15,
                                    }
                                }
                            }
                        },
                        plugins: {
                            legend: {
                                labels: {
                                    color: "white",
                                    font: {
                                        size: 14
                                    }
                                }
                            },
                            datalabels: {
                                display:"auto",
                                color: "white",
                                formatter: function (value) {
                                    return value !== null && value !== undefined ? Math.round(value) + ' კმ/სთ' : null;
                                },
                                font: {
                                    size: 16,
                                },
                                anchor :'end',
                                align :'top',
                            }
                        }
                    },
                });
                // const updateLabel = (timeArr: any, tempArr: any) =>{
                //     let Array: object[] = [];
                //     timeArr.map((item: any,index : any)=>{
                //         tempArr.map((data: any,index1: any)=>{
                //             index == index1 && index + (counter)*24  < 24 + (counter)*24 &&
                //             Array.push(
                //                 [`${tempArr[index + 24+ (counter)*24]}\u2103`,
                //                     timeArr[index + 24 + (counter)*24].substring(11,16)]
                //             )
                //
                //         })
                //     })
                //     myChart.config.data.labels = Array;
                //     myChart.update()
                // }

                const updateLabel = (timeArr: any, tempArr: any) =>{
                    let Array: object[] = [];
                    timeArr.map((item: any,index : any)=>{
                        tempArr.map((data: any,index1: any)=>{
                            index == index1 && index >= 0 && index < 24 && Array.push([`${data}\u2103`,item.substring(11,16)])

                        })
                    })
                    myChart.config.data.labels = Array;
                    myChart.update()
                }

                const updateDataset = (data: any) =>{
                    let Array:number [] = [];
                    data.map((item: any,index : any)=>{
                        // index  < 24 + (counter)*24 && Array.push(data[index + 24 +(counter)*24])
                        index >= 0 && index < 24 && Array.push(item)
                    })
                    myChart.config.data.datasets[0].data = Array;
                    myChart.update()
                }

                updateLabel(timeArr,tempArr);
                updateDataset(windArr);

            },500)
        }
        if(tabName == "ნალექი"){
            setTimeout(()=>{
                let ctx: any = null
                const canvas =  document.getElementById('myChart') as HTMLCanvasElement;
                ctx = canvas.getContext('2d');
                const data =  {
                    labels: [['15\u2103','1:00'], ['19\u2103','2:00'],  ['20\u2103','3:00'], ['21\u2103','4:00'],],
                    datasets: [{
                        label: '%',
                        data: [0],
                        backgroundColor: [
                            "rgba(238, 238, 238, 0.2)"
                        ],
                        borderColor: [
                            'rgba(238, 238, 238, 1)'
                        ],
                        borderWidth: 1,
                    }]
                }
                myChart = new Chart(ctx, {
                    type: 'bar',
                    data: data,
                    options: {
                        maintainAspectRatio: false,
                        responsive: true,
                        scales: {
                            y: {
                                display:false,
                                suggestedMin: 0,
                                suggestedMax: 100,
                                beginAtZero: true,
                                ticks:{
                                    display:false
                                },
                            },
                            x: {
                                ticks: {
                                    color:"white",
                                    font: {
                                        size: 15,
                                    }
                                }
                            }
                        },
                        plugins: {
                            legend: {
                                labels: {
                                    color: "white",
                                    font: {
                                        size: 14
                                    }
                                }
                            },
                            datalabels: {
                                display:"auto",
                                color: "white",
                                formatter: function (value) {
                                    return value !== null && value !== undefined ? Math.round(value) + '%' : null;
                                },
                                font: {
                                    size: 16,
                                },
                                anchor :'end',
                                align :'top',
                            }
                        }
                    },
                });
                // const updateLabel = (timeArr: any, tempArr: any) =>{
                //     let Array: object[] = [];
                //     timeArr.map((item: any,index : any)=>{
                //         tempArr.map((data: any,index1: any)=>{
                //             index == index1 &&
                //             index + (counter)*24  < 24 + (counter)*24 &&
                //             Array.push(
                //                 [`${tempArr[index + 24 + (counter )*24]}\u2103`,
                //                     timeArr[index + 24+ (counter)*24].substring(11,16)]
                //             )
                //
                //         })
                //     })
                //     myChart.config.data.labels = Array;
                //     myChart.update()
                // }

                const updateLabel = (timeArr: any, tempArr: any) =>{
                    let Array: object[] = [];
                    timeArr.map((item: any,index : any)=>{
                        tempArr.map((data: any,index1: any)=>{
                            index == index1 && index >= 0 && index < 24 && Array.push([`${data}\u2103`,item.substring(11,16)])

                        })
                    })
                    myChart.config.data.labels = Array;
                    myChart.update()
                }

                // const updateDataset = (data: any) =>{
                //     let Array:number [] = [];
                //     data.map((item: any,index : any)=>{
                //         index  < 24 + (counter)*24 && Array.push(data[index + 24 +(counter)*24])
                //     })
                //     myChart.config.data.datasets[0].data = Array;
                //     myChart.update()
                // }

                const updateDataset = (data: any) =>{
                    let Array:number [] = [];
                    data.map((item: any,index : any)=>{
                        index >= 0 && index < 24 && Array.push(item)
                    })
                    myChart.config.data.datasets[0].data = Array;
                    myChart.update()
                }

                updateLabel(timeArr,tempArr);
                updateDataset(precipChance);

            },500)
        }
        if (tabName == "UVI  ულტრაიისფერი გამოსხივების ინდექსი "){
            setTimeout(()=>{
                let ctx: any = null
                const canvas =  document.getElementById('myChart') as HTMLCanvasElement;
                ctx = canvas.getContext('2d');
                const data =  {
                    labels: [['15\u2103','1:00'], ['19\u2103','2:00'],  ['20\u2103','3:00'], ['21\u2103','4:00'],],
                    datasets: [{
                        label: '',
                        data: [0],
                        backgroundColor: [
                            "rgba(238, 238, 238, 0.2)"
                        ],
                        borderColor: [
                            'rgba(238, 238, 238, 1)'
                        ],
                        borderWidth: 1,
                    }]
                }
                myChart = new Chart(ctx, {
                    type: 'bar',
                    data: data,
                    options: {
                        maintainAspectRatio: false,
                        responsive: true,
                        scales: {
                            y: {
                                display:true,
                                suggestedMin: 0,
                                suggestedMax: 16,
                                beginAtZero: true,
                                ticks:{
                                    display:false
                                },
                            },
                            x: {
                                ticks: {
                                    color:"white",
                                    font: {
                                        size: 15,
                                    }
                                }
                            }
                        },
                        plugins: {
                            legend: {
                                labels: {
                                    color: "white",
                                    font: {
                                        size: 14
                                    }
                                }
                            },
                            datalabels: {
                                display:"auto",
                                color: "white",
                                formatter: function (value) {
                                    return value !== null && value !== undefined ? Math.round(value) : null;
                                },
                                font: {
                                    size: 16,
                                },
                                anchor :'end',
                                align :'top',
                            }
                        }
                    },
                });
                // const updateLabel = (timeArr: any, tempArr: any) =>{
                //     let Array: object[] = [];
                //     timeArr.map((item: any,index : any)=>{
                //         tempArr.map((data: any,index1: any)=>{
                //             index == index1 &&
                //             index + (counter)*24  < 24 + (counter)*24 &&
                //             Array.push(
                //                 [`${tempArr[index + 24+ (counter)*24]}\u2103`,
                //                     timeArr[index + 24+ (counter)*24].substring(11,16)]
                //             )
                //
                //         })
                //     })
                //     myChart.config.data.labels = Array;
                //     myChart.update()
                // }

                const updateLabel = (timeArr: any, tempArr: any) =>{
                    let Array: object[] = [];
                    timeArr.map((item: any,index : any)=>{
                        tempArr.map((data: any,index1: any)=>{
                            index == index1 && index >= 0 && index < 24 && Array.push([`${data}\u2103`,item.substring(11,16)])

                        })
                    })
                    myChart.config.data.labels = Array;
                    myChart.update()
                }

                // const updateDataset = (data: any) =>{
                //     let Array:number [] = [];
                //     data.map((item: any,index : any)=>{
                //         index  < 24 + (counter)*24 && Array.push(data[index + 24 +(counter)*24])
                //     })
                //     myChart.config.data.datasets[0].data = Array;
                //     myChart.update()
                // }

                const updateDataset = (data: any) =>{
                    let Array:number [] = [];
                    data.map((item: any,index : any)=>{
                        index >= 0 && index < 24 && Array.push(item)
                    })
                    myChart.config.data.datasets[0].data = Array;
                    myChart.update()
                }
                updateLabel(timeArr,tempArr);
                updateDataset(UVindex);

            },500)}


    }

//  const descTextFunc = () => {
//    if(descText){
//       let array = [];
//       array = descText[counter + 1].split(".")
//       return (
//         <>
//          <h3>{array[0]}</h3>
//          <br/>
//          <p>{array[1].
//         replace(array[1].split('მაქსიმა').pop().split('ლური')[0],"")
//         .replace(array[1].split('მინიმალური').pop().split('მაჩვენებლები')[0]," ")
//         .replace("მაქსიმალური", `მაქსიმალური ${array[1].split('მაქსიმა').pop().split('ლური')[0]}ºC - `)
//         .replace("მაჩვენებლები", `მაჩვენებლები ${array[1].split('მინიმალური').pop().split('მაჩვენებლები')[0]}ºC - `)
//         }</p>
//         </>     
//       )
//    }
//  }

    const bottomSection = () => {

        if(sunriseArr && !loading){
            return(
                <IonGrid className='en-weather-description'>
                    <IonRow>
                        <IonCol size='12'>
                            <div className='ion-float-left' style={{color: '#dfeef6'}}>მზის ამოსვლა:</div>
                            <div className='ion-float-right'>{sunriseArr && sunriseArr[counter].substring(11,16)}</div>
                        </IonCol>
                        <IonCol size='12'>
                            <div className='ion-float-left' style={{color: '#dfeef6'}}>მზის ჩასვლა:</div>
                            <div className='ion-float-right'>{sunsetArr && sunsetArr[counter].substring(11,16)}</div>
                        </IonCol>
                        <IonCol size='12'>
                            <div className='ion-float-left' style={{color: '#dfeef6'}}>ნალექის ალბათობა (დღე):</div>
                            <div className='ion-float-right'>{precChance && precChance[counter*2] == null ? precChance[counter+2] : precChance[counter*2]} %</div>
                        </IonCol>
                        <IonCol size='12'>
                            <div className='ion-float-left' style={{color: '#dfeef6'}}>ნალექის ალბათობა (ღამე):</div>
                            <div className='ion-float-right'>{precChance && precChance[counter*2]} %</div>
                        </IonCol>
                        <IonCol size='12'>
                            <div className='ion-float-left' style={{color: '#dfeef6'}}>ნალექის რაოდენობა (წვიმა):</div>
                            <div className='ion-float-right'>{QPF && QPF[counter*2]} მმ</div>
                        </IonCol>
                        <IonCol size='12'>
                            <div className='ion-float-left' style={{color: '#dfeef6'}}>ნალექის რაოდენობა (თოვლი):</div>
                            <div className='ion-float-right'>{QPFSnow && QPFSnow[counter*2]} სმ</div>
                        </IonCol>
                        <IonCol size='12'>
                            <div className='ion-float-left' style={{color: '#dfeef6'}}>ტენიანობა (დღე):</div>
                            <div className='ion-float-right'>{humidity && humidity[counter*2] == null ? humidity[counter+2] : humidity[counter*2]} %</div>
                        </IonCol>
                        <IonCol size='12'>
                            <div className='ion-float-left' style={{color: '#dfeef6'}}>ტენიანობა (ღამე):</div>
                            <div className='ion-float-right'>{humidity && humidity[counter*2]} %</div>
                        </IonCol>
                        <IonCol size='12'>
                            <div className='ion-float-left' style={{color: '#dfeef6'}}>ქარის სიჩქარე (დღე):</div>
                            <div className='ion-float-right'>{windSpeed && windSpeed[counter*2] == null ? windSpeed[counter+2] : windSpeed[counter*2]} კმ/სთ</div>
                        </IonCol>
                        <IonCol size='12'>
                            <div className='ion-float-left' style={{color: '#dfeef6'}}>ქარის სიჩქარე (ღამე):</div>
                            <div className='ion-float-right'>{windSpeed && windSpeed[counter*2]} კმ/სთ</div>
                        </IonCol>
                        <IonCol size='12'>
                            <div className='ion-float-left' style={{color: '#dfeef6'}}>UV ინდექსი:</div>
                            <div className='ion-float-right'>{UVINDEX && UVINDEX[counter*2] == null ? UVINDEX[counter+2] : UVINDEX[counter*2]}</div>
                        </IonCol>
                        <IonCol size='12'>
                            <div className='ion-float-left' style={{color: '#dfeef6'}}>ტემპერატურა:</div>
                            <div className='ion-float-right'>{tempMaxArr && tempMaxArr[counter ]}&#8451; </div>
                        </IonCol>
                        <IonCol size='12'>
                            <div className='ion-float-left' style={{color: '#dfeef6'}}>რეალური შეგრძნება:</div>
                            <div className='ion-float-right'>{feelsLike && feelsLike[24*(counter)]}&#8451; </div>
                        </IonCol>
                        <IonCol size='12'>
                            <div className='ion-float-left' style={{color: '#dfeef6'}}>ატმოსფერული წნევა:</div>
                            <div className='ion-float-right'>{pressure && pressure[24*(counter)]} მბ</div>
                        </IonCol>
                        <IonCol size='12'>
                            <div className='ion-float-left' style={{color: '#dfeef6'}}>ხილვადობა:</div>
                            <div className='ion-float-right'>{visibility && visibility[24*(counter)]} კმ</div>
                        </IonCol>
                    </IonRow>
                </IonGrid>
            )
        }
    }

    const accordionRender = () => {
        return(
            <IonCol size='12'>
                <IonAccordionGroup  className='en-accordion'>
                    <IonAccordion style={{backgroundColor:"rgba(31, 77, 121, 1)"}} >
                        <IonItem slot="header"  className='en-accordion-header'>
                            <IonLabel>დეტალურად</IonLabel>
                        </IonItem>

                        <IonList slot="content" className='en-accordion-list ion-no-padding'>
                            <IonItem className='en-accordion-item'>
                                <IonLabel>
                                    <IonRow>
                                        <IonCol size='12'>
                                            <div className='ion-float-left'>ქარის სიჩქარე</div>
                                            <div className='ion-float-right'>{windArr && windArr[0]} კმ/სთ</div>
                                        </IonCol>
                                    </IonRow>
                                </IonLabel>
                            </IonItem>
                            <IonItem className='en-accordion-item'>
                                <IonLabel>
                                    <IonRow>
                                        <IonCol size='12'>
                                            <div className='ion-float-left'>ქარის მიმართულება</div>
                                            <div className='ion-float-right'>{windDir && windDir[0]}</div>
                                        </IonCol>
                                    </IonRow>
                                </IonLabel>
                            </IonItem>
                            <IonItem className='en-accordion-item'>
                                <IonLabel>
                                    <IonRow>
                                        <IonCol size='12'>
                                            <div className='ion-float-left'>ტენიანობა</div>
                                            <div className='ion-float-right'>{hourHumidity && hourHumidity[0]} %</div>
                                        </IonCol>
                                    </IonRow>
                                </IonLabel>
                            </IonItem>
                            <IonItem className='en-accordion-item'>
                                <IonLabel>
                                    <IonRow>
                                        <IonCol size='12'>
                                            <div className='ion-float-left'>ნამის წერტილი</div>
                                            <div className='ion-float-right'>{dewPoint && dewPoint[0]}</div>
                                        </IonCol>
                                    </IonRow>
                                </IonLabel>
                            </IonItem>
                            <IonItem className='en-accordion-item'>
                                <IonLabel>
                                    <IonRow>
                                        <IonCol size='12'>
                                            <div className='ion-float-left'>ატმოსფერული წნევა</div>
                                            <div className='ion-float-right'>{pressure && pressure[0]} მბ</div>
                                        </IonCol>
                                    </IonRow>
                                </IonLabel>
                            </IonItem>
                            <IonItem className='en-accordion-item'>
                                <IonLabel>
                                    <IonRow>
                                        <IonCol size='12'>
                                            <div className='ion-float-left'>ხილვადობა</div>
                                            <div className='ion-float-right'>{visibility && visibility[0]} კმ</div>
                                        </IonCol>
                                    </IonRow>
                                </IonLabel>
                            </IonItem>
                            <IonItem className='en-accordion-item'>
                                <IonLabel>
                                    <IonRow>
                                        <IonCol size='12'>
                                            <div className='ion-float-left ion-margin-bottom'>ძირითადი ჯგუფი</div>
                                            <div style={{whiteSpace:"pre-line"}}  className='ion-float-right'>{AQIMessage && AQIMessage}</div>
                                        </IonCol>
                                    </IonRow>
                                </IonLabel>
                            </IonItem>
                            {/* <IonItem className='en-accordion-item'>
            <IonLabel>
              <IonRow>
                <IonCol size='12'>
                  <div className='ion-float-left ion-margin-bottom'>მგრძნობიარე ჯგუფი</div>
                  <div style={{whiteSpace:"pre-line"}}  className='ion-float-right'>{sensitiveMessage && sensitiveMessage}</div>
                </IonCol>
              </IonRow>
            </IonLabel>
          </IonItem> */}
                        </IonList>
                    </IonAccordion>
                </IonAccordionGroup>
            </IonCol>
        )
    }

    const onChange = (event: any) => {
        setLocation(event.target.value);
    };

// useEffect(() => {

// }, [GEOCODE])


    const onSearch = (searchTerm: any, geocode: any) => {
        setLocation("");
        setCurrentLocation(searchTerm);
        GEOCODE.current = geocode;
        cityChange();
        // our api to fetch the search result
    };

    const handleKeyDown = (event:any) => {
        if (event.key === 'Enter') {
            let Array = DB.filter((item: any) => {
                const searchTerm = Location.toLowerCase();
                const cityName = item.city.toLowerCase();

                return (
                    searchTerm &&
                    cityName.startsWith(searchTerm)
                );
            })
            setLocation("");
            setCurrentLocation(Array[0].city);
            GEOCODE.current = Array[0].geocode;
            cityChange();
        }
    }

    const SearchInput = () => (
        <IonCol size='12' className='ion-margin-top en-weather-search-col'>
            <IonSearchbar
                onKeyDown={handleKeyDown}
                // clearIcon="never"
                className='en-weather-search'
                style={darkTheme ? {color:"black"} : {color:"white"}}
                value={Location}
                onIonChange={onChange}
                clearIcon=""
                placeholder='აირჩიეთ ქალაქი'/>
            <div className="dropdown-wrapper">
                <div className="dropdown">
                    {DB
                        .filter((item: any) => {
                            const searchTerm = Location.toLowerCase();
                            const cityName = item.city.toLowerCase();

                            return (
                                searchTerm &&
                                cityName.startsWith(searchTerm)
                            );
                        })
                        .slice(0, 5)
                        .map((item: any,index : any) => (
                            <div
                                onClick={() => {
                                    onSearch(item.city, item.geocode);
                                }}
                                className="dropdown-row"
                                key={index}
                            >
                                {item.city}
                            </div>
                        ))}
                </div>
            </div>
        </IonCol>
    )

    const searchInputMemo = useMemo(() => SearchInput(),[Location])



    return (
        <IonPage >

            <IonHeader style={{height:"95px",paddingTop:"35px"}} className="ion-no-border en-settings-header">
                <IonBackButton style={{marginTop: "16px"}} icon={chevronBackOutline} className='ion-float-left ion-no-padding en-settings-back-button'/>
                <IonRouterLink style={{marginLeft: "30%",marginTop: "15px"}} color="dark" routerLink="/" class='en-header-logo'>
                    <IonImg src='../assets/images/header-logo2.png'/>
                </IonRouterLink>
                <IonTitle className='en-settings-title ion-no-padding'>

                    {keywords.WEATHER}
                </IonTitle>
            </IonHeader>

            <IonContent id="weather-content" className='en-weather'>
                <IonGrid className='en-weather-part1'>
                    <IonRow>
                        <IonCol size='12' style={{marginTop:"90px",textAlign: 'center'}} className='en-weather-title'>
                            <div style={{display:"flex"}}> <IonImg style={{width: "90%"}}src="../assets/images/weather-logo.png" /></div>

                        </IonCol>
                        <IonCol size='12' style={{textAlign: 'center'}} className='en-weather-title'>
                        </IonCol>

                        {searchInputMemo}

                        <IonCol id="en-weather-city-name" size='12' className='en-weather-city-name'>
                            <h5>{currentLocation}</h5>
                        </IonCol>

                        <IonCol id="en-weather-today" size='12' className='en-weather-today'>

                            <IonRow>

                                <IonCol size='6' className='en-weather-today-title'>
                                    <div>
                                        <div className='en-weather-today-title-text'>დღეს </div>
                                        {!loading1 && timeArr ? timeArr[0].substring(5,10).replace("-","/") : <IonSpinner style={{transform:"scale(1.5)",margin:"10px"}} color="light" name="bubbles" />}
                                    </div>



                                </IonCol>

                                <IonCol size='6' style={{paddingTop:'25px'}}>
                                    <div style={{display:"flex",justifyContent:"center",}}>
                                        {todayIcon ? <IonImg style={{height:"65px", marginRight: '8px'}} src={`../assets/weathericons/icon${todayIcon}.png`}/>  : <></>}
                                        <div >
                                            <span className='en-today-high-temperatures'><span style={{marginLeft:"15px",fontSize: '55px'}}>{tempArr.length > 0 ? tempArr[0] : <IonSpinner style={{transform:"scale(1.5)",margin:"10px"}} color="light" name="bubbles" />}</span>&#8451;</span>
                                        </div>
                                    </div>
                                </IonCol>



                            </IonRow>
                            {/* <IonRow>
              <IonCol size='6' className='en-weather-today-title'>
                <h5>UVI ინდექსი</h5>
                <div>{!loading1 && timeArr && timeArr[0].substring(5,10).replace("-","/")}</div>
              </IonCol>
              <IonCol size='6' className='en-weather-today-title'>
                <h5>ჰაერის ხარისხის ინდექსი</h5>
                <div>{!loading1 && timeArr && timeArr[0].substring(5,10).replace("-","/")}</div>
              </IonCol>
            </IonRow> */}
                        </IonCol>




                    </IonRow>

                </IonGrid>

                <IonGrid  className='en-weather-today-descrip'>
                    <IonRow>
                        <IonCol size='12'>

                            <div>
                                <span className='en-weather-today-title-text'>UV ინდექსი: {UVindex && UVindex[0]}</span>
                            </div>
                            <div>
                                <span className='en-weather-today-title-text'>ჰაერის ხარისხის ინდექსი: {AQI && AQI}</span>
                            </div>

                        </IonCol>

                        <IonCol size='12'>
                            {accordionRender()}
                        </IonCol>
                    </IonRow>
                </IonGrid>

                <IonGrid className='en-weather-part2'>

                    <IonRow>



                        <IonCol size='12'>

                            <Swiper
                                noSwiping={true}
                                noSwipingClass="no-swipe"
                                className="no-swipe"
                                spaceBetween={30}  modules={[Navigation]}
                                navigation={{
                                    nextEl: '.swiper-button-next',
                                    prevEl: '.swiper-button-prev',
                                }}
                                onSlideChange={(swiperCore) => {
                                    const {
                                        activeIndex,
                                        previousIndex,
                                        realIndex,
                                    } = swiperCore;
                                    tabChange(tabTitle);
                                }}
                            >
                                {weekDaysArr && weekDaysArr.map((day,index)=>{
                                    return(
                                        <SwiperSlide key={index} className='en-weather-slider-days'>
                                            <IonRow>

                                                <IonCol size='5' className='en-weather-slider-days-title'>
                                                    <h6 >{day}</h6>
                                                    {weekDayTime && weekDayTime.map((time,index3)=>{
                                                        if(index == index3){
                                                            return(
                                                                <div key={index3} className='en-weather-slider-days-title-date'>{time.substring(5,10).replace("-","/")}</div>
                                                            )
                                                        }
                                                    })}
                                                </IonCol>

                                                <IonCol size='7' style={{paddingTop:'10px'}} className='en-weather-slider-days-temperatures'>
                                                    {tempMaxArr && tempMaxArr.map((maxtemp,index1)=>{
                                                        if(index == index1){
                                                            return (
                                                                <div key={index1} className='ion-float-right'>
                                                                    <IonImg src={`../assets/weathericons/icon${iconArr[(counter+1)*2] == null ? iconArr[counter+1] : iconArr[(counter+1)*2]   }.png`} style={{marginRight:"10px"}} className='ion-float-left en-weather-slider-days-temperatures-img' />
                                                                    <span className='en-weather-slider-days-high-temperatures'><span style={{fontSize: '25px'}}>{maxtemp}</span>&#8451;</span>
                                                                    /
                                                                    {tempMinArr && tempMinArr.map((mintemp,index2)=>{
                                                                        if(index == index1 && index1 == index2){
                                                                            return (
                                                                                <span key={index2} className='en-weather-slider-days-temperatures'>{mintemp}&#8451;</span>
                                                                            )
                                                                        }
                                                                    })}
                                                                </div>
                                                            )}
                                                    })}
                                                </IonCol>

                                            </IonRow>
                                        </SwiperSlide>
                                    )

                                })}

                            </Swiper>
                            <div onTouchStart={()=>{setCounter(counter - 1) }} className="swiper-button-prev"></div>
                            <div onTouchStart={()=>{setCounter(counter + 1)}}  className="swiper-button-next"></div>
                        </IonCol>

                        {/* <IonCol size='12' className='en-weather-days-description'>
                {!loading && descTextFunc()}
            </IonCol> */}



                    </IonRow>

                </IonGrid>
                <IonGrid className='en-weather-days-precipitation ion-no-padding'>
                    <IonRow>
                        <IonCol size='12' >
                            <IonToolbar>
                                <IonSegment value={tabTitle} onIonChange={e => {

                                    tabChange(e.detail.value);
                                    setTabTitle(e.detail.value!)
                                }}>
                                    <IonSegmentButton value="ნალექი" className='en-weather-days-precipitation-segment'><IonIcon icon={rainyOutline} className='en-weather-days-precipitation-segment-icon'/></IonSegmentButton>
                                    <IonSegmentButton value="ქარი" style={{ backgroundImage: 'url("../assets/weathericons/icon24.png")'}} className='weather-icon en-weather-days-precipitation-segment'></IonSegmentButton>
                                    <IonSegmentButton value="UVI  ულტრაიისფერი გამოსხივების ინდექსი " className='en-weather-days-precipitation-segment'><IonIcon icon={partlySunnyOutline} className='en-weather-days-precipitation-segment-icon'/></IonSegmentButton>
                                </IonSegment>
                            </IonToolbar>
                        </IonCol>

                        <IonCol size='12'>
                            <h5 className='en-weather-days-precipitation-title'>{tabTitle}</h5>
                        </IonCol>

                        <IonCol size='12'>
                            <div  style={tempArr.length > 0 ? {display:"block"} : {display:"none"}} className="chartWrapper">
                                <div className="chartAreaWrapper">
                                    <canvas id="myChart" ></canvas>
                                </div>
                            </div>

                        </IonCol>
                    </IonRow>

                </IonGrid>
                {bottomSection()}

                <IonCol>
                    <div className="en-weather-copernicus-div">
                        <IonImg style={{height:"50px"}} src="../assets/images/COPERNICUS.jpg" />
                    </div>
                </IonCol>
            </IonContent>
        </IonPage>
    );
};

export default WeatherPage;
