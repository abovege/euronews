import { IonBackButton, IonCol, IonContent, IonGrid, IonHeader, IonPage, IonRow, IonTitle } from '@ionic/react';
import { chevronBackOutline } from 'ionicons/icons';
import './AboutEuronews.css';
import React from "react";

interface AboutEuronewsProps {
  keywords?: any;
}


const AboutEuronews: React.FC<AboutEuronewsProps> = ({keywords} : AboutEuronewsProps) => {
  return (
    <IonPage>
      <IonHeader className="ion-no-border en-euronews-header">
        <IonBackButton icon={chevronBackOutline} className='ion-float-left ion-no-padding en-settings-back-button'/>
        <IonTitle className='en-euronews-title'>{keywords.ABOUT_EURONEWS_GEORGIA}</IonTitle>
      </IonHeader>
      <IonContent >

        <IonGrid>
          <IonRow>

            <IonCol size='12' className=''>
              <p>
              „ევრონიუს ჯორჯია“ ევროპული “ევრონიუსის” პარტნიორია. ეს გახლავთ ქართულენოვანი მაყურებლისათვის განკუთვნილი საინფორმაციო სატელევიზიო არხი და ციფრული პლატფორმა.
              </p>
            </IonCol>

            <IonCol size='12'>
              <p>
              „ევრონიუსი“ ცნობილი ბრენდია და განსაკუთრებული რეპუტაციით სარგებლობს. მისი მაუწყებლობა დამოუკიდებელ და მიუკერძოებელ ჟურნალისტიკასთან ასოცირდება და ეს „ევრონიუს ჯორჯიაზეც“ ვრცელდება.
              </p>
            </IonCol>

            <IonCol size='12' >
              <p>
              „ევრონიუსი“, მისი ჟურნალისტების შექმნილ სიუჟეტებს უზიარებს „ევრონიუს საქართველოს“ და მას ქართული ამბებიც ემატება. მეორე  მხრივ, „ევრონიუსის“ ეთერში რეგულარულად გადის ქართველი პარტნიორების მიერ მომზადებული სიჟეტები, რომლებიც საქართველოს ცხოვრებას ასახავს. 
              </p>
            </IonCol>


            <IonCol size='12' >
             <p>
             “ევრონიუსი“ ევროპის ყველაზე რეიტინგული ახალი ამბების არხია და ერთ-ერთი ყველაზე ცნობილი და სანდო ბრენდი მთელ კონტინენტზე.
             </p>
            </IonCol>
            <IonCol size='12' >
             <p>
             „ევრონიუსის“ მიზანია, განსხვავებული მოსაზრებები მიუკერძოებლად მიაწოდოს აუდიტორიას და მისი პარტნიორი, „ევრონიუს ჯორჯიაც“ ამავე ღირებულებებს ეფუძნება – სარედაქციო დამოუკიდებლობასა და მიუკერძოებლობას.             </p>
            </IonCol>
            <IonCol size='12' >
             <p>
             შპს “სილქ მედიასა” და ფრანგულ სამაუწყებლო კომპანია “ევრონიუსს” შორის პარტნიორული ხელშეკრულება 2019 წლის 25 ივლისს გაფორმდა.             </p>
            </IonCol>

            <IonCol size='12' >
             <p>

             სამაუწყებლო კომპანიას ავტორიზაცია გავლილი აქვს საქართველოს კომუნიკაციების ეროვნულ კომისიაში და აქვს როგორც საკაბელო ასევე ციფრული მიწისზედა ღია საეთერო ქსელში მაუწყებლობის უფლება.
             </p>
            </IonCol>

          </IonRow>
        </IonGrid>
        
      </IonContent>
    </IonPage>
  );
};

export default AboutEuronews;
