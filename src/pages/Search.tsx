import { IonBackButton, IonContent, IonHeader, IonPage, IonSearchbar, IonSpinner, IonTitle } from '@ionic/react';
import { chevronBackOutline } from 'ionicons/icons';
import React, { useEffect, useState, useContext } from 'react';
import './Search.css';
import SearchNoResult from '../components/SearchNoResult'
import {darkThemeContext} from '../state/State'
import SmallStory from '../components/SmallStory';
import {offlineModeContext} from '../state/State'
import OfflineMode from "../components/OfflineMode"
import axios from "axios";


interface SearchProps {
  keywords?: any;
}

const Search: React.FC<SearchProps> = ({keywords} : SearchProps) => {
  const [searchText, setSearchText] = useState('');
  const [searchArray,setSearchArray] = useState([]);
  const [darkTheme] = useContext<any>(darkThemeContext);
  const [offlineMode] = useContext<any>(offlineModeContext);
  const [isLoading,setIsLoading] = useState(false);
  const [noResult, setNoResult] = useState(false);


  useEffect(() => {

  }, [])
  const fetchSearchData = async (searchTxt:any) => {
    setIsLoading(true)
    let isMounted = true;               
    const body =    {
                    orderby : "ID", 
                    order: "DESC", 
                    post_type : "post",
                    post_count : "50", 
                    page : "-1",
                    qs: searchTxt,
                    cache_key: "string cache key that get from end of the last response",
                  };
      axios.post('https://euronewsgeorgia.com/wp-json/above_rest_api/v1/fetch/posts/', body)
          .then(response => {
            if(response.data.posts === undefined) {return}
            response.data.posts.length == 0 && setNoResult(true);
            setIsLoading(false)
            setSearchArray(response.data.posts)
          })
          .catch(error => {
              console.error('There was an error!', error);
          });
          return () => { isMounted = false };
  
  }

  const handleKeyDown = (event:any) => {
    if (event.key === 'Enter') {
      // setSearchArray([]);
      fetchSearchData(searchText)
      setNoResult(false);
    }
  }


  
  
  
  return (
    <IonPage>

      <IonHeader className="ion-no-border en-notification-header ion-no-padding">
        <IonBackButton icon={chevronBackOutline} className='ion-float-left ion-no-padding en-settings-back-button'/>
          <IonTitle className='en-readitlater-title ion-no-padding'>{keywords.SEARCH}</IonTitle>
      </IonHeader>
  
      <IonContent fullscreen>

        <IonSearchbar 
          onKeyDown={handleKeyDown}
          // style={darkTheme && '{color:"black"}'}
          // style={{color:"black"}}
          value={searchText} 
          onIonChange={e => {
              setSearchText(e.detail.value!);
            }
            } 
          placeholder={keywords.KEYWORD} 
          className='en-search-input'/>
        {offlineMode ? (
        <OfflineMode/>
        ):(
          <div>
            <div style={{
            display:"flex",
            justifyContent: 'center',
            alignItems: 'center'
          }}>
          <IonSpinner style={{display: isLoading ? "block" : "none"}} name="dots" />
            </div>
          {searchArray.length > 0 && 
          searchArray.map((data: any,index: any)=>(
          <SmallStory key={index} data={data} ID={data.ID}/>
          ))
          }
          {noResult && <SearchNoResult/> }
          </div>
        )}


      </IonContent>
    </IonPage>
  );
};

export default Search;

