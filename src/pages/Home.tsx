import {
    IonSpinner,
    IonButton,
    IonPopover,
    IonSkeletonText,
    IonRippleEffect,
    IonRefresher,
    IonRefresherContent,
    IonButtons,
    IonCol,
    IonContent,
    IonGrid,
    IonIcon,
    IonImg,
    IonModal,
    IonPage,
    IonRouterLink,
    IonRow,
    IonSearchbar,
    IonSegment,
    IonSegmentButton,
    IonText,
    IonTitle,
    IonAlert
} from '@ionic/react';
import './Home.css';
import Header from '../components/Header';
import SmallStory from '../components/SmallStory';
import ModalVideo from '../components/ModalVideo';
import FeaturedStory from '../components/FeaturedStory';
import LatestNewsStory from '../components/LatestNewsStory';
import BigStory from '../components/BigStory';
import VideoContent from '../components/VideoContent'
import React, {useEffect, useState, useContext} from 'react';
import {caretForwardCircleOutline, chevronForwardOutline, closeOutline} from 'ionicons/icons';
import {Link, useHistory, useLocation} from 'react-router-dom';
import axios from "axios";
import {RefresherEventDetail} from '@ionic/core';
import {Storage} from '@ionic/storage';
import {Network} from '@capacitor/network';
import FadeIn from 'react-fade-in';
import OfflineIcon from '../components/OfflineIcon';
import {
    offlineModeContext,
    articleAPContext,
    videoAPContext,
    shouldVideoPlayContext,
    darkThemeContext
} from '../state/State'
import {useIdleTimer} from "react-idle-timer";
import ReactPlayer from "react-player";
import Ad from "../components/Ad";
import {Timeline, Tweet} from 'react-twitter-widgets'


function doRefresh(event: CustomEvent<RefresherEventDetail>) {

    setTimeout(() => {
        console.log('Async operation has ended');
        event.detail.complete();
    }, 2000);
}


interface HomeProps {
    keywords?: any;
    articleAP?: string;
    videoAP?: string;
    hotTopics?: any;
    featuredSection?: any;
    noCommentContent?: any;
    setRefreshPage?: any;
    refreshPage?: any;
    ads?: any;

}

const Home: React.FC<HomeProps> = ({
                                       keywords,
                                       noCommentContent,
                                       featuredSection,
                                       hotTopics,
                                       setRefreshPage,
                                       refreshPage,
                                       ads
                                   }: HomeProps) => {

    const [showModal, setShowModal] = useState(false);
    const [showGuestModal, setShowGuestModal] = useState(false);
    const [showCubeModal, setShowCubeModal] = useState(false);

    const [searchText, setSearchText] = useState('');
    const [cacheTime, setCacheTime] = useState(1000);
    const [fetchTimer, setFetchTimer] = useState(0);

    const [offlineMode, setofflineMode] = useContext<any>(offlineModeContext)
    const [selectedArticleAP, setSelectedArticleAP] = useContext<any>(articleAPContext);
    const [selectedVideoeAP, setSelectedVideoAP] = useContext<any>(videoAPContext);
    const [shouldVideoPlay, setShouldVideoPlay] = useContext<any>(shouldVideoPlayContext);
    const [darkTheme] = useContext<any>(darkThemeContext);

    const [showConnectionAlert, setShowConnectionAlert] = useState<boolean>(false);


    const [defaultAd, setDefaultAd] = useState<any>();
    const [ad1, setAd1] = useState<any>();
    const [ad2, setAd2] = useState<any>();
    const [ad3, setAd3] = useState<any>();

    const CloseModal = () => setShowModal(false)
    const OpenModal = () => setShowModal(true)
    const OpenGuestModal = () => setShowGuestModal(true)
    const OpenCubeModal = () => setShowCubeModal(true)

    const isImage = ['gif', 'jpg', 'jpeg', 'png']; //you can add more
    const isVideo = ['mpg', 'mp2', 'mpeg', 'mpe', 'mpv', 'mp4'] // you can add more extention


    let history = useHistory();

    const onIdle = () => {
        // Close Modal Prompt
        // Do some idle action like log out your user
        console.log('we are idle now');
    }

    const onActive = (event: any) => {
        // Close Modal Prompt
        // Do some active action
        console.log('we are back online now');
        history.push('/');
    }
    //This will transfer the user to homepage if he is idle for 2 min
    const idleTimer = useIdleTimer({onIdle, onActive, timeout: 1000 * 60 * 2})


    var fadeElements = document.getElementsByClassName("scrollFade");

    function scrollFade() {
        var viewportBottom = window.scrollY + window.innerHeight;
        for (var index = 0; index < fadeElements.length; index++) {
            var element = fadeElements[index];
            var rect = element.getBoundingClientRect();
            var elementFourth = rect.height;
            var fadeInPoint = window.innerHeight - elementFourth;
            var fadeOutPoint = rect.height / 20;
            if (rect.top <= fadeInPoint) {
                element.classList.add("scrollFade--visible");
                element.classList.add("scrollFade--animate");
                element.classList.remove("scrollFade--hidden");
            } else {
                element.classList.remove("scrollFade--visible");
                element.classList.add("scrollFade--hidden");
            }

            if (rect.top <= fadeOutPoint) {
                element.classList.remove("scrollFade--visible");
                element.classList.add("scrollFade--hidden");
            }
        }
    }


    useEffect(() => {
        if (ad1) {
            // console.log("ads are: ", ad1, ad2, ad3);
        }
    }, [ad1, ad2, ad3]);

    useEffect(() => {
        const populateAds = async () => {
            const store = new Storage();
            await store.create();
            // let ads = await store.get("ads");
            // console.log("ads: ", ads);
            ads?.map((ad: any, key: any) => {
                if (ad.cat_id?.toLowerCase() == "homepage_ad1") {
                    setAd1(ad);
                } else if (ad.cat_id?.toLowerCase() == "homepage_ad2") {
                    setAd2(ad);
                } else if (ad.cat_id?.toLowerCase() == "homepage_ad3") {
                    setAd3(ad);
                } else if (ad.cat_id?.toLowerCase() == "" || ad.cat_id?.toLowerCase() == "default") {
                    setDefaultAd(ad);

                }
            });
        }
        if (ads)
            populateAds();
    }, [ads]);

    useEffect(() => {
        let isMounted = true;
        if (isMounted) {
            setShouldVideoPlay(false);
            const store = new Storage();
            const setSettingModes = async () => {
                await store.create();
                // setofflineMode(await store.get("offlineMode"))
                setSelectedArticleAP(await store.get('articleAP'));
                setSelectedVideoAP(await store.get('videoAP'));
            }
            setSettingModes();
            fetchBigStoryContent();
            fetchOtherStories();
            fetchVideoModalContent();
            fetchFeaturedContentCat();
            fetchBelowFeaturedContentCat();
            fetchCube();
            fetchGuest();
            fetchLatestNews();


            console.log('checking for network now...');
            Network.addListener('networkStatusChange', async (status) => {
                console.log('Network status changed', status);
                const store = new Storage();
                await store.create();
                let $latestnews = await store.get("latestNews");

                if (!status.connected && !$latestnews) {
                    console.log('there is no internet and no offline data showConnectionAlert is', showConnectionAlert);
                    setShowConnectionAlert(true);
                }
            });

            const getLatestNewsFromIndexedDB = async () => {
                const store = new Storage();
                await store.create();
                let $latestnews = await store.get("latestNews");
                console.log('checking for latest news now...');
                console.log('$latestnews', $latestnews);

                if (!navigator.onLine && !$latestnews) {
                    setShowConnectionAlert(true);
                }
            }

            getLatestNewsFromIndexedDB();


        }

        return () => {
            isMounted = false
        };

    }, [refreshPage])

    // useEffect(() => {
    //     console.log('showConnectionAlert is: ', showConnectionAlert)
    //     // if(showConnectionAlert) {
    //     //     window.location.reload();
    //     // }
    // },[showConnectionAlert])


    const [otherStories, setOtherStories] = useState<any[]>([]);
    const [featuredContent, setFeaturedContent] = useState<any[]>([]);
    const [latestNews, setLatestNews] = useState<any[]>([]);
    const [videoModalContent, setVideoModalContent] = useState<any[]>([]);
    const [bigStoryContent, setBigStoryContent] = useState<any[]>([]);
    const [uncoveringEurope, setUncoveringEurope] = useState<any[]>([]);


// const fetchAllData = async () => {
//   const store = new Storage();
//   await store.create();
//   const fetchBigStory = axios.post('https://euronewsgeorgia.com/wp-json/above_rest_api/v1/fetch/posts/',{
//     orderby : "post_date", 
//     order : "DESC", 
//     post_type: "post", 
//     post_count : "1", 
//     page : "1", 
//     taxonomy : "category",
//     taxonomy_field  : "name",
//     taxonomy_values   : [  "მთავარი ამბავი" ],
//     cache_key : "string cache key that get from end of the last response"
//   });
//   const fetchOtherStories = axios.post('https://euronewsgeorgia.com/wp-json/above_rest_api/v1/fetch/posts/',{
//     orderby : "post_date", 
//     order : "DESC", 
//     post_type: "post", 
//     post_count : "12", 
//     page : "1", 
//     taxonomy : "category",
//     taxonomy_field  : "name",
//     taxonomy_values   : [  "ამბები" ],
//     cache_key : "string cache key that get from end of the last response"
//   });
//   const fetchHotTopics = axios.post('https://euronewsgeorgia.com/wp-json/above_rest_api/v1/fetch/posts/',{
//     orderby : "post_date", 
//     order: "DESC", 
//     post_type : "ara_options",
//     post_count : "-1", 
//     page : "1",
//     cache_key: "string cache key that get from end of the last response",
//     post_title : "tags_section"
//   });
//   const fetchLatestNews = axios.post('https://euronewsgeorgia.com/wp-json/above_rest_api/v1/fetch/posts/',{
//     orderby : "post_date", 
//     order : "DESC", 
//     post_type: "post", 
//     post_count : "12", 
//     page : "1", 
//     taxonomy : "category",
//     taxonomy_field  : "name",
//     taxonomy_values   : [  "საქართველო" ],
//     cache_key : "string cache key that get from end of the last response"
//   });
//   const fetchVideoContent = axios.post('https://euronewsgeorgia.com/wp-json/above_rest_api/v1/fetch/posts/',{
//     orderby : "post_date", 
//     order : "DESC", 
//     post_type: "post", 
//     post_count : "4", 
//     page : "1", 
//     taxonomy : "category",
//     taxonomy_field  : "name",
//     taxonomy_values   : [  "ვიდეო" ],
//     cache_key : "string cache key that get from end of the last response"
//   })
//   const fetchVideoModalContent = axios.post('https://euronewsgeorgia.com/wp-json/above_rest_api/v1/fetch/posts/',{
//     orderby : "post_date", 
//     order : "DESC", 
//     post_type: "post", 
//     post_count : "30", 
//     page : "1", 
//     taxonomy : "category",
//     taxonomy_field  : "name",
//     taxonomy_values   : [  "ვიდეო" ],
//     cache_key : "string cache key that get from end of the last response"
//   })


//   axios.all([ fetchLatestNews ]).then(axios.spread((...responses) => {
//     // fetchBigStory,
//     // const responseOne = responses[0].data.posts
//     // setBigStoryContent(responseOne); 
//     // store.set("bigStoryContent",responseOne);

//     // fetchOtherStories, 
//     // const responseTwo = responses[1].data.posts
//     // setOtherStories(responseTwo); 
//     // store.set("otherStories",responseTwo);

//     // fetchHotTopics, 
//     // const responesThree = responses[2].data.posts[0].post_content
//     // setHotTopics(JSON.parse(responesThree)); 
//     // store.set("hotTopics",JSON.parse(responesThree));

//     const responseThree = responses[0].data.posts
//     setLatestNews(responseThree);  
//     store.set("latestNews",responseThree);

//     // fetchVideoContent,
//     // const responseFour = responses[4].data.posts
//     // setVideoContent(responseFour);  
//     // store.set("videoContent",responseFour);

//     // fetchVideoModalContent
//     // const responseFive = responses[5].data.posts
//     // setVideoModalContent(responseFive);
//     // store.set("videoModalContent",responseFive);


//   })).catch(errors => {
//     console.log(errors)
//   })
// }

    const fetchLatestNews = async () => {
        let isMounted = true;
        const store = new Storage();
        await store.create();
        const body = {
            orderby: "post_date",
            order: "DESC",
            post_type: "post",
            post_count: "12",
            page: "1",
            taxonomy: "category",
            taxonomy_field: "name",
            taxonomy_values: ["საქართველო"],
            cache_key: "string cache key that get from end of the last response"
        };
        axios.post('https://euronewsgeorgia.com/wp-json/above_rest_api/v1/fetch/posts/', body,)
            .then(response => {
                setLatestNews(response.data.posts);
                store.set("latestNews", response.data.posts);
            })
            .catch(error => {
                console.error('There was an error!', error);
            });
        setLatestNews(await store.get("latestNews"));
        return () => {
            isMounted = false
        };
    }


    const [featuredContentCat, setFeaturedContentCat] = useState<any>("");

    const fetchFeaturedContentCat = async () => {
        let isMounted = true;
        const store = new Storage();
        await store.create();
        const body = {
            orderby: "post_date",
            order: "DESC",
            post_type: "ara_options",
            post_count: "10",
            page: "1",
            cache_key: "string cache key that get from end of the last response",
            post_title: "featured_content"
        };
        axios.post('https://euronewsgeorgia.com/wp-json/above_rest_api/v1/fetch/posts/', body)
            .then(response => {
                setFeaturedContentCat(JSON.stringify(response.data.posts[0].post_content).replace(/[^0-9a-z-A-Z-ა-ჰ ]/g, "").replace(/ +/, " "));
                store.set("featuredContentCat", JSON.stringify(response.data.posts[0].post_content).replace(/[^0-9a-z-A-Z-ა-ჰ ]/g, "").replace(/ +/, " "));
                fetchFeaturedContent(JSON.stringify(response.data.posts[0].post_content).replace(/[^0-9a-z-A-Z-ა-ჰ ]/g, "").replace(/ +/, " "))
            })
            .catch(error => {
                console.error('There was an error!', error);
            });
        setFeaturedContentCat(await store.get("featuredContentCat"))
        fetchFeaturedContent(await store.get("featuredContentCat"))
        return () => {
            isMounted = false
        };

    }
    const fetchFeaturedContent = async (featuredContentCat: any) => {
        console.log(featuredContentCat)
        let isMounted = true;
        const store = new Storage();
        await store.create();
        const body = {
            orderby: "post_date",
            order: "DESC",
            post_type: "post",
            post_count: "3",
            page: "1",
            taxonomy: "category",
            taxonomy_field: "name",
            taxonomy_values: [featuredContentCat],
            cache_key: "string cache key that get from end of the last response"
        };
        axios.post('https://euronewsgeorgia.com/wp-json/above_rest_api/v1/fetch/posts/', body,)
            .then(response => {
                setFeaturedContent(response.data.posts)
                store.set("featuredContent", response.data.posts);
            })
            .catch(error => {
                console.error('There was an error!', error);
            });
        setFeaturedContent(await store.get("featuredContent"));
        return () => {
            isMounted = false
        };
    }

    const [belowFeaturedContentCat, setBelowFeaturedContentCat] = useState<any>("");

    const fetchBelowFeaturedContentCat = async () => {
        let isMounted = true;
        const store = new Storage();
        await store.create();
        const body = {
            orderby: "post_date",
            order: "DESC",
            post_type: "ara_options",
            post_count: "-1",
            page: "1",
            cache_key: "string cache key that get from end of the last response",
            post_title: "below_featured_content"
        };
        axios.post('https://euronewsgeorgia.com/wp-json/above_rest_api/v1/fetch/posts/', body)
            .then(response => {
                setBelowFeaturedContentCat(JSON.stringify(response.data.posts[0].post_content).replace(/[^0-9a-z-A-Z-ა-ჰ ]/g, "").replace(/ +/, " "));
                store.set("belowFeaturedContentCat", JSON.stringify(response.data.posts[0].post_content).replace(/[^0-9a-z-A-Z-ა-ჰ ]/g, "").replace(/ +/, " "));
                fetchBelowFeaturedContent(JSON.stringify(response.data.posts[0].post_content).replace(/[^0-9a-z-A-Z-ა-ჰ ]/g, "").replace(/ +/, " "))
            })
            .catch(error => {
                console.error('There was an error!', error);
            });
        setBelowFeaturedContentCat(await store.get("belowFeaturedContentCat"))
        fetchBelowFeaturedContent(await store.get("belowFeaturedContentCat"))
        return () => {
            isMounted = false
        };
    }
    const fetchBelowFeaturedContent = async (belowFeaturedContentCat: any) => {
        console.log(featuredContentCat)
        let isMounted = true;
        const store = new Storage();
        await store.create();
        const body = {
            orderby: "post_date",
            order: "DESC",
            post_type: "post",
            post_count: "4",
            page: "1",
            taxonomy: "category",
            taxonomy_field: "name",
            taxonomy_values: [belowFeaturedContentCat],
            cache_key: "string cache key that get from end of the last response"
        };
        axios.post('https://euronewsgeorgia.com/wp-json/above_rest_api/v1/fetch/posts/', body)
            .then(response => {
                setUncoveringEurope(response.data.posts)
                store.set("uncoveringEurope", response.data.posts);
            })
            .catch(error => {
                console.error('There was an error!', error);
            });
        setUncoveringEurope(await store.get("uncoveringEurope"))
        return () => {
            isMounted = false
        };
    }


    const fetchBigStoryContent = async () => {
        let isMounted = true;
        const store = new Storage();
        await store.create();
        const body = {
            orderby: "post_date",
            order: "DESC",
            post_type: "post",
            post_count: "1",
            page: "0",
            taxonomy: "category",
            taxonomy_field: "name",
            taxonomy_values: ["მთავარი ამბავი"],
            cache_key: "string cache key that get from end of the last response"
        };
        axios.post('https://euronewsgeorgia.com/wp-json/above_rest_api/v1/fetch/posts/', body)
            .then(response => {
                setBigStoryContent(response.data.posts);
                store.set("bigStoryContent", response.data.posts);
            })
            .catch(error => {
                console.error('There was an error!', error);
            });
        setBigStoryContent(await store.get("bigStoryContent"));
        return () => {
            isMounted = false
        };
    }


    const fetchVideoModalContent = async () => {
        let isMounted = true;
        const store = new Storage();
        await store.create();
        const body = {
            orderby: "post_date",
            order: "DESC",
            post_type: "post",
            post_count: "30",
            page: "1",
            taxonomy: "category",
            taxonomy_field: "name",
            taxonomy_values: ["ვიდეო"],
            cache_key: "string cache key that get from end of the last response"
        };
        axios.post('https://euronewsgeorgia.com/wp-json/above_rest_api/v1/fetch/posts/', body)
            .then(response => {
                setVideoModalContent(response.data.posts);
                store.set("videoModalContent", response.data.posts);

            })
            .catch(error => {
                console.error('There was an error!', error);
            });
        // setVideoModalContent(await store.get("videoModalContent")); 
        return () => {
            isMounted = false
        };
    }

    const fetchOtherStories = async () => {
        let isMounted = true;
        const store = new Storage();
        await store.create();
        const body = {
            orderby: "post_date",
            order: "DESC",
            post_type: "post",
            post_count: "12",
            page: "1",
            taxonomy: "category",
            taxonomy_field: "name",
            taxonomy_values: ["ამბები"],
            cache_key: "string cache key that get from end of the last response"
        };
        axios.post('https://euronewsgeorgia.com/wp-json/above_rest_api/v1/fetch/posts/', body)
            .then(response => {
                setOtherStories(response.data.posts);
                store.set("otherStories", response.data.posts);
            })
            .catch(error => {
                console.error('There was an error!', error);
            });
        setOtherStories(await store.get("otherStories"));
        return () => {
            isMounted = false
        };
    }

    const [cube, setCube] = useState<any[]>([]);
    const fetchCube = async () => {
        let isMounted = true;
        const store = new Storage();
        await store.create();
        const body = {
            orderby: "post_date",
            order: "DESC",
            post_type: "post",
            post_count: "30",
            page: "1",
            taxonomy: "category",
            taxonomy_field: "name",
            taxonomy_values: ["კუბი"],
            cache_key: "string cache key that get from end of the last response"
        };
        axios.post('https://euronewsgeorgia.com/wp-json/above_rest_api/v1/fetch/posts/', body)
            .then(response => {
                setCube(response.data.posts);
                store.set("cube", response.data.posts);
            })
            .catch(error => {
                console.error('There was an error!', error);
            });
        setCube(await store.get("cube"));
        return () => {
            isMounted = false
        };
    }

    const [guest, setGuest] = useState<any[]>([]);
    const fetchGuest = async () => {
        let isMounted = true;
        const store = new Storage();
        await store.create();
        const body = {
            orderby: "post_date",
            order: "DESC",
            post_type: "post",
            post_count: "30",
            page: "1",
            taxonomy: "category",
            taxonomy_field: "name",
            taxonomy_values: ["ევრონიუს ჯორჯიას სტუმარი"],
            cache_key: "string cache key that get from end of the last response"
        };
        axios.post('https://euronewsgeorgia.com/wp-json/above_rest_api/v1/fetch/posts/', body)
            .then(response => {
                setGuest(response.data.posts);
                store.set("guest", response.data.posts);
            })
            .catch(error => {
                console.error('There was an error!', error);
            });
        setGuest(await store.get("guest"));
        return () => {
            isMounted = false
        };
    }

    const checkInternet = () => {
        // console.log(navigator.onLine);
        if (navigator.onLine) {
            setShowConnectionAlert(false);
            setRefreshPage(true);
        } else {
            setShowConnectionAlert(true);
        }
        return false;
    };

    const formatDate = (input: any) => {
        var datePart = input.match(/\d+/g),
            year = datePart[0].substring(2, 4), // get only two digits
            month = datePart[1], day = datePart[2];

        return day + '/' + month + '/' + year;
    }

    return (
        <IonPage>

            <IonAlert
                isOpen={showConnectionAlert}
                // onDidDismiss={() => setShowAlert(false)}
                header="No Internet Connection Detected!"
                // subHeader="No Internet"
                backdropDismiss={false}
                message="This application needs internet connection, please check your internet connection and Retry again"
                buttons={[{
                    text: "Retry",
                    handler: () => {
                        checkInternet();
                        // setRefreshPage(true);
                        // window.location.reload();
                        return false;
                    },
                },]}
            />


            {bigStoryContent && bigStoryContent.length > 0 ? (
                <>
                    <FadeIn>
                        <Header/>
                    </FadeIn>
                    <IonContent class='en-home-content'>
                        <IonRefresher id="tesst" slot="fixed" onIonRefresh={doRefresh}>
                            <IonRefresherContent refreshingSpinner="bubbles"></IonRefresherContent>
                        </IonRefresher>


                        <FadeIn>
                            {bigStoryContent && bigStoryContent.map((data, key) => (
                                <BigStory keywords={keywords} key={key} data={data} ID={data.ID}/>))}
                        </FadeIn>

                        <IonGrid className='ion-no-padding'>
                            <FadeIn>

                                {otherStories && otherStories.map((data, key) => {
                                    return (
                                        <div key={key}>
                                            {key == 5 ?
                                                <div style={{marginBottom: "10px"}}>
                                                    <Ad adSection={ad1} keywords={keywords}/>
                                                </div>
                                                : ""}
                                            <div key={key}>
                                                <SmallStory data={data} ID={data.ID}/>
                                            </div>
                                        </div>

                                    )
                                })}

                            </FadeIn>
                        </IonGrid>

                        <IonGrid className='en-home-featured-content'
                                 style={darkTheme ? {background: "#000000"} : {background: "#235383"}}>
                            <IonRow className='ion-no-padding'>
                                <IonCol className='ion-no-padding' size='12'>
                                    <IonRouterLink color="dark" routerLink={`/insidepages/${belowFeaturedContentCat}`}>
                                        <IonTitle className='en-home-featured-content-title'>
                                            ყველაზე პოპულარული
                                            <IonIcon icon={chevronForwardOutline}
                                                     className='ion-float-right en-home-icon'/>
                                        </IonTitle>
                                    </IonRouterLink>
                                    <IonSegment scrollable>
                                        {uncoveringEurope && uncoveringEurope.map((data, index) => {
                                            return <FeaturedStory key={index} data={data} ID={data.ID}/>
                                        })}
                                    </IonSegment>
                                </IonCol>
                            </IonRow>
                        </IonGrid>

                        <IonGrid className='en-home-latest-video'
                                 style={darkTheme ? {background: "#000000"} : {background: "#081f2d"}}>
                            <IonRow className='ion-no-padding'>
                                <IonCol className='ion-no-padding'>
                                    <IonRouterLink color="dark"
                                                   routerLink="/listpagewithmodal/ევრონიუს ჯორჯიას სტუმარი">
                                        <IonTitle className='en-home-latest-video-title'>
                                            {/* {keywords.NO_COMMENT} */}
                                            ევრონიუს ჯორჯიას სტუმარი
                                            <IonIcon icon={chevronForwardOutline}
                                                     className='ion-float-right en-home-icon'/>
                                        </IonTitle>
                                    </IonRouterLink>
                                    <IonSegment scrollable>

                                        {guest && guest.length > 0 ?
                                            <>
                                                {guest.map((data: any, index: any) => {
                                                    if (index < 4) {
                                                        return (
                                                            <VideoContent key={index} data={data} index={index}
                                                                          showModal={OpenGuestModal}/>
                                                        )
                                                    }
                                                })}

                                                <IonSegmentButton className='en-home-latest-video-view-more'
                                                                  onTouchStart={() => history.push("/listpagewithmodal/ევრონიუს ჯორჯიას სტუმარი")}>
                                                    <div>
                                                        {keywords.VIEW_MORE}
                                                        <IonIcon icon={chevronForwardOutline}
                                                                 className='ion-float-right en-home-icon'/>
                                                    </div>

                                                </IonSegmentButton>
                                            </>
                                            :
                                            <div style={{width: "100%", display: "flex", justifyContent: "center"}}>
                                                <IonSpinner color={"light"} name="bubbles"/>
                                            </div>
                                        }
                                    </IonSegment>

                                    <IonModal
                                        id="viewport"
                                        isOpen={showGuestModal}
                                        className='en-modal'
                                        swipeToClose={true}
                                    >
                                        <IonContent
                                            scrollEvents={true}
                                            onIonScroll={() => scrollFade()}
                                            className="ionContentModal"
                                        >
                                            <IonText className='ion-no-margin'>
                                                <IonTitle className='en-modal-title ion-no-margin'>
                                                    {keywords.LATEST_VIDEOS}
                                                    <IonButtons onClick={() => setShowGuestModal(false)}
                                                                className='ion-float-right en-modalvideo-close'><IonIcon
                                                        className="en-modalvideo-close-icon"
                                                        icon={closeOutline}/></IonButtons>
                                                </IonTitle>

                                                {guest && guest.map((data: any, key: any) => (
                                                    <ModalVideo CloseModal={() => setShowGuestModal(false)} key={key}
                                                                data={data} ID={data.ID}/>))}
                                            </IonText>
                                        </IonContent>

                                    </IonModal>
                                </IonCol>
                            </IonRow>


                            {/* {guest && guest.map((item,index)=>{
            return (
        <Link className="uncovering-link-title" key={index} style={darkTheme ? {color:"#ffffff"}:{color:"#000000"}}  
                  to={{
                  pathname: `/storypage/${item.ID}`,
                  state: { 
                    data: item,
                    id : item.ID,
                    relatedStory : item.categories[0].name
                },
                }}>
              <IonRow className='ion-no-padding' >
                <IonCol className='ion-no-padding' size='12'>
                  <IonTitle className='en-home-special-thematic-category-title' style={darkTheme ? {color:"#ffffff"}:{color:"#000000"}}>სტუმარი</IonTitle>
                  <IonButtons>
                
                    <ul >
                      <li><IonImg src={item && item.post_meta.essb_cached_image} className='en-home-special-thematic-img'/></li>
                      <li><h3 className='en-home-special-thematic-title' style={darkTheme ? {color:"#ffffff"}:{color:"#000000"}}>
                        {item.post_title}
                        </h3></li>
                      <li><div className='en-home-special-thematic-located'>{item.categories[0].name}</div></li>
                    </ul>
                  

                  </IonButtons>
  
                </IonCol>
              </IonRow>
            </Link>
               )
              })} */}

                        </IonGrid>

                        {/* <IonGrid className='ion-no-padding'>
          <IonRow className='en-home-ad en-home-ad-no-background' style={darkTheme ? {background:"#3a3a3a"}:{background:"#ffffff"}}>
            <IonCol>
              <IonTitle className='en-home-ad-title'>{keywords.ADVERTISEMENT}</IonTitle>
              <IonImg src='../assets/images/home-ads.jpg' className='en-home-ad-img'/>
            </IonCol>
          </IonRow>
        </IonGrid> */}

                        <IonGrid className='en-home-featured-sections ion-no-padding'
                                 style={darkTheme ? {background: "#000000"} : {background: "#f1f5f6"}}>
                            <IonTitle className='en-home-featured-sections-title'
                                      style={darkTheme ? {color: "#ffffff"} : {color: "#000000"}}>{keywords.FEATURED_SECTIONS}</IonTitle>

                            <IonRow className='ion-no-padding'>
                                {featuredSection && featuredSection.map((item: any, index: any) => {
                                    return (
                                        <IonCol className="featured-section" key={index} size='6'>
                                            <IonRouterLink color="dark" routerLink={`/insidepages/${item.cat_id}`}>
                                                {offlineMode ?
                                                    <OfflineIcon/> :
                                                    <IonImg src={item.img}
                                                            className='featured-img en-home-featured-section-travel'/>}
                                            </IonRouterLink>
                                        </IonCol>
                                    )
                                })}
                            </IonRow>
                        </IonGrid>
                        <IonGrid className='en-home-featured-content'
                                 style={darkTheme ? {background: "#000000"} : {background: "#235383"}}>
                            <IonRow className='ion-no-padding'>
                                <IonCol className='ion-no-padding' size='12'>
                                    <IonRouterLink color="dark" routerLink={`/insidepages/${featuredContentCat}`}>
                                        <IonTitle className='en-home-featured-content-title'>
                                            {keywords.FEATURED_CONTENT}
                                            <IonIcon icon={chevronForwardOutline}
                                                     className='ion-float-right en-home-icon'/>
                                        </IonTitle>
                                    </IonRouterLink>
                                    <IonSegment scrollable>
                                        {featuredContent && featuredContent.map((data, index) => {
                                            return <FeaturedStory key={index} data={data} ID={data.ID}/>
                                        })}
                                    </IonSegment>
                                </IonCol>
                            </IonRow>
                        </IonGrid>
                        <IonGrid className='en-home-latest-video'
                                 style={darkTheme ? {background: "#000000"} : {background: "#081f2d"}}>
                            <IonRow className='ion-no-padding'>
                                <IonCol className='ion-no-padding'>
                                    <IonRouterLink color="dark" routerLink="/listpagewithmodal/კუბი">
                                        <IonTitle className='en-home-latest-video-title'>
                                            {/* {keywords.NO_COMMENT} */}
                                            კუბი
                                            <IonIcon icon={chevronForwardOutline}
                                                     className='ion-float-right en-home-icon'/>
                                        </IonTitle>
                                    </IonRouterLink>
                                    <IonSegment scrollable>

                                        {cube && cube.length > 0 ?
                                            <>
                                                {cube.map((data: any, index: any) => {
                                                    if (index < 4) {
                                                        return (
                                                            <VideoContent key={index} data={data} index={index}
                                                                          showModal={OpenCubeModal}/>
                                                        )
                                                    }
                                                })}

                                                <IonSegmentButton className='en-home-latest-video-view-more'
                                                                  onTouchStart={() => history.push("/listpagewithmodal/კუბი")}>
                                                    <div>
                                                        {keywords.VIEW_MORE}
                                                        <IonIcon icon={chevronForwardOutline}
                                                                 className='ion-float-right en-home-icon'/>
                                                    </div>

                                                </IonSegmentButton>
                                            </>
                                            :
                                            <div style={{width: "100%", display: "flex", justifyContent: "center"}}>
                                                <IonSpinner color={"light"} name="bubbles"/>
                                            </div>
                                        }
                                    </IonSegment>

                                    <IonModal
                                        id="viewport"
                                        isOpen={showCubeModal}
                                        className='en-modal'
                                        swipeToClose={true}
                                    >
                                        <IonContent
                                            scrollEvents={true}
                                            onIonScroll={() => scrollFade()}
                                            className="ionContentModal"
                                        >
                                            <IonText className='ion-no-margin'>
                                                <IonTitle className='en-modal-title ion-no-margin'>
                                                    {keywords.LATEST_VIDEOS}
                                                    <IonButtons onClick={() => setShowCubeModal(false)}
                                                                className='ion-float-right en-modalvideo-close'><IonIcon
                                                        className="en-modalvideo-close-icon"
                                                        icon={closeOutline}/></IonButtons>
                                                </IonTitle>

                                                {cube && cube.map((data: any, key: any) => (
                                                    <ModalVideo CloseModal={() => setShowCubeModal(false)} key={key}
                                                                data={data} ID={data.ID}/>))}
                                            </IonText>
                                        </IonContent>

                                    </IonModal>
                                </IonCol>
                            </IonRow>

                        </IonGrid>

                        <Ad adSection={ad2} keywords={keywords}/>

                        <IonGrid className='en-home-latest-video'
                                 style={darkTheme ? {background: "#000000"} : {background: "#081f2d"}}>
                            <IonRow className='ion-no-padding'>
                                <IonCol className='ion-no-padding'>
                                    <IonRouterLink color="dark" routerLink="/listpagewithmodal/NO COMMENT">
                                        <IonTitle className='en-home-latest-video-title'>
                                            {keywords.NO_COMMENT}
                                            <IonIcon icon={chevronForwardOutline}
                                                     className='ion-float-right en-home-icon'/>
                                        </IonTitle>
                                    </IonRouterLink>
                                    <IonSegment scrollable>

                                        {noCommentContent && noCommentContent.length > 0 ?
                                            <>
                                                {noCommentContent.map((data: any, index: any) => {
                                                    if (index < 4) {
                                                        return (
                                                            <VideoContent key={index} data={data} index={index}
                                                                          showModal={OpenModal}/>
                                                        )
                                                    }
                                                })}

                                                <IonSegmentButton className='en-home-latest-video-view-more'
                                                                  onTouchStart={() => history.push("/listpagewithmodal/NO COMMENT")}>
                                                    <div>
                                                        {keywords.VIEW_MORE}
                                                        <IonIcon icon={chevronForwardOutline}
                                                                 className='ion-float-right en-home-icon'/>
                                                    </div>

                                                </IonSegmentButton>
                                            </>
                                            :
                                            <div style={{width: "100%", display: "flex", justifyContent: "center"}}>
                                                <IonSpinner color={"light"} name="bubbles"/>
                                            </div>
                                        }
                                    </IonSegment>

                                    <IonModal
                                        id="viewport"
                                        isOpen={showModal}
                                        className='en-modal'
                                        swipeToClose={true}
                                    >
                                        <IonContent
                                            scrollEvents={true}
                                            onIonScroll={() => scrollFade()}
                                            className="ionContentModal"
                                        >
                                            <IonText className='ion-no-margin'>
                                                <IonTitle className='en-modal-title ion-no-margin'>
                                                    {keywords.LATEST_VIDEOS}
                                                    <IonButtons onClick={() => setShowModal(false)}
                                                                className='ion-float-right en-modalvideo-close'><IonIcon
                                                        className="en-modalvideo-close-icon"
                                                        icon={closeOutline}/></IonButtons>
                                                </IonTitle>

                                                {noCommentContent && noCommentContent.map((data: any, key: any) => (
                                                    <ModalVideo CloseModal={() => setShowModal(false)} key={key}
                                                                data={data} ID={data.ID}/>))}
                                            </IonText>
                                        </IonContent>

                                    </IonModal>
                                </IonCol>
                            </IonRow>

                        </IonGrid>


                        {/*  hot-topic-were-here*/}


                        <IonGrid className='en-home-sponsored ion-no-padding'>
                            <IonTitle className='en-home-sponsored-title'>{keywords.MAIN_NEWS}</IonTitle>

                            <IonRow style={{display: "flex", justifyContent: "center"}} className='ion-no-padding'>
                                {latestNews && latestNews.length > 0 ? latestNews.map((data, key) => (
                                    <div key={key}>
                                        {key == 5 ?
                                            <div style={{marginBottom: "2rem"}}>
                                                <Ad adSection={ad3} keywords={keywords}/>
                                            </div>
                                            : ""}
                                        <LatestNewsStory key={key} data={data} ID={data.ID}/>
                                    </div>
                                )) : <IonSpinner name="bubbles"/>}
                            </IonRow>
                        </IonGrid>


                        <IonGrid className='en-home-hot-topics'
                                 style={darkTheme ? {background: "#1e1e1e"} : {background: "#fff"}}>
                            <IonRow>
                                <IonCol size='12'>
                                    <IonTitle className='en-home-hot-topics-title' style={darkTheme ? {
                                        color: "#ffffff",
                                        display: "flex",
                                        justifyConten: "center"
                                    } : {
                                        color: "#000",
                                        display: "flex",
                                        justifyConten: "center"
                                    }}>{keywords.HOT_TOPICS}</IonTitle>

                                    {hotTopics && hotTopics.length > 0 ? hotTopics.map((item: any, index: any) => {
                                        return (
                                            <Link key={index} className='en-home-hot-topic-category'
                                                  to={`/insidepages/${item.cat_id}`}
                                                  style={darkTheme ? {background: "#ffffff"} : {background: "#2f70e6"}}>
                                                <IonRippleEffect></IonRippleEffect>
                                                <div className='en-home-hot-topic-div'>
                                                    <span
                                                        style={darkTheme ? {color: "#000"} : {color: "#fff"}}>{item.cat_id}</span>
                                                    <IonIcon icon={chevronForwardOutline}
                                                             className='ion-float-right en-home-icon'
                                                             style={darkTheme ? {color: "#000"} : {color: "#fff"}}/>
                                                </div>
                                            </Link>
                                        )
                                    }) : <IonSpinner name="bubbles"/>}
                                </IonCol>
                            </IonRow>
                        </IonGrid>

                        <IonGrid className='en-home-search'>
                            <IonRow>
                                <IonCol>
                                    {/*<IonTitle className='en-home-search-title'>{keywords.SEARCH}</IonTitle>*/}
                                    <IonRouterLink color="dark" routerLink="/search">
                                        <IonSearchbar style={darkTheme ? {color: "black"} : {color: "black"}}
                                                      value={searchText}
                                                      onIonChange={e => setSearchText(e.detail.value!)}
                                                      placeholder={keywords.SEARCH}/>
                                    </IonRouterLink>
                                </IonCol>
                            </IonRow>
                        </IonGrid>

                    </IonContent>
                </>
            ) : (
                <>
                    <IonContent>
                        <Header/>
                        <IonGrid className='ion-no-padding'>
                            <IonRow className='ion-no-padding'>
                                <IonCol style={{
                                    height: "200px",
                                    display: "flex",
                                    alignItems: 'center',
                                    justifyContent: "center",
                                    background: "#e2e2e2"
                                }} className='ion-no-padding'>
                                    <IonSkeletonText className="animate-skeleton-background"
                                                     style={{width: '70%', height: "80px"}}/>
                                </IonCol>
                            </IonRow>
                        </IonGrid>

                        <IonGrid className='en-storypage-content'>
                            <IonSkeletonText animated style={{width: '30%'}}/>

                            <h3 className='en-storypage-title'>
                                <IonSkeletonText animated style={{width: '100%'}}/>
                                <IonSkeletonText animated style={{width: '100%'}}/>
                                <IonSkeletonText animated style={{width: '30%'}}/>

                            </h3>
                            <div className='en-storypage-time'>
                                <IonSkeletonText animated style={{width: '100%'}}/>
                                <br></br>
                            </div>
                            <br></br>
                            <div className='en-storypage-by'><i>
                                <IonSkeletonText animated style={{width: '100%',}}/>
                            </i></div>
                            <IonGrid className='en-storypage-description'>
                                <p>
                                    <IonSkeletonText animated style={{width: '100%'}}/>
                                    <IonSkeletonText animated style={{width: '100%'}}/>
                                    <IonSkeletonText animated style={{width: '100%'}}/>
                                    <IonSkeletonText animated style={{width: '100%'}}/>
                                    <IonSkeletonText animated style={{width: '100%'}}/>
                                    <IonSkeletonText animated style={{width: '100%'}}/>
                                    <IonSkeletonText animated style={{width: '100%'}}/>
                                    <IonSkeletonText animated style={{width: '100%'}}/>
                                    <IonSkeletonText animated style={{width: '100%'}}/>
                                    <IonSkeletonText animated style={{width: '100%'}}/>
                                    <IonSkeletonText animated style={{width: '100%'}}/>
                                    <IonSkeletonText animated style={{width: '100%'}}/>
                                    <IonSkeletonText animated style={{width: '100%'}}/>
                                    <IonSkeletonText animated style={{width: '100%'}}/>
                                </p>
                            </IonGrid>
                        </IonGrid>
                    </IonContent>
                </>
            )}
        </IonPage>

    );
};

export default React.memo(Home);
