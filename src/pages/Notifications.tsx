import {IonToggle,IonLabel, IonBackButton, IonCol, IonContent, IonGrid, IonHeader, IonIcon, IonPage, IonRow, IonText, IonTitle } from '@ionic/react';
import { stopOutline, chevronBackOutline, closeOutline } from 'ionicons/icons';
import { Link } from 'react-router-dom';
import React, { useState,useEffect, useContext } from 'react';
import './Notifications.css';
import { isPlatform } from '@ionic/react';
import {notificationContext} from '../state/State'
import { Storage } from '@ionic/storage';
import { OpenNativeSettings } from '@awesome-cordova-plugins/open-native-settings';


interface NotificationsProps {
  keywords?: any;
}

const Notifications: React.FC<NotificationsProps> = ({keywords} : NotificationsProps) => {
  const [notification,setNotification] = useContext<any>(notificationContext);

  const store = new Storage();

  useEffect(() => {
    inputStateManagement()
  }, [])

  const inputStateManagement = async () => {
    await store.create();
    setNotification(await store.get("notification"));
  }

  const  notificationModeToggler = async (checked: boolean) =>{
    await store.create();
    console.log(checked)

    await store.set('notification', checked);
    setNotification(checked);
  }
  return (
    <IonPage>

      <IonHeader className="ion-no-border en-notification-header ion-no-padding">
        <IonBackButton icon={chevronBackOutline} className='ion-float-left ion-no-padding en-settings-back-button'/>
          <IonTitle className='en-readitlater-title ion-no-padding'>{keywords.NOTIFICATIONS}</IonTitle>
      </IonHeader>
    {isPlatform('ios') ? (
      <IonContent fullscreen>

      <IonGrid>
        <IonRow>
          <IonCol size='12'>
            <IonTitle className='en-notif-title'>MANAGE PUSH NOTIFICATIONS</IonTitle>
          </IonCol>

          <IonCol size='12'>
            <div className='en-notif-subcrib ion-float-left'>Subscription:</div>
            <IonIcon icon={closeOutline} className='en-notif-subcrib-close ion-float-left'/>
          </IonCol>

          <IonCol size='12'>
            <Link to='#' className='en-notif-manage' onClick={()=>{OpenNativeSettings.open("notification_id")}}><i>Manage notifications</i></Link>
          </IonCol>

        </IonRow>
      </IonGrid>
      </IonContent>
    ):(
      <IonContent fullscreen>

      <IonGrid>
        <IonRow className='ion-padding'>
        <IonCol style={{display:"flex",justifyContent:"space-between",alignItems:"center"}} className="en-settings-item ion-padding-bottom" size='12'>
              <IonIcon style={{fontSize:"30px",width:"20%",margin:"0 !important"}} icon={stopOutline}></IonIcon>
              <IonLabel style={{marginLeft:"-70px",width:"40%"}} className="ion-padding-horizontal">Notifications</IonLabel>
              <IonToggle id="offlineModeToggler"  checked={notification} onIonChange={e => notificationModeToggler(e.detail.checked)}className='ion-float-right en-toggle' ></IonToggle>
            </IonCol>

        </IonRow>
      </IonGrid>
      </IonContent>
    )}
      
    </IonPage>
  );
};

export default Notifications;
