import {
    IonButton, IonButtons,
    IonCard,
    IonCardContent,
    IonCardHeader,
    IonCardTitle,
    IonCol,
    IonContent,
    IonGrid, IonHeader,
    IonIcon,
    IonImg,
    IonItem,
    IonLabel,
    IonPage, IonRouterLink,
    IonRow,
    IonSpinner,
    IonText, IonTitle, IonToolbar,
    useIonAlert
} from '@ionic/react';
import React, {useContext, useEffect, useRef, useState} from 'react';

import {darkThemeContext} from '../state/State'

import './WeatherPageNew.css';

import axios from "axios";
import WeeklySlider from "../components/weather/WeeklySlider";
import {Storage} from "@ionic/storage";
import {useHistory, useLocation} from "react-router-dom";
import {Swiper, SwiperSlide} from 'swiper/react';

import "swiper/swiper.min.css";
// import 'swiper/css/autoplay';
// import 'swiper/css/keyboard';
// import 'swiper/css/pagination';
// import 'swiper/css/navigation';
// import 'swiper/css/scrollbar';
// import 'swiper/css/zoom';
// import 'swiper/less';
import '@ionic/react/css/ionic-swiper.css';
import {Autoplay, Keyboard, Pagination, Scrollbar, Zoom} from "swiper";

import gpsIcon from './gps.svg';
import {Geolocation} from "@awesome-cordova-plugins/geolocation";

import useOutsideAlerter from "../hooks/useOutsideAlerter";
import Ad from "../components/Ad";
import Header from "../components/Header";
import {
    arrowBack,
    bookmark,
    bookmarkOutline,
    caretForwardOutline,
    desktopOutline,
    homeOutline,
    shareOutline
} from "ionicons/icons";

let cities = require("../JSON/cities.json");


interface WeatherPage {
    keywords?: any;
    title: any;
    adSection:any
}


interface City {
    name: string;
    location: string;
}



const WeatherPageNew: React.FC<WeatherPage> = ({keywords, adSection}: WeatherPage) => {

    const [darkTheme] = useContext<any>(darkThemeContext);

    const [hourlyData, setHourlyData] = useState<any>();
    const [dailyData, setDailyData] = useState<any>();
    const [globalAirQualityData, setGlobalAirQualityData] = useState<any>();
    const [mosquitoData, setMosquitoData] = useState<any>();
    const [painData, setPainData] = useState<any>();
    const [minDaily, setMinDaily] = useState<any>();
    const [maxDaily, setMaxDaily] = useState<any>();
    const [currentTemprature, setCurrentTemprature] = useState(0);
    const [currentTempratureDesc, setCurrentTempratureDesc] = useState("");
    const [Location, setLocation] = useState("");
    const [gridBackground, setGridBackground] = useState("");
    const [showLoading, setShowLoading] = useState(true);
    const [showCitySelectionPopup, setShowCitySelectionPopup] = useState(false);

    const [selectedCity, setSelectedCity] = useState<City>();
    const [gpsCity, setGpsCity] = useState<City>();

    // const GEOCODE = useRef("41.70301989775022,44.79371192597676");
    const store = new Storage();
    const history = useHistory();
    const [presentAlert] = useIonAlert();

    const wrapperRef = useRef(null);
    const {ref, clickedOutComponent} = useOutsideAlerter(wrapperRef);
    const location = useLocation();


    //This will hide the city selection section when clicked outside
    useEffect(() => {
        if(clickedOutComponent) {
            setShowCitySelectionPopup(false);
            console.log("clicked outside..1");
        }
    }, [clickedOutComponent]);

    useEffect(() => {
        // if(!selectedCity)

            Geolocation.getCurrentPosition().then((resp)=>{
                calculateDistance(resp.coords.latitude, resp.coords.longitude);
            }).catch((reason) => {
                console.log(reason);
                console.log("location error", reason);
                calculateDistance(41.70301989775022,44.79371192597676);
            })

    }, []);


    let Array: any = []
    function distance(lat1: any, lon1: any, lat2: any, lon2: any, unit: any) {
        var radlat1 = Math.PI * lat1/180
        var radlat2 = Math.PI * lat2/180
        var theta = lon1-lon2
        var radtheta = Math.PI * theta/180
        var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
        if (dist > 1) {
            dist = 1;
        }
        dist = Math.acos(dist)
        dist = dist * 180/Math.PI
        dist = dist * 60 * 1.1515
        if (unit=="K") { dist = dist * 1.609344 }
        if (unit=="N") { dist = dist * 0.8684 }
        Array.push(dist)
    }

    // console.log("testing...", resp);

    let calculateDistance = (lat:any, lon:any) => {
        cities.map((item:any)=>{
            distance(
                lat, lon, parseFloat(item.location.split(",")[0]), parseFloat(item.location.split(",")[1]),"K");
        })
        let newArray = Array.filter((element : any) => ![NaN].includes(element))
        const min = Math.min.apply(Math, newArray)
        // GEOCODE.current = (cities[Array.indexOf(min)].location);
        // console.log("cities[Array.indexOf(min)].name", cities[Array.indexOf(min)].name);
        // console.log("cities[Array.indexOf(min)].location", cities[Array.indexOf(min)].location);

        setGpsCity(cities[Array.indexOf(min)]);
        // setSelectedCity(cities[Array.indexOf(min)]);

        // setCurrentLocation(DB[Array.indexOf(min)].city);
        // setCurrentLocationChanged(true);
    }


    const calculateHourlyData = async (hData:any) => {
        let responseData = JSON.parse(hData);
        setCurrentTemprature(responseData?.temperature[0]);
        setCurrentTempratureDesc(responseData?.wxPhraseLong[0]);

        responseData["hourly"] = [];

        responseData?.validTimeLocal?.forEach((time: any, index: number) => {
            // let date = new Date(unix_timestamp * 1000);
            // time = time.split('+')[0];
            var n = time.indexOf("T");

            let hour =time.slice(n+1,n+3);

            // let date = new Date(time);

            responseData["hourly"][index] = hour;
            if(index < 10) {
                // console.log("time is", time);
                // console.log("time is", hour);
            }
        }, []);

        setHourlyData(responseData);
        store.create().then(() => {
            store.set("hourlyData", responseData);
        });

        checkNightOrDay();
    }
    const calculateDailyData = async (dData:any) => {
        let responseData = JSON.parse(dData);
        let min = Math.min.apply(null, responseData?.temperatureMin),
            max = Math.max.apply(null, responseData?.temperatureMax);
        responseData["tempMostMax"] = max;
        responseData["tempLeastMin"] = min;
        setDailyData(responseData);
        store.create().then(() => {
            store.set("dailyData", responseData);
        });
    }
    const calculateGlobalData = async (gData:any) => {
        let responseData = JSON.parse(gData);
        setGlobalAirQualityData(responseData);
        store.create().then(() => {
            store.set("globalAirQualityData", responseData);
        });
    }
    const calculateMosquitoData = async (mData:any) => {
        let responseData = JSON.parse(mData);
        setMosquitoData(responseData);
        store.create().then(() => {
            store.set("mosquitoData", responseData);
        });
    }
    const calculatePainData = async (pData:any) => {
        let responseData = JSON.parse(pData);
        setPainData(responseData);
        store.create().then(() => {
            store.set("painData", responseData);
        });
    }

    const getAllData = async () => {
        if(selectedCity && selectedCity?.location) {


            // await axios.post(`https://euronewsgeorgia.com/wp-json/above_rest_api/v1/fetch/hourly_15day_file`, {
            await axios.post(`https://euronewsgeorgia.com/wp-json/above_rest_api/v1/fetch/weather`, {
                lat_long: selectedCity?.location,
                // language: lang
            }).then(response => {

                calculateHourlyData(response.data[0]?.hourly);
                calculateDailyData(response.data[0]?.daily);
                calculateGlobalData(response.data[0]?.global);
                calculateMosquitoData(response.data[0]?.mosquito);
                calculatePainData(response.data[0]?.pain);


            }).catch((error) => {
                presentAlert({
                    header: "Error",
                    cssClass: "custom-alert",
                    // subHeader: 'Important message',
                    message: error.message,
                    buttons: ["OK"],
                });
            });

        }
    }

    useEffect(() => {
        if(hourlyData && dailyData ) {
            setShowLoading(false);
        }
    }, [hourlyData, dailyData, globalAirQualityData]);


    useEffect(() => {
        getAllData();
    },[]);

    useEffect(() => {
        if(gpsCity) {
            const setGpsCityAsDefaultCity = async () => {
                const store = new Storage();
                await store.create();
                let c: City = await store.get('selected_city');
                // if(c && c.name) {
                //     return;
                // }
                if(c != gpsCity) {
                    c = gpsCity;
                }
                console.log("c", c);
                console.log("gpsCity", gpsCity);
                setSelectedCity(c);
                await store.set('selected_city', c);
                // }
            }
            setGpsCityAsDefaultCity();
        }
    },[gpsCity]);

    useEffect(() => {
        const getSelectedCityFromStorage = async () => {
            const store = new Storage();
            await store.create();
            let city: City | undefined = await store.get('selected_city');
            if(!city) {
                // city = cities[0];
                city = gpsCity;
                // await store.set('selected_city', city);
                // console.log("city");
            }
            setSelectedCity(city);
        }

        getSelectedCityFromStorage();
    }, []);

    useEffect(() => {
        if(selectedCity && selectedCity.location)
            getAllData();
    }, [selectedCity]);

    const selectCity = async (city:City) => {
        const store = new Storage();
        await store.create();
        setSelectedCity(city);
        setShowLoading(true);
        setHourlyData(null);
        setDailyData(null);
        setGlobalAirQualityData(null);
        setCurrentTemprature(0);
        setCurrentTempratureDesc("");
        await store.set('selected_city', city);
        setShowCitySelectionPopup(false);
        // cityChanged();
    }

    // const cityChanged = () => {
    //     getAllData();
    // }
    //

    const checkNightOrDay = async () => {
        const store = new Storage();
        await store.create();
        let hourlyData = await store.get("hourlyData");
        if(hourlyData) {
            if (hourlyData?.dayOrNight[0] == "D") {
                setGridBackground("../assets/images/day_back.png");
            } else {
                setGridBackground("../assets/images/night_back.png");
            }
        } else {
            setGridBackground("../assets/images/day_back.png");
        }
    }


    useEffect(() => {
        console.log("setting background for grid, ", gridBackground);
    }, [gridBackground]);

    useEffect(() => {
        checkNightOrDay();
        console.log("adsection: ", adSection);
    }, []);

    return (
        <IonPage>

            <Header showBackButton={true}/>

            {/*<IonHeader  color="dark">*/}
            {/*    <IonToolbar  color="dark">*/}
            {/*        <IonButtons slot="start">*/}
            {/*            <IonRouterLink color="danger" routerLink="/" class='en-header-logo' style={{background: "#e3043a", marginTop: "10px", display: "flex"}}>*/}
            {/*                /!*<IonImg src='../assets/images/header-logo2.png'/>*!/*/}
            {/*                <IonImg style={{height: "48px"}} src='../assets/images/logo_eun_geo.png'/>*/}
            {/*                /!*<IonImg src='../assets/images/en_logo_new_transparent.png'/>*!/*/}
            {/*            </IonRouterLink>*/}
            {/*        </IonButtons>*/}
            {/*        /!*<IonTitle>Start / End Buttons</IonTitle>*!/*/}
            {/*        <IonButtons slot="end">*/}
            {/*            <IonButton size="small" style={{color: "#007df8"}} routerLink={'/'}>*/}
            {/*                <IonIcon size="large" icon={homeOutline}/>*/}
            {/*            </IonButton>*/}
            {/*            <IonButton routerLink='/livestreaming' size="small" shape="round" style={{*/}
            {/*                color: "#ffffff",*/}
            {/*                background: "red",*/}
            {/*                textTransform: "unset",*/}
            {/*                "borderRadius": "20px",*/}
            {/*                padding: "0 7px",*/}
            {/*                fontSize: "16px",*/}
            {/*                fontWeight: "600"*/}
            {/*            }}>Live TV</IonButton>*/}
            {/*        </IonButtons>*/}
            {/*    </IonToolbar>*/}
            {/*</IonHeader>*/}

            {/*<IonHeader*/}
            {/*    style={{backgroundColor: "#ffffff"}}*/}
            {/*    className={"ion-no-border en-header " + "en-header-bg"}>*/}

            {/*    <IonRouterLink color="dark" routerLink="/" class='en-header-logo' style={{marginTop: "10px", display: "flex"}}>*/}
            {/*        /!*<IonImg src='../assets/images/header-logo2.png'/>*!/*/}
            {/*        <IonImg style={{height: "48px"}} src='../assets/images/logo_eun_geo.png'/>*/}
            {/*        /!*<IonImg src='../assets/images/en_logo_new_transparent.png'/>*!/*/}
            {/*    </IonRouterLink>*/}

            {/*    <IonButtons class='en-header-offline-mode'>*/}
            {/*                <IonButton size="small" style={{color: "#007df8"}} routerLink={'/'}>*/}
            {/*                    <IonIcon size="large" icon={homeOutline}/>*/}
            {/*                </IonButton>*/}
            {/*    </IonButtons>*/}

            {/*</IonHeader>*/}

                <IonContent id="weather-content" className='en-weather'>
                    <IonGrid className='en-weather-new-content-parent'
                             style={{ backgroundImage: "url('" + gridBackground + "')"}}
                    >


                        <IonRow>
                            <IonCol size="1"/>
                            <IonCol className="weather-main-top-city-select-col" size={"10"}>
                                <IonButton fill="clear" onClick={() => setShowCitySelectionPopup(!showCitySelectionPopup)}>
                                    {selectedCity?.name}
                                </IonButton>
                            </IonCol>
                            <IonCol size="1"/>
                            {showCitySelectionPopup ?
                                <>
                                    <IonCol size="1"/>
                                    <IonCol size={"10"}>
                                        <IonCard className="glossy-card city-select-card" ref={wrapperRef}>
                                            <IonCardContent className="weather-city-select-card-content">
                                                {cities?.map((city: City, index: number) => {
                                                    // if(index<5)
                                                        return (
                                                            <IonItem key={index} button
                                                                     onClick={() => selectCity(city)}
                                                                     className="weather-weekly-item">
                                                                {
                                                                    (gpsCity?.name == city.name) ?
                                                                        <IonIcon color="light" className="weather-selected-city-gps-icon" icon={gpsIcon} slot="start"/>
                                                                    : <IonText slot="start"/>
                                                                }

                                                                <IonLabel className="weather-city-selection-label" color="light">{city.name}</IonLabel>
                                                            </IonItem>
                                                        )
                                                })}
                                            </IonCardContent>
                                        </IonCard>
                                    </IonCol>
                                    <IonCol size="1"/>
                                </>
                            :""}
                        </IonRow>
                        {
                            hourlyData ?
                                <IonRow>
                                    <IonCol className="weather-main-top-section-parent">
                                        <IonText style={{textAlign: "center"}}>
                                            <h3 className="d-inline weather-main-top-section-city-name">{selectedCity?.name}</h3>
                                            <h1 className="d-inline weather-main-top-section-degree">{currentTemprature}</h1>
                                            <h5 className="d-inline weather-main-top-section-degree-unit">°C</h5>
                                            <h3 className="d-inline weather-main-top-section-current-weather">{currentTempratureDesc}</h3>
                                        </IonText>
                                    </IonCol>
                                    <IonCol size='12' style={{textAlign: 'center'}} className='en-weather-title'>
                                    </IonCol>
                                </IonRow>
                            :""
                        }


                        {!hourlyData ? "" :
                            <IonRow>
                                <IonCol className="weather-current-day-parent-col">
                                    <IonCard className="glossy-card">
                                        {/*{dailyData?.narrative[0] ?*/}
                                        {/*    <IonCardHeader className="weather-card-header">*/}
                                        {/*        <IonCardTitle>*/}
                                        {/*            <h6 className="weather-current-day-header-title">{dailyData?.narrative[0]}</h6>*/}
                                        {/*        </IonCardTitle>*/}
                                        {/*        <hr className="card-header-hr"/>*/}
                                        {/*    </IonCardHeader>*/}
                                        {/* : ""*/}
                                        {/*}*/}

                                        <IonCardContent className="weather-card-content" style={{marginTop: "14px"}}>
                                            <IonRow>
                                                <Swiper
                                                    modules={[Autoplay, Keyboard, Pagination, Scrollbar, Zoom]}
                                                    autoplay={false}
                                                    keyboard={true}
                                                    pagination={false}
                                                    scrollbar={false}
                                                    zoom={true}
                                                    slidesPerView={5}
                                                >
                                                    {hourlyData?.hourly?.map((data: any, index: number) => {

                                                        if (index < 15)
                                                            return (
                                                                <SwiperSlide key={index} >
                                                                    {/*<IonCol size="2.4" key={index}>*/}
                                                                    <div>
                                                                        <IonText>
                                                                            <h3 className="d-inline weather-status-text">{data + ":00"}</h3>
                                                                        </IonText>
                                                                        <IonImg className="weather-icon"
                                                                                src={"../assets/weathericons/icon" + hourlyData?.iconCode[index] + ".svg"}/>
                                                                        <IonText>
                                                                            <h3 className="d-inline weather-status-text-degree">{hourlyData?.temperature[index]}°</h3>
                                                                        </IonText>
                                                                        {/*</IonCol>*/}
                                                                    </div>
                                                                </SwiperSlide>
                                                            )
                                                    })}
                                                </Swiper>
                                            </IonRow>
                                        </IonCardContent>
                                    </IonCard>
                                </IonCol>
                            </IonRow>
                        }

                        {!dailyData ? "" :
                            <IonRow style={{width: "100%"}}>
                                <IonCol className="weather-weekly-parent-col">
                                    <IonCard className="glossy-card">
                                        <IonCardHeader className="weather-card-header">
                                            <IonCardTitle
                                                className="weather-weekly-header-title">{keywords.SEVEN_DAYS_FORECAST}</IonCardTitle>
                                            <hr className="card-header-hr" style={{background: "#91d8f7"}}/>
                                        </IonCardHeader>

                                        <IonCardContent className="weather-card-content">
                                            {/*{dailyData?.daypart[0].daypartName?.map((data:any, index:number) => {*/}
                                            {dailyData?.dayOfWeek?.map((day: any, index: number) => {

                                                if (index < 7) {
                                                    // iCounter++;
                                                    // console.log("iCounter", iCounter);
                                                    return (
                                                        <IonItem className="weather-weekly-item" key={index}
                                                                 lines={index === 6 ? "none" : "inset"} button
                                                                 onClick={() => history.push("/weather/detail/" + index)}>
                                                            <IonRow>
                                                                <IonCol size="4"
                                                                        className="weekly-cols ion-justify-content-start">
                                                                    <IonLabel color="light">{day}</IonLabel>
                                                                </IonCol>
                                                                <IonCol size="2" className="weekly-cols-icon">
                                                                    <IonImg className="weekly-weather-icon"
                                                                            src={"../assets/weathericons/icon" +
                                                                                (dailyData?.daypart[0]?.iconCode[index * 2] ? dailyData?.daypart[0]?.iconCode[index * 2] : "44") +
                                                                                ".svg"}/>
                                                                </IonCol>
                                                                <IonCol size="1.5" className="weekly-cols-min">
                                                                    <IonLabel
                                                                        className="weekly-degree-label weekly-degree-min">{dailyData?.temperatureMin[index]}°</IonLabel>
                                                                </IonCol>
                                                                <IonCol size="3" className="weekly-cols">
                                                                    {/*<input className="weekly-slider" id="slide" type="range" min="10" max="100" value="62" disabled/>*/}
                                                                    {
                                                                        index == 0 ?
                                                                            <WeeklySlider
                                                                                keywords={keywords}
                                                                                current={currentTemprature}
                                                                                leftValue={dailyData?.temperatureMin[index]}
                                                                                rightValue={dailyData?.temperatureMax[index]}
                                                                                min={dailyData?.tempLeastMin}
                                                                                max={dailyData?.tempMostMax}
                                                                            />
                                                                            : <WeeklySlider
                                                                                keywords={keywords}
                                                                                leftValue={dailyData?.temperatureMin[index]}
                                                                                rightValue={dailyData?.temperatureMax[index]}
                                                                                min={dailyData?.tempLeastMin}
                                                                                max={dailyData?.tempMostMax}
                                                                            />
                                                                    }
                                                                </IonCol>
                                                                <IonCol size="1.5" className="weekly-cols-max">
                                                                    <IonLabel color="light"
                                                                              className="weekly-degree-label">{dailyData?.temperatureMax[index]}°</IonLabel>
                                                                </IonCol>
                                                            </IonRow>
                                                        </IonItem>
                                                    )
                                                }
                                            })}


                                        </IonCardContent>
                                    </IonCard>
                                </IonCol>
                            </IonRow>
                        }

                        <IonRow>
                            <IonCol>
                                {showLoading ?
                                    <div style={{textAlign: "center"}}>
                                        <IonSpinner className="weather-spinner" style={{margin: "5rem"}} name="crescent"/>
                                    </div>
                                    : ""}
                            </IonCol>
                        </IonRow>

                        <IonRow className="ion-margin-top1" style={{marginTop: "1rem"}}>
                            <IonCol>
                                <Ad adSection={adSection} showTitle={false} keywords={keywords}/>
                            </IonCol>
                        </IonRow>

                        {/*<DayDetails dailyIndex={1} dailyDayPartIndex={2} dailyData={dailyData} hourlyData={hourlyData} globalAirQualityData={globalAirQualityData}/>*/}
                        {!showLoading ?
                            <IonCol>
                                <div className="en-weather-copernicus-div">
                                    <IonImg style={{height: "50px"}} src="../assets/images/copernicus.png"/>
                                </div>
                            </IonCol>
                            :   ""
                        }
                    </IonGrid>

                </IonContent>

        </IonPage>
    );
};

export default WeatherPageNew;
