import { IonBackButton, IonButtons, IonContent, IonHeader, IonIcon, IonImg, IonItem, IonLabel, IonList, IonPage, IonTitle } from '@ionic/react';
import { chevronBackOutline, chevronForwardOutline, logoFacebook, logoInstagram, logoTwitter, logoYoutube } from 'ionicons/icons';
import './EuronewsGeorgiaApp.css';
import React from "react";

interface EuronewsGeorgiaAppProps {
  keywords?: any;
}


const EuronewsGeorgiaApp: React.FC<EuronewsGeorgiaAppProps> = ({keywords} : EuronewsGeorgiaAppProps) => {
  return (
    <IonPage>
      <IonHeader className="ion-no-border en-settings-header">
        <IonBackButton icon={chevronBackOutline} className='ion-float-left ion-no-padding en-settings-back-button'/>
        <IonTitle className='en-settings-title ion-no-padding'>{keywords.EURONEWS_GEORGIA_APPS}</IonTitle>
      </IonHeader>
      
      <IonContent fullscreen>
        <IonList>
            <IonItem className='ion-padding-end en-services-item-title'>
              <IonImg src='../assets/images/africa.jpg'/>
              <IonLabel className='ion-padding-start'>Africanews app</IonLabel>
              <IonIcon icon={chevronForwardOutline} />
            </IonItem>

            <IonItem className='ion-padding-end en-services-item-title'>
              <IonImg src='../assets/images/euronews-app.jpg'/>
              <IonLabel className='ion-padding-start'>Euronews apps</IonLabel>
              <IonIcon icon={chevronForwardOutline} />
            </IonItem>

            <div className='ion-padding-end en-services-item-title en-app-social'>
              <IonButtons><IonIcon icon={logoFacebook} className='en-app-social-icon'/></IonButtons>
              <IonButtons><IonIcon icon={logoTwitter} className='en-app-social-icon'/></IonButtons>
              <IonButtons><IonIcon icon={logoInstagram} className='en-app-social-icon'/></IonButtons>
              <IonButtons><IonIcon icon={logoYoutube} className='en-app-social-icon'/></IonButtons>
              
            </div>

        </IonList>
      </IonContent>
    </IonPage>
  );
};

export default EuronewsGeorgiaApp;
