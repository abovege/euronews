import {
    IonSpinner,
    IonButton,
    IonBackButton,
    IonSkeletonText,
    IonCol,
    IonContent,
    IonGrid,
    IonHeader,
    IonIcon,
    IonImg,
    IonPage,
    IonRouterLink,
    IonRow,
    IonTitle,
    IonButtons,
    IonModal, isPlatform, IonAlert
} from '@ionic/react';
import {
    bookmarkOutline,
    bookmark,
    chevronBackOutline,
    chevronForwardOutline,
    shareOutline,
    arrowBack, homeOutline, desktopOutline, caretForwardOutline, closeOutline
} from 'ionicons/icons';
import './StoryPage.css';
import React, { Fragment } from 'react';
import Labels from '../components/Labels';
import RelatedStories from '../components/RelatedStories';
import {useEffect,useState,useContext,useRef } from 'react';
import {useLocation,Link, useHistory} from "react-router-dom";
import axios from "axios";
import LatestNewsStory from '../components/LatestNewsStory';
import ReactPlayer from 'react-player';
import { Swiper, SwiperSlide } from 'swiper/react';
import SwiperCore,{ Pagination,Navigation } from 'swiper';
import { share } from '../services/sharing.service'
import FadeIn from 'react-fade-in';
import {offlineModeContext, fontSizeContext, articleAPContext,shouldVideoPlayContext,darkThemeContext} from '../state/State'
import OfflineSmallStory from '../components/OfflineSmallStory'
import { Network } from '@capacitor/network';


import { Storage } from '@ionic/storage';

import "swiper/swiper.min.css";
// import "swiper/modules/pagination/pagination.scss";
// import "swiper/modules/navigation/navigation.scss";
import OfflineMode from '../components/OfflineMode';
import useOnScreen from "../hooks/useOnScreen"
import Ad from "../components/Ad";
import {Tweet} from "react-twitter-widgets";
// @ts-ignore
import { eagerLoadTwitterLibrary } from "react-twitter-widgets";
import Gallery from "../components/Gallery";

interface StoryPageProps {
    keywords?: any;
    articleAP?:string;
    adSection:any
}




export const StoryPage: React.FC<StoryPageProps> = ({keywords, adSection} : StoryPageProps) => {
    const data = useLocation<any>();
    const [postData,setPostData] = useState<any>();
    const [latestNews,setLatestNews] = useState<any[]>([]);
    const [tweets,setTweets] = useState<any[]>([]);
    const [relatedStory,setRelatedStory] = useState<any[]>([]);
    const [relatedStoryList,setRelatedStoryList] = useState<any[]>([]);
    const [readItLater,setReadItLater] = useState<any>(false);
    const [offlineMode] = useContext<any>(offlineModeContext)
    const [fontSize] = useContext<any>(fontSizeContext)
    const [videoMode] = useContext<any>(articleAPContext)
    const [shouldVideoPlay, setShouldVideoPlay] = useContext<any>(shouldVideoPlayContext);
    const [darkTheme] = useContext<any>(darkThemeContext);


    const [desc,setDesc] = useState("")
    const [statusNet, setStatusNet]= useState<any>();
    const contentRef = useRef<HTMLIonContentElement | null>(null);
    const [labelDisplay, setLabelDisplay] = useState<boolean>();
    const [played, setPlayed] = useState<boolean>(true)

    const  store = new Storage();

    const ref = useRef() as React.MutableRefObject<HTMLIonColElement>
    const isVisible = useOnScreen(ref)

    useEffect(() => {
        setPlayed(isVisible);
    }, [isVisible]);

    const fetchRelatedStoryList = async (category: any) =>{
        let isMounted = true;
        const  store = new Storage();
        await store.create();
        const body = {
            orderby : "ID",
            order : "DESC",
            post_type: "post",
            post_count : "16",
            page : "1",
            taxonomy : "category",
            taxonomy_field  : "name",
            taxonomy_values   : [  `${category}` ],
            cache_key : "string cache key that get from end of the last response"
        };
        axios.post('https://euronewsgeorgia.com/wp-json/above_rest_api/v1/fetch/posts/', body)
            .then(response => {
                setRelatedStoryList(response.data.posts);
                store.set("relatedStoryList",response.data.posts);
            })
            .catch(error => {
                console.error('There was an error!', error);
            });
        //  setRelatedStoryList(await store.get("relatedStoryList"));
        return () => { isMounted = false };

    }

    const fetchRelatedStory = async (category : any) =>{
        let isMounted = true;
        const  store = new Storage();
        await store.create();
        const body = {
            orderby : "ID",
            order : "DESC",
            post_type: "post",
            post_count : "3",
            page : "1",
            taxonomy : "category",
            taxonomy_field  : "name",
            taxonomy_values   : [  `${category}` ],
            cache_key : "string cache key that get from end of the last response"
        };
        axios.post('https://euronewsgeorgia.com/wp-json/above_rest_api/v1/fetch/posts/', body)
            .then(response => {
                setRelatedStory(response.data.posts);
                store.set("relatedStory",response.data.posts);
            })
            .catch(error => {
                console.error('There was an error!', error);
            });
        // setRelatedStory(await store.get("relatedStory"));
        return () => { isMounted = false };
    }

    const fetchLatestNews = async () =>{
        let isMounted = true;
        const  store = new Storage();
        await store.create();
        const body = {
            orderby : "ID",
            order : "DESC",
            post_type: "post",
            post_count : "12",
            page : "1",
            taxonomy : "category",
            taxonomy_field  : "name",
            taxonomy_values   : [  "საქართველო" ],
            cache_key : "string cache key that get from end of the last response"
        };
        axios.post('https://euronewsgeorgia.com/wp-json/above_rest_api/v1/fetch/posts/', body)
            .then(response => {
                setLatestNews(response.data.posts);
                store.set("latestNews",response.data.posts);
            })
            .catch(error => {
                console.error('There was an error!', error);
            });
        // setLatestNews(await store.get("latestNews"));
        return () => { isMounted = false };
    }

    useEffect(() => {
        let isMounted = true;

        if (isMounted){

            const logCurrentNetworkStatus = async () => {
                setStatusNet(await Network.getStatus());
            };
            logCurrentNetworkStatus();

            if(data.state == undefined){
                fetchByID();
            }else{
                console.log("data is: ", data);
                if(data.state.data.ID){
                    const setLogo = async () =>{
                        await store.create();
                        await store.get("readItLaterList") == null && await store.set("readItLaterList",[]);
                        const array = await store.get("readItLaterList");
                        array.map((item: { ID: any; }) => {
                            if (data && data.state.data.ID == item.ID){
                                setReadItLater(true);
                                return;
                            }
                        });
                    }
                    setLogo();
                    const setLabel = () => {
                        let yourDate = new Date();
                        yourDate.toISOString().split('T')[0] == data.state.data.post_date_gmt.substring(0,10) ? setLabelDisplay(true) : setLabelDisplay(false)
                    }
                    setLabel()

                    const formatDate = (input: any) => {
                        var datePart = input.match(/\d+/g),
                            year = datePart[0].substring(2,4), // get only two digits
                            month = datePart[1], day = datePart[2];
                        return day+'/'+month+'/'+year;
                    }

                    let post_date = formatDate(data.state.data.post_date_gmt.substring(0,10)).replace("/",".").replace("/",".") + '/' + data.state.data.post_date_gmt.substring(11,16)
                    let res = data.state.data.post_content;
                    const result = res?.split('[embedyt]')?.pop()?.split('[/embedyt]')[0]
                    const final = result.includes("watch?v=") && result.replace("watch?v=","embed/");

                    setDesc(res.replace('[embedyt]'+result+"[/embedyt]",
                        `<iframe width="100%" height="250px" frameborder="0" src="${final}"></iframe>`));
                    setPostData({
                        title : data.state.data.post_title,
                        author: data.state.data.post_author_name,
                        time : post_date,
                        desc : data.state.data.post_content,
                        image : data.state.data.post_meta.essb_cached_image,
                        video: data.state.data.post_meta.td_post_video,
                        url: data.state.data.guid,
                        categories: data.state.data.categories
                    });
                    if(offlineMode == false){
                        fetchLatestNews();
                        fetchRelatedStory(data.state.relatedStory);
                        fetchRelatedStoryList(data.state.relatedStory);
                    }
                }else{
                    fetchByID();
                }
            }

        }

        return () => { isMounted = false };
    }, [])

    let arr = window.location.href.split("/")
    let POSTID = arr[arr.length-1].length
    var STID = window.location.href.substring(window.location.href.length - POSTID);

    useEffect(() => {
        let isMounted = true;
        if (isMounted){
            fetchLatestNews();
            // fetchRelatedStory(data.state.relatedStory);
            // fetchRelatedStoryList(data.state.relatedStory);
        }
        return () => { isMounted = false };

    }, [offlineMode])

    useEffect(() => {
        let isMounted = true;
        if (isMounted){
            if(arr[3] == "storypage"){

                contentRef.current && contentRef.current.scrollToTop(2000);
                fetchByID();
            }
        }
        return () => { isMounted = false };
    }, [STID])



    //  response.data.post.post_meta.essb_cached_image[0] ? response.data.post.post_meta.essb_cached_image[0]:

    // useEffect(() => {
    //     console.log("desc is changing", desc);
    // }, [desc]);

    function isNumeric(str:any) {
        return !isNaN(str) && // use type coercion to parse the _entirety_ of the string (`parseFloat` alone does not do this)...
            !isNaN(parseFloat(str)) // ...and ensure strings of whitespace fail
    }

    const extractTwitterId = (url:string) => {
        // console.log('extracting id', url);
        if(!url) return;
        let u = new URL('', url);
        // let x = u?.pathname?.match(/\/([^\/]*)\//);
        let x = u.pathname.split("/").pop();
        // console.log(x);
        return x;
    }


    const extractYoutubeUrls = (text:string):any => {
        // let urlRegex = /(https?:\/\/[^\s]+)/g;
        let urlRegex =/(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
        let tweetUrls: any = [];
        let found = false;

        return text.replace(urlRegex, function(url):any {
            if(url.toLowerCase().includes('youtube.com') && url.toLowerCase().includes('http') && url.toLowerCase().includes('watch?v=')){
                const final = url && url.includes("watch?v=") && url.replace("watch?v=","embed/");
                console.log('url found', final);
                found = true;
                return '<iframe width="100%" height="250px" frameborder="0" src="' + final + '"></iframe>';
            }
            found = false;
            return url;
        });

    }

    const extractTweetUrls = (text:string):any => {
        let urlRegex = /(https?:\/\/[^\s]+)/g;
        let urlParagraphRegex = /<p>(.*?)<\/p>/g;
        let tweetUrls:any = [];
        let found = false;

        text = text.replace(urlParagraphRegex, function(url):any {
            console.log("url is: ", url);
            // url.toLowerCase().includes('<p>') &&
            if(url.toLowerCase().includes('euronewsgeorgia.com') && url.toLowerCase().includes('http') && !url.toLowerCase().includes('<img>')){
                // tweetUrls.push(extractTwitterId(url));
                url = url.replace("</p>", '').replace('<p>', '');
                return '<a target="_blank" href="' + url + '">' + url + '</a>';
            }
            // found = false;

            return url;//'<a href="' + url + '">' + url + '</a>';
        });

        // console.log('res euronews georgia', res);

        // let x = '[gallery td_select_gallery_slide="slide" td_gallery_title_input="სამხედრო სწავლება" link="none" ids="85049,85050,85052,85053,85054,85055,85056,85048"]';
        if(text && text.includes('[gallery')) {
            var regex = /ids="([^"]*)/;
            let matchGallery = text.match(regex);

                // console.log("matchGallery", matchGallery[1]);
            if (matchGallery)
                return {"gallery_ids": matchGallery[1]};
        }

        const matches = text.match(urlRegex);

        // console.log("text", text);
        let tweet_ids = null;
        if(matches)
            tweet_ids = matches.filter((url) => {
                // console.log("matches", matches, "url", url);
                if(url.toLowerCase().includes('twitter.com') && url.toLowerCase().includes('http')) {
                    // tweetUrls.push(extractTwitterId(url));
                    found = true;
                    return true;
                }
                return false;
            });

        tweet_ids = tweet_ids?.filter((url) => {
            // console.log('url', url, 'isNumeric(id)', isNumeric(url));
            if(isNumeric(extractTwitterId(url))){
                return true;
            } else {
                return false;
            }
        });

        if(found && typeof tweet_ids !== 'undefined' && tweet_ids.length > 0){
            console.log("found",text, found,tweet_ids, isNumeric(tweet_ids));
            return {"tweet_id": extractTwitterId(tweet_ids[0])};
        }
        // console.log("found", res);
        // setTweets(tweetUrls);
        // if(found && isNumeric(res)){
        //     return {"tweet_id": res}
        // }
        return text;
        // or alternatively
        // return text.replace(urlRegex, '<a href="$1">$1</a>')
    }



    const fetchByID = async () =>{
        setPostData(null);
        let arr = window.location.href.split("/")
        let POSTID = arr[arr.length-1].length
        var ID = window.location.href.substring(window.location.href.length - POSTID);
        const body = {
            post_id : ID,
            cache_key : "string cache key that get from end of the last response"
        }
        axios.post('https://euronewsgeorgia.com/wp-json/above_rest_api/v1/fetch/posts', body)
            .then(response => {

                let res = response.data.post.post_content;
                const result = res && res.includes('[embedyt]') && res?.split('[embedyt]')?.pop()?.split('[/embedyt]')[0]
                const final = result && result.includes("watch?v=") && result.replace("watch?v=","embed/");

                const searchForCaption = res && res.includes('[caption') && res?.split('[caption')?.pop()?.split('[/caption]')[0];

                if(res && res.includes('youtube.com') && !res.includes('[embedyt')) {
                    res = extractYoutubeUrls(res);
                    console.log('includes youtube');
                }


                if(res && res.includes('[caption')) {
                    //remove all [caption] tags and keep the image only
                    let b = Array.from(res.matchAll(/\[([^\][]*)]/g), (x: any) => x[1]);
                    let r = res;
                    b.map((item, i) => {
                        r = r.replace(item, '');
                    });

                    res = r.replaceAll('[]', '');
                }


                // if(res && res.includes('[gallery')) {
                //     //remove all [gallery] tags and keep the image only
                //     let b = Array.from(res.matchAll(/\[([^\][]*)]/g), (x: any) => x[1]);
                //     let r = res;
                //     b.map((item, i) => {
                //         r = r.replace(item, '');
                //     });
                //
                //     res = r.replaceAll('[]', '');
                // }


                if(res && res.includes('[embedyt]')) {
                    setDesc(res.replace('[embedyt]' + result + "[/embedyt]",
                        `<iframe width="100%" height="250px" frameborder="0" src="${final}"></iframe>`));
                    // console.log("setting desc via if", res);
                } else {
                    setDesc(res);
                    // console.log(res);
                    // console.log("setting desc via else", res);
                }


                response.data.post.categories[0] && fetchRelatedStory(response.data.post.categories[0].name);
                response.data.post.categories[0] && fetchRelatedStoryList(response.data.post.categories[0].name);
                const formatDate = (input: any) => {
                    var datePart = input.match(/\d+/g),
                        year = datePart[0].substring(2,4), // get only two digits
                        month = datePart[1], day = datePart[2];

                    return day+'/'+month+'/'+year;
                }
                let post_date = response.data.post.post_date_gmt && formatDate(response.data.post.post_date_gmt.substring(0,10)).replace("/",".").replace("/",".") + '/' + response.data.post.post_date_gmt.substring(11,16)
                setPostData({
                    title : response.data.post.post_title,
                    author: response.data.post.post_author_name,
                    time : post_date,
                    desc : response.data.post.post_content,
                    image : response.data.post.post_meta.essb_cached_image,
                    video: response.data.post.post_meta.td_post_video,
                    url: response.data.post.guid,
                    categories: response.data.post.categories
                })
            })
            .catch(error => {
                console.error('There was an error!', error);
            });
        fetchLatestNews();
    }

    const changeReadItLater = async () =>{
        setReadItLater(!readItLater);
        await store.create();

        let Array = await store.get("readItLaterList");

        Array.length == 0 && store.set("readItLaterList",[data.state.data])
        Array.map(async (item: { ID: any; })=>{
            if(item.ID === data.state.data.ID ) {
                Array =  Array.filter((item: { ID: any; }): any => item.ID !== data.state.data.ID);
                await store.set("readItLaterList",[...Array]);
            }else {
                await store.set("readItLaterList",[...Array,data.state.data])
            }
        })
    }

    const handleShare = async () => {
        if (postData) {
            await share(`via Euronews Georgia: ${postData?.title} ${postData?.url}`)
        }
    }


    return (
        <IonPage>
            {postData ? (
                <>

                    <IonHeader

                        style={{backgroundColor: "#ffffff"}}
                        className={"ion-no-border en-header " + "en-header-bg"}>

                        <IonRouterLink color="dark" routerLink="/" class='en-header-logo'  style={{marginTop: "10px", display: "flex", justifyContent: "start", maxWidth: "48px"}}>
                            {/*<IonImg src='../assets/images/header-logo2.png'/>*/}
                            <IonImg style={{height: "48px"}} src='../assets/images/logo_eun_geo.png'/>
                            {/*<IonImg src='../assets/images/en_logo_new_transparent.png'/>*/}
                        </IonRouterLink>

                        <div style={{width:"25%", height:"100%",display:"flex", justifyContent:"flex-end", alignItems:"center"}}>
                            <IonButton  style={{backgroundColor:"transparent"}} className='ion-float-right en-save-button ion-no-padding ion-no-margin' onClick={changeReadItLater}>
                                <IonIcon icon={ readItLater ? bookmark : bookmarkOutline} className='ion-float-right en-save-button-icon ion-no-padding'/>
                            </IonButton>
                            <IonButton onClick={handleShare} className='ion-float-right en-share-button'>
                                <IonIcon icon={shareOutline} className='ion-float-right en-share-button-icon'/>
                            </IonButton>
                        </div>

                    </IonHeader>


                    {/*<IonHeader style={{display:"flex"}} className="ion-no-border e-storypage-header">*/}
                    {/*    <div style={{width:"25%",height:"100%",display:"flex", justifyContent:"flex-start", alignItems:"center"}}>*/}
                    {/*        <IonBackButton icon={chevronBackOutline} className='ion-float-left ion-no-padding en-back-button'/>*/}
                    {/*    </div>*/}
                    {/*    <div style={{width:"50%", height:"100%",display:"flex", justifyContent:"center", alignItems:"center"}}>*/}
                    {/*        <IonRouterLink style={{width:"80%"}} color="dark" routerLink="/" class='en-header-logo'>*/}
                    {/*            <IonImg  src='../assets/images/header-logo2.png'/>*/}
                    {/*        </IonRouterLink>*/}
                    {/*    </div>*/}
                    {/*    <div style={{width:"25%", height:"100%",display:"flex", justifyContent:"flex-end", alignItems:"center"}}>*/}
                    {/*        <IonButton  style={{backgroundColor:"transparent"}} className='ion-float-right en-save-button ion-no-padding ion-no-margin' onClick={changeReadItLater}>*/}
                    {/*            <IonIcon icon={ readItLater ? bookmark : bookmarkOutline} className='ion-float-right en-save-button-icon ion-no-padding'/>*/}
                    {/*        </IonButton>*/}
                    {/*        <IonButton onClick={handleShare} className='ion-float-right en-share-button'>*/}
                    {/*            <IonIcon icon={shareOutline} className='ion-float-right en-share-button-icon'/>*/}
                    {/*        </IonButton>*/}
                    {/*    </div>*/}
                    {/*</IonHeader>*/}

                    <IonContent ref={contentRef}  >
                        <IonGrid className='ion-no-padding' >
                            <IonRow className='ion-no-padding'>
                                <IonCol ref={ref} className='ion-no-padding'>
                                    <FadeIn>
                                        {postData?.video ?
                                            videoMode == "articles-wifi" && statusNet == "wifi" &&
                                            <ReactPlayer width={"100%"} height={236}
                                                         playing={played}
                                                         muted={true}
                                                         config={
                                                             {
                                                                 youtube:
                                                                     {playerVars: {
                                                                             controls: 1,
                                                                             autohide:0,
                                                                         }
                                                                     }}}
                                                         url={`${postData?.video}`} />
                                            ||
                                            videoMode == "articles-always" ?
                                                <ReactPlayer width={"100%"} height={236}
                                                             playing={played}
                                                             muted={true}
                                                             config={
                                                                 {
                                                                     youtube:
                                                                         {playerVars: {
                                                                                 controls: 1,
                                                                                 autohide:0,
                                                                             }
                                                                         }}}
                                                             url={`${postData?.video}`} />
                                                : <ReactPlayer width={"100%"} height={236}  config={{youtube: {playerVars: { controls: 1,autohide:0 }}}}  playing={played}  url={`${postData?.video}`} light={`${postData?.image}`} />
                                            :  <IonImg  src={postData?.image} />}
                                    </FadeIn>
                                </IonCol>
                            </IonRow>
                        </IonGrid>

                        <IonGrid className='en-storypage-content'>
                            <FadeIn>
                                {labelDisplay && <Labels labelBG="green" labelText="განახლებული ამბები"/>}
                            </FadeIn>
                            <FadeIn>
                                <h3 className='en-storypage-title'>{postData && postData?.title}</h3>
                                <div className='en-storypage-category'>{postData && postData?.categories[0]?.name}</div>
                                <div className='en-storypage-time'>{postData && postData?.time}</div>
                            </FadeIn>
                        </IonGrid>
                        <FadeIn>



                            {desc.split("\r\n\r\n").map((item,index) => {
                                // console.log('item: ', item);
                                let html = extractTweetUrls(item);
                                // console.log("typeof html", typeof html);
                                if(typeof html == "object") {
                                    // console.log("it is object", html);
                                    if(html?.tweet_id){
                                        return (<Fragment key={index}>
                                            <div data-theme="dark" className="ion-margin-horizontal">
                                                <Tweet key={index} tweetId={html?.tweet_id}/>
                                            </div>
                                        </Fragment>)
                                    } else {
                                        return (<Fragment key={index}>
                                            <div data-theme="dark" className="ion-margin-horizontal">
                                                <Gallery key={index} ids={html?.gallery_ids}/>
                                            </div>
                                        </Fragment>)
                                    }
                                }
                                return (
                                    <Fragment key={index}>
                                        <IonGrid style={{fontSize:fontSize}} dangerouslySetInnerHTML={{ __html: html}} id="desc-text-size" className='en-storypage-description'>
                                        </IonGrid>
                                    </Fragment>
                                )
                            })}

                            <div data-theme="dark" className="ion-margin-horizontal">
                                {tweets?.map((tweetId,index) => {
                                    return <Tweet key={index} tweetId={tweetId}/>
                                })}
                            </div>

                        </FadeIn>



                        <IonGrid className='ion-no-padding'>
                            <Link
                                style={darkTheme ? {color:"#ffffff",textDecoration:"none"}:{color:"#000000",textDecoration:"none"}}
                                to={{
                                    pathname: "/relatedstories",
                                    state: {
                                        data: relatedStoryList,
                                    },
                                }}>
                                <IonTitle className='en-related-stories-title' style={darkTheme ? {background:"#000"}:{background:"#f1f5f6"}}>
                                    {keywords.RELATED_STORIES}
                                    <IonIcon icon={chevronForwardOutline} className='ion-float-right en-home-icon'/>
                                </IonTitle>
                            </Link>

                            {offlineMode ?
                                <OfflineSmallStory/> :
                                relatedStory.length > 0 ? relatedStory.map((item,key)=>(
                                        <RelatedStories key={key} data={item} ID={item.ID}/>
                                    ))
                                    :
                                    <div style={{display:"flex",justifyContent:"center"}}>
                                        <IonSpinner name="bubbles" />
                                    </div>

                            }

                        </IonGrid>

                        <IonGrid className='en-storypage-more-about' style={darkTheme ? {background:"#1e1e1e"}:{background:"#ffffff"}}>

                            <IonTitle className='en-storypage-more-about-title'>{keywords.MORE_ABOUT}</IonTitle>
                            {postData && postData.categories && postData.categories?.map((data: any,index: any)=>{
                                return (
                                    <Link key={index} className='en-home-hot-topics-category' to={`/insidepages/${data.name}`} style={darkTheme ? {background:"#fff" , color: "#000"}:{background:"#2f70e6" , color: "fff"}}>
                                        {data.name}
                                        <IonIcon icon={chevronForwardOutline} className='ion-float-right en-home-icon'/>
                                    </Link>
                                )
                            })}


                        </IonGrid>

                        <Ad adSection={adSection} keywords={keywords}/>

                        <IonGrid className='en-home-sponsored ion-no-padding'>
                            <IonTitle className='en-home-sponsored-title'>{keywords.LATEST_NEWS}</IonTitle>
                            {offlineMode ?
                                <OfflineSmallStory /> :
                                <IonRow className='ion-no-padding'  style={darkTheme ? {background:"#1e1e1e",display:"flex",justifyContent:"center"}:{background:"#fff",display:"flex",justifyContent:"center"}}>
                                    {latestNews.length > 0  ? latestNews.map((data,key)=>(
                                            <LatestNewsStory key={key} data={data} ID={data.ID}/>
                                        )):
                                        <IonSpinner name="bubbles" />
                                    }

                                </IonRow>
                            }
                        </IonGrid>



                    </IonContent>
                </>) : (
                <>
                    {/*<IonHeader className="ion-no-border e-storypage-header">*/}
                    {/*    <IonBackButton icon={chevronBackOutline} className='ion-float-left ion-no-padding en-back-button'/>*/}
                    {/*    <IonButton onClick={handleShare} className='ion-float-right en-share-button'>*/}
                    {/*        <IonIcon icon={shareOutline} className='ion-float-right en-share-button-icon'/>*/}
                    {/*    </IonButton>*/}

                    {/*    <IonButton  style={{backgroundColor:"transparent"}} className='ion-float-right en-save-button ion-no-padding ion-no-margin' onClick={changeReadItLater}>*/}
                    {/*        <IonIcon icon={ readItLater ? bookmark : bookmarkOutline} className='ion-float-right en-save-button-icon ion-no-padding'/>*/}
                    {/*    </IonButton>*/}
                    {/*</IonHeader>*/}

                    <IonHeader
                        style={{backgroundColor: "#ffffff"}}
                        className={"ion-no-border en-header " + "en-header-bg"}>

                        <IonRouterLink color="dark" routerLink="/" class='en-header-logo' style={{marginTop: "10px", display: "flex"}}>
                            {/*<IonImg src='../assets/images/header-logo2.png'/>*/}
                            <IonImg style={{height: "48px"}} src='../assets/images/logo_eun_geo.png'/>
                            {/*<IonImg src='../assets/images/en_logo_new_transparent.png'/>*/}
                        </IonRouterLink>

                        <div style={{width:"25%", height:"100%",display:"flex", justifyContent:"flex-end", alignItems:"center"}}>
                            <IonButton  style={{backgroundColor:"transparent"}} className='ion-float-right en-save-button ion-no-padding ion-no-margin' onClick={changeReadItLater}>
                                <IonIcon icon={ readItLater ? bookmark : bookmarkOutline} className='ion-float-right en-save-button-icon ion-no-padding'/>
                            </IonButton>
                            <IonButton onClick={handleShare} className='ion-float-right en-share-button'>
                                <IonIcon icon={shareOutline} className='ion-float-right en-share-button-icon'/>
                            </IonButton>
                        </div>

                    </IonHeader>

                    <IonContent fullscreen>
                        <IonGrid className='ion-no-padding'>
                            <IonRow className='ion-no-padding'>
                                <IonCol style={{height:"200px",display:"flex",alignItems: 'center',justifyContent:"center",background:"#e2e2e2"}} className='ion-no-padding'>
                                    <IonSkeletonText className="animate-skeleton-background" style={{ width: '70%',height:"80px"}}/>
                                </IonCol>
                            </IonRow>
                        </IonGrid>

                        <IonGrid className='en-storypage-content'>
                            <IonSkeletonText animated style={{ width: '30%' }} />

                            <h3 className='en-storypage-title'>
                                <IonSkeletonText animated style={{ width: '100%' }} />
                                <IonSkeletonText animated style={{ width: '100%' }} />
                                <IonSkeletonText animated style={{ width: '30%' }} />

                            </h3>
                            <div className='en-storypage-time'>
                                <IonSkeletonText animated style={{ width: '30%'}} />
                                <br></br>
                            </div>
                            <br></br>
                            <div className='en-storypage-by'><i>
                                <IonSkeletonText animated style={{ width: '100%', }} />
                            </i></div>
                            <IonGrid className='en-storypage-description'>
                                <p >
                                    <IonSkeletonText animated style={{ width: '100%' }} />
                                    <IonSkeletonText animated style={{ width: '100%' }} />
                                    <IonSkeletonText animated style={{ width: '100%' }} />
                                    <IonSkeletonText animated style={{ width: '100%' }} />
                                    <IonSkeletonText animated style={{ width: '100%' }} />
                                    <IonSkeletonText animated style={{ width: '100%' }} />
                                    <IonSkeletonText animated style={{ width: '100%' }} />
                                    <IonSkeletonText animated style={{ width: '100%' }} />
                                    <IonSkeletonText animated style={{ width: '100%' }} />
                                    <IonSkeletonText animated style={{ width: '100%' }} />
                                    <IonSkeletonText animated style={{ width: '100%' }} />
                                    <IonSkeletonText animated style={{ width: '100%' }} />
                                </p>
                            </IonGrid>
                        </IonGrid>
                    </IonContent>
                </>)
            }

        </IonPage>
    );
};

export default StoryPage;
