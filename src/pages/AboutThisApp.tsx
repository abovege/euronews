import {IonButtons,IonText,IonModal, IonAlert, IonBackButton, IonContent, IonHeader, IonIcon, IonItem, IonLabel, IonList, IonPage, IonTitle } from '@ionic/react';
import { bulbOutline, chatboxOutline, chevronBackOutline, chevronForwardOutline, phonePortraitOutline, shareOutline, starOutline } from 'ionicons/icons';
import {useEffect, useState} from 'react';
import { closeOutline } from 'ionicons/icons';
import ModalAboutThisApp from '../components/ModalAboutThisApp'
import './AboutThisApp.css';
import {darkThemeContext} from '../state/State'
import React,{useContext} from 'react';
// import {AppRate, AppRateReviewTypeAndroid, AppRateReviewTypeIos} from "@awesome-cordova-plugins/app-rate";
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing';


interface AboutThisAppProps {
  keywords?: any;
}


const AboutThisApp: React.FC<AboutThisAppProps> = ({keywords} : AboutThisAppProps) => {
  const [showAlert1, setShowAlert1] = useState(false);
  const [showAlert2, setShowAlert2] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [darkTheme] = useContext<any>(darkThemeContext);

  const CloseModal = () => setShowModal(false)

    // AppRate.setPreferences({
    //     reviewType: {
    //         ios: AppRateReviewTypeIos.AppStoreReview,
    //         android: AppRateReviewTypeAndroid.InAppBrowser
    //     },
    //     storeAppURL: {
    //         ios: 'ge.above.euronewsgeorgia',
    //         android: 'market://details?id=ge.above.euronewsgeorgia',
    //         // blackberry: 'appworld://content/[App Id]/',
    //         windows8: 'ms-windows-store:Review?name=ge.above.euronewsgeorgia'
    //     },
    //     customLocale: {
    //         title: keywords.APPRATE_TITLE,
    //         message: keywords.APPRATE_MESSAGE,
    //         cancelButtonLabel: keywords.APPRATE_CANCEL_BUTTON_LABEL,
    //         laterButtonLabel: keywords.APPRATE_LATER_BUTTON_LABEL,
    //         rateButtonLabel: keywords.APPRATE_RATE_BUTTON_LABEL,
    //         yesButtonLabel: keywords.APPRATE_YES_BUTTON_LABEL,
    //         noButtonLabel: keywords.APPRATE_NO_BUTTON_LABEL,
    //         appRatePromptTitle: keywords.APPRATE_APPRATE_PROMPT_TITLE,
    //         feedbackPromptTitle: keywords.APPRATE_FEED_BACK_PROMPT_TITLE,
    //     },
    //     openUrl: (url) => window.open(url, '_blank', 'location=yes')
    //
    // });

  useEffect(() => {
      // AppRate.setPreferences({
      //     reviewType: {
      //         // ios: AppRateReviewTypeIos.InAppReview,
      //         // android: AppRateReviewTypeAndroid.InAppReview
      //         ios: AppRateReviewTypeIos.AppStoreReview,
      //         android: AppRateReviewTypeAndroid.InAppBrowser
      //     },
      //     storeAppURL: {
      //         ios: 'ge.above.euronewsgeorgia',
      //         android: 'market://details?id=ge.above.euronewsgeorgia',
      //         // blackberry: 'appworld://content/[App Id]/',
      //         windows8: 'ms-windows-store:Review?name=ge.above.euronewsgeorgia'
      //     },
      //     customLocale: {
      //         title: keywords.APPRATE_TITLE,
      //         message: keywords.APPRATE_MESSAGE,
      //         cancelButtonLabel: keywords.APPRATE_CANCEL_BUTTON_LABEL,
      //         laterButtonLabel: keywords.APPRATE_LATER_BUTTON_LABEL,
      //         rateButtonLabel: keywords.APPRATE_RATE_BUTTON_LABEL,
      //         yesButtonLabel: keywords.APPRATE_YES_BUTTON_LABEL,
      //         noButtonLabel: keywords.APPRATE_NO_BUTTON_LABEL,
      //         appRatePromptTitle: keywords.APPRATE_APPRATE_PROMPT_TITLE,
      //         feedbackPromptTitle: keywords.APPRATE_FEED_BACK_PROMPT_TITLE,
      //     },
      //     openUrl: (url) => window.open(url, '_blank', 'location=yes')
      //
      // });
  }, []);

    // this is the complete list of currently supported params you can pass to the plugin (all optional)
    const options = {
        message: keywords.APPSHARE_MESSAGE, // not supported on some apps (Facebook, Instagram)
        subject: keywords.APPSHARE_SUBJECT, // fi. for email
        // files: ['', ''], // an array of filenames either locally or remotely
        url: 'https://euronewsgeorgia.com/apps',
        chooserTitle: keywords.APPSHARE_CHOOSER_TITLE, // Android only, you can override the default share sheet title
        // appPackageName: 'ge.above.euronewsgeorgia', // Android only, you can provide id of the App you want to share with
        // iPadCoordinates: '0,0,0,0' //IOS only iPadCoordinates for where the popover should be point.  Format with x,y,width,height
    };

    // var onSuccess = function(result) {
    //     console.log("Share completed? " + result.completed); // On Android apps mostly return false even while it's true
    //     console.log("Shared to app: " + result.app); // On Android result.app since plugin version 5.4.0 this is no longer empty. On iOS it's empty when sharing is cancelled (result.completed=false)
    // };
    //
    // var onError = function(msg) {
    //     console.log("Sharing failed with message: " + msg);
    // };

    const showSocialShare = () => {
        SocialSharing.shareWithOptions(options).then((result) => {
            console.log("Share completed? " + result.completed); // On Android apps mostly return false even while it's true
            console.log("Shared to app: " + result.app); // On Android result.app since plugin version 5.4.0 this is no longer empty. On iOS it's empty when sharing is cancelled (result.completed=false)
        }).catch((msg) => {
            console.log("Sharing failed with message: " + msg);
        });
    }
  return (
    <IonPage>
      <IonHeader className="ion-no-border en-settings-header">
        <IonBackButton icon={chevronBackOutline} className='ion-float-left ion-no-padding en-settings-back-button'/>
        <IonTitle className='en-settings-title ion-no-padding'>{keywords.ABOUT_THIS_APP}</IonTitle>
      </IonHeader>
      <IonContent fullscreen>

      <IonList>
      
          <IonItem onClick={()=>setShowModal(true) } className='ion-padding-start ion-padding-end en-services-item-title'>
            <IonIcon icon={bulbOutline} className='ion-padding-end' />
            <IonLabel>{keywords.TUTORIAL}</IonLabel>
            <IonIcon icon={chevronForwardOutline} />
            
          </IonItem>
          <IonModal
                isOpen={showModal}
                className='en-modal-about'
                swipeToClose={true}
                
              >
                 <IonText className='ion-no-margin'>
                  <IonTitle className='en-modal-title ion-no-margin' style={darkTheme ? {background:"#121212" , color:"#fff"} :  {background: "#fff" , color: "#000"} }>
                    <IonButtons onClick={()=> CloseModal()} className='ion-float-right en-modaltutorial-close' ><IonIcon icon={closeOutline} style={darkTheme ? {color:"#fff"} :  {color: "#000"} }/></IonButtons>
                  </IonTitle>
                </IonText>
                <ModalAboutThisApp/>
          </IonModal>


          <IonItem className='ion-padding-start ion-padding-end en-services-item-title' onClick={() => setShowAlert1(true)} >
            <IonIcon icon={phonePortraitOutline} className='ion-padding-end' />
              <IonLabel>{keywords.CONTACT_US}</IonLabel>
            <IonIcon icon={chevronForwardOutline} />
            
            <IonAlert
              isOpen={showAlert1}
              onDidDismiss={() => setShowAlert1(false)}
              cssClass='my-custom-class'
              header={'დაგვიკავშირდით'}
              
              message={'შეგიძლიათ დაგვიკავშირდეთ მითითებულ ტელეფონზე: <br/><br/> +995 32 2448400'}
              buttons={['OK']}
            />
          </IonItem>

          <IonItem className='ion-padding-start ion-padding-end en-services-item-title'  onClick={() => setShowAlert2(true)}>
            <IonIcon icon={chatboxOutline} className='ion-padding-end' />
            <IonLabel>{keywords.CONTACT_THE_NEWSROOM}</IonLabel>
            <IonIcon icon={chevronForwardOutline} />
              <IonAlert
                  isOpen={showAlert2}
                  onDidDismiss={() => setShowAlert2(false)}
                  cssClass='my-custom-class'
                  header={'დაუკავშირდი ნიუსრუმს'}

                  message={'შეგიძლიათ დაუკავშირდეთ ნიუსრუმს,მითითებულ ელ. ფოსტაზე: <br/><br/> <a href="mailto: euronews@euronewsgeorgia.com">euronews@euronewsgeorgia.com</a>'}
                  buttons={['OK']}
              />
          </IonItem>

          {/*<IonItem className='ion-padding-start ion-padding-end en-services-item-title' onClick={()=>AppRate.promptForRating(true)}>*/}
          {/*  <IonIcon icon={starOutline} className='ion-padding-end' />*/}
          {/*  <IonLabel>{keywords.RATE_US}</IonLabel>*/}
          {/*  <IonIcon icon={chevronForwardOutline} />*/}
          {/*</IonItem>*/}

          <IonItem className='ion-padding-start ion-padding-end en-services-item-title' onClick={showSocialShare}>
            <IonIcon icon={shareOutline} className='ion-padding-end' />
            <IonLabel>{keywords.SHARE_THIS_APP}</IonLabel>
            <IonIcon icon={chevronForwardOutline} />
          </IonItem>

        </IonList>
        
      </IonContent>
    </IonPage>
  );
};

export default AboutThisApp;
