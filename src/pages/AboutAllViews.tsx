import { IonBackButton, IonCol, IonContent, IonGrid, IonHeader, IonImg, IonPage, IonRow, IonTitle } from '@ionic/react';
import { chevronBackOutline } from 'ionicons/icons';
import './AboutAllViews.css';
import React from "react";


interface AboutAllViewsProps {
  keywords?: any;
}

const AboutAllViews: React.FC<AboutAllViewsProps> = ({keywords} : AboutAllViewsProps) => {
  return (
    <IonPage>
      <IonHeader className="ion-no-border en-views-header">
        <IonBackButton icon={chevronBackOutline} className='ion-float-left ion-no-padding en-settings-back-button'/>
        <IonTitle className='en-views-title'>{keywords.ABOUT_ALL_VIEWS}</IonTitle>
      </IonHeader>

      <IonContent fullscreen>

        <IonGrid className='en-views-content'>
          <IonRow className='ion-padding-start ion-padding-end'>
            <IonCol size='12'>
              <IonImg src='../assets/images/views.jpg'/>
              <p>
                Euronews' new baseline <b>All Views</b> is part of its
                goal to tackle <i>disinformation</i> and <i>infobesity</i> - the
                idea that in a world with more information than
                ever we are drowning in quantity and lacking
                perspective.
              </p>

              <p>
                Michael Peters, CEO of Euronews, explained the philosophy:
              </p>

              <p className='ion-padding-start ion-padding-end'>
                "Many are trying to impose on the
                consumer their own perspective
                about the news of the world. I think
                an organisation like us should try to
                empower the consumers. At
                Euronews we think that giving them
                as many views as possible is the
                answer".
              </p>

              <p>
                "All Views" is a single source promising to cover
                all relevant views on the latest developments.
                Euronews believes every voice should be heard,
                regardless of religion, nationality or culture. Our
                aim is to create one universal perspective, "All
                Views", through the diverse, multicultural input
                of our team drawn from 35 countries.
              </p>

              <IonImg src='../assets/images/views.jpg'/>
            </IonCol>
          </IonRow>
        </IonGrid>

      </IonContent>

    </IonPage>
  );
};

export default AboutAllViews;
