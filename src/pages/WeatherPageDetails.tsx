import {IonCol, IonContent, IonGrid, IonImg, IonPage, IonRow, IonText} from '@ionic/react';
import React, {useContext, useEffect, useRef, useState} from 'react';

import {darkThemeContext} from '../state/State'

import './WeatherPageNew.css';
// import "swiper/swiper.min.css";
// import "swiper/modules/pagination/pagination.scss";
// import "swiper/modules/navigation/navigation.scss";
import DayDetails from "../components/weather/DayDetails";
import {useParams} from "react-router";
import {Storage} from "@ionic/storage";
import {useHistory} from "react-router-dom";
import Header from "../components/Header";


var DB = require("../JSON/DB.json");


interface WeatherPageDetailProps {
    keywords?: any;
    title: any;
}

interface ParamTypes {
    day_index: string
}
interface City {
    name: string;
    location: string;
}
const WeatherPageDetails: React.FC<WeatherPageDetailProps> = ({keywords}: WeatherPageDetailProps) => {

    const [darkTheme] = useContext<any>(darkThemeContext);
    const [Location, setLocation] = useState("");
    const GEOCODE = useRef("41.70301989775022,44.79371192597676");
    const [hourlyData, setHourlyData] = useState<any>();
    const [dailyData, setDailyData] = useState<any>();
    const [globalAirQualityData, setGlobalAirQualityData] = useState<any>();
    const [mosquitoData, setMosquitoData] = useState<any>();
    const [painData, setPainData] = useState<any>();

    const [currentTemprature, setCurrentTemprature] = useState(0);
    const [currentTempratureDesc, setCurrentTempratureDesc] = useState("");
    const [dailyIndex, setDailyIndex] = useState(0);
    const [dailyDayPartIndex, setDailyDayPartIndex] = useState(0);

    const [airQualityIndex, setAirQualityIndex] = useState(0);
    const [airQualityCat, setAirQualityCat] = useState("");
    const [airQualityMsg, setAirQualityMsg] = useState("");
    const [uvIndex, setUvIndex] = useState(0);
    const [uvIndexText, setUvIndexText] = useState("");
    const [sunset, setSunset] = useState("");
    const [sunrise, setSunrise] = useState("");
    const [direction, setDirection] = useState("");
    const [windSpeed, setWindSpeed] = useState(0);
    const [todayPrecipitation, setTodayPrecipitation] = useState(0);
    const [nextDayPrecipitation, setNextDayPrecipitation] = useState(0);
    const [realFeel, setRealFeel] = useState(0);
    const [humidity, setHumidity] = useState(0);
    const [dewPoint, setDewPoint] = useState(0);
    const [fov, setFov] = useState(0);
    const [currentPressure, setCurrentPressure] = useState(0);

    const [mosquitoIndex, setMosquitoIndex] = useState(0);
    const [mosquitoDesc, setMosquitoDesc] = useState("");

    const [painIndex, setPainIndex] = useState(0);
    const [painDesc, setPainDesc] = useState("");

    const [selectedCity, setSelectedCity] = useState<City>();
    const history = useHistory();
    const [gridBackground, setGridBackground] = useState("");

    const store = new Storage();

    let { day_index } = useParams<ParamTypes>();

    const checkNightOrDay = async () => {
        const store = new Storage();
        await store.create();
        let hourlyData = await store.get("hourlyData");
        if(hourlyData.dayOrNight[0] == "D") {
            setGridBackground("../assets/images/day_back.png");
        } else {
            setGridBackground("../assets/images/night_back.png");
        }
    }

    useEffect(() => {
        if(!hourlyData) {
            store.create().then(() => {
                // store.get("hourlyData").then((hourlyData) => {
                //     setHourlyData(hourlyData);
                //     setCurrentTempratureDesc(hourlyData?.wxPhraseLong[0]);
                //     setCurrentTemprature(hourlyData?.temperature[0]);
                // });
                // store.get("dailyData").then((dailyData) => {
                //     setDailyData(dailyData);
                // });
                // store.get("globalAirQualityData").then((globalAirQualityData) => {
                //     setGlobalAirQualityData(globalAirQualityData);
                // });

            });

            const getDataFromLocal = async () => {
                const store = new Storage();
                await store.create();
                let hData = await store.get("hourlyData");
                let dData = await store.get("dailyData");
                let gData = await store.get("globalAirQualityData");
                let mData = await store.get("mosquitoData");
                let pData = await store.get("painData");
                setMosquitoData(mData);
                setPainData(pData);
                setHourlyData(hData);
                setDailyData(dData);
                setGlobalAirQualityData(gData);
                setCurrentTempratureDesc(hData?.wxPhraseLong[0]);
                setCurrentTemprature(hData?.temperature[0]);
            }
            getDataFromLocal();
        } else {
            checkNightOrDay();
        }
        // getSelectedCityFromStorage();
    }, [hourlyData]);

    useEffect(() => {
        if(hourlyData && dailyData && globalAirQualityData) {
            init();
        }
    }, [hourlyData,dailyData, globalAirQualityData]);


    useEffect(() => {
        if(day_index && dailyData)
            init();
    }, [day_index, dailyData]);


    const init = () => {
        if(day_index) {
            getSelectedCityFromStorage();
            // console.log("day_index", day_index);
            // console.log("globalAirQualityData", globalAirQualityData);
            // console.log("dailyData", dailyData);
            // console.log("hourlyData", hourlyData);
            let dIndexParsed = parseInt(day_index);

            let hoursTillTodayMidnight = hourlyData?.hourly.indexOf("00");

            // console.log("hoursTillTodayMidnight", hoursTillTodayMidnight);

            let hourlyDayIndex = ((dIndexParsed * 24) - 12) + hoursTillTodayMidnight;

            if(dIndexParsed == 0) {
                hourlyDayIndex = 0;
            }
            // console.log("day_index", day_index);
            setDailyIndex(dIndexParsed);

            setDailyDayPartIndex(dIndexParsed * 2);
            if(dIndexParsed == 0) {
                setAirQualityIndex(globalAirQualityData?.globalairquality?.airQualityIndex);
                setAirQualityCat(globalAirQualityData?.globalairquality?.airQualityCategory);
                setAirQualityMsg(globalAirQualityData?.globalairquality?.messages?.General?.text);
            }
            // setUvIndex(dailyData?.daypart[0]?.uvIndex[dIndexParsed * 2]);
            // setUvIndexText(dailyData?.daypart[0]?.uvDescription[dIndexParsed * 2]);
            setUvIndex(hourlyData?.uvIndex[hourlyDayIndex]);
            setUvIndexText(hourlyData?.uvDescription[hourlyDayIndex]);

            console.log("sunrise date is: ", dailyData?.sunriseTimeLocal[dIndexParsed]);

            setSunset(new Date(dailyData?.sunsetTimeLocal[dIndexParsed])?.toLocaleTimeString('en-US', {  hour12: false }).replace(/(.*)\D\d+/, '$1'));
            setSunrise(new Date(dailyData?.sunriseTimeLocal[dIndexParsed])?.toLocaleTimeString('en-US', {  hour12: false }).replace(/(.*)\D\d+/, '$1'));
            setDirection(hourlyData?.windDirectionCardinal[hourlyDayIndex]);
            setWindSpeed(hourlyData?.windSpeed[hourlyDayIndex]);
            setTodayPrecipitation(dailyData?.daypart[0]?.precipChance[dIndexParsed * 2]);
            setNextDayPrecipitation(dailyData?.daypart[0]?.precipChance[(dIndexParsed * 2) + 2]);
            setRealFeel(hourlyData?.temperatureFeelsLike[hourlyDayIndex]);
            setHumidity(hourlyData?.relativeHumidity[hourlyDayIndex]);
            setDewPoint(hourlyData?.temperatureDewPoint[hourlyDayIndex]);
            setFov(hourlyData?.visibility[hourlyDayIndex]);
            setCurrentPressure(hourlyData?.pressureMeanSeaLevel[hourlyDayIndex]);

            setMosquitoIndex(mosquitoData?.mosquitoIndex24hour?.eveningMosquitoIndex[dIndexParsed]);
            setMosquitoDesc(mosquitoData?.mosquitoIndex24hour?.eveningMosquitoCategory[dIndexParsed]);

            // console.log("mosquitoData",mosquitoData?.mosquitoIndex24hour?.eveningMosquitoIndex[dIndexParsed], dIndexParsed);

            let selectedDate = dailyData?.validTimeLocal[dIndexParsed];
            let index = painData?.achesPainsIndex12hour?.fcstValidLocal.map(function(o:any) { return o; }).indexOf(selectedDate);

            if(!index || index < 0) {
                index = 0;
            }

            // console.log("selectedDate",selectedDate);
            // console.log("index",index);
            setPainIndex(painData?.achesPainsIndex12hour?.achesPainsIndex[index]);
            setPainDesc(painData?.achesPainsIndex12hour?.achesPainsCategory[index]);
        }
    }

    const getSelectedCityFromStorage = async () => {
        const store = new Storage();
        await store.create();
        let city: City = await store.get('selected_city');
        if(!city) {
            history.push("/weather");
        }
        setSelectedCity(city);
    }

    useEffect(() => {

    }, [painIndex, painDesc]);

    useEffect(() => {
        getSelectedCityFromStorage();
    }, []);


    return (
        <IonPage>

            {/*<IonToolbar>*/}
            {/*    <IonButtons slot="start">*/}
            {/*        <IonBackButton/>*/}
            {/*    </IonButtons>*/}
            {/*    /!*<IonTitle>Back Button</IonTitle>*!/*/}
            {/*</IonToolbar>*/}
            <Header showBackButton={true}/>
            <IonContent id="weather-content" className='en-weather'>
                <IonGrid className='en-weather-new-content-parent' style={{ backgroundImage: "url('" + gridBackground + "')"}}>
                    <IonRow>
                        <IonCol className="weather-detail-top-section-parent">
                            <IonText style={{textAlign: "center", color: (hourlyData?.dayOrNight[0] == "D") ? "#41678c" : ""}}>
                                {/*<h3 className="d-inline weather-main-top-section-city-name" onClick={() => history.goBack()}>BACK</h3>*/}
                                <h3 className="d-inline weather-main-top-section-city-name">{selectedCity?.name}</h3>
                                <h3 className="d-inline weather-detail-top-section-degree">{currentTemprature}°C / {currentTempratureDesc}</h3>
                            </IonText>
                        </IonCol>
                        <IonCol size='12' style={{textAlign: 'center'}} className='en-weather-title'>
                        </IonCol>
                    </IonRow>
                    {
                        hourlyData ?
                            <DayDetails
                                keywords={keywords}
                                airQualityIndex={airQualityIndex}
                                airQualityCat={airQualityCat}
                                airQualityMsg={airQualityMsg}
                                uvIndex={uvIndex}
                                uvIndexText={uvIndexText}
                                sunset={sunset}
                                sunrise={sunrise}
                                direction={direction}
                                windSpeed={windSpeed}
                                todayPrecipitation={todayPrecipitation}
                                nextDayPrecipitation={nextDayPrecipitation}
                                realFeel={realFeel}
                                humidity={humidity}
                                dewPoint={dewPoint}
                                fov={fov}
                                currentPressure={currentPressure}
                                painData={painData}
                                mosquitoIndex={mosquitoIndex}
                                mosquitoDesc={mosquitoDesc}
                                painIndex={painIndex}
                                painDesc={painDesc}
                            />
                        :
                        ""
                    }

                    <IonCol>
                        <div className="en-weather-copernicus-div">
                            <IonImg style={{height:"50px"}} src="../assets/images/copernicus.png" />
                        </div>
                    </IonCol>

                </IonGrid>

            </IonContent>
        </IonPage>
    );
};

export default WeatherPageDetails;
