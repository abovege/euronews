import {  IonInfiniteScroll, IonInfiniteScrollContent, IonListHeader,IonHeader, IonContent, IonGrid, IonPage, IonSkeletonText, } from '@ionic/react';
import './JustIn.css';
import Header from '../components/Header';
import JustInStory from '../components/JustInStory';
import React, { useContext,useEffect, useState } from 'react';
import FadeIn from 'react-fade-in';
import {offlineModeContext} from '../state/State'
import OfflineHeader from '../components/OfflineHeader'
import { Storage } from '@ionic/storage';
import {darkThemeContext} from '../state/State'
import Ad from "../components/Ad";

interface JustInProps {
  keywords?: any;
  justInStories?: any;
  justinStoryFunction?: any;
  adSection: any
}

const JustIn: React.FC<JustInProps> = ({keywords,justInStories,justinStoryFunction, adSection} : JustInProps) => {

  const [page, setPage] = useState<any>(1);
  const [offlineMode] = useContext<any>(offlineModeContext)
  const [darkTheme] = useContext<any>(darkThemeContext);
  const [time,setTime] = useState<any>("დღეს");
  const [currentTime,setCurrentTime] =useState<any>();



  useEffect(()=>{
    setCurrentTime(new Date())
  },[])

  function padTo2Digits(num: any) {
    return num.toString().padStart(2, '0');
  }

  function formatDate(date: any) {
    return [
      date.getFullYear(),
      padTo2Digits(date.getMonth() + 1),
      padTo2Digits(date.getDate()),
    ].join('-');
  }

  const getFormattedDate = (pDate: any) => {
    const year = new Date().getFullYear();
    const month = pDate?.substring(0,2);
    const day = pDate?.substring(3,5);
    //
    const cDate = new Date(`${year}/${month}/${day}`);
    const today = new Date;
    const yesterday = new Date;
    yesterday.setDate(today.getDate() - 1)
    if (cDate.toLocaleDateString() == today.toLocaleDateString()) {
      return 'დღეს '
    } else if (cDate.toLocaleDateString() == yesterday.toLocaleDateString()) {
      return 'გუშინ '
    }

    return month + '/' + day;
  }

  const onScroll = () => {
    let element = document.elementFromPoint(130,150)
    let finalElem = element?.getElementsByTagName('p')[0]
    // const formattedDate = getFormattedDate(finalElem?.innerText.toString());
    const formattedDate = getFormattedDate(finalElem?.dataset.date?.toString());
    finalElem?.innerText != undefined && setTime(formattedDate)
    // time == formatDate(currentTime).substring(5,10).replace("-","/").replace("-","/") && setTime("დღეს")
  }

  const [isInfiniteDisabled, setInfiniteDisabled] =  useState(false);


  const loadData = (ev: any) => {
    setTimeout(() => {
      justinStoryFunction();
      ev.target.complete();
      if (justInStories.length === 1000) {
        setInfiniteDisabled(true);
      }
    }, 500);
  }
  return (
      <IonPage>
        <Header />
        <IonHeader>
          <IonListHeader className='en-justin-day-title' style={darkTheme ? {background:"#000000"}:{background:"#163a5c"}}>
            {time}
          </IonListHeader>
        </IonHeader>
        <IonContent
            scrollEvents={true}
            onIonScroll={onScroll}
            fullscreen>

          <IonGrid  className='ion-no-padding'>
            {justInStories ? justInStories.map((data: any,key: any)=>{
                  return (
                      <FadeIn  key={key}>
                        {(key == 5 || ((key-5) % 10 == 0 && key!=0)) ?
                            <Ad adSection={adSection} keywords={keywords}/>
                        : ""}
                        <JustInStory data={data} ID={data.ID}/>
                      </FadeIn>
                  )})

                : (<IonSkeletonText animated style={{ width: '100%' }} />
                )  }
          </IonGrid>
          {offlineMode && offlineMode ? (
                  <>
                  </>
              )
              :
              (
                  <>
                    <IonInfiniteScroll
                        onIonInfinite={loadData}
                        threshold="100px"
                        disabled={isInfiniteDisabled}
                    >
                      <IonInfiniteScrollContent
                          loadingSpinner="dots"
                          loadingText=""
                      ></IonInfiniteScrollContent>
                    </IonInfiniteScroll>
                  </>
              )}

        </IonContent>
      </IonPage>
  );
};

export default JustIn;
