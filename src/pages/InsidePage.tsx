import { IonSkeletonText,IonContent, IonHeader, IonPage, IonTitle, IonGrid, IonRow, IonCol, IonImg, IonText, IonBackButton } from '@ionic/react';
import { chevronBackOutline } from 'ionicons/icons';
import React, { useEffect, useState, useContext } from 'react';
import Labels from '../components/Labels';
import SmallStory from '../components/SmallStory';
import BigStory from '../components/BigStory';
import './InsidePage.css';
import axios from "axios";
import { Storage } from '@ionic/storage';
import FadeIn from 'react-fade-in';
import {offlineModeContext} from '../state/State'
import OfflineMode from "../components/OfflineMode"
import Ad from "../components/Ad";
import {useLocation} from "react-router-dom";
interface InsidePageProps {
  keywords?: any;
  title: any;
  match?: any;
  adSection?:any
}



const InsidePage: React.FC<InsidePageProps> = ({match, keywords, title, adSection} :InsidePageProps) => {
  const [featuredSectionContent,setFeaturedSectionContent] = useState<any[]>([]);
  const [offlineMode,setOfflineMode] = useContext<any>(offlineModeContext)
  const [test,setTest] = useState(false);
  const [smalStoryAd,setSmalStoryAd] = useState();
  const [newCategoryID,setNewCategoryID] = useState<any>();
    const location = useLocation();

  let catText = title;
  if(match.params.cat){
    if(keywords[match.params.cat]){
      catText = keywords[match.params.cat];
    }else{
      catText = match.params.cat;
    }
  }



    useEffect(() => {
      offlineMode == false && fetchFeaturedContent();
    }, [offlineMode])

    useEffect(() => {
      offlineMode == false && fetchFeaturedContent();
        // console.log("adsectiopn ", adSection);

    },[]);

    const findProperAd = async () => {
        let isMounted = true;
        const store = new Storage();
        await store.create();
        const ads = await store.get("ads");
        let obj = ads.find((o:any) => o.cat_id?.toLowerCase() === catText?.toLowerCase());
        // console.log("catText: ", catText);
        // console.log("obj: ", obj);
        setSmalStoryAd(obj);
        return () => { isMounted = false };
    };


    /************
     *
     * Rerender the component
     *
     * */
    const
        routes = [{ name: 'inside_page', path: '/insidepages/:cat' }],
        escapeDots = (s:any) => Array.from(s, c => c === '.' ? '\\.' : c).join('');

    useEffect(() => {
        // console.log("location changing...!", order_id, item_id,(location?.state as any)?.item_id);

        const result =
            routes.find(o => new RegExp(`^${o.path
                .split('/')
                .map(s => s.startsWith(':') ? '[^\/]+': escapeDots(s))
                .join('\/')
            }$`).test(location.pathname));

        const arr = location.pathname.split("/")
        const POSTID = arr[arr.length-1].length
        const STID = location.pathname.substring(location.pathname.length - POSTID);

        if(result && result.name == "inside_page") {
            setNewCategoryID(STID);
        }
    }, [location]);

    useEffect(() => {
        fetchFeaturedContent();
    },[newCategoryID]);

    /************
     * End rerender the component
     * */

    const fetchFeaturedContent = async () =>{
        setFeaturedSectionContent([]);
        await findProperAd();
      let isMounted = true;               
      const store = new Storage();
      await store.create();
      const body = {
                    orderby : "post_date", 
                    order : "DESC", 
                    post_type: "post", 
                    post_count : "22", 
                    page : "1", 
                    taxonomy : "category",
                    taxonomy_field  : "name",
                    taxonomy_values   : [  catText ],
                    cache_key : "string cache key that get from end of the last response"
                  };
        axios.post('https://euronewsgeorgia.com/wp-json/above_rest_api/v1/fetch/posts/', body)
            .then(response => {

              setFeaturedSectionContent(response.data.posts);
            })
            .catch(error => {
                console.error('There was an error!', error);
            });
    return () => { isMounted = false };
    }

  
 

  return (
    <IonPage>
      <IonHeader className="ion-no-border en-header-inside en-header-bg ion-no-padding">
          <IonBackButton color={"dark"} icon={chevronBackOutline} className='ion-float-left ion-no-padding en-inside-back'/>
          <IonTitle className='en-inside-title ion-no-padding'>
            {catText}
          </IonTitle>
      </IonHeader>

      {offlineMode && offlineMode ? ( 
       <OfflineMode/>
      ) 
      : featuredSectionContent.length > 0 ?
      (
        <>
      <IonContent >
       {featuredSectionContent.map((data,index)=>{
        if(index == 0 || index == 10){
          return (
            <FadeIn key={index}>
            <BigStory  keywords={keywords}  data={data} ID={data.ID}/>
            </FadeIn>
            )
        } else {
          return (
            <FadeIn key={index}>
                {smalStoryAd && index == 15 ?
                    <div style={{marginBottom: "10px"}}>
                        <Ad adSection={smalStoryAd} keywords={keywords}/>
                    </div>
                    : adSection && index == 15 ? (<div style={{marginBottom: "10px"}}>
                        <Ad adSection={adSection} keywords={keywords}/>
                    </div>) : ""}
            <SmallStory key={index} data={data} ID={data.ID}/>
            </FadeIn>
          )
        }
      
      })}
      </IonContent>
      </>
      )  :
      (
        <>
        <IonContent >
        <IonGrid className='ion-no-padding'>
        <IonRow className='ion-no-padding'>
            <IonCol style={{height:"200px",display:"flex",alignItems: 'center',justifyContent:"center",background:"#e2e2e2"}} className='ion-no-padding'>
                <IonSkeletonText className="animate-skeleton-background" style={{ width: '70%',height:"80px"}}/>
            </IonCol>
        </IonRow>
      </IonGrid>

      <IonGrid className='en-storypage-content'>
        <IonSkeletonText animated style={{ width: '30%' }} />

        <h3 className='en-storypage-title'>
        <IonSkeletonText animated style={{ width: '100%' }} />
        <IonSkeletonText animated style={{ width: '100%' }} />
        <IonSkeletonText animated style={{ width: '30%' }} />

        </h3>
        <div className='en-storypage-time'>
        <IonSkeletonText animated style={{ width: '30%'}} />
        <br></br>
        </div>
        <br></br>
        <div className='en-storypage-by'><i> 
        <IonSkeletonText animated style={{ width: '100%', }} />
          </i></div>
          <IonGrid className='en-storypage-description'>
          <p >
          <IonSkeletonText animated style={{ width: '100%' }} />
          <IonSkeletonText animated style={{ width: '100%' }} />
          <IonSkeletonText animated style={{ width: '100%' }} />
          <IonSkeletonText animated style={{ width: '100%' }} />
          <IonSkeletonText animated style={{ width: '100%' }} />
          <IonSkeletonText animated style={{ width: '100%' }} />
          <IonSkeletonText animated style={{ width: '100%' }} />
          <IonSkeletonText animated style={{ width: '100%' }} />
          <IonSkeletonText animated style={{ width: '100%' }} />
          <IonSkeletonText animated style={{ width: '100%' }} />
          <IonSkeletonText animated style={{ width: '100%' }} />
          <IonSkeletonText animated style={{ width: '100%' }} />
          </p>
          </IonGrid>
      </IonGrid>
      </IonContent>
      </>
        )}
    </IonPage>
  );
};

export default InsidePage;
