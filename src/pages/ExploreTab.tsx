import {IonSearchbar, IonButton, IonButtons, IonCol, IonContent, IonGrid, IonImg, IonInfiniteScroll, IonPage, IonRow, IonSegment, IonSegmentButton, IonText, IonTitle, IonVirtualScroll, IonIcon, IonRouterLink } from '@ionic/react';
import ExploreContainer from '../components/ExploreContainer';
import './ExploreTab.css';
import React, {useContext, useEffect, useState } from 'react';
import FadeIn from 'react-fade-in';
import Header from '../components/Header';
import { chevronForwardOutline } from 'ionicons/icons';
import { Link } from 'react-router-dom';
import axios from "axios";
import { Storage } from '@ionic/storage';
import OfflineMode from "../components/OfflineMode"
import {offlineModeContext} from '../state/State'
import OfflineHeader from '../components/OfflineHeader'
import {darkThemeContext} from '../state/State'
import ReactPlayer from 'react-player';

interface ExploreTabProps {
  keywords?: any;
  featuredSection?: any;
  themesContent? : any;
  hotTopics? : any;
  rubricSection?: any;
  adSection?: any;
}

const ExploreTab: React.FC<ExploreTabProps> = ({keywords,rubricSection,featuredSection,themesContent,hotTopics,adSection} : ExploreTabProps) => {
  const [searchText, setSearchText] = useState('');
  const [darkTheme] = useContext<any>(darkThemeContext);
  const [offlineMode] = useContext<any>(offlineModeContext)
  const isImage = ['gif','jpg','jpeg','png']; //you can add more
  const isVideo =['mpg', 'mp2', 'mpeg', 'mpe', 'mpv', 'mp4'] // you can add more extention

  // adSection.img = 'https://euronewsgeorgia.com/wp-content/uploads/2022/11/Advertisement.mp4';


  return (
   
    <IonPage>
      <Header />
      {offlineMode && offlineMode ? (
      <>
      <OfflineMode/>
      <OfflineHeader />
      </>
     ):(
       <>
            <IonContent >
        <IonGrid className='en-home-search' style={darkTheme ? {background:"#000"}:{background:"#f1f5f6"}}>
          <IonRow>
            <IonCol>
              {/*<IonTitle className='en-home-search-title'>{keywords.SEARCH}</IonTitle>*/}
              <IonRouterLink color="dark" routerLink="/search">
                <IonSearchbar style={darkTheme ? {color:"black"} : {color:"black"}} value={searchText} onIonChange={e => setSearchText(e.detail.value!)} placeholder={keywords.SEARCH}/>
              </IonRouterLink>
            </IonCol>
          </IonRow>
        </IonGrid>

        <IonGrid className="en-home-hot-topics" style={darkTheme ? {background:"#1e1e1e"}:{background:"#ffffff"}}>
          <FadeIn>
            <IonRow>
              <IonCol size='12'>
                <IonTitle className='en-home-hot-topics-title' style={darkTheme ? {color:"#ffffff"}:{color:"#2f70e6"}}>{keywords.HOT_TOPICS}</IonTitle>
                {hotTopics && hotTopics.map((item: any,index: any) => {

                  return ( 
                    <div key={index} className='en-home-hot-topics-category' style={darkTheme ? {background:"#ffffff"}:{background:"#2f70e6"}}>
                      <Link className="en-home-hot-topics-category-link"  to={`/insidepages/${item.cat_id}`} style={darkTheme ? {background:"#ffffff"}:{background:"#2f70e6"}}>
                      <div className='en-home-hot-topic-div'>
                        <span style={darkTheme ? {color:"#000"}:{color:"#fff"}}>{item.cat_id}</span>
                        <IonIcon icon={chevronForwardOutline} className='ion-float-right en-home-icon' style={darkTheme ? {color:"#000"}:{color:"#fff"}}/>
                        </div>
                      </Link>
                    </div >
                )
                })}

              </IonCol>
            </IonRow>
          </FadeIn>
        </IonGrid>

        <IonGrid className='en-home-featured-sections ion-no-padding' style={darkTheme ? {background:"#000000"}:{background:"#f1f5f6"}}>
          <IonTitle className='en-home-featured-sections-title'>{keywords.FEATURED_SECTIONS}</IonTitle>
          <FadeIn>
          <IonRow className='ion-no-padding'>
        
            {featuredSection && featuredSection.map((item: any,index: any) => {
                return ( 
                  <IonCol key={index} size='6'>
                    
                    <IonRouterLink color="dark" routerLink={`/insidepages/${item.cat_id}`}>
                      <IonImg src={item.img} className='en-home-featured-section-travel'/>
                    </IonRouterLink>
                    
                  </IonCol>
              )
            })}   
            
          </IonRow>
          </FadeIn>
        </IonGrid>

        <IonGrid className='en-explore-theme ion-no-padding'>
          <IonTitle className='en-home-featured-sections-title'>{keywords.THEMES}</IonTitle>

          <IonRow className='ion-no-padding'>
          {themesContent && themesContent?.map((item: any,index: any) => {
            return (
            <IonCol key={index} className="theme-section ion-no-padding" size='6'>
              <IonRouterLink className='en-home-featured-section-travel' routerLink={`/insidepages/${item?.cat_id}`}>
                <div className="explore-overlay">
                  <IonImg className="theme-img" src={item?.img} />
                  <IonTitle class="centered">{item?.cat_id}</IonTitle>
                </div>
              </IonRouterLink>
            </IonCol>
              )
            })}
          </IonRow>
        </IonGrid>

              {adSection?.img ? (
                  <IonGrid className='ion-no-padding'>
                    <IonRow className='en-home-ad' style={darkTheme ? {background:"#3a3a3a"}:{background:"#f1f5f6"}}>
                      <IonCol>
                        <IonTitle className='en-home-ad-title'>{keywords.ADVERTISEMENT}</IonTitle>
                        {/*<ReactPlayer playsinline={true} width={"100%"} height={225}  muted={true} loop={true} playing={true}  url="../assets/videos/Advertisement.mp4" light={false} />*/}
                        <Link to={{ pathname: adSection.url }} target="_blank" >
                        { isImage?.includes(adSection.img.slice((adSection.img.lastIndexOf(".") - 1 >>> 0) + 2)) && <IonImg className="theme-img" src={adSection.img} />}
                        { isVideo?.includes(adSection.img.slice((adSection.img.lastIndexOf(".") - 1 >>> 0) + 2)) && <ReactPlayer playsinline={true} width={"100%"} height={225}  muted={true} loop={true} playing={true}  url={adSection.img} light={false} />}
                        </Link>
                      </IonCol>
                    </IonRow>
                  </IonGrid>
              ) : ('')}


        {/*<IonGrid className='en-home-featured-sections ion-no-padding' >*/}
        {/*<Link */}
        {/*style={{textDecoration:"none"}}*/}
        {/*to={{*/}
        {/*        pathname: `/rubric`,*/}
        {/*        state: { */}
        {/*          data: rubricSection,*/}
        {/*        }*/}
        {/*    }}>*/}
        {/*    <IonTitle className='en-home-featured-sections-title en-home-featured-sections-title-PROGRAMMES' style={darkTheme ? {background:"#000000"}:{background:"#235383"}}>*/}
        {/*      {keywords.RUBRIC}*/}
        {/*      <IonIcon icon={chevronForwardOutline} className='ion-float-right en-home-icon'/>*/}
        {/*    </IonTitle>*/}
        {/*  </Link>*/}

        {/*  <IonRow className='ion-no-padding'>*/}
        {/*  {rubricSection && rubricSection.map((item: any,index: any) => {*/}
        {/*    return(*/}
        {/*    <IonCol key={index}  size='4'>*/}
        {/*      <IonRouterLink className='en-home-featured-section-travel' routerLink={`/insidepages/${item.cat_id}`}>*/}
        {/*        <IonImg src={item.img}/>*/}
        {/*      </IonRouterLink>*/}
        {/*    </IonCol>*/}
        {/*      )*/}
        {/*    })}*/}
        {/*  </IonRow>*/}
        {/*</IonGrid>*/}
        
      </IonContent>
       </>
     )}
 
    </IonPage>
  );
};

export default ExploreTab;
