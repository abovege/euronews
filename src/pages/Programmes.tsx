import { IonBackButton, IonButtons, IonCol, IonContent, IonGrid, IonHeader, IonImg, IonPage, IonRouterLink, IonRow, IonTitle } from '@ionic/react';
import './Programmes.css';
import React, { useEffect, useState } from 'react';
import { useHistory,useLocation } from 'react-router-dom';
import { chevronBackOutline } from 'ionicons/icons';

interface ProgrammesProps {
  keywords?: any;
}

const Programmes: React.FC<ProgrammesProps> = ({keywords} : ProgrammesProps) => {
  const data = useLocation<any>();
  const [postData,setPostData] = useState<any>([]);

  useEffect(() => {
    let isMounted = true; 
    if (isMounted){
      setPostData(data.state.data)
    }
    return () => { isMounted = false };  
  }, [])
  

  return (
    <IonPage>
      <IonHeader className="ion-no-border en-settings-header ion-no-padding">
        <IonBackButton icon={chevronBackOutline} className='ion-float-left ion-no-padding en-settings-back-button'/>
        <IonTitle className='en-settings-title ion-no-padding'>
          {keywords.RUBRIC}
        </IonTitle>
      </IonHeader>
      
      <IonContent fullscreen>

        <IonGrid className='ion-no-padding en-video-programmes'>

          <IonRow className='ion-no-padding'>
            {postData && postData.map((item:any,index: any)=>{
            return(
              <IonCol key={index} className='ion-no-padding ' size='6'>
              <IonRouterLink className='en-video-programmes-category-left' routerLink={`/insidepages/${item.cat_id}`}>
                <IonImg src={item.img}/>
              </IonRouterLink>
            </IonCol>
            )
            }) }
       

          </IonRow>
        </IonGrid>

      </IonContent>
    </IonPage>
  );
};

export default Programmes;
