import {IonSpinner, IonButton, IonButtons, IonCol, IonContent, IonGrid, IonIcon, IonImg, IonModal, IonPage, IonRippleEffect, IonRouterLink, IonRow, IonSegment, IonSegmentButton, IonText, IonTitle } from '@ionic/react';
import './Videos.css';
import Header from '../components/Header';
import Labels from '../components/Labels';
import ModalVideo from '../components/ModalVideo';
import GlobalStory from '../components/GlobalStory';
import { caretForwardCircleOutline, chevronForwardOutline, closeOutline } from 'ionicons/icons';
import React, { useState,useEffect,useContext } from 'react';
import { Link, useHistory,useLocation } from 'react-router-dom';
import axios from "axios";
import {offlineModeContext} from '../state/State'
import OfflineMode from "../components/OfflineMode"
import OfflineHeader from '../components/OfflineHeader';
import OfflineIcon from '../components/OfflineIcon';
import OfflineSmallStory from '../components/OfflineSmallStory';
import LinesEllipsis from 'react-lines-ellipsis'
import VideoContent from '../components/VideoContent'
import NoComment from '../components/NoComment';

import { Storage } from '@ionic/storage';
import Ad from "../components/Ad";


interface VideoProps {
  keywords?: any;
  articleAP?: string;
  noCommentContent?: any;
  globalConversationContent?: any;
  videoContent?: any;
  programsSection?: any;
  editorsChoiceContent?:any;
  editorsChoiceCat?:any;
  adSection: any;
}

const Videos: React.FC<VideoProps> = ({keywords,editorsChoiceCat,editorsChoiceContent,noCommentContent,globalConversationContent,videoContent,programsSection,adSection} : VideoProps) => {

  const [showModal, setShowModal] = useState(false);
  const [showLatestModal, setShowLatestModal] = useState(false);
  const [showCommentModal, setShowCommentModal] = useState(false);
  const [showMusicaModal, setShowMusicaModal] = useState(false);
  const [offlineMode] = useContext<any>(offlineModeContext);


  const CloseModal = () =>{
    setShowModal(false);
    setShowLatestModal(false);
    setShowCommentModal(false);
    setShowMusicaModal(false)
  } 

  const OpenModal = () => setShowModal(true)
  const OpenCommentModal = () => setShowCommentModal(true)
  const OpenLatestModal = () => setShowLatestModal(true)

  let location = useLocation();

  useEffect(()=>{
    CloseModal();
  },[location])


 

  const handleShowModal = () => {
    setShowModal(true)
  }

  const handleMusicaModal = () => {
    setShowMusicaModal(true)
  }
  let history = useHistory();


  
  var fadeElements = document.getElementsByClassName("scrollFade");

  function scrollFade() {
    var viewportBottom = window.scrollY + window.innerHeight;
    for (var index = 0; index < fadeElements.length; index++) {
      var element = fadeElements[index];
      var rect = element.getBoundingClientRect();
  
      var elementFourth = rect.height;
      var fadeInPoint = window.innerHeight - elementFourth;
      var fadeOutPoint = rect.height / 10;
  
      if (rect.top <= fadeInPoint) {
        element.classList.add("scrollFade--visible");
        element.classList.add("scrollFade--animate");
        element.classList.remove("scrollFade--hidden");
      } else {
        element.classList.remove("scrollFade--visible");
        element.classList.add("scrollFade--hidden");
      }
  
      if (rect.top <= fadeOutPoint) {
        element.classList.remove("scrollFade--visible");
        element.classList.add("scrollFade--hidden");
      }
    }
  }

  return (
    <IonPage>
      <Header darkBlueBg/>
      
      {offlineMode && offlineMode ? (
      <>
      <OfflineMode/>
      <OfflineHeader />
      </>
     ):(
     <>
     <IonContent >

      <IonGrid className='en-video-latest-programmes'>
        <IonRow className='ion-no-padding'>
          <IonCol className='ion-no-padding'>
              <IonRouterLink color='dark' routerLink='/listpagewithmodal/ვიდეო'>
                <IonTitle className='en-video-latest-programmes-title'>
                  {keywords.LATEST_PROGRAMMES}
                  <IonIcon icon={chevronForwardOutline} className='ion-float-right en-home-icon'/>
                </IonTitle>
              </IonRouterLink>
            <IonSegment scrollable>

                {videoContent && videoContent.map((data: any,index: any)=>{
                  if(index < 4){
                    return <VideoContent key={index} data={data} index={index} showModal={OpenLatestModal} />
                  }
                })}


              <IonSegmentButton onTouchStart={()=> history.push("/listpagewithmodal/ვიდეო")} className='en-video-latest-programmes-view-more'>

                <div>
                  {keywords.VIEW_MORE}
                  <IonIcon icon={chevronForwardOutline} className='ion-float-right en-home-icon'/>
                </div>

              </IonSegmentButton>

            </IonSegment>

            <IonModal
              isOpen={showLatestModal}
              className='en-modal'
              swipeToClose={true}
              onDidDismiss={() => setShowLatestModal(false)}
          
            >
            <IonContent
                  className="ionContentModal"
                  scrollEvents={true}
                  onIonScroll={() => scrollFade()}
                   >
              <IonText className='ion-no-margin'>
                <IonTitle className='en-modal-title ion-no-margin' >
                  {keywords.LATEST_PROGRAMMES}
                  <IonButtons onClick={() => setShowLatestModal(false)} className='ion-float-right en-modalvideo-close'><IonIcon icon={closeOutline} /></IonButtons>
                </IonTitle>
                {videoContent && videoContent.map((data: any,key: any)=>(<ModalVideo  key={key} data={data} ID={data.ID}/>))}
              </IonText>
              </IonContent>
            </IonModal>
          </IonCol>
        </IonRow>

      </IonGrid>


      <IonGrid className='ion-no-padding en-video-global-conversation'>
        <IonRouterLink color='dark' routerLink='/listpagewithmodal/ევრონიუს ჯორჯიას სტუმარი'>
          <IonTitle className='en-video-global-conversation-title'>
            {keywords.THE_GLOBAL_CONVERSATION}
            <IonIcon icon={chevronForwardOutline} className='ion-float-right en-home-icon'/>
          </IonTitle>
        </IonRouterLink>
        {globalConversationContent.length > 0 ? globalConversationContent.map((data: any,key: any)=>{
          if(key < 3){
            return (<GlobalStory toggleState={() => handleShowModal()} borderBottom key={key} data={data} ID={data.ID}/>)
          }
        })
      :
      <div style={{padding:"10px",width:"100%",display:"flex",justifyContent:"center", backgroundColor: "#081f2d"}}>
        <IonSpinner color="light" name="bubbles" />
      </div>    
      
      }
        <IonModal
          isOpen={showModal}
          className='en-modal'
          swipeToClose={true}
          onDidDismiss={CloseModal}
        >
           <IonContent
                  className="ionContentModal"
                  scrollEvents={true}
                  onIonScroll={() => scrollFade()}
                   >
          <IonText className='ion-no-margin'>
         
            <IonTitle className='en-modal-title ion-no-margin' >
              {keywords.THE_GLOBAL_CONVERSATION}
              <IonButtons onClick={CloseModal} className='ion-float-right en-modalvideo-close'><IonIcon icon={closeOutline} /></IonButtons>
            </IonTitle>
            {globalConversationContent && globalConversationContent.map((data: any,key: any)=>(<ModalVideo key={key} data={data} ID={data.ID}/>))}
          </IonText>
          </IonContent>
        </IonModal>

      </IonGrid>

       <Ad adSection={adSection} keywords={keywords}/>

      <IonGrid className='en-video-latest-programmes'>
        <IonRow className='ion-no-padding'>
          <IonCol className='ion-no-padding'>
            <IonRouterLink color='dark' routerLink='/listpagewithmodal/NO COMMENT'>
              <IonTitle className='en-video-latest-programmes-title'>
                {keywords.NO_COMMENT}
                <IonIcon icon={chevronForwardOutline} className='ion-float-right en-home-icon'/>
              </IonTitle>
            </IonRouterLink>

            <IonSegment scrollable>
              {noCommentContent.length > 0 ? 
              <>
              {noCommentContent.map((data: any,index: any)=>{
                if(index < 4){
                  return (
                    <IonSegmentButton key={index} className='en-video-latest-programmes-news' onClick={() => setShowCommentModal(true)}>
                    <IonImg src={data.post_meta.essb_cached_image ? data.post_meta.essb_cached_image[0] : '../assets/images/no-image.png'} className='en-video-latest-programmes-news-img'/>
                 {data &&  data.post_meta.td_post_video && <IonIcon icon={caretForwardCircleOutline} className='en-icon-nocomment-play'/>}
                 <div className='en-video-latest-programmes-news-content ion-activatable'>
                   <IonRippleEffect></IonRippleEffect>
                   <div className='en-video-latest-programmes-news-title'>{data && data.post_title}</div>
                   {/* <div className='en-video-latest-programmes-news-time'>{data && data.post_date_gmt}</div> */}
                 </div>
                 </IonSegmentButton> 
                  )
                }
               })}
             
              
              <IonSegmentButton className='en-video-latest-programmes-view-more' onTouchStart={()=> history.push("/listpagewithmodal/NO COMMENT")}>
                <div>
                  {keywords.VIEW_MORE}
                  <IonIcon icon={chevronForwardOutline} className='ion-float-right en-home-icon'/>
                </div>
                
              </IonSegmentButton>
              </>
              :
              <div style={{width:"100%",display:"flex",justifyContent:"center", backgroundColor: "#081f2d"}}>
              <IonSpinner color={"light"} name="bubbles" />
              </div>
              
              }
            </IonSegment>

            <IonModal
              isOpen={showCommentModal}
              className='en-modal'
              swipeToClose={true}
              onDidDismiss={() => setShowCommentModal(false)}
          
            >
           <IonContent
                  className="ionContentModal"
                  scrollEvents={true}
                  onIonScroll={() => scrollFade()}
                   >
              <IonText className='ion-no-margin'>
                <IonTitle className='en-modal-title ion-no-margin' >
                  {keywords.NO_COMMENT}
                  <IonButtons onClick={() => setShowCommentModal(false)} className='ion-float-right en-modalvideo-close'><IonIcon icon={closeOutline} /></IonButtons>
                </IonTitle>
                {noCommentContent && noCommentContent.map((data: any,key: any)=>(<ModalVideo key={key} data={data} ID={data.ID}/>))}

              </IonText>
              </IonContent>
            </IonModal>
          </IonCol>
        </IonRow>

      </IonGrid>

      <IonGrid className='ion-no-padding en-video-musica'>
        <IonRouterLink color='dark' routerLink={`/listpagewithmodal/${editorsChoiceCat}`}>
          <IonTitle className='en-video-musica-title'>
            {editorsChoiceCat}
            <IonIcon icon={chevronForwardOutline} className='ion-float-right en-home-icon'/>
          </IonTitle>
        </IonRouterLink>

        {editorsChoiceContent && editorsChoiceContent.map((data:any,key:any)=>{
          if(key < 4){
            return (<GlobalStory toggleState={() => handleMusicaModal()} borderBottom key={key} data={data} ID={data.ID}/>)
          }
          })}

        <IonModal
          isOpen={showMusicaModal}
          className='en-modal'
          swipeToClose={true}
          onDidDismiss={() => setShowMusicaModal(false)}
          
        >
           <IonContent
                  className="ionContentModal"
                  scrollEvents={true}
                  onIonScroll={() => scrollFade()}
                   >
          <IonText className='ion-no-margin'>
            <IonTitle className='en-modal-title ion-no-margin' >
              {keywords.MUSICA}
              <IonButtons onClick={() => setShowMusicaModal(false)} className='ion-float-right en-modalvideo-close'><IonIcon icon={closeOutline} /></IonButtons>
            </IonTitle>
            {editorsChoiceContent && editorsChoiceContent.map((data:any,key:any)=>(<ModalVideo key={key} data={data} ID={data.ID}/>))}
          </IonText>
          </IonContent>
        </IonModal>

      </IonGrid>

      <IonGrid className='ion-no-padding en-video-programmes'>
        <IonTitle className='en-video-programmes-title'>{keywords.PROGRAMMES}</IonTitle>

        <IonRow className='ion-no-padding'>
        {programsSection.length > 0 ?
        programsSection.map((item: any,index: any) => {
            return(
          <IonCol key={index} className='programs-section ion-no-padding ion-activatable' size='6'>
            <IonRippleEffect></IonRippleEffect>
            <IonRouterLink className='en-video-programmes-category-left' routerLink={`/listpagewithmodal/${item.cat_id}`}>
              <IonImg className="programs-img" src={item.img}/>
            </IonRouterLink>
          </IonCol>
            ) 
            })
            :
        <div style={{display:"flex",justifyContent:"center",width:"100%"}}>
            <IonSpinner  name="bubbles" />
        </div>
            }
        </IonRow>
      </IonGrid>
    </IonContent>
     </>
     )}
    </IonPage>
  );
};

export default Videos;
