import { CreateAnimation, Animation, IonButton, IonBackButton, IonCol, IonContent, IonGrid, IonHeader, IonIcon, IonPage, IonRow, IonTitle, IonCheckbox } from '@ionic/react';
import { bookmarkOutline, chevronBackOutline } from 'ionicons/icons';
import './ReadItLater.css';
import React, {useEffect,useState,useRef, Key, FormEvent, useContext } from 'react';
import { Storage } from '@ionic/storage';
import SmallStory from '../components/SmallStory';
import {useLocation} from "react-router-dom";
import {darkThemeContext} from '../state/State'



interface ReadItLaterProps {
  keywords?: any;
}

const ReadItLater: React.FC<ReadItLaterProps> = ({keywords} : ReadItLaterProps) => {
  const [readItLaterArray, setReadItLaterArray] = useState<any>([])
  const [selected, setSelected] = useState<any>([]);
  const [editMode, setEditMode] = useState<any>(false)
  const  store = new Storage();
  const location = useLocation()
  const [darkTheme] = useContext<any>(darkThemeContext);


  const displayReadItLater = async () =>{
    await store.create();
    if(await store.get("readItLaterList") !== null) {
    console.log(await store.get("readItLaterList"));
    setReadItLaterArray(await store.get("readItLaterList"));
    }
 }
  useEffect(() => {
    displayReadItLater();

    
    
  }, [location.key])


  const deleteChosen = async ()  =>{
    await store.create();
    let filtered: any[]=[];
     filtered = readItLaterArray.filter((item: { ID: any; }): any=> {
      return !selected.includes(item.ID);
    })
      setReadItLaterArray(filtered);
      setSelected([]);

      await store.set("readItLaterList",[...filtered])

  }

  const cleanArray = async () =>{
      console.log('shemovedi');
      await store.create();
      await store.set("readItLaterList",[])
      setReadItLaterArray([]);
      setEditMode(false);
   }

  const handleChange = (event: FormEvent<HTMLIonCheckboxElement>) => {
    const { checked, value } = event.currentTarget;
    setSelected((prev: any[]) => checked ? [...prev, value] : prev.filter(val => val !== value));

    console.log(selected);
};
  
  return (
    <IonPage>
      <IonHeader className="ion-no-border en-readitlater-header ion-no-padding">
        {editMode ? selected.length == 0 ? 
        <IonButton onClick={cleanArray} className='ion-float-left ion-no-padding en-delete-button'>წაშლა</IonButton> : 
        <IonButton onClick={deleteChosen} className='ion-float-left ion-no-padding en-delete-button'>წაშალე ({selected.length})</IonButton> :
         <IonBackButton icon={chevronBackOutline} className='ion-float-left ion-no-padding en-readitlater-back-button'/>}
        {/* <IonTitle className='en-readitlater-title ion-no-padding ion-no-margin-right'>{keywords.READ_IT_LATER}</IonTitle> */}
        { editMode ? <IonButton onClick={()=>{
          setEditMode(!editMode);
          setSelected([])}} className='ion-float-right en-cancel-button' >გაუქმება</IonButton> : readItLaterArray.length == 0 ? null :
           <IonButton 
          onClick={()=>{
            setEditMode(!editMode); 
          }} 
          className='ion-float-right ion-float-right en-edit-button' >რედაქტირება</IonButton> }
      </IonHeader>
      <IonContent >
        { readItLaterArray.length > 0 ? readItLaterArray.map((data: { ID: any; },key: Key | null | undefined)=>{
          return (
            <div key={key} className='en-readitlater-story'>
              <SmallStory  data={data} ID={data.ID}/>

              <div className='en-readitlater-story-checked' style={{width: editMode ? '15%' : '0' , background: darkTheme ? "rgba(0, 0, 0, .7)" : "rgba(255, 255, 255, .93)"}}>
                  <IonCheckbox onClick={(event) => handleChange(event)} 
                  onChange={(event) => handleChange(event)} 
                  checked={selected.some((val: any) => val === data.ID)}
                  value={data.ID} className='en-readitlater-story-checkbox'
                  style={{opacity: editMode ? '1' : '0' }}/>
                </div>
                { editMode &&
                <div id="pot" className={editMode && 'en-readitlater-story-checked testClassname'}>
                  <IonCheckbox onClick={(event) => handleChange(event)} 
                  onChange={(event) => handleChange(event)} 
                  checked={selected.some((val: any) => val === data.ID)}
                  value={data.ID} className='en-readitlater-story-checkbox'/>
                </div>
                }
            </div>
            
          )
        }) : (  
           <IonGrid className='en-read-content'>
        <IonRow>
          <IonCol size='12'>
            <IonIcon icon={bookmarkOutline} className='en-read-icon'/>
            <h2 className='en-read-text'>თქვენ ჯერ არ შეგირჩევიათ სტატია</h2>
          </IonCol>
        </IonRow>
      </IonGrid>)}
     
        
      </IonContent>
    </IonPage>
  );
};

export default ReadItLater;
