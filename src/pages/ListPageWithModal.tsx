import { IonSkeletonText, IonBackButton, IonButtons, IonCol, IonContent, IonGrid, IonHeader, IonIcon, IonImg, IonModal, IonPage, IonRouterLink, IonRow, IonText, IonTitle } from '@ionic/react';
import './ListPageWithModal.css';
import { bookmarkOutline, bookmark, chevronBackOutline, chevronForwardOutline, shareOutline } from 'ionicons/icons';
import GlobalStory from '../components/GlobalStory';
import React, { useState ,useEffect,useContext} from 'react';
import { useLocation} from 'react-router-dom';
import axios from "axios";
import { Storage } from '@ionic/storage';
import SmallStory from '../components/SmallStory';
import BigStoryDark from '../components/BigStoryDark';
import BigStory from '../components/BigStory';
import {offlineModeContext} from '../state/State'
import OfflineMode from "../components/OfflineMode"
import ListpagewithmodalStory from '../components/ListpagewithmodalStory';
import Ad from "../components/Ad";

interface ListPageWithModalProps {
  keywords?: any;
  title: any;
  match?: any;
  articleAP?: string;
  videoAP?: string;
    adSection?:any;
}

const ListPageWithModal: React.FC<ListPageWithModalProps> = ({keywords, title, match,articleAP,videoAP, adSection} : ListPageWithModalProps) => {

  const [showModal, setShowModal] = useState(false);
  const [listPageWithModalContent,setListPageWithModalContent] = useState<any[]>([]);
  const [offlineMode] = useContext<any>(offlineModeContext)
    const [smalStoryAd,setSmalStoryAd] = useState();

  const handleShowModal = () => {
    setShowModal(true)
  }


  const CloseModal = () => setShowModal(false)
  let location = useLocation();

useEffect(()=>{
  CloseModal();
  offlineMode == false && fetchlistPageWithModalContent();
},[offlineMode])

useEffect(()=>{
  CloseModal();
  offlineMode == false && fetchlistPageWithModalContent();
    findProperAd();
},[])

    const findProperAd = async () => {
        let isMounted = true;
        const store = new Storage();
        await store.create();
        const ads = await store.get("ads");
        let obj = ads.find((o:any) => o.cat_id?.toLowerCase() === catText?.toLowerCase());
        // console.log("catText: ", catText);
        // console.log("obj: ", obj);
        setSmalStoryAd(obj);
    };

  let catText = title;
  if(match.params.cat){
    if(keywords[match.params.cat]){
      catText = keywords[match.params.cat];
    }else{
      catText = match.params.cat;
    }
  }

  const fetchlistPageWithModalContent = async () =>{
    console.log("gaveshvi")
    let isMounted = true;               
    const store = new Storage();
    await store.create();
    const body = {
                  orderby : "ID", 
                  order : "DESC", 
                  post_type: "post", 
                  post_count : "22", 
                  page : "1", 
                  taxonomy : "category",
                  taxonomy_field  : "name",
                  taxonomy_values   : [  catText ],
                  cache_key : "string cache key that get from end of the last response"
                };
      axios.post('https://euronewsgeorgia.com/wp-json/above_rest_api/v1/fetch/posts/', body)
          .then(response => {
            setListPageWithModalContent(response.data.posts);
          })
          .catch(error => {
              console.error('There was an error!', error);
          });
          return () => { isMounted = false };   
  }


  return (
    <IonPage>
      <IonHeader className="ion-no-border en-header-inside en-header-bg ion-no-padding">
        <IonBackButton color="dark" icon={chevronBackOutline} className='ion-float-left ion-no-padding en-inside-back'/>
        <IonTitle color="dark" className='en-inside-title ion-no-padding'>
          {catText}         
        </IonTitle>
      </IonHeader>

     {offlineMode && offlineMode ? ( 
       <OfflineMode/>
      ) 
      : listPageWithModalContent.length > 0 ? (
      <IonContent fullscreen>
       {listPageWithModalContent.map((data,index)=>{
        if(index == 0 || index == 10){
          return (
            <BigStoryDark  keywords={keywords} key={index} data={data} ID={data.ID}/>
            )
        } else {
          return (
            <>
                {smalStoryAd && index == 15 ?
                    <div style={{marginBottom: "10px"}}>
                        <Ad adSection={smalStoryAd} keywords={keywords}/>
                    </div>
                    : adSection && index == 15 ? (<div style={{marginBottom: "10px"}}>
                        <Ad adSection={adSection} keywords={keywords}/>
                    </div>) : ""}
                <ListpagewithmodalStory  key={index} data={data} ID={data.ID}/>
            </>
          )
        }
      
      })}
      </IonContent>
      )
      :
      (
        <>
        <IonContent fullscreen>
        <IonGrid className='ion-no-padding'>
        <IonRow className='ion-no-padding'>
            <IonCol style={{height:"200px",display:"flex",alignItems: 'center',justifyContent:"center",background:"#e2e2e2"}} className='ion-no-padding'>
                <IonSkeletonText className="animate-skeleton-background" style={{ width: '70%',height:"80px"}}/>
            </IonCol>
        </IonRow>
      </IonGrid>

      <IonGrid className='en-storypage-content'>
        <IonSkeletonText animated style={{ width: '30%' }} />

        <h3 className='en-storypage-title'>
        <IonSkeletonText animated style={{ width: '100%' }} />
        <IonSkeletonText animated style={{ width: '100%' }} />
        <IonSkeletonText animated style={{ width: '30%' }} />

        </h3>
        <div className='en-storypage-time'>
        <IonSkeletonText animated style={{ width: '30%'}} />
        <br></br>
        </div>
        <br></br>
        <div className='en-storypage-by'><i> 
        <IonSkeletonText animated style={{ width: '100%', }} />
          </i></div>
          <IonGrid className='en-storypage-description'>
          <p >
          <IonSkeletonText animated style={{ width: '100%' }} />
          <IonSkeletonText animated style={{ width: '100%' }} />
          <IonSkeletonText animated style={{ width: '100%' }} />
          <IonSkeletonText animated style={{ width: '100%' }} />
          <IonSkeletonText animated style={{ width: '100%' }} />
          <IonSkeletonText animated style={{ width: '100%' }} />
          <IonSkeletonText animated style={{ width: '100%' }} />
          <IonSkeletonText animated style={{ width: '100%' }} />
          <IonSkeletonText animated style={{ width: '100%' }} />
          <IonSkeletonText animated style={{ width: '100%' }} />
          <IonSkeletonText animated style={{ width: '100%' }} />
          <IonSkeletonText animated style={{ width: '100%' }} />
          </p>
          </IonGrid>
      </IonGrid>
      </IonContent>
      </>)}
    </IonPage>
  );
};

export default ListPageWithModal;
