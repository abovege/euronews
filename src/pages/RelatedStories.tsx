import {IonHeader,IonContent, IonGrid, IonPage, IonTitle, IonBackButton,IonButton, IonIcon } from '@ionic/react';
import './JustIn.css';
import SmallStory from '../components/SmallStory';
import { chevronBackOutline } from 'ionicons/icons';
import React, {useEffect,useState, useContext } from 'react';
import {useLocation,useHistory} from "react-router-dom";

import { Storage } from '@ionic/storage';

import {offlineModeContext} from '../state/State'
import OfflineMode from "../components/OfflineMode"

interface RelatedStoriesProps {
  keywords?: any;
}

const RelatedStories: React.FC<RelatedStoriesProps> = ({keywords} : RelatedStoriesProps) => {
  const data = useLocation<any>();
  const [array,setArray] = useState<any>([])
  const [offlineMode] = useContext<any>(offlineModeContext)
  let history = useHistory();

  const test = async () =>{
    const  store = new Storage();
    await store.create();
    await store.set("lastRelate",data.state.data);

    setArray(data.state.data);

  }
  useEffect(() => {
    let isMounted = true;
    if (isMounted) {
      setArray(data.state.data);
      test();  
    }        
    return () => { isMounted = false }; // cleanup toggles value, if unmounted
  
  }, [])
  
  return (
    <IonPage>
      <IonHeader  className="ion-no-border en-settings-header">
        <IonButton style={{marginTop:"35px"}} className='ion-float-left ion-no-padding en-relatedstories-back-button' onClick={() => history.goBack()}><IonIcon icon={chevronBackOutline}/></IonButton>
        <IonTitle className='ion-padding-left en-settings-title'>{keywords.RELATED_STORIES}</IonTitle>
      </IonHeader>
      <IonContent fullscreen>
      {offlineMode && offlineMode ? ( 
       <OfflineMode/>
      ) 
      :(
        <IonGrid>
        {array && array.map((data:any,key:any)=>(<SmallStory key={key} data={data} ID={data.ID}/>))}
        </IonGrid>
      )}
      
      </IonContent>
    </IonPage>
  );
};

export default RelatedStories;
