import { IonBackButton, IonCol, IonContent, IonGrid, IonHeader, IonPage, IonRow, IonTitle } from '@ionic/react';
import { chevronBackOutline } from 'ionicons/icons';
import { Link } from 'react-router-dom';
import './Privacy.css';
import React from "react";

interface PrivacyProps {
  keywords?: any;
}


const Privacy: React.FC<PrivacyProps> = ({keywords} : PrivacyProps) => {
  return (
    <IonPage>
      <IonHeader className="ion-no-border en-privacy-header">
        <IonBackButton icon={chevronBackOutline} className='ion-float-left ion-no-padding en-settings-back-button'/>
        <IonTitle className='en-privacy-title ion-no-padding'>{keywords.PRIVACY}</IonTitle>
      </IonHeader>
      <IonContent fullscreen>

        <IonGrid>
          <IonRow>

            <IonCol size='12' className='en-version'>
              <ul>
                <li><i>Last update: 31st March 2021</i></li>
                <li><i>This information is accessible through the last versions of our application:</i>
                  <ul>
                    <li><i>Euronews iOS v3.11</i></li>
                    <li><i>Euronews Android v5.4.3</i></li>
                  </ul>
                </li>
              </ul>
            </IonCol>

            <IonCol size='12' className='en-terms'>
              <h2 className='en-terms-title'>Terms and Conditions</h2>
              <p>
                By using our Websites or Applications, you
                signify your agreement to our terms of use.
                Please read carefully our <Link to='#' className='en-terms-link'>Terms and Conditions</Link>.
              </p>
            </IonCol>

            <IonCol size='12' className='en-terms'>
              <h2 className='en-terms-title'>We respect your privacy!</h2>
              <p>Please read carefully our data protection documentation:</p>

              <ul>
                <li><Link to='#' className='en-terms-link'>Privacy Policy</Link></li>
                <li><Link to='#' className='en-terms-link'>Cookies Policy</Link></li>
              </ul>

              <p>
                We and our partners use non-sensitive
                information provided through SDKs* for
                purposes like displaying personalized ads,
                measuring traffic and preferences of our visitors.
              </p>

              <p>
                You can manage your cookies choice whenever
                you want through our Consent Management
                Platform (CMP) <Link to='#' className='en-terms-link'>by clicking here.</Link>.
              </p>

              <p><i>The app restart is sometimes necessary to apply new privacy settings.</i></p>
            </IonCol>



          </IonRow>
        </IonGrid>

        
        
      </IonContent>
    </IonPage>
  );
};

export default Privacy;
