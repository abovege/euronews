import { SocialSharing } from '@ionic-native/social-sharing'

export async function share(message: string): Promise<any> {
  try {
    return await SocialSharing.share(message)
  } catch (error) {
    console.error(error)
  }
}
