import { Redirect, Route } from 'react-router-dom';
import {
  IonAlert,
  IonApp,
  IonIcon,
  IonLabel,
  IonRouterOutlet,
  IonTabBar,
  IonTabButton,
  IonTabs,
  setupIonicReact,
} from '@ionic/react';

import { IonReactRouter } from '@ionic/react-router';
import { grid, home, ellipsisHorizontal, play, timeSharp, arrowForwardOutline } from 'ionicons/icons';
import Tab1 from './pages/Home';
import Tab2 from './pages/JustIn';
import Tab3 from './pages/Videos';
import Tab4 from './pages/ExploreTab';
import Tab5 from './pages/Services';
import Languages from './pages/Languages';
import ReadItLater from './pages/ReadItLater';
import Settings from './pages/Settings';
import Notifications from './pages/Notifications';
import Privacy from './pages/Privacy';
import AboutEuronews from './pages/AboutEuronews';
import AboutAllViews from './pages/AboutAllViews';
import AboutThisApp from './pages/AboutThisApp';
import EuronewsGeorgiaApp from './pages/EuronewsGeorgiaApp';
import StoryPage from './pages/StoryPage';
import InsidePage from './pages/InsidePage';
import ListPageWithModal from './pages/ListPageWithModal';
import WeatherPage from './pages/WeatherPage';
import Programmes from './pages/Programmes';
import RelatedStories from './pages/RelatedStories';
import LiveStreaming from './pages/LiveStreaming';
import Search from './pages/Search';
import { isPlatform } from '@ionic/react';
import { StatusBar } from "@awesome-cordova-plugins/status-bar"

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';

import React, { useState, useEffect, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { Storage } from '@ionic/storage';
import axios from "axios";
import { offlineModeContext, notificationContext,darkThemeContext } from './state/State'

import OneSignal from 'onesignal-cordova-plugin'
import {useIdleTimer} from "react-idle-timer";
import {Capacitor} from "@capacitor/core";

import { Network } from '@capacitor/network';
import useDidMountEffect from "./hooks/useDidMountEffect";
import WeatherPageNew from "./pages/WeatherPageNew";
import WeatherPageDetails from "./pages/WeatherPageDetails";


setupIonicReact();

const App: React.FC = () => {

  const history = useHistory();
  const [language, setLanguage] = useState<string>('ka');
  const [keywords, setKeywords] = useState<any>({});
  const [articleAP, setArticleAP] = useState<string>();
  const [time, setTime] = useState('time');
  const [refreshPage, setRefreshPage] = useState<boolean>(false);

  const [offlineMode, setOfflineMode] = useContext<any>(offlineModeContext);
  const [notification, setNotification] = useContext<any>(notificationContext)

  const [justInStories, setJustInStories] = useState<any[]>([]);


  const [page, setPage] = useState<any>(40);


  // console.log('is the platform desktop?:', Capacitor.getPlatform());


  // useEffect(() => {
  //   if(Capacitor.getPlatform()!='web') {
  //     const OneSignalInit = () => {
  //       OneSignal.setAppId("4f65d545-eb22-40d9-875d-eb3c27f8ef23");
  //       OneSignal.addPermissionObserver();
  //       console.log('notif inited:');
  //       OneSignal.setNotificationOpenedHandler((jsonData) => {
  //         //Getting page ID from Signal push  notification
  //         let pageId = jsonData["notification"]["additionalData"]["post_id"];
  //         window.location.href = `/storypage/${pageId}`
  //       });
  //
  //     }
  //     OneSignalInit();
  //   }
  //
  // }, []);
  //
  //   useEffect(() => {
  //     if(Capacitor.getPlatform()!='web') {
  //       !isPlatform('ios') && OneSignal.disablePush(!notification);
  //     }
  //   }, [notification])
  //
  // console.log('is the platform desktop?:', Capacitor.getPlatform());
  

  useEffect(() => {
  function OneSignalInit() :void {

     if (Capacitor.getPlatform() != 'web') {
      
        OneSignal.initialize("4f65d545-eb22-40d9-875d-eb3c27f8ef23");
     
        let myClickListener = async function(event: any) {
              let notificationData = JSON.stringify(event);
              console.log("Notification data: " + notificationData);
              let pageId = event["notification"]["additionalData"]["post_id"];
              console.log("pageId: " + pageId);
              // window.location.href = `/storypage/${pageId}`;
              window.open(`/storypage/${pageId}`, "_self");

        };
        OneSignal.Notifications.addEventListener("click", myClickListener);
        
        // Prompts the user for notification permissions.
        //    * Since this shows a generic native prompt, we recommend instead using an In-App Message to prompt for notification permission (See step 7) to better communicate to your users what notifications they will get.
        OneSignal.Notifications.requestPermission(true).then((accepted: boolean) => {
          console.log("User accepted notifications web: " + accepted);
        });

    }
  }



    if (Capacitor.getPlatform() !== 'web') {
      console.log('platform is', Capacitor.getPlatform());
      window['plugins'].OneSignal.Debug.setLogLevel(6);
    }

  OneSignalInit();
  }, []);


  // useEffect(() => {
  //     if(Capacitor.getPlatform()!='web') {
  //       !isPlatform('ios') && OneSignal.disablePush(!notification);
  //     }
  // }, [notification])


  const fetchJustInStories = async (page: any) => {
    const store = new Storage();
    await store.create();



    const body = {
      orderby: "post_date",
      order: "DESC",
      post_type: "post",
      post_count: page,
      page: "1",
      taxonomy: "category",
      taxonomy_field: "name",
      taxonomy_values: ["ამბები"],
      // taxonomy_values: ["საქართველო"],
      cache_key: "string cache key that get from end of the last response"
    };
    axios.post('https://euronewsgeorgia.com/wp-json/above_rest_api/v1/fetch/posts/', body)
      .then(response => {
        store.set("justInStories", response.data.posts);
        setJustInStories([...response.data.posts]);
      })
      .catch(error => {
        console.error('There was an error!', error);
      });
    setJustInStories(await store.get("justInStories"));
  }

  const justinStoryFunction = () => {
    setPage(page + 40);
    fetchJustInStories(page);
  }

  useEffect(() => {
    StatusBar.overlaysWebView(false);
    StatusBar.styleLightContent();
    fetchJustInStories(page);
    document.body.setAttribute("color-theme", "light")
    darkThemeSetter();
    getLanguage();

  }, [refreshPage])
  const store = new Storage;
  useEffect(() => {
    const inputStateManagement = async () => {
      await store.create();
      await store.get("notification") == null ? setNotification(false) : setNotification(await store.get("notification"));
      await store.get("offlineMode") == null ? setOfflineMode(false) : setOfflineMode(await store.get("offlineMode"))
    }
    inputStateManagement();
    if (offlineMode == false) {
      fetchNoCommentContent();
      fetchGlobalConversationContent();
      fetchVideoContent();
      fetchProgramms();
      fetchEditorsChoiceCat();
      fetchHotTopics();
      fetchThemes();
      fetchFeaturedSection();
      fetchRubric();
      fetchAd();
    }
  }, [refreshPage])

  useDidMountEffect(() => {
    if (offlineMode == false) {
      fetchNoCommentContent();
      fetchGlobalConversationContent();
      fetchVideoContent();
      fetchProgramms();
      fetchEditorsChoiceCat();
      fetchHotTopics();
      fetchThemes();
      fetchFeaturedSection();
    }
  }, [offlineMode])

  const [noCommentContent, setNoCommentContent] = useState<any[]>([]);
  const [globalConversationContent, setGlobalConversationContent] = useState<any[]>([]);
  const [videoContent, setVideoContent] = useState<any[]>([]);
  const [programsSection, setProgramsSection] = useState<any[]>([]);
  const [rubricSection, setRubricSection] = useState<any[]>([]);
  const [ads, setAds] = useState<any[]>([]);
  const [adSection, setAdSection] = useState<any[]>([]);
  const [justinAd, setJustinAd] = useState<any[]>([]);
  const [videosAd, setVideosAd] = useState<any[]>([]);
  const [exploreAd, setExploreAd] = useState<any[]>([]);
  const [newsDetailAd, setNewsDetailAd] = useState<any[]>([]);
  const [insidePageAd, setInsidePageAd] = useState<any[]>([]);
  const [weatherAd, setWeatherAd] = useState<any[]>([]);


  const fetchNoCommentContent = async () => {
    let isMounted = true;
    const store = new Storage();
    await store.create();
    const body = {
      orderby: "post_date",
      order: "DESC",
      post_type: "post",
      post_count: "30",
      page: "1",
      taxonomy: "category",
      taxonomy_field: "name",
      taxonomy_values: ["no comment"],
      cache_key: "string cache key that get from end of the last response"
    };
    axios.post('https://euronewsgeorgia.com/wp-json/above_rest_api/v1/fetch/posts/', body)
      .then(response => {
        setNoCommentContent(response.data.posts);
        store.set("noCommentContent", response.data.posts);
      })
      .catch(error => {
        console.error('There was an error!', error);
      });
    // setNoCommentContent(await store.get("noCommentContent"));
    return () => { isMounted = false };
  }

  const fetchGlobalConversationContent = async () => {
    let isMounted = true;
    const store = new Storage();
    await store.create();
    const body = {
      orderby: "post_date",
      order: "DESC",
      post_type: "post",
      post_count: "30",
      page: "1",
      taxonomy: "category",
      taxonomy_field: "name",
      taxonomy_values: ["ევრონიუს ჯორჯიას სტუმარი"],
      cache_key: "string cache key that get from end of the last response"
    };
    axios.post('https://euronewsgeorgia.com/wp-json/above_rest_api/v1/fetch/posts/', body)
      .then(response => {
        setGlobalConversationContent(response.data.posts);
        store.set("globalConversationContent", response.data.posts);
      })
      .catch(error => {
        console.error('There was an error!', error);
      });
    // setGlobalConversationContent(await store.get("globalConversationContent"));    
    return () => { isMounted = false };
  }

  const fetchVideoContent = async () => {
    let isMounted = true;
    const store = new Storage();
    await store.create();
    const body = {
      orderby: "post_date",
      order: "DESC",
      post_type: "post",
      post_count: "30",
      page: "1",
      taxonomy: "category",
      taxonomy_field: "name",
      taxonomy_values: ["ვიდეო"],
      cache_key: "string cache key that get from end of the last response"
    };
    axios.post('https://euronewsgeorgia.com/wp-json/above_rest_api/v1/fetch/posts/', body)
      .then(response => {
        setVideoContent(response.data.posts);
        store.set("videoContent", response.data.posts);
      })
      .catch(error => {
        console.error('There was an error!', error);
      });
    setVideoContent(await store.get("videoContent"));
    return () => { isMounted = false };
  }


  const fetchProgramms = async () => {

    let isMounted = true;
    const store = new Storage();
    await store.create();
    const body = {
      orderby: "post_date",
      order: "DESC",
      post_type: "ara_options",
      post_count: "10",
      page: "1",
      cache_key: "string cache key that get from end of the last response",
      post_title: "programs_section"
    }
    axios.post('https://euronewsgeorgia.com/wp-json/above_rest_api/v1/fetch/posts/', body)
      .then(response => {
        setProgramsSection(JSON.parse(response.data.posts[0].post_content))
        store.set("programsSection", JSON.parse(response.data.posts[0].post_content));
      })
      .catch(error => {
        console.error('There was an error!', error);
      });
    return () => { isMounted = false };
  }

  const fetchRubric = async () => {

    let isMounted = true;
    const store = new Storage();
    await store.create();
    const body = {
      orderby: "post_date",
      order: "DESC",
      post_type: "ara_options",
      post_count: "10",
      page: "1",
      cache_key: "string cache key that get from end of the last response",
      post_title: "rubric_section"
    }
    axios.post('https://euronewsgeorgia.com/wp-json/above_rest_api/v1/fetch/posts/', body)
      .then(response => {
        setRubricSection(JSON.parse(response.data.posts[0].post_content))
        store.set("rubricSection", JSON.parse(response.data.posts[0].post_content));
      })
      .catch(error => {
        console.error('There was an error!', error);
      });
    // setProgramsSection(await store.get("rubricContent")); 
    return () => { isMounted = false };
  }

  const fetchAd = async () => {

    let isMounted = true;
    const store = new Storage();
    await store.create();
    const body = {
      orderby: "post_date",
      order: "DESC",
      post_type: "ara_options",
      post_count: "10",
      page: "1",
      cache_key: "string cache key that get from end of the last response",
      post_title: "ads_section"
    }
    axios.post('https://euronewsgeorgia.com/wp-json/above_rest_api/v1/fetch/posts/', body)
      .then(response => {
        setAdSection(JSON.parse(response.data.posts[0].post_content)[0]);
        const ads = JSON.parse(response.data.posts[0].post_content);
        let default_ad = ads.find((o:any) => (o.cat_id?.toLowerCase() === "default") || (o.cat_id?.toLowerCase() === ""));
        let justin_tab_ad = ads.find((o:any) => (o.cat_id?.toLowerCase() === "justin_tab_ad"));
        let videos_tab_ad = ads.find((o:any) => (o.cat_id?.toLowerCase() === "videos_tab_ad"));
        let explore_tab_ad = ads.find((o:any) => (o.cat_id?.toLowerCase() === "explore_tab_ad"));
        let news_detail_ad = ads.find((o:any) => (o.cat_id?.toLowerCase() === "news_detail_ad"));
        let inside_page_ad = ads.find((o:any) => (o.cat_id?.toLowerCase() === "inside_page_ad"));
        let weather_page_ad = ads.find((o:any) => (o.cat_id?.toLowerCase() === "weather_page_ad"));

        setAdSection(default_ad);
        setVideosAd(videos_tab_ad);
        setJustinAd(justin_tab_ad);
        setExploreAd(explore_tab_ad);
        setNewsDetailAd(news_detail_ad);
        setInsidePageAd(inside_page_ad);
        setWeatherAd(weather_page_ad);

        store.set("videos_tab_ad", videos_tab_ad);
        store.set("justin_tab_ad", justin_tab_ad);
        store.set("explore_tab_ad", explore_tab_ad);
        store.set("news_detail_ad", news_detail_ad);
        store.set("inside_page_ad", inside_page_ad);
        store.set("weather_page_ad", weather_page_ad);

        if(videos_tab_ad && !videos_tab_ad?.img)
          setVideosAd(default_ad);

        if(justin_tab_ad && !justin_tab_ad?.img)
          setJustinAd(default_ad);

        if(explore_tab_ad && !explore_tab_ad?.img)
          setExploreAd(default_ad);

        if(news_detail_ad && !news_detail_ad?.img)
          setNewsDetailAd(default_ad);

        if(inside_page_ad && !inside_page_ad?.img)
          setInsidePageAd(default_ad);

        if(weather_page_ad && !weather_page_ad?.img)
          setWeatherAd(default_ad);


        store.set("adSection", default_ad);
        store.set("ads", ads);
        setAds(ads); //this is for passing to tab1
      })
      .catch(error => {
        console.error('There was an error!', error);
      });
    setAdSection(await store.get("adSection"));
    setAds(await store.get("ads"));
    // setVideosAd(await store.get("otherPagesAd"));
    // setJustinAd(await store.get("otherPagesAd"));
    // setExploreAd(await store.get("otherPagesAd"));
    // setNewsDetailAd(await store.get("otherPagesAd"));
    // setProgramsSection(await store.get("rubricContent"));
    return () => { isMounted = false };
  }

  const [editorsChoiceCat, seteditorsChoiceCat] = useState<any>("");

  const fetchEditorsChoiceCat = async () => {
    let isMounted = true;
    const store = new Storage();
    await store.create();
    const body = {
      orderby: "post_date",
      order: "DESC",
      post_type: "ara_options",
      post_count: "10",
      page: "1",
      cache_key: "string cache key that get from end of the last response",
      post_title: "editorchoice_section"
    };
    axios.post('https://euronewsgeorgia.com/wp-json/above_rest_api/v1/fetch/posts/', body)
      .then(response => {
        seteditorsChoiceCat(JSON.parse(response.data.posts[0].post_content)[0].cat_id);
        store.set("editorsChoiceCat", JSON.parse(response.data.posts[0].post_content)[0].cat_id);
        fetchEditorsChoiceContent(JSON.parse(response.data.posts[0].post_content)[0].cat_id)
      })
      .catch(error => {
        console.error('There was an error!', error);
      });
    return () => { isMounted = false };
  }

  const [editorsChoiceContent, seteditorsChoiceContent] = useState<any>();
  const fetchEditorsChoiceContent = async (editorsChoiceCat: any) => {
    let isMounted = true;
    const store = new Storage();
    await store.create();
    const body = {
      orderby: "post_date",
      order: "DESC",
      post_type: "post",
      post_count: "30",
      page: "1",
      taxonomy: "category",
      taxonomy_field: "name",
      taxonomy_values: [editorsChoiceCat],
      cache_key: "string cache key that get from end of the last response"
    };
    axios.post('https://euronewsgeorgia.com/wp-json/above_rest_api/v1/fetch/posts/', body)
      .then(response => {
        seteditorsChoiceContent(response.data.posts)
        store.set("editorsChoiceContent", response.data.posts);
      })
      .catch(error => {
        console.error('There was an error!', error);
      });
    return () => { isMounted = false };
  }



  const [hotTopics, setHotTopics] = useState<any[]>([]);
  const [themesContent, setThemesContent] = useState<any[]>([]);
  const [featuredSection, setFeaturedSection] = useState<any[]>([]);


  const fetchFeaturedSection = async () => {
    let isMounted = true;
    const store = new Storage();
    await store.create();
    const body = {
      orderby: "post_date",
      order: "DESC",
      post_type: "ara_options",
      post_count: "10",
      page: "1",
      cache_key: "string cache key that get from end of the last response",
      post_title: "featured_section"
    }
    axios.post('https://euronewsgeorgia.com/wp-json/above_rest_api/v1/fetch/posts/', body)
      .then(response => {
        setFeaturedSection(JSON.parse(response.data.posts[0].post_content))
        store.set("featuredSection", JSON.parse(response.data.posts[0].post_content));
      })
      .catch(error => {
        console.error('There was an error!', error);
      });
    // setFeaturedSection(await store.get("featuredSection")); 
    return () => { isMounted = false };
  }
  const fetchHotTopics = async () => {

    let isMounted = true;
    const store = new Storage();
    await store.create();
    const body = {
      orderby: "post_date",
      order: "DESC",
      post_type: "ara_options",
      post_count: "-1",
      page: "1",
      cache_key: "string cache key that get from end of the last response",
      post_title: "tags_section"
    }
    axios.post('https://euronewsgeorgia.com/wp-json/above_rest_api/v1/fetch/posts/', body)
      .then(response => {
        setHotTopics(JSON.parse(response.data.posts[0].post_content))
        store.set("hotTopics", JSON.parse(response.data.posts[0].post_content));
      })
      .catch(error => {
        console.error('There was an error!', error);
      });
    return () => { isMounted = false };
  }
  const fetchThemes = async () => {

    let isMounted = true;
    const store = new Storage();
    await store.create();
    const body = {
      orderby: "post_date",
      order: "DESC",
      post_type: "ara_options",
      post_count: "10",
      page: "1",
      cache_key: "string cache key that get from end of the last response",
      post_title: "theme_section"
    }
    axios.post('https://euronewsgeorgia.com/wp-json/above_rest_api/v1/fetch/posts/', body)
      .then(response => {
        setThemesContent(JSON.parse(response.data.posts[0].post_content))
        store.set("themesContent", JSON.parse(response.data.posts[0].post_content));
      })
      .catch(error => {
        console.error('There was an error!', error);
      });
    // setThemesContent(await store.get("themesContent")); 
    return () => { isMounted = false };
  }
  const [darkTheme,setDarkTheme] = useContext<any>(darkThemeContext);

  const darkThemeSetter = async () => {
    const store = new Storage();
    await store.create();
    let defaultTheme = await store.get('theme');
    // console.log(defaultTheme)
    defaultTheme ? setDarkTheme(true) : setDarkTheme(false)
    defaultTheme ? document.body.setAttribute("color-theme", "dark") : document.body.setAttribute("color-theme", "light")
  }

  async function getLanguage() {
    //Connect to Local Storage
    const store = new Storage();
    await store.create();

    //Get language from the local storage
    let defaultLanguage = await store.get('language');
    if (!defaultLanguage) {
      defaultLanguage = 'ka';
    }

    //Get the language file
    let response = await fetch('/assets/i18n/' + defaultLanguage + '.json');
    let responseJson = await response.json();
    setKeywords(responseJson);

    //Set language state
    setLanguage(defaultLanguage);
  }


  return (
    <IonApp>
      <IonReactRouter>
        <IonTabs>
          <IonRouterOutlet>


            <Route path="/languages" render={() => (<Languages changeLanguage={getLanguage} keywords={keywords} />)} />
            <Route path="/readitlater" render={() => (<ReadItLater keywords={keywords} />)} />
            <Route path="/settings" render={() => (<Settings keywords={keywords} />)} />
            <Route path="/notifications" render={() => (<Notifications keywords={keywords} />)} />
            <Route path="/privacy" render={() => (<Privacy keywords={keywords} />)} />
            <Route path="/abouteuronews" render={() => (<AboutEuronews keywords={keywords} />)} />
            <Route path="/aboutallviews" render={() => (<AboutAllViews keywords={keywords} />)} />
            <Route path="/aboutthisapp" render={() => (<AboutThisApp keywords={keywords} />)} />
            <Route path="/euronewsgeorgiaapp" render={() => (<EuronewsGeorgiaApp keywords={keywords} />)} />
            <Route path="/storypage/:id" render={() => (<StoryPage adSection={newsDetailAd} keywords={keywords} articleAP={articleAP} />)} />
            <Route path="/insidepages/:cat" render={(props) => (<InsidePage {...props} adSection={insidePageAd} title={keywords.FEATURED_CONTENT} keywords={keywords} />)} />
            <Route path="/listpagewithmodal/:cat" render={(props) => (<ListPageWithModal adSection={insidePageAd} {...props} title={keywords.LATEST_VIDEOS} keywords={keywords} articleAP={articleAP} />)} />
            {/*<Route path="/weather" render={() => (<WeatherPage title={keywords.LATEST_VIDEOS} keywords={keywords} />)} />*/}
            <Route path="/weather" exact render={() => (<WeatherPageNew adSection={weatherAd} title={keywords.LATEST_VIDEOS} keywords={keywords} />)} />
            <Route path="/weather/detail/:day_index" exact render={() => (<WeatherPageDetails title={keywords.LATEST_VIDEOS} keywords={keywords} />)} />
            <Route path="/rubric" render={() => (<Programmes keywords={keywords} />)} />
            <Route path="/relatedstories" render={() => (<RelatedStories keywords={keywords} />)} />
            <Route path="/search" render={() => (<Search keywords={keywords} />)} />
            <Route path="/livestreaming" render={() => (<LiveStreaming />)} />


            <Route exact path="/tab1">
              <Tab1
                ads={ads}
                hotTopics={hotTopics}
                keywords={keywords}
                articleAP={articleAP}
                featuredSection={featuredSection}
                noCommentContent={noCommentContent}
                setRefreshPage={setRefreshPage}
                refreshPage={refreshPage}
              />
            </Route>
            <Route exact path="/tab2">
              <Tab2 justinStoryFunction={justinStoryFunction} justInStories={justInStories} keywords={keywords} adSection={justinAd}/>
            </Route>
            <Route
              path="/tab3" >
              <Tab3
                noCommentContent={noCommentContent}
                globalConversationContent={globalConversationContent}
                videoContent={videoContent}
                programsSection={programsSection}
                editorsChoiceContent={editorsChoiceContent}
                editorsChoiceCat={editorsChoiceCat}
                adSection={videosAd}
                keywords={keywords} articleAP={articleAP} />
            </Route>
            <Route path="/tab4">
              <Tab4
                hotTopics={hotTopics}
                themesContent={themesContent}
                featuredSection={featuredSection}
                rubricSection={rubricSection}
                adSection={exploreAd}
                keywords={keywords} />
            </Route>
            <Route path="/tab5">
              <Tab5 keywords={keywords} />
            </Route>
            <Route exact path="/">
              <Redirect to="/tab1" />
            </Route>
          </IonRouterOutlet>
          <IonTabBar id="bottom-tab-bar" slot="bottom" >
            <IonTabButton tab="tab1" href="/tab1">
              <IonIcon icon={home} />
              <IonLabel>{keywords.HOME}</IonLabel>
            </IonTabButton>
            <IonTabButton tab="tab2" href="/tab2">
              <IonIcon icon={timeSharp} />
              <IonLabel>{keywords.JUST_IN}</IonLabel>
            </IonTabButton>
            <IonTabButton tab="tab3" href="/tab3">
              <IonIcon icon={play} />
              <IonLabel>{keywords.VIDEOS}</IonLabel>
            </IonTabButton>
            <IonTabButton tab="tab4" href="/tab4">
              <IonIcon icon={grid} />
              <IonLabel>{keywords.EXPLORE}</IonLabel>
            </IonTabButton>
            <IonTabButton tab="tab5" href="/tab5">
              <IonIcon icon={ellipsisHorizontal} />
              <IonLabel>{keywords.SERVICES}</IonLabel>
            </IonTabButton>
          </IonTabBar>
        </IonTabs>
      </IonReactRouter>
    </IonApp>

  )
};

export default App;
