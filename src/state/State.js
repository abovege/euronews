import React, { useState } from "react";

export const offlineModeContext = React.createContext(false);
export const darkThemeContext = React.createContext(false);
export const fontSizeContext = React.createContext(14);
export const articleAPContext = React.createContext("articles-always");
export const videoAPContext = React.createContext("video-always");
export const globalIDContext = React.createContext("");
export const notificationContext = React.createContext(false);
export const shouldVideoPlayContext = React.createContext(false);

const State = ({ children }) => {
  const [offlineMode, setOfflineMode] = useState(false);
  const [darkTheme, setDarkTheme] = useState(false);
  const [fontSize, setFontSize] = useState(14);
  const [selectedArticleAP, setSelectedArticleAP] = useState("articles-always");
  const [selectedVideoeAP, setSelectedVideoAP] = useState("video-always");
  const [globalID, setGlobalID] = useState("");
  const [notification, setNotification] = useState(false);
  const [shouldVideoPlay, setShouldVideoPlay] = useState(false);

  return (
    <offlineModeContext.Provider value={[offlineMode, setOfflineMode]}>
      <shouldVideoPlayContext.Provider
        value={[shouldVideoPlay, setShouldVideoPlay]}
      >
        <darkThemeContext.Provider value={[darkTheme, setDarkTheme]}>
          <fontSizeContext.Provider value={[fontSize, setFontSize]}>
            <articleAPContext.Provider
              value={[selectedArticleAP, setSelectedArticleAP]}
            >
              <videoAPContext.Provider
                value={[selectedVideoeAP, setSelectedVideoAP]}
              >
                <globalIDContext.Provider value={[globalID, setGlobalID]}>
                  <notificationContext.Provider
                    value={[notification, setNotification]}
                  >
                    {children}
                  </notificationContext.Provider>
                </globalIDContext.Provider>
              </videoAPContext.Provider>
            </articleAPContext.Provider>
          </fontSizeContext.Provider>
        </darkThemeContext.Provider>
      </shouldVideoPlayContext.Provider>
    </offlineModeContext.Provider>
  );
};

export default State;
