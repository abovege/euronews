import { useEffect } from 'react';

const useScript = (url:any, adSection:any) => {
    useEffect(() => {
        const ins = document.createElement('ins');
        ins.setAttribute("revive-zoneid", adSection?.revive_zone_id);
        ins.setAttribute("revive-id", adSection?.revive_id);
        if(!document.querySelector("ins #adsection" + adSection?.revive_zone_id)) {
            document.getElementById("adsection" + adSection?.revive_zone_id)?.appendChild(ins);
        }


        const script = document.createElement('script');
        script.src = url;
        script.async = true;
        // document.body.appendChild(script);
        // document.getElementById("adsection" + adSection.revive_zone_id)?.append('<ins data-revive-zoneid={adSection.revive_zone_id} data-revive-id={adSection.revive_id}/>');
        document.getElementById("adsection" + adSection?.revive_zone_id)?.appendChild(script);




        return () => {
            document.getElementById("adsection" + adSection?.revive_zone_id)?.removeChild(script);
            document.getElementById("adsection" + adSection?.revive_zone_id)?.removeChild(ins);
        }
    }, [{url}]);
};

export default useScript;