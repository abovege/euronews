import React, {useRef, useEffect, useState} from "react";

/**
 * Hook that alerts clicks outside of the passed ref
 */
function useOutsideAlerter(ref:any, initialIsVisible:boolean=false) {
    const [clickedOutComponent, setClickedOutComponent] = useState(initialIsVisible);

    useEffect(() => {
        /**
         * Alert if clicked on outside of element
         */
        function handleClickOutside(event:any) {
            if (ref.current && !ref.current.contains(event.target)) {
                setClickedOutComponent(true);
                console.log("clicked outside");
            } else {
                setClickedOutComponent(false);
            }
        }
        // Bind the event listener
        document.addEventListener("mousedown", handleClickOutside);
        return () => {
            // Unbind the event listener on clean up
            document.removeEventListener("mousedown", handleClickOutside);
        };
    }, [ref]);

    return { ref, clickedOutComponent, setClickedOutComponent };

}

export default useOutsideAlerter;