import React, {useRef, useEffect, useState, useContext} from 'react';
import {IonRow, IonButton, IonCol, IonIcon, IonGrid} from '@ionic/react';
import './ModalVideo.css';
import {bookmarkOutline, bookmark, shareOutline} from 'ionicons/icons';
import {Link} from 'react-router-dom';
import ReactPlayer from 'react-player';
import LinesEllipsis from 'react-lines-ellipsis'
import {offlineModeContext, videoAPContext, globalIDContext} from '../state/State'
import OfflineModeVideo from "../components/OfflineModeVideo"
import {Storage} from '@ionic/storage';
import {share} from '../services/sharing.service'

import HTMLEllipsis from 'react-lines-ellipsis/lib/html'


interface ModalVideoProps {
    data?: any;
    ID?: any;
    CloseModal?: any;
}

export const ModalVideo: React.FC<ModalVideoProps> = ({CloseModal, data, ID}) => {
    const [offlineMode] = useContext<any>(offlineModeContext)
    const [videoMode] = useContext<any>(videoAPContext)
    const [autoplayBool, setAutoplayBool] = useState(false)
    const [array, setArray] = useState<any[]>([])
    const [postDate, setPostDate] = useState<any>();

    useEffect(() => {
        const formatDate = (input: any) => {
            var datePart = input.match(/\d+/g),
                year = datePart[0].substring(2, 4), // get only two digits
                month = datePart[1], day = datePart[2];

            const cDate = new Date(`${datePart[0]}/${month}/${day}`);

            const today = new Date;
            const yesterday = new Date;
            yesterday.setDate(today.getDate() - 1)
            if (cDate.toLocaleDateString() == today.toLocaleDateString()) {
                return ' ' // today should be empty
            } else if (cDate.toLocaleDateString() == yesterday.toLocaleDateString()) {
                return 'გუშინ '
            }

            return day+'.'+month+'.'+year + '/';

            // return day + '/' + month + '/' + year;
        }
        setPostDate(formatDate(data.post_date.substring(0, 10)).replace("/", ".").replace("/", ".") + data.post_date.substring(11, 16))


        var elemToObserve = document.getElementById(ID)!;
        var prevClassState = elemToObserve?.classList.contains('scrollFade--visible');
        var observer = new MutationObserver(function (mutations) {
            mutations.forEach(function (mutation) {
                if (mutation.attributeName == "class") {
                    var currentClass = mutation.target as HTMLElement
                    var currentClassState = currentClass.classList.contains('scrollFade--visible');
                    if (prevClassState !== currentClassState) {
                        prevClassState = currentClassState;
                        if (currentClassState) {
                            setAutoplayBool(true)
                        } else {
                            setAutoplayBool(false)
                        }
                    }
                }
            });
        });
        observer.observe(elemToObserve, {attributes: true});


        const setLogo = async () => {
            await store.create();
            await store.get("readItLaterList") == null && await store.set("readItLaterList", []);
            const array = await store.get("readItLaterList");

            array.map((item: { ID: any; }) => {
                if (item && ID == item.ID) {
                    setReadItLater(true);
                    return;
                }
            });
        }
        setLogo()
    }, [])

    const [readItLater, setReadItLater] = useState<any>(false);
    const store = new Storage();


    const changeReadItLater = async () => {
        setReadItLater(!readItLater);
        await store.create();

        let Array = await store.get("readItLaterList");

        Array.length == 0 && store.set("readItLaterList", [data])
        Array.map(async (item: { ID: any; }) => {
            if (item.ID === data.ID) {
                Array = Array.filter((item: { ID: any; }): any => item.ID !== data.ID);
                await store.set("readItLaterList", [...Array]);
            } else {
                await store.set("readItLaterList", [...Array, data])
            }
        })
    }

    const handleShare = async () => {
        if (data) {
            await share(`via Euronews Georgia: ${data.post_title} ${data.guid}`)
        }
    }


    return (
        <>
            {offlineMode ? (
                    <>
                        <OfflineModeVideo/>
                    </>
                )
                : (
                    <>
                        <IonGrid id={ID} className='scrollFade en-modalvideo'>
                            <IonRow>
                                <IonCol size='12'>
                                    <h3 className='en-modalvideo-title'>{data && data.post_title}</h3>
                                </IonCol>
                                <IonCol size='12'>
                                    {videoMode == "video-always" ?
                                        <ReactPlayer width={"100%"} height={236}
                                                     config={{youtube: {playerVars: {controls: 1, autohide: 0}}}}
                                                     muted={false}
                                                     url={`${data.post_meta.td_post_video}`}/> :
                                        <ReactPlayer width={"100%"} height={236}
                                                     config={{youtube: {playerVars: {controls: 1, autohide: 0}}}}
                                                     playing={false} url={`${data.post_meta.td_post_video}`}
                                                     light={`${data.post_meta.essb_cached_image}`}/>
                                    }
                                </IonCol>
                                <IonCol size='12'>
                                    <HTMLEllipsis
                                        style={{color: "#ffffff"}}
                                        unsafeHTML={`${data && data.post_content}`}
                                        // ellipsisHTML={`${data && data.post_content}`}
                                        maxLine='3'
                                        ellipsis='...'
                                        basedOn='letters'
                                        className="video-dialog-video-title"
                                    />

                                    <div className='en-modalvideo-time'>{data && postDate}</div>
                                    {/* <IonIcon icon={bookmarkOutline} className='en-modalvideo-button'/>
            <IonIcon icon={shareOutline} className='en-modalvideo-button'/> */}

                                    <Link className='en-modalvideo-read-more ion-float-right'
                                          onClick={CloseModal}
                                          to={{
                                              pathname: `/storypage/${ID}`,
                                              state: {
                                                  id: ID,
                                                  data: data,
                                                  relatedStory: data.categories[0].name
                                              }
                                          }}>განაგრძე კითხვა &gt;&gt;</Link>


                                    <IonButton style={{backgroundColor: "transparent"}}
                                               className='ion-float-left en-save-button ion-no-padding ion-no-margin'
                                               onClick={changeReadItLater}>
                                        <IonIcon style={{border: "1px solid", padding: "2px"}}
                                                 icon={readItLater ? bookmark : bookmarkOutline}
                                                 className='ion-float-left en-save-button-icon ion-no-padding'/>
                                    </IonButton>
                                    <IonButton onClick={handleShare} className='ion-float-left en-share-button'>
                                        <IonIcon style={{border: "1px solid", padding: "2px"}} icon={shareOutline}
                                                 className='ion-float-left en-share-button-icon'/>
                                    </IonButton>
                                </IonCol>
                            </IonRow>
                        </IonGrid>
                    </>
                )}
        </>
    )
};

export default ModalVideo;
