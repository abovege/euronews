import React, {useContext, useEffect, useState} from 'react';
import {IonicSlides, IonSpinner} from '@ionic/react';
import './Gallery.css';
import {darkThemeContext} from '../state/State'
import {Swiper, SwiperSlide} from "swiper/react";
import {Storage} from "@ionic/storage";
import axios from "axios";

import "swiper/swiper.min.css";
// import 'swiper/modules/autoplay';
// import 'swiper/modules/keyboard';
// import 'swiper/modules/pagination';
// import 'swiper/modules/scrollbar';
// import 'swiper/modules/zoom';
import '@ionic/react/css/ionic-swiper.css';
import {Autoplay, Keyboard, Pagination, Scrollbar, Zoom} from "swiper";

interface LabelsProps {
    ids?: string;
}

export const Gallery: React.FC<LabelsProps> = ({ids}: LabelsProps) => {
    const [darkTheme] = useContext<any>(darkThemeContext);
    const [images, setImages] = useState<any>();
    const [showLoading, setShowLoading] = useState(false);

    useEffect(() => {
        getGalleries();
    }, []);

    const getGalleries = async () => {
        let error = false;
        const store = new Storage();
        await store.create();
        try {
            setShowLoading(true);
            const response = await axios.get('https://euronewsgeorgia.com/wp-json/wp/v2/media?include=' + ids, {});
            console.log("galleries...", response?.data);
            setImages(response?.data);
            await store.set('/images', response?.data);
            setShowLoading(false);

        } catch (error: any) {
            setShowLoading(false);

            console.log(error);//, ' error code: ',error?.code, ' process.env.VITE_APP_API_URL ',process.env.VITE_APP_API_URL);
            console.log("error json is: ", JSON.stringify(error));
        }
    }

    return (
        <>

            <div>
                {showLoading ?
                    <div style={{textAlign: "center"}}>
                        <IonSpinner style={{margin: "5rem"}} name="crescent"/>
                    </div>
                    :
                    images ?
                        <Swiper
                            className="gallery-slide"
                            spaceBetween={40}
                            modules={[Autoplay, Keyboard, Pagination, Scrollbar, Zoom, IonicSlides]}
                            slidesPerView={1}
                            // autoplay={true}
                            keyboard={true}
                            pagination={true}
                            scrollbar={true}
                            zoom={true}
                        >

                            {images?.map((image: any, index: number) => {
                                return (
                                    <SwiperSlide key={image?.id}>
                                        <img
                                            // onClick={() => {setShowModal(true); setEventImage(process.env.VITE_APP_BASE_URL + image?.attachment[0]?.url);}}
                                            style={{width: "100%", borderRadius: "10px"}}
                                            alt="avatar"
                                            src={image?.media_details.sizes.medium_large?.source_url}/>
                                    </SwiperSlide>
                                )
                            })}
                        </Swiper>
                        : ""
                }
                {/*<div className="swiper-pagination"/>*/}
                {/*<div className="swiper-button-prev"/>*/}
                {/*<div className="swiper-button-next"/>*/}
            </div>
            {/*<IonCard className={["general-card", "faq-card"].join(" ")}>*/}
            {/*  <IonCardContent>*/}
            {/*    */}
            {/*  </IonCardContent>*/}
            {/*</IonCard>*/}


        </>
    );
}

export default Gallery;
