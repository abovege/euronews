import React,{useContext} from 'react';
import { IonIcon, IonImg, IonRippleEffect } from '@ionic/react';
import { Link} from 'react-router-dom';
import { caretForwardCircleOutline, chevronForwardOutline } from 'ionicons/icons';
import LinesEllipsis from 'react-lines-ellipsis';
import classes from './FeaturedStory.module.css';
import {darkThemeContext} from '../state/State';



interface FeaturedStoryProps {
    data?: any;
    ID?:any;
  }

  export const FeaturedStory: React.FC<FeaturedStoryProps> = ({data,ID}) => {
    const [darkTheme] = useContext<any>(darkThemeContext);

  return (
    <div className='en-home-featured-content-news ' style={darkTheme ? {background:"#1e1e1e"}:{background:"#ffffff"}}>
      
        <Link to={{
                pathname: `/storypage/${ID}`,
                state: { 
                  id : ID,
                  data:data,
                  relatedStory :data.categories[0].name
                }
            }}
            className='en-home-featured-content-news-link'>
            <div className='en-home-featured-content-news-img-div'>
              <IonImg src={data.post_meta.essb_cached_image ? data.post_meta.essb_cached_image[0] : '../assets/images/no-image.png'} className='en-home-featured-content-news-img'/>
              {data.post_meta.td_post_video && <IonIcon icon={caretForwardCircleOutline} className={classes.en_icon_play  + " " + 'en-icon-video-play'}/>}
            </div>
            
            {/* <div className='en-home-featured-content-news-title'>
                {data.post_title}
            </div> */}
            <div className='ion-activatable'>
              <IonRippleEffect></IonRippleEffect>
              <LinesEllipsis  style={darkTheme ? {color:"#ffffff"}:{color:"#000000"}}
                className='en-home-featured-content-news-title max-lines-3'
                text={`${data && data.post_title} `}
                maxLine='3'
                ellipsis='...'
                trimRight
                basedOn='letters'
              /> 
            </div>
            
        </Link>
        <Link className='en-home-featured-content-news-category' to={`/insidepages/${data.categories[0].name}`}>
                    {data.categories[0].name}
            <IonIcon icon={chevronForwardOutline} className='ion-float-right en-home-icon'/>
        </Link>
    </div>
  )
}

export default FeaturedStory