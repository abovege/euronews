import React, {useContext, useState,useEffect} from 'react';
import { IonRow, IonCol, IonIcon, IonRippleEffect,  } from '@ionic/react';
import './JustInStory.css';
import { caretForwardCircleOutline, ellipse } from 'ionicons/icons';
import Labels from '../components/Labels';
import { Link } from 'react-router-dom';
import {darkThemeContext} from '../state/State'
import LinesEllipsis from 'react-lines-ellipsis'


interface JustInStoryProps {
  labelShow?: boolean;
  labelBG?: string;
  labelText?: string;
  data?: any;
  ID?:any;
}

export const JustInStory: React.FC<JustInStoryProps> = ({data,ID,labelShow, labelBG, labelText} : JustInStoryProps) => {
  const [darkTheme] = useContext<any>(darkThemeContext);
  const [postDate, setPostDate] = useState<any>();
  const [dataPostDate, setDataPostDate] = useState<any>();
const formatDate = (input: any) => {
    const year = input.substring(0,4);
    const month = input.substring(5,7);;
    const day = input.substring(8,10);;

    // console.log(year,month,day);

    const cDate = new Date(`${year}/${month}/${day}`);

    const today = new Date;
    const yesterday = new Date;
    yesterday.setDate(today.getDate() - 1)
    if (cDate.toLocaleDateString() == today.toLocaleDateString()) {
        return ' ' // today should be empty
    } else if (cDate.toLocaleDateString() == yesterday.toLocaleDateString()) {
        return 'გუშინ '
    }

    return month + "/" + day;
}

  useEffect(() => {

      // formatDate(data.post_date);
    setPostDate(formatDate(data.post_date)+" "+data.post_date.substring(11,16))
      setDataPostDate(data.post_date.substring(5,10).replace("-","/")+" "+data.post_date.substring(11,16))
  }, [])



  return (
  <>
 <Link  style={darkTheme ? {color:"#ffffff"}:{color:"#000000"}}  
 className='en-story-link'
    to={{
    pathname: `/storypage/${ID}`,
    state: { 
      id : ID,
      data:data,
      relatedStory :data.categories[0].name
    }
  }}>      <IonRow id={ID} className='en-justin-news' style={darkTheme? {background:"#1e1e1e",borderTop:"4px solid black",borderBottom:"4px solid black"}:{background:"#ffffff",borderTop:"4px solid white",borderBottom:"4px solid white"}}>
        <IonCol className='ion-float-left ion-activatable' style={darkTheme? {background:"#1e1e1e !important"}:{background:"#ffffff !important"}} size='8'>
        <IonRippleEffect></IonRippleEffect>
          <div >
            <div className='en-justin-news-time'>
              <IonIcon icon={ellipse} className='en-time-icon'/>
              <p className='en-justin-news-time-p' data-date={data.post_date && dataPostDate}>{data.post_date && postDate}</p>
            </div>
              { labelShow &&
                <Labels labelBG={labelBG} labelText={labelText}/>
              }
            <h3 className='en-justin-news-title'>
            <LinesEllipsis
              style={darkTheme ? {color:"#ffffff"}:{color:"#000000"}}
              className='en-home-news-title max-lines-3'
              text={`${data && data.post_title} `}
              maxLine='3'
              ellipsis='...'
              trimRight
              basedOn='words'
            /> 
            {/* {data && data.post_title} */}
            </h3>
          </div>
        </IonCol>
        <IonCol className='ion-float-right ion-no-padding justin-image-parent' size='4'>
          <img alt="" src={data.post_meta.essb_cached_image ? data.post_meta.essb_cached_image[0] : '../assets/images/no-image.png'} className='en-justin-news-img'/>
          {data.post_meta.td_post_video && <IonIcon icon={caretForwardCircleOutline} className='en-icon-justin-play'/>}
        </IonCol>
      </IonRow>
    </Link>
  </>
);}

export default JustInStory;
