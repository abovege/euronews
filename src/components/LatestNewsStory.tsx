import React, {useState, useEffect, useContext} from 'react'
import { IonCol, IonIcon, IonImg, IonRippleEffect } from '@ionic/react';
import { Link} from 'react-router-dom';
import { caretForwardCircleOutline } from 'ionicons/icons';
import {darkThemeContext} from '../state/State'

interface LatestNewsStoryProps {
    data?: any;
    ID?:any;
  }

export const LatestNewsStory: React.FC<LatestNewsStoryProps> = ({data,ID}) => {
  const [postDate, setPostDate] = useState<any>();
  const [darkTheme] = useContext<any>(darkThemeContext);

  useEffect(() => {
    const formatDate = (input: any) => {
      var datePart = input.match(/\d+/g),
      year = datePart[0].substring(2,4), // get only two digits
      month = datePart[1], day = datePart[2];

        const cDate = new Date(`${datePart[0]}/${month}/${day}`);

        const today = new Date;
        const yesterday = new Date;
        yesterday.setDate(today.getDate() - 1)
        if (cDate.toLocaleDateString() == today.toLocaleDateString()) {
            return ' ' // today should be empty
        } else if (cDate.toLocaleDateString() == yesterday.toLocaleDateString()) {
            return 'გუშინ '
        }

        return day+'.'+month+'.'+year + '/';

      // return day+'/'+month+'/'+year;
    }
    setPostDate(formatDate(data.post_date.substring(0,10)).replace("/",".").replace("/",".") + data.post_date.substring(11,16))  }, [])

  return (
    <Link 
    // onClick={()=>{
    //   window.location.pathname == "/storypage" && window.location.reload();
    //   return false;}}
       style={darkTheme ? {color:"#ffffff",textDecoration:"none"}:{color:"#000000",textDecoration:"none"}} 
    to={{
        pathname: `/storypage/${ID}`,
        state: { 
          data:data,
          id : ID,
          relatedStory :data.categories[0].name
      },
    }}>
        <IonCol size='12' className='ion-no-padding '>
          
          <div className='en-latest-news ion-activatable'>
            <IonRippleEffect></IonRippleEffect>
            <IonImg src={data.post_meta.essb_cached_image ? data.post_meta.essb_cached_image[0] : '../assets/images/no-image.png'}  />
            {data.post_meta.td_post_video && <IonIcon icon={caretForwardCircleOutline} className='en-icon-news-play'/>}
            <h2 className='en-home-sponsored-content-title max-lines-3'>{data &&  data.post_title}</h2>
            <p className='en-home-sponsored-content-time'>{data && postDate}</p>
          </div>
                                
        </IonCol>
    </Link>  
    )
}

export default LatestNewsStory