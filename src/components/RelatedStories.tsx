import React,{useContext} from 'react';
import {  IonRow, IonCol, IonRouterLink } from '@ionic/react';
import './RelatedStories.css';
import { Link } from 'react-router-dom';
import {darkThemeContext} from '../state/State'


interface RelatedStoriesProps {
  keywords?: any;
  data?: any;
  ID?:any;
}

export const RelatedStories: React.FC<RelatedStoriesProps> = ({ID,data}:RelatedStoriesProps) => {
  const [darkTheme] = useContext<any>(darkThemeContext);

  return(
    <>
    
    <Link 
    // onClick={()=>{
    //   window.location.pathname == "/storypage" && window.location.reload();
    //   return false;}}
    style={darkTheme ? {color:"#ffffff",textDecoration:"none"}:{color:"#000000",textDecoration:"none"}}  
              to={{
              pathname: `/storypage/${ID}`,
              state: { 
                  data: data,
                  id : ID,
                  relatedStory: data.categories[0].name
              },
          }}>      
        <IonRow className='en-related-stories-news ion-no-padding' style={darkTheme ? {background:"#1e1e1e"}:{background:"#fff"}}>
          
          <IonCol className='ion-float-left ion-no-padding' size='12' >
            <div >
              <h3 className='en-home-news-title'>
               {data && data.post_title}
              </h3>
              <div className='en-home-news-locate'>{data.categories[0].name}</div>
            </div>
          </IonCol>
        </IonRow>
      </Link>
      
      
      
      
    </>
  );
}

export default RelatedStories;
