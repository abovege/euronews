import React from 'react';
import {
    IonModal,
    isPlatform,
    getPlatforms,
    IonContent,
    IonButton,
    IonPopover,
    IonAlert,
    IonHeader,
    IonButtons,
    IonIcon,
    IonImg,
    IonRouterLink,
  IonBackButton
} from '@ionic/react';
import {
  arrowBack,
  caretForwardOutline,
  closeOutline,
  desktopOutline,
  home,
  homeOutline,
  partlySunnyOutline
} from 'ionicons/icons';
import {useState, useContext, useEffect} from 'react';
import {offlineModeContext} from '../state/State'
import {Storage} from '@ionic/storage';
import ReactPlayer from 'react-player';
import {ScreenOrientation} from "@awesome-cordova-plugins/screen-orientation";
import {useLocation} from 'react-router-dom';
import {StatusBar} from "@awesome-cordova-plugins/status-bar"
import { useHistory } from "react-router-dom";


import './Header.css';

interface HeaderProps {
    transparent?: boolean;
    darkBlueBg?: boolean;
    showBackButton?: boolean;
}

export const Header: React.FC<HeaderProps> = ({transparent, darkBlueBg, showBackButton}: HeaderProps) => {
    const [showAlert, setShowAlert] = useState(false);
    const [offlineMode, setOfflineMode] = useContext<any>(offlineModeContext);
    const [showModal, setShowModal] = useState(false);

    const store = new Storage();

    let history = useHistory();
    const location = useLocation();

    useEffect(() => {
      console.log('location[\'pathname\']', location['pathname']);
    }, [location]);

    // let location = useLocation();
    // const launchIntoFullscreen = (element: any) => {
    //   if(element.requestFullscreen) {
    //     element.requestFullscreen();
    //   } else if(element.mozRequestFullScreen) {
    //     element.mozRequestFullScreen();
    //   } else if(element.webkitRequestFullscreen) {
    //     element.webkitRequestFullscreen();
    //   } else if(element.msRequestFullscreen) {
    //     element.msRequestFullscreen();
    //   }
    // }
    // useEffect(() => {
    //   // StatusBar.hide();
    //   StatusBar.overlaysWebView(false);
    //   // showModal && launchIntoFullscreen(document.getElementById("test"))
    //   ScreenOrientation.lock(ScreenOrientation.ORIENTATIONS.LANDSCAPE);
    //  }, [])

    //  useEffect(() => {
    //   ScreenOrientation.unlock();
    //   // StatusBar.show();
    //  }, [location.pathname])

    return (
        <>

            <IonHeader
                style={{backgroundColor: "#ffffff"}}
                className={"ion-no-border en-header " + (transparent ? " en-header-fixed" : darkBlueBg ? "en-header-drak-bg" : "en-header-bg")}>


                { (showBackButton)?
                    <IonRouterLink color="dark" onClick={() => history.goBack()}  class='en-header-logo' style={{marginTop: "10px", display: "flex", justifyContent: "start", maxWidth: "48px"}}>
                        {/*<IonImg src='../assets/images/header-logo2.png'/>*/}
                        {/*<IonImg style={{height: "48px"}} src='../assets/images/logo_eun_geo.png'/>*/}
                        <IonIcon size="medium" style={{color: "#007df8", fontSize: "24px"}} icon={arrowBack}/>
                        {/*<IonImg src='../assets/images/en_logo_new_transparent.png'/>*/}
                    </IonRouterLink>
                        :
                    <IonRouterLink color="dark" routerLink="/" class='en-header-logo' style={{marginTop: "10px", display: "flex",  justifyContent: "start",  maxWidth: "48px", textAlign: "left"}}>
                        {/*<IonImg src='../assets/images/header-logo2.png'/>*/}
                        <IonImg style={{height: "48px"}} src='../assets/images/logo_eun_geo.png'/>
                        {/*<IonImg src='../assets/images/en_logo_new_transparent.png'/>*/}
                    </IonRouterLink>
                }

                <div style={{display: "flex", alignItems: "center", marginTop: "8px", justifyContent: "end", width: "145px"}}>
                    <IonButtons class='en-header-offline-mode'>

                        {location['pathname'].includes("/weather/detail") ?
                            <IonButton slot="end" onClick={() => history.goBack()}>
                                <IonIcon slot="icon-only" icon={arrowBack}/>
                            </IonButton>
                            :
                            location['pathname'].includes("/weather") ?
                                <IonButton size="small" style={{color: "#007df8"}} routerLink={'/'}>
                                    <IonIcon size="large" icon={homeOutline}/>
                                </IonButton>
                                :
                                <IonButton size="small" style={{color: "#007df8"}} routerLink={"/weather"}>
                                    {/*<IonIcon size="large" icon={partlySunnyOutline}/>*/}
                                    <IonImg style={{height: "40px"}} src='../assets/images/header_weather_icon.png'/>
                                </IonButton>
                        }
                    </IonButtons>

                    <IonButtons class='en-header-streaming'>
                        {offlineMode ?
                            <IonButton size="small" style={{color: "#ffffff"}} routerLink='/livestreaming'>
                                <IonIcon icon={desktopOutline} className='en-live-streaming'/>
                                <IonIcon icon={caretForwardOutline} className='en-live-streaming-play'/>

                            </IonButton> :
                            // routerLink='/livestreaming'
                            <>
                                <IonButton routerLink='/livestreaming' size="small" shape="round" style={{
                                    color: "#ffffff",
                                    background: "red",
                                    textTransform: "unset",
                                    "borderRadius": "20px",
                                    padding: "0 7px",
                                    fontSize: "16px",
                                    fontWeight: "600"
                                }}>Live TV</IonButton>
                                {/*<IonButton  routerLink='/livestreaming' size="small" style={{color:"#ffffff"}} >*/}
                                {/*<IonIcon icon={desktopOutline}  className='en-live-streaming'/>*/}
                                {/*<IonIcon icon={caretForwardOutline} className='en-live-streaming-play'/>*/}
                                {/*</IonButton>*/}

                                <IonModal
                                    id="viewport"
                                    isOpen={showModal}
                                    className='en-modal'
                                    swipeToClose={true}
                                >
                                    <IonContent
                                        scrollEvents={true}
                                        className="ionContentModal"
                                    >
                                        <IonButtons style={{zIndex: "99999999999999999999", marginRight: "15px"}}
                                                    onClick={() => setShowModal(false)}
                                                    className='ion-float-right en-modalvideo-close'><IonIcon
                                            className="en-modalvideo-close-icon" icon={closeOutline}/></IonButtons>

                                        <ReactPlayer
                                            id='test'
                                            style={{
                                                zIndex: -1,
                                                position: "absolute",
                                                top: "0",
                                                left: "0"
                                            }}
                                            width='100%'
                                            height='100%'
                                            config={{
                                                file: {
                                                    forceHLS: isPlatform("ios") ? /^((?!chrome|android).)*safari/i.test(navigator.userAgent) : true,
                                                    attributes: {
                                                        controls: true,
                                                        controlsList: "noremoteplayback"
                                                    }
                                                }
                                            }} type="application/x-mpegURL"
                                            frameBorder="0"
                                            allow="autoplay; encrypted-media; picture-in-picture"
                                            allowFullScreen
                                            playing={true}
                                            url="https://live2.tvg.ge/eng/EURONEWSGEORGIA/playlist.m3u8"
                                        />
                                    </IonContent>

                                </IonModal>

                            </>
                        }
                        <IonAlert
                            isOpen={showAlert}
                            onDidDismiss={() => setShowAlert(false)}
                            cssClass='my-custom-class en-live-streaming-alert'
                            header={'OFFLINE რეჟიმი '}

                            message={'ამ კონტენტს ვერ ნახავთ offline რეჟიმში. გინდა გადართო ონლაინ რეჟიმში? '}
                            buttons={["დახურვა",
                                {
                                    text: 'დიახ',
                                    handler: async () => {
                                        setOfflineMode(false)
                                        await store.create();
                                        await store.set('offlineMode', false);
                                    }
                                }]}
                        />

                    </IonButtons>
                </div>

            </IonHeader>


        </>
    );
}

export default Header;
