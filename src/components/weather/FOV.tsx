import React, {useEffect, useState} from 'react';
import './DayDetails.css';
import {IonCard, IonCardContent, IonCardHeader, IonCardTitle, IonCol, IonImg, IonRow} from "@ionic/react";
import PressureGauge from "./PressureGauge";


interface FOVProps {
    keywords: any;
    fov: number;
}

export const FOV: React.FC<FOVProps> = ({keywords, fov} : FOVProps) => {

    const [FOVText, setFOVText] = useState<any>();

    useEffect(() => {
        // console.log("FOVText", FOVText);
    }, [FOVText]);

    useEffect(() => {
        if(fov || fov == 0){
            if(fov >= 0 &&  fov <= 0.2)
                setFOVText(keywords.THICK_FOG);
            else if(fov > 0.2 &&  fov <= 0.5)
                setFOVText(keywords.FOG);
            else if(fov > 0.5 &&  fov <= 2)
                setFOVText(keywords.LIGHT_FOG);
            else if(fov > 2 &&  fov <= 2.8)
                setFOVText(keywords.SLIGHT_FOG);
            else if(fov > 2.8 &&  fov <= 4)
                setFOVText(keywords.VERY_SLIGHT_FOG);
            else if(fov > 4 &&  fov <= 10)
                setFOVText(keywords.FOG_IN_PLACES);
            else if(fov > 10 &&  fov <= 18)
                setFOVText(keywords.GOOD_VISIBILITY);
            else if(fov > 18)
                setFOVText(keywords.IDEAL_VISIBILITY);
            console.log("fov", fov);
        }

        // console.log("keywords.fog", keywords.THICK_FOG);
    }, [fov]);

  return (
      <>
          <IonCol className="weather-current-day-parent-col" size="6">
              <IonCard className="glossy-card two-cards-row-card-left">
                  <IonCardHeader className="weather-card-header">
                      <IonCardTitle className="weather-second-color">
                          <IonRow>
                              <IonCol size="2">
                                  <IonImg className="weather-boxes-heading-icon"
                                          src="../assets/weathericons/box_icons/12.png"/>
                              </IonCol>
                              <IonCol className="weather-boxes-main-heading-parent-col" size="8">
                                  <h3 className="weather-boxes-main-heading">{keywords.FIELD_OF_VIEW}</h3>
                              </IonCol>
                          </IonRow>

                      </IonCardTitle>
                  </IonCardHeader>

                  <IonCardContent className="weather-card-content">
                      <IonRow>
                          <IonCol size="12">
                              {/*<h1 className="weather-boxes-second-heading">{fov}</h1>*/}
                              <div className="ion-text-center">
                                  <h1 className="fov-heading d-inline">{fov}  </h1>
                                  <h3 className="fov-heading-sub d-inline" style={{marginLeft:"5px"}}>    {keywords.KM}</h3>
                              </div>
                              <h3 className="fov-heading-sub">{FOVText}</h3>
                              {/*<input className="gradient-slider" id="slide" type="range" min="10" max="100" value="62" disabled/>*/}
                              {/*<h3 className="weather-boxes-description mt-top-1">*/}
                                  {/*Use sunscreen - get an umbrella */}
                                  {/*{FOVText}*/}
                              {/*</h3>*/}
                          </IonCol>
                      </IonRow>
                  </IonCardContent>
              </IonCard>
          </IonCol>
      </>
);}

export default FOV;
