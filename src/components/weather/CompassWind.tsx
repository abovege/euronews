import React, {useEffect, useState} from 'react';
import './DayDetails.css';
import {IonCard, IonCardContent, IonCardHeader, IonCardTitle, IonCol, IonImg, IonRow} from "@ionic/react";
import PressureGauge from "./PressureGauge";


interface CompassProps {
    keywords: any;
    direction: any;
    windSpeed: any;
}

export const CompassWind: React.FC<CompassProps> = ({keywords, direction, windSpeed} : CompassProps) => {
    const [degree, setDegree] = useState<any>();
    useEffect(() => {
        switch(direction) {
            case "N":
                setDegree(0);
                break;
            case "NNE":
                setDegree(22.5);
                break;
            case "NE":
                setDegree(45);
                break;
            case "ENE":
                setDegree(67.5);
                break;
            case "E":
                setDegree(90);
                break;
            case "ESE":
                setDegree(112.5);
                break;
            case "SE":
                setDegree(135);
                break;
            case "SSE":
                setDegree(157.5);
                break;
            case "S":
                setDegree(180);
                break;
            case "SSW":
                setDegree(202.5);
                break;
            case "SW":
                setDegree(225);
                break;
            case "WSW":
                setDegree(247.5);
                break;
            case "W":
                setDegree(270);
                break;
            case "WNW":
                setDegree(292.5);
                break;
            case "NW":
                setDegree(315);
                break;
            case "NNW":
                setDegree(337.5);
                break;
            default:
                setDegree(0);
            // code block
        }
    }, [direction]);
  return (
      <>
          <IonCol className="weather-current-day-parent-col" size="6">
              <IonCard className="glossy-card two-cards-row-card-left">
                  <IonCardHeader className="weather-card-header">
                      <IonCardTitle className="weather-second-color">
                          <IonRow>
                              <IonCol size="2">
                                  <IonImg className="weather-boxes-heading-icon"
                                          src="../assets/weathericons/box_icons/3.png"/>
                              </IonCol>
                              <IonCol className="weather-boxes-main-heading-parent-col" size="8">
                                  <h3 className="weather-boxes-main-heading">{keywords.WIND}</h3>
                              </IonCol>
                          </IonRow>

                      </IonCardTitle>
                  </IonCardHeader>

                  <IonCardContent className="weather-card-content">
                      <IonRow>
                          <IonCol size="12"
                                  className="weather-compass-img-parent ion-justify-content-center">
                              <IonImg className="weather-compass-img-degrees"
                                      src="../assets/images/compass/degrees.png"/>
                              <div>
                                  <IonImg className="weather-compass-img-hand"
                                          style={{
                                              transform: "translate(-50%, -50%) rotate(" + degree + "deg)",
                                              msTransform: "translate(-50%, -50%) rotate(" + degree + "deg)",
                                              WebkitTransform: "translate(-50%, -50%) rotate(" + degree + "deg)",
                                              MozTransform: "translate(-53%, -53%) rotate(" + degree + "deg)",
                                              OTransform: "translate(-50%, -50%) rotate(" + degree + "deg)",
                                              // "-moz-transform": "translate(-50%, -50%) rotate(" + degree + "deg)",
                                              // "-o-transform": "translate(-50%, -50%) rotate(" + degree + "deg)",
                                              // animation: degree ? "spin 0.5s linear 0s 2 alternate" : ""
                                  }}
                                          src="../assets/images/compass/hand_with_circle2.png"/>
                                  <h2 className="weather-compass-img-center-text">{windSpeed}</h2>
                                  <h6 className="weather-compass-img-center-text-mini-desc">{keywords.KM_PER_HOUR}</h6>
                              </div>
                          </IonCol>
                      </IonRow>
                  </IonCardContent>

              </IonCard>
          </IonCol>
      </>
);}

export default CompassWind;
