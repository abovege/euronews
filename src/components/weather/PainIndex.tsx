import React, {useEffect, useState} from 'react';
import './DayDetails.css';
import {IonCard, IonCardContent, IonCardHeader, IonCardTitle, IonCol, IonImg, IonRow} from "@ionic/react";
import PressureGauge from "./PressureGauge";


interface PainIndexProps {
    keywords: any;
    painIndex: number;
    painDesc: string;
}

export const PainIndex: React.FC<PainIndexProps> = ({keywords, painIndex, painDesc} : PainIndexProps) => {

  return (
      <>
          <IonCol className="weather-current-day-parent-col" size="6">
              {painIndex ?
              <IonCard className="glossy-card two-cards-row-card-right">
                  <IonCardHeader className="weather-card-header">
                      <IonCardTitle className="weather-second-color">
                          <IonRow>
                              <IonCol size="2">
                                  <IonImg className="weather-boxes-heading-icon"
                                          src="../assets/weathericons/box_icons/pain.png"/>
                              </IonCol>
                              <IonCol className="weather-boxes-main-heading-parent-col" size="8">
                                  <h3 className="weather-boxes-main-heading">{keywords.PAIN_INDEX}</h3>
                              </IonCol>
                          </IonRow>

                      </IonCardTitle>
                  </IonCardHeader>

                  <IonCardContent className="weather-card-content">
                      <IonRow>
                          <IonCol size="12">
                              <h1 className="weather-boxes-second-heading pain-index-heading">{painIndex}</h1>
                              <h3 className="weather-boxes-description pain-index-desc mt-top-1">
                                  {painDesc}
                              </h3>
                          </IonCol>

                      </IonRow>
                  </IonCardContent>
              </IonCard>
                  : ""}
          </IonCol>
      </>
);}

export default PainIndex;
