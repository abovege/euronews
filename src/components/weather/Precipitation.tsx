import React, {useEffect, useState} from 'react';
import './DayDetails.css';
import {IonCard, IonCardContent, IonCardHeader, IonCardTitle, IonCol, IonImg, IonRow} from "@ionic/react";
import PressureGauge from "./PressureGauge";


interface PrecipitationProps {
    keywords: any;
    todayPrecipitation: any;
    nextDayPrecipitation: any;
}

export const Precipitation: React.FC<PrecipitationProps> = ({keywords, todayPrecipitation, nextDayPrecipitation} : PrecipitationProps) => {
    const [degree, setDegree] = useState<any>();

  return (
      <>
          <IonCol className="weather-current-day-parent-col" size="6">
              <IonCard className="glossy-card two-cards-row-card-right">
                  <IonCardHeader className="weather-card-header">
                      <IonCardTitle className="weather-second-color">
                          <IonRow>
                              <IonCol size="2">
                                  <IonImg className="weather-boxes-heading-icon"
                                          src="../assets/weathericons/box_icons/10.png"/>
                              </IonCol>
                              <IonCol className="weather-boxes-main-heading-parent-col" size="8">
                                  <h3 className="weather-boxes-main-heading">{keywords.PRECIPITATION}</h3>
                              </IonCol>
                          </IonRow>

                      </IonCardTitle>
                  </IonCardHeader>

                  <IonCardContent className="weather-card-content">
                      <IonRow>
                          <IonCol size="12">
                              <h1 className="weather-boxes-second-heading">{todayPrecipitation}-mm</h1>
                              <h3 className="weather-boxes-description">{keywords.DURING_THE_LAST_24_HOURS}</h3>
                              <br/>
                              <h3 className="weather-boxes-description weather-second-color">
                                  {keywords?.IS_EXPECTED_IN_THE_NEXT_24_HOURS?.replace('%', nextDayPrecipitation + "-mm")}
                              </h3>
                          </IonCol>
                      </IonRow>
                  </IonCardContent>
              </IonCard>
          </IonCol>
      </>
);}

export default Precipitation;
