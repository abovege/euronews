import React, {useEffect, useState} from 'react';
import './DayDetails.css';
import {IonCard, IonCardContent, IonCardHeader, IonCardTitle, IonCol, IonImg, IonRow} from "@ionic/react";
import PressureGauge from "./PressureGauge";


interface RealFeelProps {
    keywords: any;
    realFeel: number;
}

export const RealFeel: React.FC<RealFeelProps> = ({keywords, realFeel} : RealFeelProps) => {

  return (
      <>
          <IonCol className="weather-current-day-parent-col" size="6">
              <IonCard className="glossy-card two-cards-row-card-left">
                  <IonCardHeader className="weather-card-header">
                      <IonCardTitle className="weather-second-color">
                          <IonRow>
                              <IonCol size="2">
                                  <IonImg className="weather-boxes-heading-icon"
                                          src="../assets/weathericons/box_icons/4.png"/>
                              </IonCol>
                              <IonCol className="weather-boxes-main-heading-parent-col" size="8">
                                  <h3 className="weather-boxes-main-heading">{keywords.REAL_FEELING}</h3>
                              </IonCol>
                          </IonRow>

                      </IonCardTitle>
                  </IonCardHeader>

                  <IonCardContent className="weather-card-content">
                      <IonRow>
                          <IonCol size="12">
                              {/*<br/>*/}
                              {/*<h3 className="weather-boxes-description mt-top-1"> </h3>*/}
                              <div className="ion-text-center">
                                <h1 className="realfeel-heading">{realFeel}°</h1>
                              </div>
                              {/*<h3 className="weather-boxes-description mt-top-1">*/}
                              {/*    {keywords.CLOSE_TO_THE_ACTUAL_TEMPERATURE}*/}
                              {/*</h3>*/}
                          </IonCol>

                      </IonRow>
                  </IonCardContent>
              </IonCard>
          </IonCol>
      </>
);}

export default RealFeel;
