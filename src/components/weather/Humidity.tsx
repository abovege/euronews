import React, {useEffect, useState} from 'react';
import './DayDetails.css';
import {IonCard, IonCardContent, IonCardHeader, IonCardTitle, IonCol, IonImg, IonRow} from "@ionic/react";
import PressureGauge from "./PressureGauge";


interface HumidityProps {
    keywords: any;
    humidity: number;
    dewPoint: number;
}

export const Humidity: React.FC<HumidityProps> = ({keywords, humidity, dewPoint} : HumidityProps) => {

  return (
      <>
          <IonCol className="weather-current-day-parent-col" size="6">
              <IonCard className="glossy-card two-cards-row-card-right">
                  <IonCardHeader className="weather-card-header">
                      <IonCardTitle className="weather-second-color">
                          <IonRow>
                              <IonCol size="2">
                                  <IonImg className="weather-boxes-heading-icon"
                                          src="../assets/weathericons/box_icons/10.png"/>
                              </IonCol>
                              <IonCol className="weather-boxes-main-heading-parent-col" size="8">
                                  <h3 className="weather-boxes-main-heading">{keywords.HUMIDITY}</h3>
                              </IonCol>
                          </IonRow>

                      </IonCardTitle>
                  </IonCardHeader>

                  <IonCardContent className="weather-card-content">
                      <IonRow>
                          <IonCol size="12">
                              <br/>
                              <h1 className="weather-boxes-second-heading">{humidity}%</h1>
                              <h3 className="weather-boxes-description mt-top-1">
                                  {keywords?.DEW_FORMATION_TEMPERATURE_IS?.replace('%', dewPoint + "°")}
                              </h3>
                          </IonCol>
                      </IonRow>
                  </IonCardContent>
              </IonCard>
          </IonCol>
      </>
);}

export default Humidity;
