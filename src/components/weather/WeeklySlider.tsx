import React, {useContext, useState,useEffect} from 'react';
import { IonRow, IonCol, IonIcon, IonRippleEffect,  } from '@ionic/react';
import './WeeklySlider.css';
import { caretForwardCircleOutline, ellipse } from 'ionicons/icons';

import { Link } from 'react-router-dom';
import {darkThemeContext} from '../../state/State'
import LinesEllipsis from 'react-lines-ellipsis'


interface WeeklySliderProps {
    keywords: any;
    min: number;
    max: number;
    leftValue: number;
    rightValue: number;
    current?: number;
}

export const WeeklySlider: React.FC<WeeklySliderProps> = ({keywords, min, max, leftValue, rightValue,current} : WeeklySliderProps) => {

    const totalRange = max - min;
    const rangeStart = ((leftValue - min) / totalRange) * 95;
    const rangeWidth = ((rightValue - leftValue) / totalRange) * 95;
    const color = getColor(leftValue, rightValue);

    // const [totalRange, setTotalRange] = useState<any>();
    // const [rangeStart, setRangeStart] = useState<any>();
    // const [rangeWidth, setRangeWidth] = useState<any>();
    // const [color, setColor] = useState<any>();

    // let min1 = 7;
    // let max1 = 56;
    leftValue = (leftValue - min) / (max - min) * 100;
    rightValue = 100 - (rightValue / (max) * 100);

    if(current)
        current = ((current - min) / (max - min) * 100);

    // console.log("leftValue", leftValue);
    // console.log("min,max", min,max);



    function getColor(low:any, high:any) {
        const avg = (low + high) / 2;
        if (avg < 0) return 'linear-gradient(to right, #E0F7FA, #B3E5FC)';
        if (avg < 10) return 'linear-gradient(to right, #ADD8E6, #87CEEB)';
        if (avg < 20) return 'linear-gradient(to right, #87CEEB, #FFD700)';
        if (avg < 30) return 'linear-gradient(to right, #FFD700, #FFA500)';
        return 'linear-gradient(to right, #FF6347, #B22222)';
    }

    // useEffect(() => {
    //     setTotalRange(rightValue - leftValue);
    //     setColor(getColor(min, max));
    // }, []);
    //
    // useEffect(() => {
    //     if(totalRange) {
    //         setRangeStart(((min - leftValue) / totalRange) * 300);
    //         setRangeWidth(((max - min) / totalRange) * 300);
    //     }
    // }, [totalRange]);
    //
    // useEffect(() => {
    //     if(color)
    //         console.log('color', color);
    // }, [color]);


  return (
      <>
          <div className="slider-container">
              {/*<span className="temp-label" style={{ left: 0 }}>{low}°</span>*/}
              <div className="slider-bar">
                  <div
                      className="temperature-range"
                      style={{ left:`${rangeStart}px`, width: `${rangeWidth}px`, background: color }}
                  ></div>
              </div>
              {/*<span className="temp-label" style={{ right: 0 }}>{high}°</span>*/}
          </div>

          {/*<div id="slider-container">*/}
          {/*<div className="slider-bar">*/}
          {/*    <div className="temperature-range" style={{left:rangeStart + "px",width:rangeWidth + "px",background:color}}></div>*/}
          {/*</div>*/}
          {/*</div>*/}
          {/*<div className="slider-range">*/}
          {/*    <div className="slider-back"/>*/}
          {/*    <div className="right-mask" style={{width: rightValue + "%"}}/>*/}
          {/*    <div className="left-mask" style={{width:leftValue + "%"}}/>*/}
          {/*    <div className="range" style={{left:leftValue + "%",right: rightValue + "%"}}/>*/}
          {/*    {current ?*/}
          {/*        <span className="current-knob" style={{left: current + "%"}}/> : ""*/}
          {/*    }*/}
          {/*    /!*<span style={{left:"60%"}}/>*!/*/}
          {/*</div>*/}
      </>
);}

export default WeeklySlider;
