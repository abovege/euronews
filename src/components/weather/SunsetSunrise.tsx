import React, {useEffect, useState} from 'react';
import './DayDetails.css';
import {IonCard, IonCardContent, IonCardHeader, IonCardTitle, IonCol, IonImg, IonRow} from "@ionic/react";
import PressureGauge from "./PressureGauge";


interface SunsetSunriseProps {
    keywords: any;
    sunset: any;
    sunrise: any;
}

export const SunsetSunrise: React.FC<SunsetSunriseProps> = ({keywords, sunset, sunrise} : SunsetSunriseProps) => {
    const [degree, setDegree] = useState<any>();

  return (
      <>
          <IonCol className="weather-current-day-parent-col" size="6">
              <IonCard className="glossy-card two-cards-row-card-right">
                  <IonCardHeader className="weather-card-header">
                      <IonCardTitle className="weather-second-color">
                          <IonRow>
                              <IonCol size="2">
                                  <IonImg className="weather-boxes-heading-icon"
                                          src="../assets/weathericons/box_icons/8.png"/>
                              </IonCol>
                              <IonCol className="weather-boxes-main-heading-parent-col" size="8">
                                  <h3 className="weather-boxes-main-heading">{keywords.SUNSET}</h3>
                              </IonCol>
                          </IonRow>
                      </IonCardTitle>
                  </IonCardHeader>

                  <IonCardContent className="weather-card-content">
                      <IonRow>
                          <IonCol size="12">
                              <h1 className="weather-boxes-second-heading">{sunset}</h1>
                              <IonRow>
                                  <IonCol size="2">
                                      <IonImg className="weather-boxes-heading-icon"
                                              src="../assets/weathericons/box_icons/9.png"/>
                                  </IonCol>
                                  <IonCol className="weather-boxes-main-heading-parent-col" size="8">
                                      <h3 className="weather-boxes-main-heading weather-second-color">{keywords.SUNRISE}</h3>
                                  </IonCol>
                              </IonRow>
                              <h1 className="weather-boxes-second-heading">{sunrise}</h1>
                          </IonCol>

                      </IonRow>
                  </IonCardContent>
              </IonCard>
          </IonCol>
      </>
);}

export default SunsetSunrise;
