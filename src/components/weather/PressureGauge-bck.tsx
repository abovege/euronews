import React, {useEffect, useState} from 'react';
import './PressureGauge.css';
import {IonCard, IonCardContent, IonCardHeader, IonCardTitle, IonCol, IonImg, IonRow} from "@ionic/react";


interface PressureGaugeProps {
    keywords: any;
    currentPressure: number;
}

export const PressureGauge: React.FC<PressureGaugeProps> = ({keywords, currentPressure} : PressureGaugeProps) => {
    const [value, setValue] = useState(0); // integer state
    //create your forceUpdate hook
    function useForceUpdate(){
        console.log('value',value);
        return () => setValue(value => value + 1); // update state to force render
        // A function that increment 👆🏻 the previous state like here
        // is better than directly setting `setValue(value + 1)`

    }

    function canvas_arrow(context:any, fromx:number, fromy:number, tox:number, toy:number) {
        var headlen = 15; // length of head in pixels
        var dx = tox - fromx;
        var dy = toy - fromy;
        var angle = Math.atan2(dy, dx);
        context.moveTo(fromx, fromy);
        context.lineTo(tox, toy);
        context.lineWidth = 2.5;
        context.moveTo(tox, toy);
        context.lineTo(tox - headlen * Math.cos(angle - Math.PI / 6), toy - headlen * Math.sin(angle - Math.PI / 6));
        context.moveTo(tox, toy);
        context.lineTo(tox - headlen * Math.cos(angle + Math.PI / 6), toy - headlen * Math.sin(angle + Math.PI / 6));
    }

    function drawRotated(ctx:any, fromx:number, fromy:number, tox:number, toy:number, canvas:any, image:any, currentPressure:number) {
        var dx = tox - fromx;
        var dy = toy - fromy;
        var angle = Math.atan2(dy, dx);
        console.log('fromx ', fromx, 'fromy ', fromy, 'tox ', tox, 'toy ', toy);
        rotateBase64Image("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAbklEQVR4nO3WwQnCQBAF0A1qG6nEblKJ7URbMiXkmOvzoCDk5CHuhvAfzPnDMLOzpUTECjoMOJeacPN2rxqOHs9P+AOXhP9d2u7XgcMV80a1+Bpx2l/wxkM2Vd1tCT3cw6HVkdDwLHZNPgIRu/MC/NK7ClbsEAQAAAAASUVORK5CYII=",
            (base64_arrow:any) => {
                let base64_arrow_image = new Image();
                base64_arrow_image.src = base64_arrow;
                base64_arrow_image.onload = function() {
                    // drawRotated(context, xy.x, xy.y, centerxy.x, centerxy.y, canvas, base64_arrow_image);
                    ctx.save();
                    let xoffset = 0;
                    let yoffset = 0;
                    if(currentPressure <= 915) {
                        xoffset = 3;
                        yoffset = 3;
                    } else if(currentPressure <= 925 && currentPressure > 915 ) {
                        xoffset = 5;
                        yoffset = 5;
                    } else if(currentPressure <= 940 && currentPressure > 925 ) {
                        xoffset = 5;
                        yoffset = 0;
                    } else if(currentPressure <= 960 && currentPressure > 940 ) {
                        xoffset = 3;
                        yoffset = -5;
                    } else if(currentPressure <= 980 && currentPressure > 960 ) {
                        xoffset = 3;
                        yoffset = -10;
                    } else if(currentPressure <= 1000 && currentPressure > 980 ) {
                        xoffset = -2;
                        yoffset = -10;
                    } else if(currentPressure <= 1030 && currentPressure > 1000 ) {
                        xoffset = -4;
                        yoffset = -10;
                    } else if(currentPressure <= 1060 && currentPressure > 1030 ) {
                        xoffset = -8;
                        yoffset = -10;
                    } else if(currentPressure <= 1099 && currentPressure > 1060 ) {
                        xoffset = -12;
                        yoffset = -5;
                    }

                    ctx.drawImage(base64_arrow_image,fromx-(base64_arrow_image.width/2 + xoffset),fromy-(base64_arrow_image.height/2 + yoffset), 28,28);
                    ctx.restore();
                }
            },angle);


    }

    function rotateBase64Image(base64data:any, callback:any, angle:any) {
         let canvas = document.getElementById("arrow_canvas") as HTMLCanvasElement;
         let ctx = canvas?.getContext("2d");

        var image = new Image();
        image.src = base64data;
        // await image.decode();

        image.onload = function() {
            if(ctx) {
                ctx.clearRect(0,0,canvas.width,canvas.height);
                ctx.translate(image.width / 2, image.height / 2);
                ctx.rotate(angle);
                ctx.translate(-(image.width / 2), -(image.height / 2));
                ctx.drawImage(image, 0, 0);
                console.log('angle', angle);
                // return canvas.toDataURL();
                callback(canvas.toDataURL());
                // window.eval(""+callback+"('"+canvas.toDataURL()+"')");
            }
        }
        // return null;
            // window.eval(""+callback+"('"+canvas.toDataURL()+"')");
        // };

    }

    function getPositionAlongTheLine(x1:number, y1:number, x2:number, y2:number, percentage:number) {
        return {x : x1 * (1.0 - percentage) + x2 * percentage, y : y1 * (1.0 - percentage) + y2 * percentage};
    }

    const forceUpdate = useForceUpdate();

    useEffect(() => {
        if(!currentPressure)
            return;

        let canvas = document.getElementById("canvas") as HTMLCanvasElement;
        let context = canvas?.getContext("2d");
        if (context) {
            console.log('force update');
            forceUpdate();
            context.clearRect(0, 0, canvas.width, canvas.height);
            context.beginPath();

        }
        let dotsPerCircle=97;

        let interval=(Math.PI*2)/dotsPerCircle;

        let centerX=75;
        let centerY=70;
        let radius=70;

        // currentPressure = 950;

        let coefficient = 200 / 70; //200 comes from: (1100 - 900) the min and maximu air pressure. 70 comes from dotsPerCircle - 37 + 10
        let calculatedpressure = 0;
        calculatedpressure = (currentPressure - 900) / coefficient;
        calculatedpressure = calculatedpressure - 10;
        // console.log("calculatedpressure", calculatedpressure);
        calculatedpressure = Math.round(calculatedpressure);
        currentPressure = Math.round(currentPressure);

        var image=document.createElement("img");
        image.onload=function(){
            // context?.drawImage(image,canvas.width/8-image.width/8,canvas.height/8-image.width/8);
        }
        image.src="/assets/images/arrow_right.png";

        for(var i=-10;i<dotsPerCircle-37;i++){


            let desiredRadianAngleOnCircle;
            desiredRadianAngleOnCircle = interval * -i;

            var x = centerX + radius * Math.cos(desiredRadianAngleOnCircle);
            var y = centerY + radius * Math.sin(desiredRadianAngleOnCircle);

            // console.log("desiredRadianAngleOnCircle", desiredRadianAngleOnCircle);
            if (context) {

                context?.beginPath();
                /* context.arc(x,y,2,0,Math.PI*2) */
                context?.rect(x, y, 2, 2);
                if(i < calculatedpressure) {
                    context.fillStyle = "white";
                    // console.log("i less than 20", desiredRadianAngleOnCircle);
                }
                else
                    context.fillStyle = "#91d8f7";
                // canvas_arrow(context, 10, 30, 200, 150);

                // console.log("currentPressure", calculatedpressure);

                context?.closePath();
                context?.fill();
                if(i == calculatedpressure - 1) {
                    context.beginPath();
                    context.strokeStyle = "white";
                    let mag = Math.sqrt(centerX * centerX + centerY * centerY);
                    // console.log("centerX: ", centerX);
                    // console.log("centerY: ", centerY);

                    var centerxy = getPositionAlongTheLine(x, y, centerX, centerY, 0.45);

                    var xy = getPositionAlongTheLine(x, y, centerX, centerY, 0.15);

                    // canvas_arrow(context, xy.x, xy.y, centerxy.x, centerxy.y);
                    image.onload=function(){
                        // context?.drawImage(image,canvas.width/8-image.width/8,canvas.height/8-image.width/8);
                        drawRotated(context, xy.x, xy.y, centerxy.x, centerxy.y, canvas, image,currentPressure);
                    }

                    context.font = "38px sans-serif";
                    context.fillStyle = "white";
                    context.textAlign = "center";
                    context.fillText(currentPressure.toString(), canvas.width/2, canvas.height/2+23);

                    context?.closePath();
                    context.stroke();
                }
            }

        }

    }, [currentPressure]);

  return (
      <>
          <IonCol className="weather-current-day-parent-col" size="6">
              <IonCard className="glossy-card two-cards-row-card-right">
                  <IonCardHeader className="weather-card-header">
                      <IonCardTitle className="weather-second-color">
                          <IonRow>
                              <IonCol size="2">
                                  <IonImg className="weather-boxes-heading-icon" src="../assets/weathericons/box_icons/11.png"/>
                              </IonCol>
                              <IonCol className="weather-boxes-main-heading-parent-col" size="8">
                                  <h3 className="weather-boxes-main-heading">{keywords.PRESSURE}</h3>
                              </IonCol>
                          </IonRow>
                      </IonCardTitle>
                  </IonCardHeader>

                  <IonCardContent className="weather-card-content">
                      <IonRow>
                          <IonCol size="12" className="weather-compass-img-parent ion-justify-content-center">
                              {/*<IonImg className="weather-compass-img" src="../assets/images/compass/se1.png"/>*/}
                              <canvas id="canvas" width="150" height="120"/>
                              <canvas id="arrow_canvas" width="32" height="32" style={{display: "none"}}/>

                          </IonCol>
                      </IonRow>
                  </IonCardContent>
              </IonCard>
          </IonCol>
      </>
);}

export default PressureGauge;
