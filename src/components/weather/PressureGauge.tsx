import React, {useEffect, useState} from 'react';
import './PressureGauge.css';
import {IonCard, IonCardContent, IonCardHeader, IonCardTitle, IonCol, IonImg, IonRow} from "@ionic/react";


interface PressureGaugeProps {
    keywords: any;
    currentPressure: number;
}

export const PressureGauge: React.FC<PressureGaugeProps> = ({keywords, currentPressure} : PressureGaugeProps) => {


    function drawRotated(ctx:any, fromx:number, fromy:number, tox:number, toy:number, canvas:any, image:any, currentPressure:number) {
        var dx = tox - fromx;
        var dy = toy - fromy;
        var angle = Math.atan2(dy, dx);
        console.log('fromx ', fromx, 'fromy ', fromy, 'tox ', tox, 'toy ', toy);
        rotateBase64Image("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAbklEQVR4nO3WwQnCQBAF0A1qG6nEblKJ7URbMiXkmOvzoCDk5CHuhvAfzPnDMLOzpUTECjoMOJeacPN2rxqOHs9P+AOXhP9d2u7XgcMV80a1+Bpx2l/wxkM2Vd1tCT3cw6HVkdDwLHZNPgIRu/MC/NK7ClbsEAQAAAAASUVORK5CYII=",
            (base64_arrow:any) => {
                let base64_arrow_image = new Image();
                base64_arrow_image.src = base64_arrow;
                base64_arrow_image.onload = function() {
                    // drawRotated(context, xy.x, xy.y, centerxy.x, centerxy.y, canvas, base64_arrow_image);
                    ctx.save();
                    let xoffset = 0;
                    let yoffset = 0;
                    if(currentPressure <= 915) {
                        xoffset = 3;
                        yoffset = 3;
                    } else if(currentPressure <= 925 && currentPressure > 915 ) {
                        xoffset = 5;
                        yoffset = 5;
                    } else if(currentPressure <= 940 && currentPressure > 925 ) {
                        xoffset = 5;
                        yoffset = 0;
                    } else if(currentPressure <= 960 && currentPressure > 940 ) {
                        xoffset = 3;
                        yoffset = -5;
                    } else if(currentPressure <= 980 && currentPressure > 960 ) {
                        xoffset = 3;
                        yoffset = -10;
                    } else if(currentPressure <= 1000 && currentPressure > 980 ) {
                        xoffset = -2;
                        yoffset = -10;
                    } else if(currentPressure <= 1030 && currentPressure > 1000 ) {
                        xoffset = -4;
                        yoffset = -10;
                    } else if(currentPressure <= 1060 && currentPressure > 1030 ) {
                        xoffset = -8;
                        yoffset = -10;
                    } else if(currentPressure <= 1099 && currentPressure > 1060 ) {
                        xoffset = -12;
                        yoffset = -5;
                    }

                    ctx.drawImage(base64_arrow_image,fromx-(base64_arrow_image.width/2 + xoffset),fromy-(base64_arrow_image.height/2 + yoffset), 28,28);
                    ctx.restore();
                }
            },angle);


    }

    function rotateBase64Image(base64data:any, callback:any, angle:any) {
         let canvas = document.getElementById("arrow_canvas") as HTMLCanvasElement;
         let ctx = canvas?.getContext("2d");

        var image = new Image();
        image.src = base64data;
        // await image.decode();

        image.onload = function() {
            if(ctx) {
                ctx.clearRect(0,0,canvas.width,canvas.height);
                ctx.translate(image.width / 2, image.height / 2);
                ctx.rotate(angle);
                ctx.translate(-(image.width / 2), -(image.height / 2));
                ctx.drawImage(image, 0, 0);
                console.log('angle', angle);
                // return canvas.toDataURL();
                callback(canvas.toDataURL());
                // window.eval(""+callback+"('"+canvas.toDataURL()+"')");
            }
        }
        // return null;
            // window.eval(""+callback+"('"+canvas.toDataURL()+"')");
        // };

    }

    function getPositionAlongTheLine(x1:number, y1:number, x2:number, y2:number, percentage:number) {
        return {x : x1 * (1.0 - percentage) + x2 * percentage, y : y1 * (1.0 - percentage) + y2 * percentage};
    }


    function drawWidget(pressure:any, ctx:any, canvas:any) {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.fillStyle = "transparent";
        ctx.fillRect(0, 0, canvas.width, canvas.height);

        const angle = mapPressureToAngle(pressure);
        ctx.lineWidth = 2; // Adjust line width for smaller canvas

        // Increase the arc size to fit better
        const arcRadius = 55; // Increase the arc radius to make it larger but still fit within the canvas

        // Darker Arc (after arrow)
        ctx.setLineDash([2, 5]);
        ctx.strokeStyle = "#B0C4DE";
        ctx.beginPath();
        ctx.arc(75, 60, arcRadius, angle, 0.75 * Math.PI, true);
        ctx.stroke();

        // White Arc (before arrow)
        ctx.strokeStyle = "white";
        ctx.beginPath();
        ctx.arc(75, 60, arcRadius, 2.25 * Math.PI, angle, true);
        ctx.stroke();

        // Arrow with adjusted gap from Arc
        const gap = 15; // Increased gap to move the arrow closer to the center
        const arcX = 75 + (arcRadius - gap) * Math.cos(angle);
        const arcY = 60 + (arcRadius - gap) * Math.sin(angle);

        // Decrease the arrow size
        const arrowSize = 24; // Smaller arrow size
        ctx.save(); // Save current context
        ctx.translate(arcX, arcY); // Move to the center of the arc
        ctx.rotate(angle + Math.PI); // Rotate the arrow

        const arrowImage = new Image();
        arrowImage.src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAbklEQVR4nO3WwQnCQBAF0A1qG6nEblKJ7URbMiXkmOvzoCDk5CHuhvAfzPnDMLOzpUTECjoMOJeacPN2rxqOHs9P+AOXhP9d2u7XgcMV80a1+Bpx2l/wxkM2Vd1tCT3cw6HVkdDwLHZNPgIRu/MC/NK7ClbsEAQAAAAASUVORK5CYII=";

        // Wait for the image to load before drawing
        arrowImage.onload = function() {
            ctx.drawImage(arrowImage, -arrowSize / 2, -arrowSize / 2, arrowSize, arrowSize); // Draw image at the new position with smaller size
            ctx.restore(); // Restore the context

            // Pressure Value Display (increased font size to 20px)
            ctx.font = "20px Arial"; // Set font size to 20px
            ctx.fillStyle = "white";
            ctx.textAlign = "center";
            ctx.fillText(pressure, 75, 70); // Adjusted y-position to move text higher
        };


    }


    function mapPressureToAngle(pressure:any) {
        const minPressure = 915;
        const maxPressure = 1060;
        return 2.25 * Math.PI - ((2.25 * Math.PI - 0.75 * Math.PI) * (pressure - minPressure) / (maxPressure - minPressure));
    }

    useEffect(() => {
        if(!currentPressure)
            return;

        let canvas = document.getElementById("canvas") as HTMLCanvasElement;
        let context = canvas?.getContext("2d");
        if (context) {
            context.clearRect(0, 0, canvas.width, canvas.height);
            context.beginPath();

        }


        currentPressure = Math.round(currentPressure);

        drawWidget(currentPressure, context, canvas);

    }, [currentPressure]);

  return (
      <>
          <IonCol className="weather-current-day-parent-col" size="6">
              <IonCard className="glossy-card two-cards-row-card-right">
                  <IonCardHeader className="weather-card-header">
                      <IonCardTitle className="weather-second-color">
                          <IonRow>
                              <IonCol size="2">
                                  <IonImg className="weather-boxes-heading-icon" src="../assets/weathericons/box_icons/11.png"/>
                              </IonCol>
                              <IonCol className="weather-boxes-main-heading-parent-col" size="8">
                                  <h3 className="weather-boxes-main-heading">{keywords.PRESSURE}</h3>
                              </IonCol>
                          </IonRow>
                      </IonCardTitle>
                  </IonCardHeader>

                  <IonCardContent className="weather-card-content">
                      <IonRow>
                          <IonCol size="12" className="weather-compass-img-parent ion-justify-content-center">
                              {/*<IonImg className="weather-compass-img" src="../assets/images/compass/se1.png"/>*/}
                              <canvas id="canvas" width="150" height="120"/>
                              <canvas id="arrow_canvas" width="32" height="32" style={{display: "none"}}/>

                          </IonCol>
                      </IonRow>
                  </IonCardContent>
              </IonCard>
          </IonCol>
      </>
);}

export default PressureGauge;
