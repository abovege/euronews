import React, {useEffect, useState} from 'react';
import './DayDetails.css';
import {IonCard, IonCardContent, IonCardHeader, IonCardTitle, IonCol, IonImg, IonRow} from "@ionic/react";
import PressureGauge from "./PressureGauge";


interface AirQualityProps {
    keywords: any;
    airQualityIndex: number;
    airQualityCat: string;
    airQualityMsg: string;
}

export const AirQuality: React.FC<AirQualityProps> = ({keywords, airQualityIndex,airQualityCat,airQualityMsg} : AirQualityProps) => {

  return (
      <>
          <IonRow>
              <IonCol className="weather-current-day-parent-col">
                  <IonCard className="glossy-card">
                      <IonCardHeader className="weather-card-header">
                          <IonCardTitle className="weather-second-color">
                              <IonRow>
                                  <IonCol size="1.2">
                                      <IonImg className="weather-boxes-heading-icon"
                                              src="../assets/weathericons/box_icons/6.png"/>
                                  </IonCol>
                                  <IonCol className="weather-boxes-main-heading-parent-col" size="10">
                                      <h3 className="weather-boxes-main-heading">{keywords.AIR_QUALITY}</h3>
                                  </IonCol>
                              </IonRow>

                          </IonCardTitle>
                      </IonCardHeader>

                      <IonCardContent className="weather-card-content">
                          <IonRow>
                              <IonCol size="12">
                                  <h1 className="weather-boxes-second-heading">{airQualityIndex} - {airQualityCat}</h1>
                                  <input className="gradient-slider-aqi" id="slide" type="range" min="0" max="300" value={airQualityIndex || 0} disabled/>
                                  <h3 className="weather-boxes-description mt-top-1"> {airQualityMsg}</h3>
                              </IonCol>

                          </IonRow>
                      </IonCardContent>
                  </IonCard>
              </IonCol>
          </IonRow>
      </>
);}

export default AirQuality;
