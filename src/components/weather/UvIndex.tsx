import React, {useEffect, useState} from 'react';
import './DayDetails.css';
import {IonCard, IonCardContent, IonCardHeader, IonCardTitle, IonCol, IonImg, IonRow} from "@ionic/react";
import PressureGauge from "./PressureGauge";


interface UVIndexProps {
    keywords: any;
    uvIndex: any;
    uvIndexText: any;
}

export const UvIndex: React.FC<UVIndexProps> = ({keywords, uvIndex, uvIndexText} : UVIndexProps) => {
    const [uvIndexDesc, setUvIndexDesc] = useState<any>();
    useEffect(() => {
        if(uvIndex || uvIndex == 0){
            if(uvIndex >= 1 &&  uvIndex <= 2)
                setUvIndexDesc(keywords.SAFE_TO_BE_OUTSIDE);
            else if(uvIndex >= 3 &&  uvIndex <= 5)
                setUvIndexDesc(keywords.GET_LESS_EXPOSED_TO_SUNLIGHT);
            else if(uvIndex >= 6 &&  uvIndex <= 7)
                // setUvIndexDesc("Cover up, wear a hat, sunglasses & use sunscreen");
                setUvIndexDesc(keywords.HIGH_RISK_OF_SUN_BURN);
            else if(uvIndex >= 8 &&  uvIndex <= 10)
                // setUvIndexDesc("Try to avoid sun, use umbrella and sunscreen");
                // setUvIndexDesc([keywords.TRY_TO_AVOID_SUN, keywords.USE_UMBRELLA, keywords.USE_SUNSCREEN, keywords.USE_SUNGLASSES][Math.floor(Math.random()*4)]);
                setUvIndexDesc(keywords.DANGEROUS);
            else if(uvIndex >= 11)
                setUvIndexDesc(keywords.GET_NOT_EXPOSED_TO_SUNLIGHT);
            else
                setUvIndexDesc(keywords.SAFE_TO_BE_OUTSIDE);

        }

        // console.log("keywords", keywords);
    }, [uvIndex]);
  return (
      <>
          <IonCol className="weather-current-day-parent-col" size="6">
              <IonCard className="glossy-card two-cards-row-card-left">
                  <IonCardHeader className="weather-card-header">
                      <IonCardTitle className="weather-second-color">
                          <IonRow>
                              <IonCol size="2">
                                  <IonImg className="weather-boxes-heading-icon"
                                          src="../assets/weathericons/box_icons/5.png"/>
                              </IonCol>
                              <IonCol className="weather-boxes-main-heading-parent-col" size="8">
                                  <h3 className="weather-boxes-main-heading">{keywords.UV_INDEX}</h3>
                              </IonCol>
                          </IonRow>

                      </IonCardTitle>
                  </IonCardHeader>

                  <IonCardContent className="weather-card-content">
                      <IonRow>
                          <IonCol size="12">
                              <h1 className="weather-boxes-second-heading">{uvIndex}</h1>
                              <h3 className="weather-boxes-third-heading"> - {uvIndexText}</h3>
                              <input className="gradient-slider-uv" id="slide" type="range" min="0" max="11" value={uvIndex || 0} disabled/>
                              <h3 className="weather-boxes-description-uv mt-top-1">
                                  {uvIndexDesc}
                              </h3>
                          </IonCol>

                      </IonRow>
                  </IonCardContent>
              </IonCard>
          </IonCol>
      </>
);}

export default UvIndex;
