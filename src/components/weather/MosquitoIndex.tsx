import React, {useEffect, useState} from 'react';
import './DayDetails.css';
import {IonCard, IonCardContent, IonCardHeader, IonCardTitle, IonCol, IonImg, IonRow} from "@ionic/react";
import PressureGauge from "./PressureGauge";


interface MosquitoIndexProps {
    keywords: any;
    mosquitoIndex:number;
    mosquitoDesc: string;
}

export const MosquitoIndex: React.FC<MosquitoIndexProps> = ({keywords, mosquitoIndex, mosquitoDesc} : MosquitoIndexProps) => {

  return (
      <>
          <IonCol className="weather-current-day-parent-col" size="6">
              {(typeof mosquitoIndex !== 'undefined')  ?
                  <IonCard className="glossy-card two-cards-row-card-left">
                      <IonCardHeader className="weather-card-header">
                          <IonCardTitle className="weather-second-color">
                              <IonRow>
                                  <IonCol size="2">
                                      <IonImg className="weather-boxes-heading-icon"
                                              src="../assets/weathericons/box_icons/mosquito.png"/>
                                  </IonCol>
                                  <IonCol className="weather-boxes-main-heading-parent-col" size="8">
                                      <h3 className="weather-boxes-main-heading">{keywords.MOSQUITO_INDEX}</h3>
                                  </IonCol>
                              </IonRow>

                          </IonCardTitle>
                      </IonCardHeader>

                      <IonCardContent className="weather-card-content">
                          <IonRow>
                              <IonCol size="12">
                                  <h1 className="weather-boxes-second-heading mosquito-index-heading">{mosquitoIndex}</h1>
                                  <h3 className="weather-boxes-description mosquito-index-desc mt-top-1">
                                      {mosquitoDesc}
                                  </h3>
                              </IonCol>

                          </IonRow>
                      </IonCardContent>
                  </IonCard>
                  : ""}
          </IonCol>
      </>
);}

export default MosquitoIndex;
