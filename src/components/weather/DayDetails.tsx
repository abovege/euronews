import React, {useEffect, useState} from 'react';
import './DayDetails.css';
import {IonCard, IonCardContent, IonCardHeader, IonCardTitle, IonCol, IonImg, IonRow} from "@ionic/react";
import PressureGauge from "./PressureGauge";
import CompassWind from "./CompassWind";
import UvIndex from "./UvIndex";
import SunsetSunrise from "./SunsetSunrise";
import Precipitation from "./Precipitation";
import RealFeel from "./RealFeel";
import Humidity from "./Humidity";
import FOV from "./FOV";
import AirQuality from "./AirQuality";
import PainIndex from "./PainIndex";
import MosquitoIndex from "./MosquitoIndex";


interface WeeklySliderProps {
    keywords: any;
    airQualityIndex: number;
    airQualityCat: string;
    airQualityMsg: string;
    uvIndex: number;
    uvIndexText: string;
    sunset: string;
    sunrise: string;
    direction: string;
    windSpeed: number;
    todayPrecipitation: number;
    nextDayPrecipitation: number;
    realFeel: number;
    humidity: number;
    dewPoint: number;
    fov: number;
    currentPressure: number;
    painData: any;
    mosquitoIndex: number;
    mosquitoDesc: string;
    painIndex: number;
    painDesc: string;
    // dailyDayPartIndex?: any;
}

export const DayDetails: React.FC<WeeklySliderProps> = ({
                                                            keywords, airQualityIndex,airQualityCat,airQualityMsg,
                                                            uvIndex,uvIndexText,sunset,sunrise,
                                                            direction,windSpeed,todayPrecipitation,nextDayPrecipitation,
                                                            realFeel,humidity,dewPoint,fov,currentPressure, painData,
                                                            mosquitoIndex, mosquitoDesc, painIndex, painDesc
                                                        } : WeeklySliderProps) => {

    return (
        <>
            {airQualityIndex ?
                <AirQuality
                    keywords={keywords}
                    airQualityIndex={airQualityIndex}
                    airQualityCat={airQualityCat}
                    airQualityMsg={airQualityMsg}/>
            : ""}
            <IonRow>
                <UvIndex keywords={keywords} uvIndex={uvIndex}
                         uvIndexText={uvIndexText}/>
                <SunsetSunrise keywords={keywords} sunset={sunset}
                               sunrise={sunrise}/>
                <CompassWind keywords={keywords} direction={direction}
                             windSpeed={windSpeed}/>
                <Precipitation keywords={keywords} todayPrecipitation={todayPrecipitation}
                               nextDayPrecipitation={nextDayPrecipitation}/>
                <RealFeel keywords={keywords} realFeel={realFeel}/>
                <Humidity keywords={keywords} humidity={humidity}
                          dewPoint={dewPoint}/>
                <FOV keywords={keywords} fov={fov}/>
                <PressureGauge keywords={keywords} currentPressure={currentPressure}/>
                {/*{mosquitoIndex ?*/}
                    <MosquitoIndex mosquitoIndex={mosquitoIndex} mosquitoDesc={mosquitoDesc} keywords={keywords}/>
                {/*: ""}*/}
                {/*{painIndex ?*/}
                    <PainIndex keywords={keywords} painIndex={painIndex} painDesc={painDesc}/>
                {/*: ""}*/}

            </IonRow>
        </>
    );}

export default DayDetails;
