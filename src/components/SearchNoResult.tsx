import {IonButton, IonCol, IonGrid, IonIcon, IonRow } from '@ionic/react';
import { cloudOfflineOutline, newspaperOutline} from 'ionicons/icons';
import { Link } from 'react-router-dom';
import './SearchNoResult.css';
import {offlineModeContext} from '../state/State'
import React, { useContext } from 'react';
import { Storage } from '@ionic/storage';
import {darkThemeContext} from '../state/State';


interface SearchNoResultProps {
  keywords?: any;
}

const SearchNoResult: React.FC<SearchNoResultProps> = ({keywords} : SearchNoResultProps) => {
  const [offlineMode,setOfflineMode] = useContext<any>(offlineModeContext)
  const [darkTheme] = useContext<any>(darkThemeContext);
  const store = new Storage();

  return (
  
        <IonGrid className='en-offline'>
          <IonRow className='en-offline-content'>

            <IonCol size='12'><IonIcon icon={newspaperOutline} className='en-searchnoresult-icon' style={darkTheme ? {borderColor:"#fff" , color: "#fff"} :  {borderColor: "#bdbec0" , color: "#bdbec0"} }/></IonCol>

            <IonCol size='12'>
              <h2 className='en-searchnoresult-title' style={darkTheme ? {color:"#fff"} :  {color: "#000"}}>
                ამ საძიებო სიტყვაზე
                პოსტები არ მოიძებნა
              </h2>
            </IonCol>
            
          </IonRow>
        </IonGrid>
      
  );
};

export default SearchNoResult;
