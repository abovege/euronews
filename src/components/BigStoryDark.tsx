import {  IonCol, IonGrid,IonImg, IonRippleEffect, IonRouterLink, IonRow,  IonText } from '@ionic/react';
// import './Home.css';
import React, { useContext, useState } from 'react';
import Labels from '../components/Labels';
import { Link, useHistory ,useLocation} from 'react-router-dom';
import axios from "axios";
import LinesEllipsis from 'react-lines-ellipsis';
import './BigStoryDark.css'
import {darkThemeContext} from '../state/State'

interface BigStoryDarkProps {
    keywords?: any;
    data?: any;
    ID?:any;
  }

export const BigStoryDark: React.FC<BigStoryDarkProps> = ({data,ID,keywords} :BigStoryDarkProps) => {
  const [loaded, setLoaded] = useState(false);
  const [darkTheme] = useContext<any>(darkThemeContext);

  console.log(data)
  return (
        <IonGrid className='ion-no-padding en-bg-dark'>
        <Link  style={darkTheme ? {color:"#ffffff",textDecoration:"none"}:{color:"#000000",textDecoration:"none"}}  
            to={{
              pathname: `/storypage/${ID}`,
              state: { 
                  data: data,
                  id : ID,
                  relatedStory :data.categories[0].name
              },
        }}>            
        <IonRow className='ion-no-padding'>
              <IonCol className='ion-no-padding'>
              
                <div className='en-home-big-story'>
                  <img  src={data.post_meta.essb_cached_image ? data.post_meta.essb_cached_image[0] : '../assets/images/no-image.png'} className="en-home-news-1-img"/>
                  {data.post_meta.td_post_video && <IonImg className="RightBottom" style={{height:"30px"}} src='../assets/images/Video-Player-Logo.png'/>}
                  <div className="en-big-story-dark-label">
                    <Labels labelBG="red" labelText={keywords.TOP_STORIES} borderBlue/>
                  </div>
                  
                </div>
                <IonText className='ion-activatable'>
                <IonRippleEffect></IonRippleEffect>
                  <h1 style={{
                    textOverflow:"ellipsis",
                  }}className='en-big-story-dark-tithe'>{data && data.post_title}</h1>
                </IonText>
              </IonCol>
            </IonRow>
          </Link>
        </IonGrid>
          )
}

export default BigStoryDark