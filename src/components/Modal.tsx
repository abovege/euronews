import React from 'react';
import { IonButtons, IonCol, IonContent, IonGrid, IonIcon, IonImg, IonModal, IonPage, IonRouterLink, IonRow, IonSearchbar, IonSegment, IonSegmentButton, IonText, IonTitle } from '@ionic/react';
import './ModalVideo.css';
import { bookmarkOutline, shareOutline } from 'ionicons/icons';
import { Link } from 'react-router-dom';
import ReactPlayer from 'react-player';
import LinesEllipsis from 'react-lines-ellipsis';
import ModalVideo from '../components/ModalVideo';
import { chevronForwardOutline, closeOutline } from 'ionicons/icons';


interface ModalProps {
    data?: any;
    ID?:any;
    CloseModal?:any;
    keywords?: any;
  }

export const Modal: React.FC<ModalProps> = ({CloseModal,data,ID} : ModalProps) => {
    return (
    <IonContent style={{backgroundColor: "#081f2d"}}>
    <IonText className='ion-no-margin'>
    <IonTitle className='en-modal-title ion-no-margin' >
      {/* {keywords.LATEST_VIDEOS} */}
      <IonButtons  className='ion-float-right en-modalvideo-close'><IonIcon  icon={closeOutline} /></IonButtons>
    </IonTitle>
    </IonText>
        {/* <ModalVideo/> */}
    </IonContent>
  )
}

export default Modal