import {IonButton, IonCol, IonGrid, IonIcon, IonRow } from '@ionic/react';
import { cloudOfflineOutline} from 'ionicons/icons';
import { Link } from 'react-router-dom';
import './OfflineMode.css';
import {offlineModeContext,darkThemeContext} from '../state/State'
import React, { useContext } from 'react';
import { Storage } from '@ionic/storage';


interface OfflineModeVideoProps {
  keywords?: any;
}

const OfflineModeVideo: React.FC<OfflineModeVideoProps> = ({keywords} : OfflineModeVideoProps) => {
  const [offlineMode,setOfflineMode] = useContext<any>(offlineModeContext)
  const [darkTheme] = useContext<any>(darkThemeContext);

  const store = new Storage();

  return (
  
        <IonGrid className='en-offline'>
          <IonRow className='en-offline-content'>
            <IonCol size='12'><IonIcon icon={cloudOfflineOutline} className='en-offline-icon' style={darkTheme ? {borderColor:"#fff" , color: "#fff"} :  {borderColor: "#bdbec0" , color: "#bdbec0"} }/></IonCol>
            <IonCol size='12'><h2 className='en-modals-title' style={darkTheme ? {color:"#fff"} :  {color: "#bdbec0"}}>This content is not available offline.</h2></IonCol>
            <IonCol size='12'>
              <IonButton onClick={async ()=> {
                setOfflineMode(false);
                await store.create();
                await store.set('offlineMode', false);
                }}
                 
                className='en-switch-online'>
                SWITCH ONLINE
              </IonButton>
            </IonCol>
          </IonRow>
        </IonGrid>
      
  );
};

export default OfflineModeVideo;
