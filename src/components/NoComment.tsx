import { IonSpinner, IonButton, IonButtons, IonCol, IonContent, IonGrid, IonIcon, IonImg, IonModal, IonPage, IonRippleEffect, IonRouterLink, IonRow, IonSegment, IonSegmentButton, IonText, IonTitle } from '@ionic/react';
import Header from '../components/Header';
import Labels from '../components/Labels';
import "../pages/Videos.css"
import ModalVideo from '../components/ModalVideo';
import GlobalStory from '../components/GlobalStory';
import { caretForwardCircleOutline, chevronForwardOutline, closeOutline } from 'ionicons/icons';
import { useState,useEffect,useContext } from 'react';
import { Link, useHistory,useLocation } from 'react-router-dom';
import axios from "axios";
import {offlineModeContext} from '../state/State'
import OfflineMode from "../components/OfflineMode"
import OfflineHeader from '../components/OfflineHeader';
import OfflineIcon from '../components/OfflineIcon';
import OfflineSmallStory from '../components/OfflineSmallStory';
import LinesEllipsis from 'react-lines-ellipsis'
import VideoContent from '../components/VideoContent'

const NoComment = (index: any,data: any,showModal: any) => {
 return (
    <IonSegmentButton key={index} className='en-video-latest-programmes-news' onClick={() => showModal()}>
       <IonImg src={data.post_meta.essb_cached_image ? data.post_meta.essb_cached_image[0] : '../assets/images/no-image.png'} className='en-video-latest-programmes-news-img'/>
    {data &&  data.post_meta.td_post_video && <IonIcon icon={caretForwardCircleOutline} className='en-icon-nocomment-play'/>}
    <div className='en-video-latest-programmes-news-content ion-activatable'>
      <IonRippleEffect></IonRippleEffect>
      <div className='en-video-latest-programmes-news-title'>{data && data.post_title}</div>
      <div className='en-video-latest-programmes-news-time'>{data && data.post_date_gmt}</div>
    </div>
    </IonSegmentButton> 
  )
}

export default NoComment