import React, {useContext, useEffect} from 'react';
import {IonCol, IonGrid, IonImg, IonRow, IonText, IonTitle} from '@ionic/react';
import './Labels.css';
import {darkThemeContext} from '../state/State'
import {Link} from "react-router-dom";
import ReactPlayer from "react-player";
import useScript from "../hooks/useScript";

interface AdsProps {
    adSection: any;
    keywords?: any;
    showTitle?: boolean;
}

export const Ad: React.FC<AdsProps> = ({adSection, keywords, showTitle=true}: AdsProps) => {
    const [darkTheme] = useContext<any>(darkThemeContext);
    const isImage = ['gif', 'jpg', 'jpeg', 'png']; //you can add more
    const isVideo = ['mpg', 'mp2', 'mpeg', 'mpe', 'mpv', 'mp4'] // you can add more extention
    // useScript('//ads.euronewsgeorgia.com/www/delivery/asyncjs.php', adSection);

    useEffect(() => {
        // console.log("addsection", adSection);
    }, []);
    // console.log("addsection", adSection);
    return (
        <>

            {adSection?.img ? (
                <IonGrid className='ion-no-padding' style={{marginBottom: "10px"}}>
                    <IonRow className='en-home-ad'
                            id={"adsection" + adSection.revive_zone_id}
                            style={darkTheme ? {background: "#3a3a3a"} : {background: "#f1f5f6"}}>
                        <IonCol>
                            {showTitle ? <IonTitle className='en-home-ad-title'>{keywords.ADVERTISEMENT}</IonTitle> : "" }
                            {/*<ReactPlayer playsinline={true} width={"100%"} height={225}  muted={true} loop={true} playing={true}  url="../assets/videos/Advertisement.mp4" light={false} />*/}
                            <Link to={{pathname: adSection.url}} target="_blank">
                                {isImage?.includes(adSection.img.slice((adSection.img.lastIndexOf(".") - 1 >>> 0) + 2)) &&
                                    <IonImg className="theme-img" src={adSection.img}/>}
                                {isVideo?.includes(adSection.img.slice((adSection.img.lastIndexOf(".") - 1 >>> 0) + 2)) &&
                                    <ReactPlayer playsinline={true} width={"100%"} height={225}
                                                 muted={true} loop={true} playing={true}
                                                 url={adSection.img} light={false}/>}
                            </Link>



                        </IonCol>
                    </IonRow>
                </IonGrid>
            ) : ('')}


        </>
    );
}

export default Ad;
