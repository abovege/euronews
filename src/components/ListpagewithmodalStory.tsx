import React,{useContext} from 'react';
import {IonRippleEffect, IonImg, IonRow, IonCol,  } from '@ionic/react';
import './SmallStory.css';
import Labels from './Labels';
import { Link } from 'react-router-dom'
import LinesEllipsis from 'react-lines-ellipsis'
import {darkThemeContext} from '../state/State'


interface SmallStoryProps {
  labelShow?: boolean;
  labelBG?: string;
  labelText?: string;
  data?: any;
  ID?:any;
}

export const ListpagewithmodalStory: React.FC<SmallStoryProps> = ({ID,data,labelShow, labelBG, labelText} : SmallStoryProps) =>{
  const [darkTheme] = useContext<any>(darkThemeContext);

  return (
    <>
  
      <Link  style={darkTheme ? {color:"#ffffff"}:{color:"#000000"}}  
      className='en-story-link'
      to={{
      pathname: `/storypage/${ID}`,
      state: { 
        data: data,
        id : ID,
        relatedStory :data.categories[0].name
    },
    }}>
        <IonRow className='en-global-news en-listpage-story'>
          <IonCol className='ion-activatable ion-float-left ion-no-padding' size='8' >
          <IonRippleEffect></IonRippleEffect>
            <div>
              { labelShow &&
                <Labels labelBG={labelBG} labelText={labelText}/>
              }
              {/* <h3 className='en-home-news-title'>
                {data && data.post_title}
              </h3> */}
              <LinesEllipsis
                className='en-home-news-title'
                style={{color:"#ffffff"}}
                text={`${data && data.post_title} `}
                maxLine='2'
                ellipsis='...'
                trimRight
                basedOn='letters'
              />  
              {/* <div className='en-home-news-locate'>AUSTRALIA</div> */}
              <div className='en-home-news-locate'>{data && data.categories[0].name}</div>
            </div>
          </IonCol>
          <IonCol className='ion-float-right ion-no-padding' size='4'>
            <img src={data.post_meta.essb_cached_image ? data.post_meta.essb_cached_image[0] : '../assets/images/no-image.png'} className='en-justin-news-img'/>
            {data.post_meta.td_post_video && <IonImg className="RightBottom" style={{height:"20px"}} src='../assets/images/Video-Player-Logo.png'/>}
          </IonCol>
        </IonRow>
      </Link>
      
      
      
      
    </>
  );
}

export default ListpagewithmodalStory;
