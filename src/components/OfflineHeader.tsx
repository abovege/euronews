import React, { useEffect, useState } from 'react';
import { IonButton,IonItem,IonHeader, IonButtons, IonIcon, IonImg, IonDatetime } from '@ionic/react';
import { caretForwardOutline, cloudDownloadOutline, desktopOutline, partlySunnyOutline} from 'ionicons/icons';
import './OfflineHeader.css';
import { time } from 'console';
import { Storage } from '@ionic/storage';

interface OfflineHeaderProps {
  keywords?: any;
  
}


export const OfflineHeader: React.FC<OfflineHeaderProps> = ({keywords} : OfflineHeaderProps) => {

  const [offlineTime, setOfflineTime] = useState('');

  async function getOfflineTime() {

    const store = new Storage();
    await store.create();

    let offlineModeTime = await store.get('offlineModeTime');
    
    const currentTime = new Date();
    const diffMs = new Date(currentTime).getTime() - new Date(offlineModeTime).getTime();

    const diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000);

    
    let startOfflineTime = '';
    if (diffMins < 1) {
      
      startOfflineTime =  'Just Now';

    }else if (diffMins < 59) {

      startOfflineTime = diffMins + ' minutes ago'; 

    }else if (diffMins < 1439) {

      startOfflineTime = Math.floor(diffMins / 60) + ' hours ago';
      console.log(startOfflineTime)

    }else if(diffMins > 1440) {

      startOfflineTime = Math.floor((diffMins / 60) / 24) + ' days ago'

    }
    
    setOfflineTime(startOfflineTime);
  }

  

  useEffect(() => {
    if(!offlineTime){
      getOfflineTime();
    }
   }, []);
  


  return (
    <div className='en-offline-header'>
      <div className='en-offline-header-text'>
        Offline Mode: {offlineTime}
    
      </div>
    </div>
  )

}

export default OfflineHeader;
