import React, {useContext} from 'react';
import {IonRippleEffect, IonImg, IonRow, IonCol, IonIcon, IonButton,  } from '@ionic/react';
import './OfflineSmallStory.css';
import Labels from './Labels';
import { Link } from 'react-router-dom'
import LinesEllipsis from 'react-lines-ellipsis'
import { cloudOfflineOutline } from 'ionicons/icons';
import {offlineModeContext,darkThemeContext} from '../state/State'
import { Storage } from '@ionic/storage';




export const OfflineSmallStory: React.FC = () => {
  const [offlineMode,setOfflineMode] = useContext<any>(offlineModeContext)
  const [darkTheme] = useContext<any>(darkThemeContext);

  const store = new Storage();

  return (
  <>
      <IonRow className='en-home-news ion-no-padding' style={darkTheme ? {background:"#1e1e1e"}:{background:"#ffffff"}}>
        <IonCol 
        className='ion-no-padding en-offline-icon-smallstory-col' size='3'
        >
          <IonIcon icon={cloudOfflineOutline} className='en-offline-icon-smallstory'/>
        </IonCol>
        <IonCol
        className='ion-float-left ion-no-padding en-offline-icon-smallstory-row' size='9' >
          <div 
          style={{display:"flex",justifyContent:"flex-start"}}
          >
          <h2 className='en-offline-title' style={darkTheme ? {color:"#fff"} :  {color: "#000"}}>This content is not available offline.</h2>
          </div>
          <div
          style={{display:"flex",justifyContent:"flex-start"}}
          >
          <IonButton 
          onClick={async ()=>{
            setOfflineMode(false)
            await store.create();
            await store.set('offlineMode', false);}}
          className='en-switch-online-smallstory'>
            SWITCH ONLINE
          </IonButton>
          </div>
        </IonCol> 
      </IonRow>
    
    
    
    
    
  </>
);}

export default OfflineSmallStory;
