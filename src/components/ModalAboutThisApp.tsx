import React, {useContext} from 'react';
import { IonSlides, IonSlide, IonContent, IonIcon, IonRow, IonCol } from '@ionic/react';
import { bookmarkOutline, cloudDownloadOutline, newspaperOutline, notificationsOutline, phonePortraitOutline, playOutline } from 'ionicons/icons';
import './ModalAboutThisApp.css';
import {darkThemeContext} from '../state/State'

const slideOpts = {
  initialSlide: 1,
  speed: 400
};

const ModalAboutThisApp: React.FC = () =>{
  const [darkTheme] = useContext<any>(darkThemeContext);

  return(
    <IonContent >
    <IonSlides style={{height:"100%"}} pager={true} options={slideOpts} className='en-modals'>
      <IonSlide>

        <IonRow>
          <IonCol size='12'><IonIcon icon={notificationsOutline} className='en-madal-icon' style={darkTheme ? {borderColor:"#fff" , color: "#fff"} :  {borderColor: "#000" , color: "#000"} }/></IonCol>
          <IonCol size='12' ><h2 className='en-modals-title' style={darkTheme ? {color:"#fff"} :  {color: "#000"}}>იყავით განახლებული სიახლეებით</h2></IonCol>
          <IonCol size='12'>
            <p className='en-modals-text'>
              სწრაფი შეტყობინებების საშუალებით
            </p>
          </IonCol>
        </IonRow>
        
      </IonSlide>

      <IonSlide>

        <IonRow>
          <IonCol size='12'><IonIcon icon={cloudDownloadOutline} className='en-madal-icon' style={darkTheme ? {borderColor:"#fff" , color: "#fff"} :  {borderColor: "#000" , color: "#000"} }/></IonCol>
          <IonCol size='12' ><h2 className='en-modals-title' style={darkTheme ? {color:"#fff"} :  {color: "#000"}}>ინტერნეტის დაზოგვა: Offline რეჟიმი </h2></IonCol>
          <IonCol size='12' className='ion-padding-start ion-padding-end'>
            <p className='en-modals-text'>
            ეს აპლიკაცია შექმნილია იმისთვის, რომ შესაძლებლობა მოგცეთ, თავად მართოთ ინტერნეტის გამოყენება offline რეჟიმით:  ჩამოტვირთეთ სტატიები offline წასაკითხად. 
           
            </p>
            <p className='en-modals-text'>
            ხელმისაწვდომია საწყის გვერდსა და  განახლებული ამბების გვერდზე. 
            </p>
          </IonCol>
        </IonRow>

      </IonSlide>

      <IonSlide>

        <IonRow>
          <IonCol size='12'><IonIcon icon={bookmarkOutline} className='en-madal-icon' style={darkTheme ? {borderColor:"#fff" , color: "#fff"} :  {borderColor: "#000" , color: "#000"} }/></IonCol>
          <IonCol size='12' ><h2 className='en-modals-title' style={darkTheme ? {color:"#fff"} :  {color: "#000"}}>შერჩეული ამბები / ნახეთ ისინი მოგვიანებით.  </h2></IonCol>
          <IonCol size='12' className='ion-padding-start ion-padding-end'>
            <p className='en-modals-text'>
            წაკითხვის დრო არ გაქვთ? მონიშნეთ ეს სტატია, მოგვიანებით წასაკითხად. 
<br/>
თქვენ ნახავთ მათ მენიუში / ნახე მოგვიანებით

            </p>
          </IonCol>
        </IonRow>
        
      </IonSlide>

      <IonSlide>

        <IonRow>
          <IonCol size='12'><IonIcon icon={phonePortraitOutline} className='en-madal-icon' style={darkTheme ? {borderColor:"#fff" , color: "#fff"} :  {borderColor: "#000" , color: "#000"} }/></IonCol>
          <IonCol size='12' ><h2 className='en-modals-title' style={darkTheme ? {color:"#fff"} :  {color: "#000"}}>აირჩიეთ ტექსტის ზომა </h2></IonCol>
          <IonCol size='12'>
            <p className='en-modals-text'>
            მცირე ტექსტები მოგწონთ? ვრცელი ტექსტები გირჩევნიათ? შეარჩიეთ თქვენთვის სასურველი ტექსტის ზომა პარამეტრების გვერდზე
            </p>
          </IonCol>
        </IonRow>
        
      </IonSlide>

      <IonSlide>

        <IonRow>
          <IonCol size='12'><IonIcon icon={playOutline} className='en-madal-icon' style={darkTheme ? {borderColor:"#fff" , color: "#fff"} :  {borderColor: "#000" , color: "#000"} }/></IonCol>
          <IonCol size='12' ><h2 className='en-modals-title' style={darkTheme ? {color:"#fff"} :  {color: "#000"}}>აირჩიეთ როდის გაეშვას ვიდეო ავტომატურად </h2></IonCol>
          <IonCol size='12'>
            <p className='en-modals-text'>
            "მოგწონთ ვიდეოამბები?<br/>
             მაგრამ არ მოგწონთ როცა თქვენთვის არასასურველ დროს ირთვება?  
დააყენეთ ავტომატური გაშვების რეჟიმი -  პარამეტრების გვერდზე

            </p>
          </IonCol>
        </IonRow>
        
      </IonSlide>
    </IonSlides>
  </IonContent>
  )
}
export default ModalAboutThisApp;
