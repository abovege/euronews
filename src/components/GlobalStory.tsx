import React from 'react';
import { IonImg, IonRow, IonCol, IonButton, IonIcon, IonRippleEffect } from '@ionic/react';
import './GlobalStory.css';
import Labels from './Labels';
import LinesEllipsis from 'react-lines-ellipsis'
import { caretForwardCircleOutline } from 'ionicons/icons';


interface GlobalStoryProps {
  labelShow?: boolean;
  labelBG?: string;
  labelText?: string;
  borderBottom?: boolean;
  toggleState: () => void;
  data?: any;
  ID?:any;
}


export const GlobalStory: React.FC<GlobalStoryProps> = ({data, ID, borderBottom, toggleState } : GlobalStoryProps) => (
  <>

    <IonRow className={'en-global-news' + (borderBottom ? " en-global-news-borderbottom" : '')} onClick={() => toggleState()}>
      <IonCol className=' ion-float-left ion-activatable' size='8'>
        <IonRippleEffect></IonRippleEffect>
        <div className="global-column" >
          <h3 className='en-global-news-title max-lines-3'>
          <LinesEllipsis
          style={{color:"#ffffff"}}
          text={`${data && data.post_title} `}
          maxLine='3'
          ellipsis='...'
          trimRight
          basedOn='letters'
        />       
          </h3>
          {/*<div className='en-home-news-locate'>{data && data.categories[0].name}</div>*/}
        </div>
      </IonCol>
      <IonCol className='ion-float-right ion-no-padding' size='4'>
        <IonImg className='global-story-images' src={data.post_meta.essb_cached_image ? data.post_meta.essb_cached_image[0] : '../assets/images/no-image.png'} />
        {data && data.post_meta.td_post_video && <IonIcon icon={caretForwardCircleOutline} className='en-icon-global-play'/>}
      </IonCol>
    </IonRow>
    
  </>
);

export default GlobalStory;
