import { IonRippleEffect, IonIcon, IonImg } from '@ionic/react';

import React, { useEffect, useState, useContext } from 'react';
import { caretForwardCircleOutline, chevronForwardOutline } from 'ionicons/icons';
import { Link} from 'react-router-dom';
import OfflineIcon from '../components/OfflineIcon';
import {offlineModeContext,darkThemeContext } from '../state/State'
import LinesEllipsis from 'react-lines-ellipsis'
import classes from "./VideoContent.module.css";

const VideoContent = React.memo(({data, showModal}: any) => {
    const [offlineMode,setofflineMode] = useContext<any>(offlineModeContext)
    const [darkTheme] = useContext<any>(darkThemeContext);

    const [postDate, setPostDate] = useState<any>();
    useEffect(() => {
      const formatDate = (input: any) => {
        var datePart = input.match(/\d+/g),
        year = datePart[0].substring(2,4), // get only two digits
        month = datePart[1], day = datePart[2];

        const cDate = new Date(`${datePart[0]}/${month}/${day}`);

        const today = new Date;
        const yesterday = new Date;
        yesterday.setDate(today.getDate() - 1)
        if (cDate.toLocaleDateString() == today.toLocaleDateString()) {
          return ' ' // today should be empty
        } else if (cDate.toLocaleDateString() == yesterday.toLocaleDateString()) {
          return 'გუშინ '
        }

        return day+'.'+month+'.'+year + '/';
      }

      setPostDate(formatDate(data.post_date.substring(0,10)) + data.post_date.substring(11,16))  }, [])
    return (
        <div className='en-home-latest-video-news' style={darkTheme ? {background:"#1e1e1e",marginRight:"10px"}:{background:"#1c3247",marginRight:"10px"}}>
        <div onClick={() => showModal()}>
          {offlineMode ? <OfflineIcon/> : <IonImg src={data.post_meta.essb_cached_image ? data.post_meta.essb_cached_image[0] : '../assets/images/no-image.png'} className='en-home-latest-video-news-img'/>}
          {data.post_meta.td_post_video && <IonIcon icon={caretForwardCircleOutline} className={classes.en_icon_play + " " + 'en-icon-video-play'}/>}
          <div className='cube-section  ion-activatable en-home-latest-video-news-content'>
          <IonRippleEffect></IonRippleEffect>
        
            <div className='en-home-latest-video-news-title'>
            <LinesEllipsis
            style={{color:"#ffffff"}}
            text={`${data && data.post_title} `}
            className='max-lines-3'
            maxLine='3'
            ellipsis='...'
            trimRight
            basedOn='letters'
            /></div>
            <div className={classes.date_time_container + " " + 'en-home-latest-video-news-time'}>{data && postDate}</div>
          </div>
        </div>
        
        {/*<Link className='en-home-latest-video-news-category' to={`/insidepages/${data.categories[0].name}`}>*/}
        {/*  {data.categories[0].name}*/}
        {/*  <IonIcon icon={chevronForwardOutline} className='ion-float-right en-home-icon'/>*/}
        {/*</Link>*/}
      </div>
      )
})
export default VideoContent