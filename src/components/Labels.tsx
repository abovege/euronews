import React,{useContext} from 'react';
import { IonText } from '@ionic/react';
import './Labels.css';
import {darkThemeContext} from '../state/State'

interface LabelsProps {
  labelBG?: string;
  labelText?: string;
  borderBlue ?: boolean;
  borderDrakBule?: string;
}

export const Labels: React.FC<LabelsProps> = ({ labelBG, labelText, borderBlue, borderDrakBule  } : LabelsProps) => {
  const [darkTheme] = useContext<any>(darkThemeContext);

  return(
    <>
      
      <IonText>
        <div style={darkTheme ? {borderColor:"#000000"}: borderBlue ? {borderColor: "#1c3247"} : {borderColor: "#fff"}} className={'en-label' + (labelBG ? " en-label-"+labelBG : '') + (borderBlue ? " en-label-"+borderBlue : '') + (borderDrakBule ? " en-label-"+borderDrakBule : '')}>
          <span style={darkTheme ? {borderColor:"#000000"}: borderBlue ? {borderColor: "#1c3247"} : {borderColor: "#fff"}} className={'en-label-span' + (borderBlue ? " en-label-"+borderBlue : '')} ></span>
          <div className='en-label-div'>{labelText}</div>
        </div>
      </IonText>
      
      
    </>
  );
}

export default Labels;
