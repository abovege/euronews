import React, {useEffect,useContext,useState} from 'react';
import {IonRippleEffect, IonImg, IonRow, IonCol,  } from '@ionic/react';
import './SmallStory.css';
import Labels from './Labels';
import { Link } from 'react-router-dom'
import LinesEllipsis from 'react-lines-ellipsis'
import {darkThemeContext} from '../state/State'


interface SmallStoryProps {
  labelShow?: boolean;
  labelBG?: string;
  labelText?: string;
  data?: any;
  ID?:any;
}

export const SmallStory: React.FC<SmallStoryProps> = ({ID,data,labelShow, labelBG, labelText} : SmallStoryProps) =>{ 
  const [postDate, setPostDate] = useState<any>();
  const [darkTheme] = useContext<any>(darkThemeContext);

  useEffect(() => {
    const formatDate = (input: any) => {
      var datePart = input.match(/\d+/g),
      year = datePart[0].substring(2,4), // get only two digits
      month = datePart[1], day = datePart[2];

      const cDate = new Date(`${datePart[0]}/${month}/${day}`);

      const today = new Date;
      const yesterday = new Date;
      yesterday.setDate(today.getDate() - 1)
      if (cDate.toLocaleDateString() == today.toLocaleDateString()) {
        return ' ' // today should be empty
      } else if (cDate.toLocaleDateString() == yesterday.toLocaleDateString()) {
        return 'გუშინ '
      }

      return day+'.'+month+'.'+year + '/';

      // return day+'/'+month+'/'+year;
    }
    setPostDate(formatDate(data.post_date.substring(0,10)).replace("/",".").replace("/",".") + data.post_date.substring(11,16))
  }, [data])
  // console.log(darkTheme)

  return (
  <>

    <Link  style={darkTheme? {color:"#ffffff"}:{color:"#000000"}}  
    className='en-story-link'
    to={{
    pathname: `/storypage/${ID}`,
    state: { 
      data: data,
      id : ID,
      relatedStory: data.categories[0].name
  },
  }}>
      <IonRow className='en-home-news ion-no-padding' style={darkTheme ? {background:"#1e1e1e"}:{background:"#ffffff"}}>
        <IonCol className='ion-activatable ion-float-left ion-no-padding' style={darkTheme ? {background:"#1e1e1e"}:{background:"#ffffff"}} size='8' >
        <IonRippleEffect></IonRippleEffect>
          <div className="small-story-column" >
            { labelShow &&
              <Labels labelBG={labelBG} labelText={labelText}/>
            }
            {/* <h3 className='en-home-news-title'>
              {data && data.post_title}
            </h3> */}
            <LinesEllipsis
              style={darkTheme ? {color:"#ffffff"}:{color:"#000000"}}
              className='en-home-news-title max-lines-3'
              text={`${data && data.post_title} `}
              maxLine='3'
              ellipsis='...'
              trimRight
              basedOn='letters'
            />  
            {/* <div className='en-home-news-locate'>AUSTRALIA</div> */}
            <div className='en-home-news-time'>{data && postDate}</div>
          </div>
        </IonCol>
        <IonCol className='ion-float-right ion-no-padding max-height-small-story' size='4'>
          <img src={data.post_meta.essb_cached_image ? data.post_meta.essb_cached_image[0] : '../assets/images/no-image.png'} className='en-justin-news-img'/>
          {data.post_meta.td_post_video && <IonImg className="RightBottom" style={{height:"20px"}} src='../assets/images/Video-Player-Logo.png'/>}
        </IonCol>
      </IonRow>
    </Link>
    
    
    
    
  </>
);}

export default SmallStory;
