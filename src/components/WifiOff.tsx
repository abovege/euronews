import {IonButton, IonCol, IonGrid, IonIcon, IonRow } from '@ionic/react';
import { cloudOfflineOutline, wifiOutline} from 'ionicons/icons';
import { Link } from 'react-router-dom';
import './WifiOff.css';
import {darkThemeContext} from '../state/State'
import React,{useContext} from 'react';


interface WifiOffProps {
  keywords?: any;
}

const WifiOff: React.FC<WifiOffProps> = ({keywords} : WifiOffProps) => {
  const [darkTheme] = useContext<any>(darkThemeContext);

  return (
  
        <IonGrid className='en-offline'>
          <IonRow className='en-offline-content'>
            <IonCol size='12'><IonIcon icon={wifiOutline} className='en-offline-icon' style={darkTheme ? {borderColor:"#fff" , color: "#fff"} :  {borderColor: "#bdbec0" , color: "#bdbec0"} }/></IonCol>
            <IonCol size='12'><h2 className='en-modals-title' style={darkTheme ? {color:"#fff"} :  {color: "#000"}}>No connection available</h2></IonCol>
            <IonCol size='12'>
              <IonButton className='en-retray'>
                Retry
              </IonButton>
            </IonCol>
          </IonRow>
        </IonGrid>
      
  );
};

export default WifiOff;
