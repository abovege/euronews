import {IonButton, IonCol, IonGrid, IonIcon, IonRow } from '@ionic/react';
import { cloudOfflineOutline} from 'ionicons/icons';
import { Link } from 'react-router-dom';
import './OfflineIcon.css';
import React from "react";


interface OfflineIconProps {
  keywords?: any;
}

const OfflineIcon: React.FC<OfflineIconProps> = ({keywords} : OfflineIconProps) => {

  return(
    
    <div className='en-offline-icon-background'>
      <IonIcon icon={cloudOfflineOutline} className='en-offline-icon-image'/>
    </div>
      
    
  );
    
    
    
  
  
};

export default OfflineIcon;
