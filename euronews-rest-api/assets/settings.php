<?php 
$categories = wp_dropdown_categories(array(
            'class' => 'd-block col-md-7  mt-1',
            'name' => 'featured_content_categories_ara[]',
            'id' => 'featured_content_categories_ara[]',
            'orderby' => 'name',
            'echo' => false,
            'value_field' => 'name'
        ));  
$categories = str_replace('"',"'",json_decode(str_replace('\n','',json_encode($categories))));        
?>

<?php

            $cats = get_categories([
                'hide_empty' => true,
            ]);

            $advertisement_html = '<select class="d-block col-md-7  mt-1" name="featured_content_categories_ara[]" id="featured_content_categories_ara[]">' .
            '<option value="" >Default</option>';

            foreach($cats as $cat)
            {
                $advertisement_html = $advertisement_html .  '<option value="'.$cat->cat_name .'">'. $cat->cat_name . '</option>';
            }

    $advertisement_html = $advertisement_html . '</select>';
$advertisement_html = str_replace('"',"'",json_decode(str_replace('\n','',json_encode($advertisement_html))));
?>


<script>

function add_new_section(ele,img = true){
    if(img){
        var html = 
        "<div class='col-md-4 border border-1 rounded bg-secondary p-2'>"+
            "<label  class='col-md-5 p-1 mt-1 text-white' for='featured_content_categories_ara[]'><strong>Select your category</strong></label><?php echo $categories.$media_btn; ?>"+
            "<input class='col-md-12 p-2 mt-1' name='url[]' id='url[]' type='text' placeholder='URL...'/>"+
            "<button type='button' onclick='open_media(jQuery(this))' class='col-md-12 btn btn-primary p-1 mt-1 text-center text-decoratoin-none'>Feature Image</button>"+
            "<button type='button' class='col-md-12 btn btn-primary p-1 mt-1 text-center text-decoratoin-none' onclick='remove_row(jQuery(this))'>Close</button>"+        
        "<div/>";
    }else{
        var html = 
        "<div class='col-md-4 border border-1 rounded bg-secondary p-2'>"+
            "<label  class='col-md-5 p-1 mt-1 text-white' for='featured_content_categories_ara[]'><strong>Select your category</strong></label><?php echo $categories.$media_btn; ?>"+
            "<input class='col-md-12 p-2 mt-1' name='url[]' id='url[]' type='text' placeholder='URL...'/>"+
            "<button type='button' class='col-md-12 btn btn-primary p-1 mt-1 text-center text-decoratoin-none' onclick='remove_row(jQuery(this))'>Close</button>"+        
        "<div/>";        
    }
     
    ele.find("#input-group").append(html);

}

function add_new_advertisement_section(ele,img = true){
    if(img){
        var html =
        "<div class='col-md-4 border border-1 rounded bg-secondary p-2'>"+
            "<label  class='col-md-5 p-1 mt-1 text-white' for='featured_content_categories_ara[]'><strong>Select your category</strong></label><?php echo $advertisement_html.$media_btn; ?>"+
            "<input class='col-md-12 p-2 mt-1' name='url[]' id='url[]' type='text' placeholder='URL...'/>"+
            "<button type='button' onclick='open_media(jQuery(this))' class='col-md-12 btn btn-primary p-1 mt-1 text-center text-decoratoin-none'>Upload File</button>"+
            "<button type='button' class='col-md-12 btn btn-primary p-1 mt-1 text-center text-decoratoin-none' onclick='remove_row(jQuery(this))'>Close</button>"+
        "<div/>";
    }else{
        var html =
        "<div class='col-md-4 border border-1 rounded bg-secondary p-2'>"+
            "<label  class='col-md-5 p-1 mt-1 text-white' for='featured_content_categories_ara[]'><strong>Select your category</strong></label><?php echo $advertisement_html.$media_btn; ?>"+
            "<input class='col-md-12 p-2 mt-1' name='url[]' id='url[]' type='text' placeholder='URL...'/>"+
            "<button type='button' class='col-md-12 btn btn-primary p-1 mt-1 text-center text-decoratoin-none' onclick='remove_row(jQuery(this))'>Close</button>"+
        "<div/>";
    }

    ele.find("#input-group").append(html);

}

     
function open_media(t){
        var button = t;
        aw_uploader = wp.media({
            title: 'Choose Image',
            button: {
                text: 'Choose Image'
            }, multiple: false
        });
        aw_uploader.on('select', function() { 
            var attachment = aw_uploader.state().get( 'selection' ).first().toJSON();      
            var attachment_html = "<input class='d-none' hidden name='attachment[]' type='text' value='"+attachment.url+"' />";
            if(button.parent().find('input[name="attachment[]"]') !== undefined){
                button.parent().find('input[name="attachment[]"]').remove();
            }
            button.parent().append(attachment_html);
        });
        aw_uploader.open();
    }

function open_advertisment_media(t){
    var button = t;
    aw_uploader = wp.media({
        title: 'Choose Image',
        button: {
            text: 'Choose Image'
        }, multiple: false
    });
    aw_uploader.on('select', function() {
        var attachment = aw_uploader.state().get( 'selection' ).first().toJSON();
        var attachment_html = "<input class='col-md-12 p-2 mt-1'' name='attachment[]' type='text' value='"+attachment.url+"' />";
        if(button.parent().find('input[name="attachment[]"]') !== undefined){
            button.parent().find('input[name="attachment[]"]').remove();
        }

        jQuery(attachment_html).insertBefore(button);
        // button.prepend(attachment_html);
    });
    aw_uploader.open();
}

function remove_row(e){
    e.parent().remove();
}

</script>