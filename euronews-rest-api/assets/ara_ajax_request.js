var handle;
function ara_ajax_save(ele, action, advertisment = false){

    if(advertisment) {
        jQuery(ele).find('.adcheckbox   ').not(".defaultad").remove();
        jQuery(ele).find('input[type=checkbox]').each(function () {
            var checkbox = jQuery(this);
            // add a hidden field with the same name before the checkbox with value = 0
            if (!checkbox.prop('checked')) {
                console.log(checkbox);
                checkbox.clone()
                    .prop('type', 'hidden')
                    .addClass('adcheckbox')
                    // .prop("disabled", false)
                    .val(0)
                    .insertBefore(checkbox);
            }
        });
        $form = jQuery("#adv_form");
        ele = $form;
    }
    jQuery.ajax({
        url: ara_js_url.url,
        method: "POST",
        dataType: "json",
        data: {
            action: 'ara_ajax_request',
            function: action,
            ele: ele.serialize(),
            nonce: ara_js_url.nonce
        },
        success:function(res){
            console.log(res);
            jQuery('.showAlertAsDialog').remove();

            $alert = '<div class="alert alert-warning alert-dismissible fade show fixed-bottom showAlertAsDialog" role="alert" style=" position: fixed; bottom: 5px; z-index: 9999; right: 24px; max-width: 350px; left: unset;">' +
                '<strong>Settings Saved</strong>' +
                '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>' +
                '</div>';
            jQuery('body').append($alert);
            jQuery(".showAlertAsDialog").delay(2000).fadeOut(300);

        }
    })
    return false;
}