<?php
define('ARA_DEFINE', true);
define('ARA_VERSION', '1.1.0');
define('ARA__MINIMUM_WP_VERSION', '5.0');
define('ARA__PLUGIN_DIR', plugin_dir_path(__FILE__));

/**
 * Plugin Name: Above REST API
 * Plugin URI: https://www.above.ge/
 * Description: This is the RestAPI that developed by above team to organize the contents on the multi-platforms
 * Version: 1.0.7
 * Author: Parham R & Mahdi S.
 * Author URI: parham@above.ge & smahdis@gmail.com
 **/
class above_rest_api
{

    public $namespace;
    public $version;
    public $base;

    public function __construct()
    {
        $this->version = '1';
        $this->namespace = '/above_rest_api/v' . $this->version;
        $this->base = '/fetch/';
    }


    public function register_routes()
    {


        register_rest_route($this->namespace, $this->base . 'hourly_15day', array(
            'methods' => 'GET',
            'callback' => array($this, 'hourly_15day'),
        ));
        register_rest_route($this->namespace, $this->base . 'hourly_15day_file', array(
            'methods' => 'POST',
            'callback' => array($this, 'hourly_15day_file'),
        ));

        register_rest_route($this->namespace, $this->base . 'daily_15day', array(
            'methods' => 'GET',
            'callback' => array($this, 'daily_15day'),
        ));
        register_rest_route($this->namespace, $this->base . 'daily_15day_file', array(
            'methods' => 'POST',
            'callback' => array($this, 'daily_15day_file'),
        ));

        register_rest_route($this->namespace, $this->base . 'global_air_quality', array(
            'methods' => 'GET',
            'callback' => array($this, 'global_air_quality'),
        ));
        register_rest_route($this->namespace, $this->base . 'global_air_quality_file', array(
            'methods' => 'POST',
            'callback' => array($this, 'global_air_quality_file'),
        ));

        register_rest_route($this->namespace, $this->base . 'posts', array(
            'methods' => 'POST',
            'callback' => array($this, 'fetch_information_base_on_category'),
        ));

        register_rest_route($this->namespace, $this->base . 'tags', array(
            'methods' => 'POST',
            'callback' => array($this, 'fetch_information_tags'),
        ));


        register_rest_route($this->namespace, $this->base . 'categories', array(
            'methods' => 'POST',
            'callback' => array($this, 'fetch_information_categories'),
        ));


        register_rest_route($this->namespace, $this->base . 'post', array(
            'methods' => 'POST',
            'callback' => array($this, 'fetch_information_post'),
        ));

    }


    public function fetch_information_base_on_category(WP_REST_Request $request)
    {

        $inputs = $request->get_params();

        $cache_key = isset($inputs['cache_key']) ? $inputs['cache_key'] : null;
        $post_id = isset($inputs['post_id']) ? $inputs['post_id'] : null;

        if ($cache_key) {
            $cache_content = wp_cache_get($cache_key);
            if ($cache_content)
                return new WP_REST_Response(array('posts' => $cache_content), 200);
        }

        if ($post_id) {
            $post = get_post($post_id);
            $post->post_meta = get_post_meta($post->ID);
            $post->post_author_name = get_the_author_meta('display_name', $post->post_author);
            $post->post_meta = get_post_meta($post->ID);
            $post->categories = get_the_terms($post->ID, 'category');
            $post = $this->get_webp_image_address($post);

            wp_cache_add($cache_key, $post, "", 86400);
            return new WP_REST_Response(array('post' => $post, 'cache_key' => $cache_key), 200);
        }

        $post_type = isset($inputs['post_type']) ? $inputs['post_type'] : 'post';
        $post_title = isset($inputs['post_title']) ? $inputs['post_title'] : '';
        $orderby = isset($inputs['orderby']) ? $inputs['orderby'] : 'id';
        $order = isset($inputs['order']) ? $inputs['order'] : 'DESC';
        $post_count = isset($inputs['post_count']) ? $inputs['post_count'] : 10;
        $offset = isset($inputs['page']) ? $inputs['page'] : 1;
        $taxonomy = isset($inputs['taxonomy']) ? $inputs['taxonomy'] : null;
        $taxonomy_field = isset($inputs['taxonomy_field']) ? $inputs['taxonomy_field'] : null;
        $taxonomy_values = isset($inputs['taxonomy_values']) ? $inputs['taxonomy_values'] : null;
        $query_search = isset($inputs['qs']) ? $inputs['qs'] : null;

        if ($query_search) {

            $posts = new WP_Query(
                array(
                    's' => $query_search,
                    'posts_per_page'  => $post_count,
                )
            );
            $posts = $posts->get_posts();
            foreach ($posts as $key => $post) {
                $posts[$key]->post_author_name = get_the_author_meta('display_name', $post->post_author);
                $posts[$key]->post_meta = get_post_meta($post->ID);
                $posts[$key]->categories = get_the_terms($post->ID, 'category');
                $posts[$key] = $this->get_webp_image_address($post);
            }

            $cache_key = base64_encode('euronewsapp-' . date("ymd-Gi") . mt_rand(5, 15));
            wp_cache_add($cache_key, $posts, "", 86400);
            return new WP_REST_Response(array('posts' => $posts, 'cache_key' => $cache_key), 200);
        }
        $tax_query = array();
        if ($taxonomy and $taxonomy_field and $taxonomy_values) {
            foreach($taxonomy_values as $taxonomy_value){
                $taxonomy_values[] = str_replace('+','-',strtolower(urlencode($taxonomy_value)));
            }
            $tax_query['tax_query'] =
                array(
                    array(
                        'taxonomy' => $taxonomy,
                        'field'    => $taxonomy_field,
                        'terms'    => $taxonomy_values
                    )
                );
        }

        if(isset($offset) && $offset > 0) {
            $offset = ($offset - 1);
        }

        if ($tax_query['tax_query']) {
            $arg = array(
                'numberposts'      => $post_count,
                'offset'           => $offset,
                'orderby'          => $orderby,
                'order'            => $order,
                'post_type'        => $post_type,
                'tax_query'        => $tax_query['tax_query'],
                'name'             => $post_title
            );
        } else {
            $arg = array(
                'numberposts'      => $post_count,
                'offset'           => $offset,
                'orderby'          => $orderby,
                'order'            => $order,
                'post_type'        => $post_type,
                'name'             => $post_title
            );
        }

        $posts = get_posts($arg);

        foreach ($posts as $key => $post) {
            $posts[$key]->post_author_name = get_the_author_meta('display_name', $post->post_author);
            $posts[$key]->post_meta = get_post_meta($post->ID);
            $posts[$key]->categories = get_the_terms($post->ID, 'category');
            $posts[$key] = $this->get_webp_image_address($posts[$key]);

            if($post_title == "ads_section") {
                $post_content = json_decode($post->post_content,true);

                foreach($post_content as $mkey => &$item) {
                    if($item['status'] == 0) {
                        unset($post_content[$mkey]);
                    }
                }
                $posts[$key]->post_content = json_encode(array_values($post_content));//json_encode((array)$post_content);
//		    var_dump("hello there");
//		    die();
            }

        }

        $cache_key = base64_encode('euronewsapp-' . date("ymd-Gi") . mt_rand(5, 15));
        wp_cache_add($cache_key, $posts, "", 86400);
        return new WP_REST_Response(array('posts' => $posts, 'cache_key' => $cache_key), 200);
    }


    public function fetch_information_categories(WP_REST_Request $request)
    {
        $inputs = $request->get_params();
        $cache_key = isset($inputs['cache_key']) ? $inputs['cache_key'] : null;

        if ($cache_key) {
            $cache_content = wp_cache_get($cache_key);
            if ($cache_content)
                return new WP_REST_Response(array('posts' => $cache_content), 200);
        }

        $category_name = isset($inputs['category_name']) ? $inputs['category_name'] : '';
        $category_slug = isset($inputs['category_slug']) ? $inputs['category_slug'] : '';
        $orderby = isset($inputs['orderby']) ? $inputs['orderby'] : 'name';
        $order = isset($inputs['order']) ? $inputs['order'] : 'DESC';
        $parent = isset($inputs['parent']) ? $inputs['parent'] : 0;

        if ($parent != 0) {
            $categories = get_categories(
                array(
                    'parent' => $parent,
                    'orderby' => $orderby,
                    'order'   => $order
                )
            );

            $cache_key = base64_encode('euronewsapp-' . date("ymd-Gi") . mt_rand(5, 15));
            wp_cache_add($cache_key, $posts, "", 86400);
            return new WP_REST_Response(array('categories' => $categories, 'cache_key' => $cache_key), 200);
        }

        $arg = array(
            'taxonomy' => 'category',
            'orderby' => $orderby,
            'order' => $order,
            'hide_empty' => true,
            'include' => 'all',
            'exclude' => 'all',
            'exclude_tree' => 'all',
            'number' => false,
            'offset' => '',
            'fields' => 'all',
            'name' => $category_name,
            'slug' => $category_slug,
            'hierarchical' => true,
            'search' => '',
            'name__like' => '',
            'description__like' => '',
            'pad_counts' => false,
            'get' => '',
            'child_of' => false,
            'childless' => false,
            'cache_domain' => 'core',
            'update_term_meta_cache' => true,
            'meta_query' => '',
            'meta_key' => array(),
            'meta_value' => ''
        );

        $categories = get_terms($arg);

        $cache_key = base64_encode('euronewsapp-' . date("ymd-Gi") . mt_rand(5, 15));
        wp_cache_add($cache_key, $posts, "", 86400);
        return new WP_REST_Response(array('categories' => $categories, 'cache_key' => $cache_key), 200);
    }

    public function fetch_information_tags(WP_REST_Request $request)
    {
        $inputs = $request->get_params();
        $cache_key = isset($inputs['cache_key']) ? $inputs['cache_key'] : null;

        if ($cache_key) {
            $cache_content = wp_cache_get($cache_key);
            if ($cache_content)
                return new WP_REST_Response(array('posts' => $cache_content), 200);
        }

        $tag_name = isset($inputs['tag_name']) ? $inputs['tag_name'] : '';
        $tag_slug = isset($inputs['tag_slug']) ? $inputs['tag_slug'] : '';
        $orderby = isset($inputs['orderby']) ? $inputs['orderby'] : 'name';
        $order = isset($inputs['order']) ? $inputs['order'] : 'DESC';
        $parent = isset($inputs['parent']) ? $inputs['parent'] : 0;

        if ($parent != 0) {
            $tags = get_tags(
                array(
                    'parent' => $parent,
                    'orderby' => $orderby,
                    'order'   => $order
                )
            );

            $cache_key = base64_encode('euronewsapp-' . date("ymd-Gi") . mt_rand(5, 15));
            wp_cache_add($cache_key, $posts, "", 86400);
            return new WP_REST_Response(array('tags' => $tags, 'cache_key' => $cache_key), 200);
        }

        $arg = array(
            'taxonomy' => 'post_tag',
            'orderby' => $orderby,
            'order' => $order,
            'hide_empty' => true,
            'include' => 'all',
            'exclude' => 'all',
            'exclude_tree' => 'all',
            'number' => false,
            'offset' => '',
            'fields' => 'all',
            'name' => $tag_name,
            'slug' => $tag_slug,
            'hierarchical' => true,
            'search' => '',
            'name__like' => '',
            'description__like' => '',
            'pad_counts' => false,
            'get' => '',
            'child_of' => false,
            'childless' => false,
            'cache_domain' => 'core',
            'update_term_meta_cache' => true,
            'meta_query' => '',
            'meta_key' => array(),
            'meta_value' => ''
        );

        $categories = get_terms($arg);

        $cache_key = base64_encode('euronewsapp-' . date("ymd-Gi") . mt_rand(5, 15));
        wp_cache_add($cache_key, $posts, "", 86400);
        return new WP_REST_Response(array('categories' => $categories, 'cache_key' => $cache_key), 200);
    }


    public function get_webp_image_address($post)
    {
        if (isset($post->post_meta["essb_cached_image"]) and $post->post_meta["essb_cached_image"]) {
            $post_image = str_replace(wp_upload_dir()['baseurl'], "", $post->post_meta["essb_cached_image"]);
            if (!file_exists(pathinfo(wp_upload_dir()['basedir'] . $post_image[0])['dirname'] . '/' . pathinfo(wp_upload_dir()['basedir'] . $post_image[0])['filename'] . '.webp')) {
                $img_src = $this->convertImageToWebP(wp_upload_dir()['basedir'] . $post_image[0], pathinfo(wp_upload_dir()['basedir'] . $post_image[0])['dirname'] . '/' . pathinfo(wp_upload_dir()['basedir'] . $post_image[0])['filename'] . '.webp');
                $post->post_meta["essb_cached_image_webp"] = $img_src;
            } elseif (file_exists(pathinfo(wp_upload_dir()['basedir'] . $post_image[0])['dirname'] . '/' . pathinfo(wp_upload_dir()['basedir'] . $post_image[0])['filename'] . '.webp')) {
                $post->post_meta["essb_cached_image_webp"] = wp_upload_dir()['baseurl'] . str_replace('.' . pathinfo(wp_upload_dir()['basedir'] . $post_image[0])["extension"], ".webp", $post_image[0]);
            }
        }
        return $post;
    }

    public function convertImageToWebP($source, $destination, $quality = 70)
    {
        $extension = pathinfo($source, PATHINFO_EXTENSION);
        if ($extension == 'jpeg' || $extension == 'jpg')
            $image = imagecreatefromjpeg($source);
        elseif ($extension == 'gif')
            $image = imagecreatefromgif($source);
        elseif ($extension == 'png')
            $image = imagecreatefrompng($source);
        $img_converted = imagewebp($image, $destination, $quality);

        if ($img_converted) return $destination;
        else return '';
    }


    public function global_air_quality()
    {
        global $wpdb;

        $weather_locations = $wpdb->get_results( "SELECT * FROM `wp_weather_locations`");
        $globalAirQuality = [];

        $tz = 'Asia/Tbilisi';
        $timestamp = time();
        $dt = new DateTime("now", new DateTimeZone($tz));
        $dt->setTimestamp($timestamp);

        foreach($weather_locations as $weather_location){
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.weather.com/v3/wx/globalAirQuality?geocode=".str_replace(' ','',$weather_location->latlng)."&language=ka-GE&scale=EPA&format=json&apiKey=a2e41d862f6b46d3a41d862f6b66d3b2",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET"
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                echo "cURL Error #:" . $err;
            } else {
                if (!file_exists('path/to/directory')) {
                    mkdir(wp_upload_dir()['basedir'].'/weather_info/global_air_quality/'.$weather_location->id, 0777, true);
                }
                $fp = fopen(wp_upload_dir()['basedir'].'/weather_info/global_air_quality/'.$weather_location->id.'/'.$dt->format('Ymd').'.json', 'w');
                fwrite($fp, $response);
                fclose($fp);

                unlink(wp_upload_dir()['basedir'].'/weather_info/global_air_quality/'.$weather_location->id.'/'.date('Ymd',strtotime('-1 day')).'.json');
            }
        }

        exit();
    }

    public function global_air_quality_file(WP_REST_Request $request){
        $inputs = $request->get_params();
        $latlng = ($inputs['lat_long']) ? $inputs['lat_long'] : '';
        $date = ($inputs['date']) ? $inputs['date'] : 'now';
        global $wpdb;
        $weather_locations = $wpdb->get_results( "SELECT * FROM `wp_weather_locations` WHERE latlng like '".$latlng."'");


        $tz = 'Asia/Tbilisi';
        $timestamp = time();
        $dt = new DateTime("now", new DateTimeZone($tz));
        $dt->setTimestamp($timestamp);

        $file_json = [];
        foreach($weather_locations as $weather_location){
            if(file_exists(wp_upload_dir()['basedir'].'/weather_info/global_air_quality/'.$weather_location->id.'/'.$dt->format('Ymd').'.json')){
                $file_json[] = file_get_contents(wp_upload_dir()['basedir'].'/weather_info/global_air_quality/'.$weather_location->id.'/'.$dt->format('Ymd').'.json');
                unlink(wp_upload_dir()['basedir'].'/weather_info/global_air_quality/'.$weather_location->id.'/'.date('Ymd',strtotime('-1 day')).'.json');
            }else{
                date_default_timezone_set('Asia/Tbilisi');
                $file_json[] = file_get_contents(wp_upload_dir()['basedir'].'/weather_info/global_air_quality/'.$weather_location->id.'/'.date('Ymd',strtotime('-1 day')).'.json');
            }
        }
        return new WP_REST_Response(array($file_json), 200);
    }


    public function daily_15day()
    {
        global $wpdb;

        $weather_locations = $wpdb->get_results( "SELECT * FROM `wp_weather_locations`");
        $globalAirQuality = [];

        $tz = 'Asia/Tbilisi';
        $timestamp = time();
        $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
        $dt->setTimestamp($timestamp); //adjust the object to correct timestamp

        foreach($weather_locations as $weather_location){
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.weather.com/v3/wx/forecast/daily/15day?geocode=".str_replace(' ','',$weather_location->latlng)."&format=json&units=m&language=ka-GE&apiKey=a2e41d862f6b46d3a41d862f6b66d3b2",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET"
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                echo "cURL Error #:" . $err;
            } else {
                if (!file_exists(wp_upload_dir()['basedir'].'/weather_info/daily_15day/')) {
                    mkdir(wp_upload_dir()['basedir'].'/weather_info/daily_15day/'.$weather_location->id, 0777, true);
                }
                $fp = fopen(wp_upload_dir()['basedir'].'/weather_info/daily_15day/'.$weather_location->id.'/'.$dt->format('Ymd').'.json', 'w');
                fwrite($fp, $response);
                fclose($fp);

                unlink(wp_upload_dir()['basedir'].'/weather_info/daily_15day/'.$weather_location->id.'/'.date('Ymd',strtotime('-1 day')).'.json');
            }
        }


        exit();
    }

    public function daily_15day_file(WP_REST_Request $request){
        $inputs = $request->get_params();
        $latlng = ($inputs['lat_long']) ? $inputs['lat_long'] : '';
        $date = ($inputs['date']) ? $inputs['date'] : 'now';
        global $wpdb;
        $weather_locations = $wpdb->get_results( "SELECT * FROM `wp_weather_locations` WHERE latlng like '".$latlng."'");


        $tz = 'Asia/Tbilisi';
        $timestamp = time();
        $dt = new DateTime("now", new DateTimeZone($tz));
        $dt->setTimestamp($timestamp);

        $file_json = [];
        foreach($weather_locations as $weather_location){
            if(file_exists(wp_upload_dir()['basedir'].'/weather_info/daily_15day/'.$weather_location->id.'/'.$dt->format('Ymd').'.json')){
                $file_json[] = file_get_contents(wp_upload_dir()['basedir'].'/weather_info/daily_15day/'.$weather_location->id.'/'.$dt->format('Ymd').'.json');
            }else{
                date_default_timezone_set('Asia/Tbilisi');
                $file_json[] = file_get_contents(wp_upload_dir()['basedir'].'/weather_info/daily_15day/'.$weather_location->id.'/'.date('Ymd',strtotime('-1 day')).'.json');
            }
        }
        return new WP_REST_Response(array($file_json), 200);
    }



    public function hourly_15day()
    {
        global $wpdb;

        $weather_locations = $wpdb->get_results( "SELECT * FROM `wp_weather_locations`");
        $globalAirQuality = [];

        $tz = 'Asia/Tbilisi';
        $timestamp = time();
        $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
        $dt->setTimestamp($timestamp); //adjust the object to correct timestamp

        foreach($weather_locations as $weather_location){
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.weather.com/v3/wx/forecast/hourly/15day?geocode=".str_replace(' ','',$weather_location->latlng)."&format=json&units=m&language=ka-GE&apiKey=a2e41d862f6b46d3a41d862f6b66d3b2",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET"
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                echo "cURL Error #:" . $err;
            } else {
                if ($err) {
                    echo "cURL Error #:" . $err;
                } else {
                    if (!file_exists(wp_upload_dir()['basedir'].'/weather_info/hourly_15day/')) {
                        mkdir(wp_upload_dir()['basedir'].'/weather_info/hourly_15day/'.$weather_location->id, 0777, true);
                    }
                    $fp = fopen(wp_upload_dir()['basedir'].'/weather_info/hourly_15day/'.$weather_location->id.'/'.$dt->format('Ymd_H').'.json', 'w');
                    fwrite($fp, $response);
                    fclose($fp);
                    unlink(wp_upload_dir()['basedir'].'/weather_info/hourly_15day/'.$weather_location->id.'/'.date('Ymd_H',strtotime('-1 hour')).'.json');
                }
            }
        }

        exit();
    }


    public function hourly_15day_file(WP_REST_Request $request){
        $inputs = $request->get_params();
        $latlng = ($inputs['lat_long']) ? $inputs['lat_long'] : '';
        $date = ($inputs['date']) ? $inputs['date'] : 'now';
        global $wpdb;
        $weather_locations = $wpdb->get_results( "SELECT * FROM `wp_weather_locations` WHERE latlng like '".$latlng."'");


        $tz = 'Asia/Tbilisi';
        $timestamp = time();
        $dt = new DateTime("now", new DateTimeZone($tz));
        $dt->setTimestamp($timestamp);

        $file_json = [];
        foreach($weather_locations as $weather_location){
            if(file_exists(wp_upload_dir()['basedir'].'/weather_info/hourly_15day/'.$weather_location->id.'/'.$dt->format('Ymd_H').'.json')){
                $file_json[] = file_get_contents(wp_upload_dir()['basedir'].'/weather_info/hourly_15day/'.$weather_location->id.'/'.$dt->format('Ymd_H').'.json');
                unlink(wp_upload_dir()['basedir'].'/weather_info/hourly_15day/'.$weather_location->id.'/'.date('Ymd_H',strtotime('-1 hour')).'.json');
            }else{
                date_default_timezone_set('Asia/Tbilisi');
                $file_json[] = file_get_contents(wp_upload_dir()['basedir'].'/weather_info/hourly_15day/'.$weather_location->id.'/'.date('Ymd_H',strtotime('-1 hour')).'.json');
            }
        }
        return new WP_REST_Response(array($file_json), 200);
    }

    //////////////////////////////////////// settings

}


add_action('init', header("Access-Control-Allow-Origin: *"));

add_filter('kses_allowed_protocols', function ($protocols) {
    $protocols[] = 'capacitor';
    return $protocols;
});

add_filter('kses_allowed_protocols', function ($protocols) {
    $protocols[] = 'ionic';
    return $protocols;
});



function prefix_register_above_rest_api_rest_routes()
{


    $controller = new above_rest_api();
    $controller->register_routes();
}
add_action('rest_api_init', 'prefix_register_above_rest_api_rest_routes');

add_filter('onesignal_send_notification', 'push_notification_onesignal_filter_post_intergration', 10, 4);
function push_notification_onesignal_filter_post_intergration($fields, $new_status, $old_status, $post)
{

    $fields['headings'] = array("en" => $post->post_title);

    $separator = '/[ ]/';
    $word_count = 10;

    if (!$post->post_content) $post->post_content = 'Breaking News!';
    $string_array = preg_split($separator, $post->post_content);
    $string_handler = '';
    if (count($string_array) > 1) {
        for ($i = 0; $i < $word_count; $i++) {
            if (isset($string_array[$i]))
                $string_handler .= $string_array[$i] . ' ';
        }
        $string_handler .= '...';
    } else {
        $string_handler = $post->post_content . '...';
    }
    $string_handler = strip_tags($string_handler);
    $fields['contents'] = array("en" => $string_handler);
    $fields['data'] = array("post_id" => $post->ID);


    $ImageId = get_post_thumbnail_id($post->ID);
    $ImageSrc = wp_get_attachment_image_src($ImageId);
    $ImageUrl = $ImageSrc[0];

    $fields['android_background_layout']['image'] = $ImageUrl;
    //     "myappurl" => $fields['url'],
    //     "thumbnail_id" => $ImageId,
    //     "thumbnail_url" => $ImageUrl,
    // );
    /* Unset the URL to prevent opening the browser when the notification is clicked */
    unset($fields['url']);

    return $fields;
}




//////////////////////////////////////////////////////////////////////////////////
/**
 * Register a custom menu page.
 */



function ARA_register_admin_menu()
{
    add_menu_page(__('Above API', 'above-rest-api'), 'Above API', 'manage_options', 'above-rest-api', 'settings', plugins_url('above-rest-api/cropped-favicon.webp'), 6);
}
add_action('admin_menu', 'ARA_register_admin_menu');

require_once ARA__PLUGIN_DIR . 'settings.php';
