<?php

class settings{
    public $page;
    public $options;

    public function __construct($func = ''){
        $this->page = isset($_GET['page']) ? $_GET['page'] : '';
        $vars = array_merge($_POST,$_GET);

        if($this->page == "above-rest-api" and $func == ''){
            $this->setting_page($vars);
        }elseif($func == "below_featured_content" or $func == "featured_content"){
            $this->featured_content($vars);
        }elseif($func == "featured_section" or $func == "programs_section"){
            $this->feature_section($vars);
        }elseif($func == "tags_section" or $func == "editorChoice_section"){
            $this->tags_section($vars);
        }elseif($func == "theme_section"){
            $this->theme_section($vars);
        }elseif($func == "ads_section"){
            $this->ads_section($vars);

        }elseif($func == "rubric_section"){
            $this->rubric_section($vars);
        }
    }

    public function setting_page($vars = ''){
        $params = $vars;
        wp_enqueue_script( 'ara_js', plugin_dir_url(__FILE__). 'assets/ara_ajax_request.js', array(), '1.0.1', false );
        wp_localize_script( 'ara_js', 'ara_js_url', array(
            'url'   => admin_url( 'admin-ajax.php' ),
            'nonce' => wp_create_nonce( 'ara_js_nonce' )
        ) );
        global $wpdb;

        $this->options = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}posts WHERE `post_type` = 'ara_options'");
        include_once('views/settings.php');
    }

    public function featured_content($params = '')
    {
        global $wpdb;

        if(!wp_verify_nonce($params['nonce'], 'ara_js_nonce')) return false;

        $below_featured_content_category = isset($params['function']) ? $params['function'] : '';
        $vars = isset($params['ele']) ? $params['ele'] : '';
        parse_str($vars);

        $data = isset($featured_content_categories_ara) ? $featured_content_categories_ara : (isset($below_featured_content_categories_ara) ? $below_featured_content_categories_ara : '');

        $exists = $wpdb->get_results( "SELECT ID FROM {$wpdb->prefix}posts WHERE `post_type` = 'ara_options' AND `post_title`='$below_featured_content_category' ");
        if(!$exists){
            $post_id = wp_insert_post(array(
                'post_author' => '0',
                'post_title' => $below_featured_content_category,
                'post_content' => $data, #json_encode($data,JSON_UNESCAPED_UNICODE),
                'post_type' => 'ara_options',
                'post_status' => 'publish'
            ));
        }else{
            $post_id = $wpdb->update( "{$wpdb->prefix}posts",
                array(
                    'post_author' => '0',
                    'post_content' => $data, #json_encode($data,JSON_UNESCAPED_UNICODE),
                    'post_type' => 'ara_options',
                    'post_status' => 'publish'
                ),
                array(
                    'post_type' => 'ara_options',
                    'post_title' => $below_featured_content_category
                )
            );
        }
        echo json_encode($post_id);
        wp_die();
    }

    public function feature_section($params = ''){
        $data = array();
        $vars = isset($params['ele']) ? $params['ele'] : '';
        parse_str($vars);
        $function = isset($params['function']) ? $params['function'] : '';

        foreach($featured_content_categories_ara as $key=>$item){

            $data[$key]['cat_id'] = isset($item) ? $item : '';
            $data[$key]['url'] =  isset($url[$key]) ? $url[$key] : '';
            $data[$key]['img'] = isset($attachment[$key]) ? $attachment[$key] : '';
        }

        global $wpdb;
        $exists = $wpdb->get_results( "SELECT ID FROM {$wpdb->prefix}posts WHERE `post_type` = 'ara_options' AND `post_title`='$function' ");
        if(!$exists){
            $post_id = wp_insert_post(array(
                'post_author' => '0',
                'post_title' => $function,
                'post_content' => json_encode($data,JSON_UNESCAPED_UNICODE),
                'post_type' => 'ara_options',
                'post_status' => 'publish'
            ));
        }else{
            $post_id = $wpdb->update( "{$wpdb->prefix}posts",
                array(
                    'post_author' => '0',
                    'post_content' => json_encode($data,JSON_UNESCAPED_UNICODE),
                    'post_type' => 'ara_options',
                    'post_status' => 'publish'
                ),
                array(
                    'post_type' => 'ara_options',
                    'post_title' => $function
                )
            );
        }
        echo json_encode($post_id);

        wp_die();
    }

    public function tags_section($params = ''){
        $data = array();
        $vars = isset($params['ele']) ? $params['ele'] : '';
        parse_str($vars);
        $function = isset($params['function']) ? $params['function'] : '';

        foreach($featured_content_categories_ara as $key=>$item){

            $data[$key]['cat_id'] = isset($item) ? $item : '';
            $data[$key]['url'] =  isset($url[$key]) ? $url[$key] : '';
            $data[$key]['img'] = isset($attachment[$key]) ? $attachment[$key] : '';
        }

        global $wpdb;
        $exists = $wpdb->get_results( "SELECT ID FROM {$wpdb->prefix}posts WHERE `post_type` = 'ara_options' AND `post_title`='$function' ");
        if(!$exists){
            $post_id = wp_insert_post(array(
                'post_author' => '0',
                'post_title' => $function,
                'post_content' => json_encode($data,JSON_UNESCAPED_UNICODE),
                'post_type' => 'ara_options',
                'post_status' => 'publish'
            ));
        }else{
            $post_id = $wpdb->update( "{$wpdb->prefix}posts",
                array(
                    'post_author' => '0',
                    'post_content' => json_encode($data,JSON_UNESCAPED_UNICODE),
                    'post_type' => 'ara_options',
                    'post_status' => 'publish'
                ),
                array(
                    'post_type' => 'ara_options',
                    'post_title' => $function
                )
            );
        }
        echo json_encode($post_id);

        wp_die();
    }

    public function theme_section($params = ''){
        $data = array();
        $vars = isset($params['ele']) ? $params['ele'] : '';
        parse_str($vars);

        foreach($featured_content_categories_ara as $key=>$item){

            $data[$key]['cat_id'] = isset($item) ? $item : '';
            $data[$key]['url'] =  isset($url[$key]) ? $url[$key] : '';
            $data[$key]['img'] = isset($attachment[$key]) ? $attachment[$key] : '';
        }

        global $wpdb;
        $exists = $wpdb->get_results( "SELECT ID FROM {$wpdb->prefix}posts WHERE `post_type` = 'ara_options' AND `post_title`='theme_section' ");
        if(!$exists){
            $post_id = wp_insert_post(array(
                'post_author' => '0',
                'post_title' => 'theme_section',
                'post_content' => json_encode($data,JSON_UNESCAPED_UNICODE),
                'post_type' => 'ara_options',
                'post_status' => 'publish'
            ));
        }else{
            $post_id = $wpdb->update( "{$wpdb->prefix}posts",
                array(
                    'post_author' => '0',
                    'post_content' => json_encode($data,JSON_UNESCAPED_UNICODE),
                    'post_type' => 'ara_options',
                    'post_status' => 'publish'
                ),
                array(
                    'post_type' => 'ara_options',
                    'post_title' => 'theme_section'
                )
            );
        }
        echo json_encode($post_id);

        wp_die();
    }

    public function rubric_section($params = ''){
        $data = array();
        $vars = isset($params['ele']) ? $params['ele'] : '';
        parse_str($vars);

        foreach($featured_content_categories_ara as $key=>$item){

            $data[$key]['cat_id'] = isset($item) ? $item : '';
            $data[$key]['url'] =  isset($url[$key]) ? $url[$key] : '';
            $data[$key]['img'] = isset($attachment[$key]) ? $attachment[$key] : '';
        }

        global $wpdb;
        $exists = $wpdb->get_results( "SELECT ID FROM {$wpdb->prefix}posts WHERE `post_type` = 'ara_options' AND `post_title`='rubric_section' ");
        if(!$exists){
            $post_id = wp_insert_post(array(
                'post_author' => '0',
                'post_title' => 'rubric_section',
                'post_content' => json_encode($data,JSON_UNESCAPED_UNICODE),
                'post_type' => 'ara_options',
                'post_status' => 'publish'
            ));
        }else{
            $post_id = $wpdb->update( "{$wpdb->prefix}posts",
                array(
                    'post_author' => '0',
                    'post_content' => json_encode($data,JSON_UNESCAPED_UNICODE),
                    'post_type' => 'ara_options',
                    'post_status' => 'publish'
                ),
                array(
                    'post_type' => 'ara_options',
                    'post_title' => 'rubric_section'
                )
            );
        }
        echo json_encode($post_id);

        wp_die();
    }


    public function ads_section($params = ''){
        $data = array();
        $vars = isset($params['ele']) ? $params['ele'] : '';
        parse_str($vars);
        $function = isset($params['function']) ? $params['function'] : '';

//		var_dump($result);


        foreach($featured_content_categories_ara as $key=>$item){

            $data[$key]['cat_id'] = isset($item) ? $item : '';
            $data[$key]['url'] =  isset($url[$key]) ? $url[$key] : '';
            $data[$key]['img'] = isset($attachment[$key]) ? $attachment[$key] : '';
            $data[$key]['revive_zone_id'] = isset($revive_zone_id[$key]) ? $revive_zone_id[$key] : '';
            $data[$key]['revive_id'] = isset($revive_id[$key]) ? $revive_id[$key] : '';
            $data[$key]['status'] = empty($status[$key]) ? 0 : 1;

//			var_dump($item . ': $status['.$key.']: ' . $data[$key]['status']);
        }
//die();

        global $wpdb;
        $exists = $wpdb->get_results( "SELECT ID FROM {$wpdb->prefix}posts WHERE `post_type` = 'ara_options' AND `post_title`='$function' ");
        if(!$exists){
            $post_id = wp_insert_post(array(
                'post_author' => '0',
                'post_title' => $function,
                'post_content' => json_encode($data,JSON_UNESCAPED_UNICODE),
                'post_type' => 'ara_options',
                'post_status' => 'publish'
            ));
        }else{
            $post_id = $wpdb->update( "{$wpdb->prefix}posts",
                array(
                    'post_author' => '0',
                    'post_content' => json_encode($data,JSON_UNESCAPED_UNICODE),
                    'post_type' => 'ara_options',
                    'post_status' => 'publish'
                ),
                array(
                    'post_type' => 'ara_options',
                    'post_title' => $function
                )
            );
        }
        echo json_encode($post_id);

        wp_die();
    }
}



function ara_ajax_request(){
    $vars = array_merge($_POST, $_GET);
    if(isset($vars['function']) and $vars['function']){
        $SETTING = new settings($vars['function']);
    }else{
        return false;
    }
    wp_die();
}

add_action( 'wp_ajax_nopriv_ara_ajax_request', 'ara_ajax_request');
add_action( 'wp_ajax_ara_ajax_request',  'ara_ajax_request');

function settings(){
    $setting_class = new settings();
}
