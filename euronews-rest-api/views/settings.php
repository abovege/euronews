<?php
require_once(ARA__PLUGIN_DIR.'assets/settings.php');

foreach($this->options as $option){

    switch ($option->post_name){
//            case 'featured_content_categories_ara':
        case 'featured_content':
            $featured_content = $option;
            break;
//            case 'below_featured_content_categories_ara':
        case 'below_featured_content':
            $below_featured_content = $option;
            break;
        case 'featured_section':
            $featured_section = $option;
            break;
        case 'programs_section':
            $programs_section = $option;
            break;
        case 'tags_section':
            $tags_section = $option;
            break;
        case 'theme_section':
            $theme_section = $option;
            break;
        case 'editorchoice_section':
            $editorChoice_section = $option;
            break;
            break;
        case 'ads_section':
            $ads_section = $option;
            break;
        case 'rubric_section':
            $rubric_section = $option;
            break;
    }

}

?>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<div class="container mt-3">
    <div class="row">

        <section class="col-md-6 border border-primary mb-2 p-2 rounded" id="featured_content">
            <form method="post" accept-charset="utf-8" action="#" class="col-12" onsubmit="return ara_ajax_save(jQuery(this), 'featured_content');">
                <legend>
                    <h4><strong><?php echo __("რედაქტორის რჩეული","above-rest-api") ?></strong></h4>
                </legend>
                <div class="mb-3">
                    <label for="featured_content_categories_ara" class="form-label"><?php echo __("Showing Categories:","above-rest-api") ?></label>
                    <?php
                    wp_dropdown_categories(array(
                        'class' => 'form-select',
                        'name' => 'featured_content_categories_ara',
                        'id' => 'featured_content_categories_ara',
                        'orderby' => 'name',
                        'selected' => $featured_content->post_content,
                        'value_field' => 'name'
                    ));
                    ?>
                </div>
                <button type="submit" class="btn btn-success"><?php echo __("Save","above-rest-api") ?></button>
            </form>
        </section>
        <section class="col-md-6 border border-primary mb-2 p-2 rounded" id="below_featured_content">
            <form method="post" accept-charset="utf-8" action="#" class="col-12" onsubmit="return ara_ajax_save(jQuery(this), 'below_featured_content');">
                <legend>
                    <h4><strong><?php echo __("ყველაზე პოპულარული","above-rest-api") ?></strong></h4>
                </legend>
                <div class="mb-3">
                    <label for="below_featured_content_categories_ara" class="form-label"><?php echo __("Showing Categories:","above-rest-api") ?></label>
                    <?php
                    wp_dropdown_categories(array(
                        'class' => 'form-select',
                        'name' => 'below_featured_content_categories_ara',
                        'id' => 'below_featured_content_categories_ara',
                        'orderby' => 'name',
                        'selected' => $below_featured_content->post_content,
                        'value_field' => 'name'
                    ));
                    ?>
                </div>
                <button type="submit" class="btn btn-success"><?php echo __("Save","above-rest-api") ?></button>
            </form>
        </section>
        <section class="col-md-12 border border-primary mb-2 p-2 rounded" id="featured_section">
            <form method="post" accept-charset="utf-8"  action="#"class="col-12" onsubmit="return ara_ajax_save(jQuery(this), 'featured_section');">
                <legend>
                    <h4 class="d-inline"><strong><?php echo __("შეარჩიეთ გვერდი") ?></strong></h4>
                    <button type="button" class="btn btn-primary" onclick="add_new_section(jQuery(this).parent().parent())"><?php echo __("Add new section","above-rest-api") ?></button>
                    <button type="submit" class="btn btn-success"><?php echo __("Save","above-rest-api") ?></button>
                </legend>
                <div class="mb-1" >
                    <form actio="#">
                        <div class="row p-2" id="input-group">
                            <?php foreach(json_decode($featured_section->post_content) as $item): ?>
                                <div class='col-md-4 border border-1 rounded bg-secondary p-2'>
                                    <label  class='col-md-5 p-1 mt-1 text-white' for='featured_content_categories_ara[]'><strong>Select your category</strong></label>
                                    <?php
                                    wp_dropdown_categories(array(
                                        'class' => 'col-md-7  mt-1',
                                        'name' => 'featured_content_categories_ara[]',
                                        'id' => 'featured_content_categories_ara[]',
                                        'orderby' => 'name',
                                        'selected' => $item->cat_id,
                                        'value_field' => 'name'
                                    ));
                                    ?>
                                    <input class='col-md-12 p-2 mt-1' name='url[]' id='url[]' type='text'  value='<?php echo $item->url ?>' placeholder='URL...'/>
                                    <input class='d-none ara_img_attch' hidden name='attachment[]' type='text' value='<?php echo $item->img ?>' />
                                    <button type='button' class='col-md-12 btn btn-primary p-1 mt-1 text-center text-decoratoin-none'  onclick='open_media(jQuery(this))'>Feature Image</button>
                                    <button type='button' class='col-md-12 btn btn-primary p-1 mt-1 text-center text-decoratoin-none' onclick='remove_row(jQuery(this))'>Close</button>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </form>
                </div>
            </form>
        </section>


        <section class="col-md-12 border border-primary mb-2 p-2 rounded" id="programs_section">
            <form method="post" accept-charset="utf-8"  action="#"class="col-12" onsubmit="return ara_ajax_save(jQuery(this), 'programs_section');">
                <legend>
                    <h4 class="d-inline"><strong><?php echo __("პროგრამები") ?></strong></h4>
                    <button type="button" class="btn btn-primary" onclick="add_new_section(jQuery(this).parent().parent())"><?php echo __("Add new section","above-rest-api") ?></button>
                    <button type="submit" class="btn btn-success"><?php echo __("Save","above-rest-api") ?></button>
                </legend>
                <div class="mb-1" >
                    <form actio="#">
                        <div class="row p-2" id="input-group">
                            <?php foreach(json_decode($programs_section->post_content) as $item): ?>
                                <div class='col-md-4 border border-1 rounded bg-secondary p-2'>
                                    <label  class='col-md-5 p-1 mt-1 text-white' for='featured_content_categories_ara[]'><strong>Select your category</strong></label>
                                    <?php
                                    wp_dropdown_categories(array(
                                        'class' => 'col-md-7  mt-1',
                                        'name' => 'featured_content_categories_ara[]',
                                        'id' => 'featured_content_categories_ara[]',
                                        'orderby' => 'name',
                                        'selected' => $item->cat_id,
                                        'value_field' => 'name'
                                    ));
                                    ?>
                                    <input class='col-md-12 p-2 mt-1' name='url[]' id='url[]' type='text'  value='<?php echo $item->url ?>' placeholder='URL...'/>
                                    <input class='d-none ara_img_attch' hidden name='attachment[]' type='text' value='<?php echo $item->img ?>' />
                                    <button type='button' class='col-md-12 btn btn-primary p-1 mt-1 text-center text-decoratoin-none'  onclick='open_media(jQuery(this))'>Feature Image</button>
                                    <button type='button' class='col-md-12 btn btn-primary p-1 mt-1 text-center text-decoratoin-none' onclick='remove_row(jQuery(this))'>Close</button>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </form>
                </div>
            </form>
        </section>


        <section class="col-md-12 border border-primary mb-2 p-2 rounded" id="rubric_section">
            <form method="post" accept-charset="utf-8"  action="#"class="col-12" onsubmit="return ara_ajax_save(jQuery(this), 'rubric_section');">
                <legend>
                    <h4 class="d-inline"><strong><?php echo __("რუბრიკა") ?></strong></h4>
                    <button type="button" class="btn btn-primary" onclick="add_new_section(jQuery(this).parent().parent())"><?php echo __("Add new section","above-rest-api") ?></button>
                    <button type="submit" class="btn btn-success"><?php echo __("Save","above-rest-api") ?></button>
                </legend>
                <div class="mb-1" >
                    <form actio="#">
                        <div class="row p-2" id="input-group">
                            <?php foreach(json_decode($rubric_section->post_content) as $item): ?>
                                <div class='col-md-4 border border-1 rounded bg-secondary p-2'>
                                    <label  class='col-md-5 p-1 mt-1 text-white' for='featured_content_categories_ara[]'><strong>Select your category</strong></label>
                                    <?php
                                    wp_dropdown_categories(array(
                                        'class' => 'col-md-7  mt-1',
                                        'name' => 'featured_content_categories_ara[]',
                                        'id' => 'featured_content_categories_ara[]',
                                        'orderby' => 'name',
                                        'selected' => $item->cat_id,
                                        'value_field' => 'name'
                                    ));
                                    ?>
                                    <input class='col-md-12 p-2 mt-1' name='url[]' id='url[]' type='text'  value='<?php echo $item->url ?>' placeholder='URL...'/>
                                    <input class='d-none ara_img_attch' hidden name='attachment[]' type='text' value='<?php echo $item->img ?>' />
                                    <button type='button' class='col-md-12 btn btn-primary p-1 mt-1 text-center text-decoratoin-none'  onclick='open_media(jQuery(this))'>Feature Image</button>
                                    <button type='button' class='col-md-12 btn btn-primary p-1 mt-1 text-center text-decoratoin-none' onclick='remove_row(jQuery(this))'>Close</button>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </form>
                </div>
            </form>
        </section>

        <section class="col-md-12 border border-primary mb-2 p-2 rounded" id="tags_section">
            <form method="post" accept-charset="utf-8"  action="#"class="col-12" onsubmit="return ara_ajax_save(jQuery(this), 'tags_section');">
                <legend>
                    <h4 class="d-inline"><strong><?php echo __("ყველაზე მნიშვნელოვანი თემები") ?></strong></h4>
                    <button type="button" class="btn btn-primary" onclick="add_new_section(jQuery(this).parent().parent(),false)"><?php echo __("Add new section","above-rest-api") ?></button>
                    <button type="submit" class="btn btn-success"><?php echo __("Save","above-rest-api") ?></button>
                </legend>
                <div class="mb-1" >
                    <form actio="#">
                        <div class="row p-2" id="input-group">

                            <?php foreach(json_decode($tags_section->post_content) as $item): ?>
                                <div class='col-md-4 border border-1 rounded bg-secondary p-2'>
                                    <label  class='col-md-5 p-1 mt-1 text-white' for='featured_content_categories_ara[]'><strong>Select your category</strong></label>
                                    <?php
                                    wp_dropdown_categories(array(
                                        'class' => 'col-md-7  mt-1',
                                        'name' => 'featured_content_categories_ara[]',
                                        'id' => 'featured_content_categories_ara[]',
                                        'orderby' => 'name',
                                        'selected' => $item->cat_id,
                                        'value_field' => 'name'
                                    ));
                                    ?>
                                    <input class='col-md-12 p-2 mt-1' name='url[]' id='url[]' type='text'  value='<?php echo $item->url ?>' placeholder='URL...'/>
                                    <button type='button' class='col-md-12 btn btn-primary p-1 mt-1 text-center text-decoratoin-none' onclick='remove_row(jQuery(this))'>Close</button>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </form>
                </div>
            </form>
        </section>

        <section class="col-md-12 border border-primary mb-2 p-2 rounded" id="theme_section">
            <form method="post" accept-charset="utf-8"  action="#"class="col-12" onsubmit="return ara_ajax_save(jQuery(this), 'theme_section');">
                <legend>
                    <h4 class="d-inline"><strong><?php echo __("თემები") ?></strong></h4>
                    <button type="button" class="btn btn-primary" onclick="add_new_section(jQuery(this).parent().parent())"><?php echo __("Add new section","above-rest-api") ?></button>
                    <button type="submit" class="btn btn-success"><?php echo __("Save","above-rest-api") ?></button>
                </legend>
                <div class="mb-1" >
                    <form actio="#">
                        <div class="row p-2" id="input-group">
                            <?php foreach(json_decode($theme_section->post_content) as $item): ?>
                                <div class='col-md-4 border border-1 rounded bg-secondary p-2'>
                                    <label  class='col-md-5 p-1 mt-1 text-white' for='featured_content_categories_ara[]'><strong>Select your category</strong></label>
                                    <?php
                                    wp_dropdown_categories(array(
                                        'class' => 'col-md-7  mt-1',
                                        'name' => 'featured_content_categories_ara[]',
                                        'id' => 'featured_content_categories_ara[]',
                                        'orderby' => 'name',
                                        'selected' => $item->cat_id,
                                        'value_field' => 'name'
                                    ));
                                    ?>
                                    <input class='col-md-12 p-2 mt-1' name='url[]' id='url[]' type='text'  value='<?php echo $item->url ?>' placeholder='URL...'/>
                                    <input class='d-none ara_img_attch' hidden name='attachment[]' type='text' value='<?php echo $item->img ?>' />
                                    <button type='button' class='col-md-12 btn btn-primary p-1 mt-1 text-center text-decoratoin-none' onclick='open_media(jQuery(this))'>Feature Image</button>
                                    <button type='button' class='col-md-12 btn btn-primary p-1 mt-1 text-center text-decoratoin-none' onclick='remove_row(jQuery(this))'>Close</button>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </form>
                </div>
            </form>
        </section>


        <section class="col-md-12 border border-primary mb-2 p-2 rounded" id="editorChoice_section">
            <form method="post" accept-charset="utf-8"  action="#"class="col-12" onsubmit="return ara_ajax_save(jQuery(this), 'editorChoice_section');">
                <legend>
                    <h4 class="d-inline"><strong><?php echo __("ედიტორის რჩეული") ?></strong></h4>
                    <button type="button" class="btn btn-primary" onclick="add_new_section(jQuery(this).parent().parent(),false)"><?php echo __("Add new section","above-rest-api") ?></button>
                    <button type="submit" class="btn btn-success"><?php echo __("Save","above-rest-api") ?></button>
                </legend>
                <div class="mb-1" >
                    <form actio="#">
                        <div class="row p-2" id="input-group">

                            <?php foreach(json_decode($editorChoice_section->post_content) as $item): ?>
                                <div class='col-md-4 border border-1 rounded bg-secondary p-2'>
                                    <label  class='col-md-5 p-1 mt-1 text-white' for='featured_content_categories_ara[]'><strong>Select your category</strong></label>
                                    <?php
                                    wp_dropdown_categories(array(
                                        'class' => 'col-md-7  mt-1',
                                        'name' => 'featured_content_categories_ara[]',
                                        'id' => 'featured_content_categories_ara[]',
                                        'orderby' => 'name',
                                        'selected' => $item->cat_id,
                                        'value_field' => 'name'
                                    ));
                                    ?>
                                    <input class='col-md-12 p-2 mt-1' name='url[]' id='url[]' type='text'  value='<?php echo $item->url ?>' placeholder='URL...'/>
                                    <button type='button' class='col-md-12 btn btn-primary p-1 mt-1 text-center text-decoratoin-none' onclick='remove_row(jQuery(this))'>Close</button>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </form>
                </div>
            </form>
        </section>


        <section class="col-md-12 border border-primary mb-2 p-2 rounded" id="ads_section">
            <form method="post" accept-charset="utf-8"  action="#" class="col-12" id="adv_form" onsubmit="return ara_ajax_save(jQuery(this), 'ads_section', true);">
                <legend>
                    <h4 class="d-inline"><strong><?php echo __("Advertisement") ?></strong></h4>
                    <!--                    <button type="button" class="btn btn-primary" onclick="add_new_advertisement_section(jQuery(this).parent().parent(),true)">--><?php //echo __("Add new section","above-rest-api") ?><!--</button>-->
                    <button type="submit" class="btn btn-success"><?php echo __("Save","above-rest-api") ?></button>
                </legend>
                <div class="mb-1" >
                    <form actio="#">
                        <div class="row p-2" id="input-group">

                            <!--                        --><?php //foreach(json_decode($ads_section->post_content) as $item): ?>
                            <!--                                <div class='col-md-4 border border-1 rounded bg-secondary p-2'>-->
                            <!--                                <label  class='col-md-5 p-1 mt-1 text-white' for='featured_content_categories_ara[]'><strong>Select your category</strong></label>-->
                            <!---->
                            <!--                                    <select class="col-md-7  mt-1" name="featured_content_categories_ara[]" id="featured_content_categories_ara[]">-->
                            <!--                                        <option value="" >Default</option>-->
                            <!--                                        --><?php
                            //
                            //                                        $cats = get_categories([
                            //                                            'hide_empty' => true,
                            //                                        ]);
                            //
                            //
                            //                                        foreach($cats as $cat)
                            //                                        {
                            //                                            if($item->cat_id == $cat->name)
                            //                                                echo '<option selected="selected" value="'.$cat->cat_name .'">'. $cat->cat_name . '</option>';
                            //                                            else
                            //                                                echo '<option value="'.$cat->cat_name .'">'. $cat->cat_name . '</option>';
                            //                                        }
                            //
                            //                                        // wp_dropdown_categories(array(
                            //                                        //     'class' => 'col-md-7  mt-1',
                            //                                        //     'name' => 'featured_content_categories_ara[]',
                            //                                        //     'id' => 'featured_content_categories_ara[]',
                            //                                        //     'orderby' => 'name',
                            //                                        //     'selected' => $item->cat_id,
                            //                                        //     'value_field' => 'name'
                            //                                        // ));
                            //                                        ?>
                            <!---->
                            <!--                                    </select>-->
                            <!---->
                            <!--                                    <input class='col-md-12 p-2 mt-1' name='url[]' id='url[]' type='text'  value='--><?php //echo $item->url ?><!--' placeholder='URL...'/>-->
                            <!--                                    <input class='d-none ara_img_attch' hidden name='attachment[]' type='text' value='--><?php //echo $item->img ?><!--' />-->
                            <!--                                    <button type='button' class='col-md-12 btn btn-primary p-1 mt-1 text-center text-decoratoin-none'  onclick='open_media(jQuery(this))'>Upload file</button>-->
                            <!--                                    <button type='button' class='col-md-12 btn btn-primary p-1 mt-1 text-center text-decoratoin-none' onclick='remove_row(jQuery(this))'>Close</button>  -->
                            <!--                                </div>-->
                            <!--                            --><?php //endforeach; ?>

                            <?php
                            $ad = array_reduce(json_decode($ads_section->post_content), static function ($carry, $item) {
                                return isset($carry) ? $carry : ($item->cat_id === 'default' ? $item : $carry);
                            }, null);
                            ?>
                            <div class='col-md-4 border border-1 rounded bg-secondary p-2'>
                                <label  class='col-md-12 p-1 mt-1 text-white' for='featured_content_categories_ara[]'><strong>Default Ad</strong></label>

                                <label style="cursor:default;" class='col-md-12 p-1 mt-1 text-white d-block'>
                                    <input class='col-md-12 p-2 mt-1 defaultad' name='status[]' disabled id='status[]' type='checkbox' checked />
                                    <input value="1" name='status[]' type='hidden' />
                                    <strong>Enabled</strong>
                                </label>


                                <input class='col-md-12 p-2 mt-1' name='url[]' id='url[]' type='text'  value='<?php echo $ad->url ?>' placeholder='URL...'/>
                                <input class='col-md-12 p-2 mt-1 d-none' name='revive_zone_id[]' id='revive_zone_id[]' type='text'  value='<?php echo $ad->revive_zone_id ?>' placeholder='Revive Zone ID...'/>
                                <input class='col-md-12 p-2 mt-1 d-none' name='revive_id[]' id='revive_id[]' type='text'  value='<?php echo $ad->revive_id ?>' placeholder='Revive ID...'/>
                                <input name='featured_content_categories_ara[]' type='hidden' value="default" />
                                <input class='col-md-12 p-2 mt-1'  name='attachment[]' type='text' value='<?php echo $ad->img ?>' />
                                <button type='button' class='col-md-12 btn btn-primary p-1 mt-1 text-center text-decoratoin-none'  onclick='open_advertisment_media(jQuery(this))'>Upload file</button>
                                <!--                                    <button type='button' class='col-md-12 btn btn-primary p-1 mt-1 text-center text-decoratoin-none' onclick='remove_row(jQuery(this))'>Close</button>  -->
                            </div>

                            <!--                            <div class='col-md-4 border border-1 rounded bg-secondary p-2'>-->
                            <!--                                --><?php
                            //                                $ad = array_reduce(json_decode($ads_section->post_content), static function ($carry, $item) {
                            //                                    return $carry ?? ($item->cat_id === 'other_pages_ad' ? $item : $carry);
                            //                                }, null);
                            //                                ?>
                            <!--                                <label  class='col-md-12 p-1 mt-1 text-white d-block' for='featured_content_categories_ara[]'><strong>Other Pages Ad</strong></label>-->
                            <!--                                <label class='col-md-12 p-1 mt-1 text-white d-block'>-->
                            <!--                                    <input type='hidden' value='0' name='status[]'>-->
                            <!--                                    <input class='' name='status[]' id='status[]' value="1" type='checkbox' --><?php //echo $ad->status ? "checked" : "" ?><!-- />-->
                            <!--                                    <strong>Enabled</strong>-->
                            <!--                                </label>-->
                            <!--                                <input name='featured_content_categories_ara[]' type='hidden' value="other_pages_ad" />-->
                            <!--                                <input class='col-md-12 p-2 mt-1' name='url[]' id='url[]' type='text'  value='--><?php //echo $ad->url ?><!--' placeholder='URL...'/>-->
                            <!--                                <input class='col-md-12 p-2 mt-1' name='revive_zone_id[]' id='revive_zone_id[]' type='text'  value='--><?php //echo $ad->revive_zone_id ?><!--' placeholder='Revive Zone ID...'/>-->
                            <!--                                <input class='col-md-12 p-2 mt-1' name='revive_id[]' id='revive_id[]' type='text'  value='--><?php //echo $ad->revive_id ?><!--' placeholder='Revive ID...'/>-->
                            <!--                                <input class='col-md-12 p-2 mt-1' name='attachment[]' type='text' value='--><?php //echo $ad->img ?><!--' />-->
                            <!--                                <button type='button' class='col-md-12 btn btn-primary p-1 mt-1 text-center text-decoratoin-none'  onclick='open_advertisment_media(jQuery(this))'>Select file</button>-->
                            <!--                                <button type='button' class='col-md-12 btn btn-primary p-1 mt-1 text-center text-decoratoin-none' onclick='remove_row(jQuery(this))'>Close</button>-->
                            <!--                            </div>-->
                            <div class='col-md-4 border border-1 rounded bg-secondary p-2'>
                                <?php
                                $ad = array_reduce(json_decode($ads_section->post_content), static function ($carry, $item) {
                                    return isset($carry) ? $carry : ($item->cat_id === 'news_detail_ad' ? $item : $carry);
                                }, null);
                                ?>
                                <label  class='col-md-12 p-1 mt-1 text-white d-block' for='featured_content_categories_ara[]'><strong>News Detail Ad</strong></label>
                                <label  class='col-md-12 p-1 mt-1 text-white d-block'>
                                    <!--                                    <input type='hidden' value='0' name='status[]'>-->
                                    <input class='' name='status[]' id='status[]' value="1" type='checkbox' <?php echo $ad->status ? "checked" : "" ?> />
                                    <strong>Enabled</strong>
                                </label>
                                <input name='featured_content_categories_ara[]' type='hidden' value="news_detail_ad" />
                                <input class='col-md-12 p-2 mt-1' name='url[]' id='url[]' type='text'  value='<?php echo $ad->url ?>' placeholder='URL...'/>
                                <input class='col-md-12 p-2 mt-1 d-none' name='revive_zone_id[]' id='revive_zone_id[]' type='text'  value='<?php echo $ad->revive_zone_id ?>' placeholder='Revive Zone ID...'/>
                                <input class='col-md-12 p-2 mt-1 d-none' name='revive_id[]' id='revive_id[]' type='text'  value='<?php echo $ad->revive_id ?>' placeholder='Revive ID...'/>
                                <input class='col-md-12 p-2 mt-1' name='attachment[]' type='text' value='<?php echo $ad->img ?>' />
                                <button type='button' class='col-md-12 btn btn-primary p-1 mt-1 text-center text-decoratoin-none'  onclick='open_advertisment_media(jQuery(this))'>Upload file</button>
                                <!--                                <button type='button' class='col-md-12 btn btn-primary p-1 mt-1 text-center text-decoratoin-none' onclick='remove_row(jQuery(this))'>Close</button>-->
                            </div>

                            <div class='col-md-4 border border-1 rounded bg-secondary p-2'>
                                <?php
                                $ad = array_reduce(json_decode($ads_section->post_content), static function ($carry, $item) {
                                    return isset($carry) ? $carry : ($item->cat_id === 'inside_page_ad' ? $item : $carry);
                                }, null);
                                ?>
                                <label  class='col-md-12 p-1 mt-1 text-white d-block' for='featured_content_categories_ara[]'><strong>Inside Page Ad</strong></label>
                                <label  class='col-md-12 p-1 mt-1 text-white d-block'>
                                    <!--                                    <input type='hidden' value='0' name='status[]'>-->
                                    <input class='' name='status[]' id='status[]' value="1" type='checkbox' <?php echo $ad->status ? "checked" : "" ?> />
                                    <strong>Enabled</strong>
                                </label>
                                <input name='featured_content_categories_ara[]' type='hidden' value="inside_page_ad" />
                                <input class='col-md-12 p-2 mt-1' name='url[]' id='url[]' type='text'  value='<?php echo $ad->url ?>' placeholder='URL...'/>
                                <input class='col-md-12 p-2 mt-1 d-none' name='revive_zone_id[]' id='revive_zone_id[]' type='text'  value='<?php echo $ad->revive_zone_id ?>' placeholder='Revive Zone ID...'/>
                                <input class='col-md-12 p-2 mt-1 d-none' name='revive_id[]' id='revive_id[]' type='text'  value='<?php echo $ad->revive_id ?>' placeholder='Revive ID...'/>
                                <input class='col-md-12 p-2 mt-1' name='attachment[]' type='text' value='<?php echo $ad->img ?>' />
                                <button type='button' class='col-md-12 btn btn-primary p-1 mt-1 text-center text-decoratoin-none'  onclick='open_advertisment_media(jQuery(this))'>Upload file</button>
                                <!--                                <button type='button' class='col-md-12 btn btn-primary p-1 mt-1 text-center text-decoratoin-none' onclick='remove_row(jQuery(this))'>Close</button>-->
                            </div>


                        </div>
                        <div class="row p-2" id="input-group">
                            <div class='col-md-4 border border-1 rounded bg-secondary p-2'>
                                <?php
                                $ad = array_reduce(json_decode($ads_section->post_content), static function ($carry, $item) {
                                    return isset($carry) ? $carry : ($item->cat_id === 'homepage_ad1' ? $item : $carry);
                                }, null);
                                ?>
                                <label  class='col-md-12 p-1 mt-1 text-white d-block' for='featured_content_categories_ara[]'><strong>Homepage Ad1</strong></label>
                                <label  class='col-md-12 p-1 mt-1 text-white d-block'>
                                    <!--                                    <input type='hidden' value='0' name='status[]'>-->
                                    <input class='' name='status[]' id='status[]' value="1" type='checkbox' <?php echo $ad->status ? "checked" : "" ?> />
                                    <strong>Enabled</strong>
                                </label>
                                <input name='featured_content_categories_ara[]' type='hidden' value="homepage_ad1" />
                                <input class='col-md-12 p-2 mt-1' name='url[]' id='url[]' type='text'  value='<?php echo $ad->url ?>' placeholder='URL...'/>
                                <input class='col-md-12 p-2 mt-1 d-none' name='revive_zone_id[]' id='revive_zone_id[]' type='text'  value='<?php echo $ad->revive_zone_id ?>' placeholder='Revive Zone ID...'/>
                                <input class='col-md-12 p-2 mt-1 d-none' name='revive_id[]' id='revive_id[]' type='text'  value='<?php echo $ad->revive_id ?>' placeholder='Revive ID...'/>
                                <input class='col-md-12 p-2 mt-1' name='attachment[]' type='text' value='<?php echo $ad->img ?>' />
                                <button type='button' class='col-md-12 btn btn-primary p-1 mt-1 text-center text-decoratoin-none'  onclick='open_advertisment_media(jQuery(this))'>Upload file</button>
                                <!--                                <button type='button' class='col-md-12 btn btn-primary p-1 mt-1 text-center text-decoratoin-none' onclick='remove_row(jQuery(this))'>Close</button>-->
                            </div>

                            <div class='col-md-4 border border-1 rounded bg-secondary p-2'>
                                <?php
                                $ad = array_reduce(json_decode($ads_section->post_content), static function ($carry, $item) {
                                    return isset($carry) ? $carry : ($item->cat_id === 'homepage_ad2' ? $item : $carry);
                                }, null);
                                ?>
                                <label  class='col-md-12 p-1 mt-1 text-white d-block' for='featured_content_categories_ara[]'><strong>Homepage Ad2</strong></label>
                                <label  class='col-md-12 p-1 mt-1 text-white d-block'>
                                    <!--                                    <input type='hidden' value='0' name='status[]'>-->
                                    <input class='' name='status[]' id='status[]' value="1" type='checkbox' <?php echo $ad->status ? "checked" : "" ?> />
                                    <strong>Enabled</strong>
                                </label>
                                <input name='featured_content_categories_ara[]' type='hidden' value="homepage_ad2" />
                                <input class='col-md-12 p-2 mt-1' name='url[]' id='url[]' type='text'  value='<?php echo $ad->url ?>' placeholder='URL...'/>
                                <input class='col-md-12 p-2 mt-1 d-none' name='revive_zone_id[]' id='revive_zone_id[]' type='text'  value='<?php echo $ad->revive_zone_id ?>' placeholder='Revive Zone ID...'/>
                                <input class='col-md-12 p-2 mt-1 d-none' name='revive_id[]' id='revive_id[]' type='text'  value='<?php echo $ad->revive_id ?>' placeholder='Revive ID...'/>
                                <input class='col-md-12 p-2 mt-1' name='attachment[]' type='text' value='<?php echo $ad->img ?>' />
                                <button type='button' class='col-md-12 btn btn-primary p-1 mt-1 text-center text-decoratoin-none'  onclick='open_advertisment_media(jQuery(this))'>Upload file</button>
                                <!--                                <button type='button' class='col-md-12 btn btn-primary p-1 mt-1 text-center text-decoratoin-none' onclick='remove_row(jQuery(this))'>Close</button>-->
                            </div>

                            <div class='col-md-4 border border-1 rounded bg-secondary p-2'>
                                <?php
                                $ad = array_reduce(json_decode($ads_section->post_content), static function ($carry, $item) {
                                    return isset($carry) ? $carry : ($item->cat_id === 'homepage_ad3' ? $item : $carry);
                                }, null);
                                ?>
                                <label  class='col-md-12 p-1 mt-1 text-white d-block' for='featured_content_categories_ara[]'><strong>Homepage Ad3</strong></label>
                                <label  class='col-md-12 p-1 mt-1 text-white d-block'>
                                    <!--                                    <input type='hidden' value='0' name='status[]'>-->
                                    <input class='' name='status[]' id='status[]' value="1" type='checkbox' <?php echo $ad->status ? "checked" : "" ?> />
                                    <strong>Enabled</strong>
                                </label>
                                <input name='featured_content_categories_ara[]' type='hidden' value="homepage_ad3" />
                                <input class='col-md-12 p-2 mt-1' name='url[]' id='url[]' type='text'  value='<?php echo $ad->url ?>' placeholder='URL...'/>
                                <input class='col-md-12 p-2 mt-1 d-none' name='revive_zone_id[]' id='revive_zone_id[]' type='text'  value='<?php echo $ad->revive_zone_id ?>' placeholder='Revive Zone ID...'/>
                                <input class='col-md-12 p-2 mt-1 d-none' name='revive_id[]' id='revive_id[]' type='text'  value='<?php echo $ad->revive_id ?>' placeholder='Revive ID...'/>
                                <input class='col-md-12 p-2 mt-1' name='attachment[]' type='text' value='<?php echo $ad->img ?>' />
                                <button type='button' class='col-md-12 btn btn-primary p-1 mt-1 text-center text-decoratoin-none'  onclick='open_advertisment_media(jQuery(this))'>Upload file</button>
                                <!--                                <button type='button' class='col-md-12 btn btn-primary p-1 mt-1 text-center text-decoratoin-none' onclick='remove_row(jQuery(this))'>Close</button>-->
                            </div>

                        </div>

                        <div class="row p-2" id="input-group">

                            <div class='col-md-4 border border-1 rounded bg-secondary p-2'>
                                <?php
                                $ad = array_reduce(json_decode($ads_section->post_content), static function ($carry, $item) {
                                    return isset($carry) ? $carry : ($item->cat_id === 'justin_tab_ad' ? $item : $carry);
                                }, null);
                                ?>
                                <label  class='col-md-12 p-1 mt-1 text-white d-block' for='featured_content_categories_ara[]'><strong>Justin Tab</strong></label>
                                <label  class='col-md-12 p-1 mt-1 text-white d-block'>
                                    <!--                                    <input type='hidden' value='0' name='status[]'>-->
                                    <input class='' name='status[]' id='status[]' value="1" type='checkbox' <?php echo $ad->status ? "checked" : "" ?> />
                                    <strong>Enabled</strong>
                                </label>
                                <input name='featured_content_categories_ara[]' type='hidden' value="justin_tab_ad" />
                                <input class='col-md-12 p-2 mt-1' name='url[]' id='url[]' type='text'  value='<?php echo $ad->url ?>' placeholder='URL...'/>
                                <input class='col-md-12 p-2 mt-1 d-none' name='revive_zone_id[]' id='revive_zone_id[]' type='text'  value='<?php echo $ad->revive_zone_id ?>' placeholder='Revive Zone ID...'/>
                                <input class='col-md-12 p-2 mt-1 d-none' name='revive_id[]' id='revive_id[]' type='text'  value='<?php echo $ad->revive_id ?>' placeholder='Revive ID...'/>
                                <input class='col-md-12 p-2 mt-1' name='attachment[]' type='text' value='<?php echo $ad->img ?>' />
                                <button type='button' class='col-md-12 btn btn-primary p-1 mt-1 text-center text-decoratoin-none'  onclick='open_advertisment_media(jQuery(this))'>Upload file</button>
                                <!--                                <button type='button' class='col-md-12 btn btn-primary p-1 mt-1 text-center text-decoratoin-none' onclick='remove_row(jQuery(this))'>Close</button>-->
                            </div>

                            <div class='col-md-4 border border-1 rounded bg-secondary p-2'>
                                <?php
                                $ad = array_reduce(json_decode($ads_section->post_content), static function ($carry, $item) {
                                    return isset($carry) ? $carry : ($item->cat_id === 'videos_tab_ad' ? $item : $carry);
                                }, null);
                                ?>
                                <label  class='col-md-12 p-1 mt-1 text-white d-block' for='featured_content_categories_ara[]'><strong>Video Tab</strong></label>
                                <label  class='col-md-12 p-1 mt-1 text-white d-block'>
                                    <!--                                    <input type='hidden' value='0' name='status[]'>-->
                                    <input class='' name='status[]' id='status[]' value="1" type='checkbox' <?php echo $ad->status ? "checked" : "" ?> />
                                    <strong>Enabled</strong>
                                </label>
                                <input name='featured_content_categories_ara[]' type='hidden' value="videos_tab_ad" />
                                <input class='col-md-12 p-2 mt-1' name='url[]' id='url[]' type='text'  value='<?php echo $ad->url ?>' placeholder='URL...'/>
                                <input class='col-md-12 p-2 mt-1 d-none' name='revive_zone_id[]' id='revive_zone_id[]' type='text'  value='<?php echo $ad->revive_zone_id ?>' placeholder='Revive Zone ID...'/>
                                <input class='col-md-12 p-2 mt-1 d-none' name='revive_id[]' id='revive_id[]' type='text'  value='<?php echo $ad->revive_id ?>' placeholder='Revive ID...'/>
                                <input class='col-md-12 p-2 mt-1' name='attachment[]' type='text' value='<?php echo $ad->img ?>' />
                                <button type='button' class='col-md-12 btn btn-primary p-1 mt-1 text-center text-decoratoin-none'  onclick='open_advertisment_media(jQuery(this))'>Upload file</button>
                                <!--                                <button type='button' class='col-md-12 btn btn-primary p-1 mt-1 text-center text-decoratoin-none' onclick='remove_row(jQuery(this))'>Close</button>-->
                            </div>

                            <div class='col-md-4 border border-1 rounded bg-secondary p-2'>
                                <?php
                                $ad = array_reduce(json_decode($ads_section->post_content), static function ($carry, $item) {
                                    return isset($carry) ? $carry : ($item->cat_id === 'explore_tab_ad' ? $item : $carry);
                                }, null);
                                ?>
                                <label  class='col-md-12 p-1 mt-1 text-white d-block' for='featured_content_categories_ara[]'><strong>Explor Tab</strong></label>
                                <label  class='col-md-12 p-1 mt-1 text-white d-block'>
                                    <!--                                    <input type='hidden' value='0' name='status[]'>-->
                                    <input class='' name='status[]' id='status[]' value="1" type='checkbox' <?php echo $ad->status ? "checked" : "" ?> />
                                    <strong>Enabled</strong>
                                </label>
                                <input name='featured_content_categories_ara[]' type='hidden' value="explore_tab_ad" />
                                <input class='col-md-12 p-2 mt-1' name='url[]' id='url[]' type='text'  value='<?php echo $ad->url ?>' placeholder='URL...'/>
                                <input class='col-md-12 p-2 mt-1 d-none' name='revive_zone_id[]' id='revive_zone_id[]' type='text'  value='<?php echo $ad->revive_zone_id ?>' placeholder='Revive Zone ID...'/>
                                <input class='col-md-12 p-2 mt-1 d-none' name='revive_id[]' id='revive_id[]' type='text'  value='<?php echo $ad->revive_id ?>' placeholder='Revive ID...'/>
                                <input class='col-md-12 p-2 mt-1' name='attachment[]' type='text' value='<?php echo $ad->img ?>' />
                                <button type='button' class='col-md-12 btn btn-primary p-1 mt-1 text-center text-decoratoin-none'  onclick='open_advertisment_media(jQuery(this))'>Upload file</button>
                                <!--                                <button type='button' class='col-md-12 btn btn-primary p-1 mt-1 text-center text-decoratoin-none' onclick='remove_row(jQuery(this))'>Close</button>-->
                            </div>




                        </div>

                    </form>
                </div>
            </form>
        </section>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
